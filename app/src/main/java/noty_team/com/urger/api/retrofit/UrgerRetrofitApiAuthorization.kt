package noty_team.com.urger.api.retrofit

import android.content.Context
import android.util.Log
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import noty_team.com.urger.api.request_interfaces.AuthorizationApi
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.utils.api.request.create_company.CreateCompanyRequest
import noty_team.com.urger.utils.api.request.create_executor.CreateExecutorRequest
import noty_team.com.urger.utils.api.request.refresh_token_request.RefreshTokenRequest
import noty_team.com.urger.utils.api.request.sms_phone_validation.UserPhoneValidationRequest
import noty_team.com.urger.utils.api.response.create_employer_company.CreateEmployerCompanyResponse
import noty_team.com.urger.utils.api.response.create_executor_user.CreateExecutorUserResponse
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.api.response.nip.GetCompanyNameByNipResponse
import noty_team.com.urger.utils.api.response.refresh_token.RefreshTokenResponse
import noty_team.com.urger.utils.api.response.validate_phone.ValidatePhoneResponse
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.ref.WeakReference
import javax.inject.Inject

class UrgerRetrofitApiAuthorization @Inject constructor(
		val context: Context,
		val retrofit: Retrofit
) {

	@Inject
	lateinit var AuthorizationApiInterface: AuthorizationApi

	init {
		AppUrger[context].appComponent.inject(this)
	}

	fun refreshToken(
			data: RefreshTokenRequest,
			onSuccess: (response: RefreshTokenResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<RefreshTokenResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.refreshToken(data), onSuccess, onError)
	}

	fun getCompanyNameByNip(companyNip: String,
							onSuccess: (response: GetCompanyNameByNipResponse) -> Unit,
							onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<GetCompanyNameByNipResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.getCompanyNameByNip(companyNip), onSuccess, onError)
	}

	fun sendPhone(userPhoneValidation: UserPhoneValidationRequest,
				  onSuccess: (response: ValidatePhoneResponse) -> Unit,
				  onError: (errorMessage: String, errorCode: Int) -> Unit): Single<ValidatePhoneResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.sendPhone(userPhoneValidation), onSuccess, onError)
	}

	/*fun createCompanyн(createCompanyRequest: CreateCompanyRequest,
					  onSuccess: (response: CreateEmployerResponse) -> Unit,
					  onError: (errorMessage: String, errorCode: Int) -> Unit): Single<CreateEmployerResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.createCompany(createCompanyRequest), onSuccess, onError)
	}*/

	fun createCompany(createCompanyRequest: CreateCompanyRequest,
					  onSuccess: (response: CreateEmployerCompanyResponse) -> Unit,
					  onError: (errorMessage: String, errorCode: Int) -> Unit): Single<CreateEmployerCompanyResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.createCompany(createCompanyRequest), onSuccess, onError)
	}

	fun createExecutor(createExecutorRequest: CreateExecutorRequest,
					  onSuccess: (response: CreateExecutorUserResponse) -> Unit,
           			  onError: (errorMessage: String, errorCode: Int) -> Unit): Single<CreateExecutorUserResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.createExecutor(createExecutorRequest), onSuccess, onError)
	}


	fun getCategory(lang: String, onSuccess: (response: CityResponse) -> Unit,
					onError: (errorMessage: String, errorCode: Int) -> Unit): Single<CityResponse> {
		return initRequestSingleForResult(AuthorizationApiInterface.getCategory(lang), onSuccess, onError)
	}


	private fun <T, V> initRequestSingleForResult(
			request: Single<T>,
			onSuccess: (response: V) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<T> {
		val callback = CallbackWrapper<T, V>(WeakReference(context), retrofit, onSuccess, onError)
		val requestSingle = request.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.doOnSuccess {
					callback.onSuccess(it)
				}
				.doOnError {
					callback.onError(it)
				}
		callback.request = requestSingle
		return requestSingle
	}

	fun zip(requests: ArrayList<Single<*>>, onLoadingEnded: () -> Unit): Disposable {
		return Single.zip(requests) { result -> }.subscribe({ onLoadingEnded() }, {})
	}
}