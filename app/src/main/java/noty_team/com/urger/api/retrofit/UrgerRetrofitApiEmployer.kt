package noty_team.com.urger.api.retrofit

import android.content.Context
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.utils.api.request.edit_company_position.EditCompanyPositionRequest
import noty_team.com.urger.utils.api.request.update_validate_company.UpdateValidateCompanyRequest
import noty_team.com.urger.api.request_interfaces.UserProfileApi
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.ModelFilter
import noty_team.com.urger.utils.api.request.create_house.CreateHouseRequest
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.request.create_vacancy.CreateVacancyRequest
import noty_team.com.urger.utils.api.request.edit_user_personal_info.FcmRequest
import noty_team.com.urger.utils.api.response.add_document.create_document.CreateDocumentExecutorResponse
import noty_team.com.urger.utils.api.response.add_document.create_document.CreateDocumentResponse
import noty_team.com.urger.utils.api.response.all_info.AllInfoExecutorResponse
import noty_team.com.urger.utils.api.response.callback.AddFeedbackFromExecutorResponse
import noty_team.com.urger.utils.api.response.car.AddCarExecutorResponse
import noty_team.com.urger.utils.api.response.car.DeleteAutoExecutorResponse
import noty_team.com.urger.utils.api.response.car.GetAutosExecutorResponse
import noty_team.com.urger.utils.api.response.car.GetModelCarResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.AddFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.categories_response.CategoryResponse
import noty_team.com.urger.utils.api.response.center.CenterUrgerResponse
import noty_team.com.urger.utils.api.response.chat.TranslateResponse
import noty_team.com.urger.utils.api.response.comapny_photo.CompanyPhotoResponse
import noty_team.com.urger.utils.api.response.company.GetCompanyInfoResponse
import noty_team.com.urger.utils.api.response.create_house.CreateHouseResponce
import noty_team.com.urger.utils.api.response.create_vacancy.CreateVacancyResponse
import noty_team.com.urger.utils.api.response.currency.CurrencyResponse
import noty_team.com.urger.utils.api.response.delete_document_response.DeleteDocumentResponse
import noty_team.com.urger.utils.api.response.delete_house.DeleteHouseResponse
import noty_team.com.urger.utils.api.response.delete_photo.DeletePhotoResponse
import noty_team.com.urger.utils.api.response.delete_vacancy.DeleteVacancyResponse
import noty_team.com.urger.utils.api.response.edit_executor.EditExecutorPersonalInfoResponse
import noty_team.com.urger.utils.api.response.edit_user_personal_info.EditUserPersonalInfoResponse
import noty_team.com.urger.utils.api.response.experience_executor.AddExperienceExecutorResponse
import noty_team.com.urger.utils.api.response.experience_executor.get.GetExperiencesListExecutorResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.DeleteFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.favorite.GetFavoriteVacanciesResponse
import noty_team.com.urger.utils.api.response.feedback.FeedbackFromCompanyResponse
import noty_team.com.urger.utils.api.response.feedback.FeedbackFromExecutorResponse
import noty_team.com.urger.utils.api.response.feedback.get.employer.GetFeedbackCompanyResponse
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.get_all_house.AllHouseResponse
import noty_team.com.urger.utils.api.response.get_document.GetDocumentsExecutorResponse
import noty_team.com.urger.utils.api.response.get_document.GetDocumentsPaginationResponse
import noty_team.com.urger.utils.api.response.get_employer.GetEmployerInfoResponse
import noty_team.com.urger.utils.api.response.get_executor.GetExecutorInfoResponse
import noty_team.com.urger.utils.api.response.get_house.GetHouseResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.statistic.StatisticResponse
import noty_team.com.urger.utils.api.response.update_validate_company.UpdateValidateCompanyResponse
import noty_team.com.urger.utils.api.response.upload_company_logo.UploadCompanyLogoResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyResponse
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.PreliminaryVacancyResponse
import noty_team.com.urger.utils.api.response.news.NewsResponse
import noty_team.com.urger.utils.api.response.portfolio.AddPortfolioExecutorResponse
import noty_team.com.urger.utils.api.response.portfolio.GetPortfolioListExecutorResponse
import noty_team.com.urger.utils.api.response.vacancy.ParticipateResponse
import noty_team.com.urger.utils.api.response.vacancy.apply.ApplyVacancyResponse
import noty_team.com.urger.utils.api.response.vacancy.detailExecutor.VacancyResponseDetail
import noty_team.com.urger.utils.api.response.vacancy.requests.GetRequestsVacancyResponse
import noty_team.com.urger.utils.api.response.wage_executor.GetWageResponse
import noty_team.com.urger.utils.api.response.wage_type.WageTypeResponse
import noty_team.com.urger.utils.api.response.work_schedule.WorkScheduleResponse
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Retrofit
import java.io.File
import java.lang.ref.WeakReference
import javax.inject.Inject

class UrgerRetrofitApiEmployer @Inject constructor(
		val context: Context,
		val retrofit: Retrofit
) {
	@Inject
	lateinit var UserProfileApi: UserProfileApi
	private var pageForFilterVacancy = 0
	init {
		AppUrger[context].appComponent.inject(this)
	}

	fun updateFcmEmployer(id: Int, fcm: FcmRequest, onSuccess: (response: GetEmployerInfoResponse) -> Unit,
						  onError: (errorMessage: String, errorCode: Int) -> Unit):Single<GetEmployerInfoResponse> {
		return initRequestSingleForResult(UserProfileApi.updateFcmEmployer(id ,fcm), onSuccess, onError)
	}

	fun updateFcmExecutor(fcm: FcmRequest, onSuccess: (response: EditExecutorPersonalInfoResponse) -> Unit,
						  onError: (errorMessage: String, errorCode: Int) -> Unit):Single<EditExecutorPersonalInfoResponse> {
		return initRequestSingleForResult(UserProfileApi.updateFcmExecutor(fcm), onSuccess, onError)
	}

	fun getWageExecutor(
		onSuccess: (response: GetWageResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<GetWageResponse> {
		return initRequestSingleForResult(UserProfileApi.getWageExecutor(), onSuccess, onError)
	}

	fun updateWageExecutor(wage: Int,
		onSuccess: (response: GetWageResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<GetWageResponse> {
		return initRequestSingleForResult(UserProfileApi.updateWageSallary(wage), onSuccess, onError)
	}


	fun updateValidateCompany(
		companyId: Int, updateValidateCompanyRequest: UpdateValidateCompanyRequest,
		onSuccess: (response: UpdateValidateCompanyResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	):
			Single<UpdateValidateCompanyResponse> {
		return initRequestSingleForResult(
			UserProfileApi.updateValidateCompany(companyId, updateValidateCompanyRequest),
			onSuccess, onError
		)
	}

	fun getInfoCompany(
		companyId: Int,
		onSuccess: (response: GetCompanyInfoResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	):
			Single<GetCompanyInfoResponse> {
		return initRequestSingleForResult(
			UserProfileApi.getInfoCompany(companyId),
			onSuccess, onError
		)
	}

	fun getEmployerInfo(
		userId: Int,
		onSuccess: (response: GetEmployerInfoResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<GetEmployerInfoResponse> {

		return initRequestSingleForResult(UserProfileApi.getEmployerInfo(userId), onSuccess, onError)
	}

	fun getExecutorInfo(
		userId: Int,
		onSuccess: (response: GetExecutorInfoResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<GetExecutorInfoResponse> {
		return initRequestSingleForResult(UserProfileApi.getExecutorInfo(userId), onSuccess, onError)
	}

	fun uploadCompanyLogo(
			companyId: Int, filePart: MultipartBody.Part,
			onSuccess: (response: UploadCompanyLogoResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<UploadCompanyLogoResponse> {
		return initRequestSingleForResult(UserProfileApi.uploadCompanyLogo(companyId, filePart), onSuccess, onError)
	}

	fun editCompanyPosition(
			userId: Int,
			editCompanyPositionRequest: EditCompanyPositionRequest,
			onSuccess: (response: EditUserPersonalInfoResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<EditUserPersonalInfoResponse> {
		return initRequestSingleForResult(
				UserProfileApi.editEmployerPosition(userId, editCompanyPositionRequest),
				onSuccess,
				onError
		)
	}

	fun getDocumentPagination(
			companyId: Int,
			pageSize: Int,
			page: Int,
			onSuccess: (response: GetDocumentsPaginationResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<GetDocumentsPaginationResponse> {
		return initRequestSingleForResult(
				UserProfileApi.getDocumentPagination(companyId, pageSize, page),
				onSuccess,
				onError
		)
	}

	fun getExperincePagination(
		pageSize: Int,
		page: Int,
		onSuccess: (response: GetExperiencesListExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<GetExperiencesListExecutorResponse> {
		return initRequestSingleForResult(
			UserProfileApi.getExperiencesListExecutor(pageSize, page),
			onSuccess,
			onError
		)
	}

	fun getPortfolioPagination(
		pageSize: Int,
		page: Int,
		onSuccess: (response: GetPortfolioListExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit): Single<GetPortfolioListExecutorResponse> {
			return initRequestSingleForResult(UserProfileApi.getPortfolioListExecutor(pageSize, page),
				onSuccess, onError)
	}

	fun addCarExecutor(model: RequestBody, issued: RequestBody, file: MultipartBody.Part,
					   color: String, fuel: RequestBody,
					   onSuccess: (response: AddCarExecutorResponse) -> Unit,
					   onError: (errorMessage: String, errorCode: Int) -> Unit): Single<AddCarExecutorResponse> {

		return initRequestSingleForResult(UserProfileApi.addCarExecutor(model, issued, file, color, fuel), onSuccess, onError)
	}

	fun getModelCar(onSuccess: (response: GetModelCarResponse) -> Unit,
					onError: (errorMessage: String, errorCode: Int) -> Unit): Single<GetModelCarResponse> {
		return initRequestSingleForResult(UserProfileApi.getModelCar(), onSuccess, onError)
	}

	fun getCarsListExecutor(
		pageSize: Int,
		page: Int,
		onSuccess: (response: GetAutosExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit): Single<GetAutosExecutorResponse> {
			return initRequestSingleForResult(UserProfileApi.getAutoListExecutor(pageSize, page), onSuccess, onError)
	}

	fun deleteCarExecutor(
		id: Int,
		onSuccess: (response: DeleteAutoExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit): Single<DeleteAutoExecutorResponse> {
			return initRequestSingleForResult(UserProfileApi.deleteCarExecutor(id), onSuccess, onError)
	}

	fun getDocumentExecutorPagination(
		onSuccess: (response: GetDocumentsExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<GetDocumentsExecutorResponse> {
		return initRequestSingleForResult(
			UserProfileApi.getDocumentExecutorPagination(),
			onSuccess,
			onError
		)
	}

	fun loadDocument(
			companyId: Int,
			documentName: String,
			fileBitmap: MultipartBody.Part,
			filePDF: MultipartBody.Part,
			onSuccess: (response: CreateDocumentResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CreateDocumentResponse> {
		return initRequestSingleForResult(
				UserProfileApi.loadDocument(companyId, documentName, fileBitmap, filePDF),
				onSuccess,
				onError
		)
	}

	fun addExperienceExecutor(
		name: RequestBody,
		city: String,
		position: RequestBody,
		period: String,
		file: MultipartBody.Part,
		onSuccess: (response: AddExperienceExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<AddExperienceExecutorResponse> {
		return initRequestSingleForResult(UserProfileApi.addExperienceExecutor(name, city, position, period, file),
			onSuccess, onError)
	}

	fun loadDocumentExecutor(
		documentName: RequestBody,
		fileBitmap: MultipartBody.Part,
		filePDF: MultipartBody.Part,
		onSuccess: (response: CreateDocumentExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CreateDocumentExecutorResponse> {
		return initRequestSingleForResult(
			UserProfileApi.loadDocumentExecutor(documentName, fileBitmap, filePDF),
			onSuccess,
			onError
		)
	}

	fun deleteDocument(
			companyId: Int,
			documentId: Int,
			onSuccess: (response: DeleteDocumentResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<DeleteDocumentResponse> {
		return initRequestSingleForResult(UserProfileApi.deleteDocument(companyId, documentId), onSuccess, onError)
	}

	fun deleteDocumentExecutor(
		documentId: Int,
		onSuccess: (response: DeleteDocumentResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<DeleteDocumentResponse> {
		return initRequestSingleForResult(UserProfileApi.deleteDocumentExecutor(documentId), onSuccess, onError)
	}

	fun addCompanyPhoto(
			companyId: Int,
			photos: ArrayList<File>,
			onSuccess: (response: CompanyPhotoResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CompanyPhotoResponse> {

		val multipartFilesList = arrayListOf<MultipartBody.Part>()
		photos.forEach {
			multipartFilesList.add(
					MultipartBody.Part.createFormData(
							"photos[]", it.name,
							RequestBody.create("image".toMediaTypeOrNull(), it)
					)
			)
		}

		return initRequestSingleForResult(
				UserProfileApi.addCompanyPhoto(companyId, *multipartFilesList.toTypedArray()),
				onSuccess,
				onError
		)
	}

	fun getPhotosCompany(
			companyId: Int,
			onSuccess: (response: CompanyPhotoResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CompanyPhotoResponse> {
		return initRequestSingleForResult(UserProfileApi.getPhotosCompany(companyId), onSuccess, onError)
	}


	fun deletePhotoCompany(
			companyId: Int,
			photoName: String, onSuccess: (response: DeletePhotoResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<DeletePhotoResponse> {

		return initRequestSingleForResult(UserProfileApi.deleteCompanyPhoto(companyId, photoName), onSuccess, onError)
	}


	fun getCurrency(
			onSuccess: (response: CurrencyResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CurrencyResponse> {
		return initRequestSingleForResult(UserProfileApi.getCurrency(), onSuccess, onError)
	}

	fun getCategoriesList(
			onSuccess: (response: CategoryResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CategoryResponse> {

		val companyId = PaperIO.companyData?.companyId ?: -1

		return initRequestSingleForResult(UserProfileApi.getCategoriesList(companyId), onSuccess, onError)
	}

	fun getWageType(
			onSuccess: (response: WageTypeResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<WageTypeResponse> {
		val companyId = PaperIO.companyData?.companyId ?: -1
		return initRequestSingleForResult(UserProfileApi.getWageType(companyId), onSuccess, onError)
	}

	fun getWorkSchedule(
			onSuccess: (response: WorkScheduleResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<WorkScheduleResponse> {
		val companyId = PaperIO.companyData?.companyId ?: -1
		return initRequestSingleForResult(UserProfileApi.getWorkSchedule(companyId), onSuccess, onError)
	}

	fun createVacancy(
			body: CreateVacancyRequest,
			onSuccess: (response: CreateVacancyResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CreateVacancyResponse> {
		val companyId = PaperIO.companyData?.companyId ?: -1

		return initRequestSingleForResult(UserProfileApi.createVacancy(companyId, body), onSuccess, onError)
	}

	fun editVacancy(vacancyId: Int, body: CreateVacancyRequest,
					onSuccess: (response: CreateVacancyResponse) -> Unit,
					onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CreateVacancyResponse> {
		val companyId = PaperIO.companyData?.companyId ?: -1
		return initRequestSingleForResult(UserProfileApi.editVacancy(companyId, vacancyId, body), onSuccess, onError)
	}

	fun getHousingPagination(companyId: Int,
							 pageSize: Int,
							 page: Int,
							 onSuccess: (response: AllHouseResponse) -> Unit,
							 onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<AllHouseResponse> {
		return initRequestSingleForResult(UserProfileApi.getHousingPagination(companyId, pageSize, page),
				onSuccess,
				onError
		)
	}

	fun addPortfolioExecutor(name: RequestBody,
							 description: RequestBody,
							 photos: ArrayList<File>,
							 onSuccess: (response: AddPortfolioExecutorResponse) -> Unit,
							 onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<AddPortfolioExecutorResponse> {

		val multipartFilesList = arrayListOf<MultipartBody.Part>()
		photos.forEachIndexed { index, fileData ->
			multipartFilesList.add(
				MultipartBody.Part.createFormData(
					"file[$index]", fileData.name,
					RequestBody.create("image".toMediaTypeOrNull(), fileData)
				)
			)
		}
		return initRequestSingleForResult(UserProfileApi.addPortfolioExecutor(name, description,
			*multipartFilesList.toTypedArray()), onSuccess, onError)
	}


	fun createHousing(companyId: Int,
					  createHouseRequest: CreateHouseRequest,
					  description: String,
					  photos: ArrayList<File>,
					  onSuccess: (response: CreateHouseResponce) -> Unit,
					  onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<CreateHouseResponce> {

		val multipartFilesList = arrayListOf<MultipartBody.Part>()

		photos.forEach {
			multipartFilesList.add(
					MultipartBody.Part.createFormData(
							"photos[]", it.name,
							RequestBody.create("multipart/form-data".toMediaTypeOrNull(), it)
					)
			)
		}

		val requestItem = hashMapOf(
				"address" to createHouseRequest.address,
				"rooms" to createHouseRequest.rooms,
				"cost_of_rent" to createHouseRequest.cost_of_rent,
				"currency_of_rent" to createHouseRequest.currency_of_rent,
				"address_place_id" to createHouseRequest.address_place_id,
				"lat" to createHouseRequest.lat,
				"lng" to createHouseRequest.lng,
				"available" to createHouseRequest.available

		)

		return initRequestSingleForResult(UserProfileApi.createHouse(companyId, requestItem,
			RequestBody.create("multipart/form-data".toMediaTypeOrNull(), description), *multipartFilesList.toTypedArray()), onSuccess, onError)
	}

	fun deleteHouse(
			houseId: Int,
			onSuccess: (response: DeleteHouseResponse) -> Unit,
			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<DeleteHouseResponse> {

		val companyId = PaperIO.companyData?.companyId ?: -1

		return initRequestSingleForResult(UserProfileApi.deleteHouse(companyId, houseId), onSuccess, onError)
	}

	fun getHouse(houseId: Int, companyId: Int,
				 onSuccess: (response: GetHouseResponse) -> Unit,
				 onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<GetHouseResponse> {

		return initRequestSingleForResult(UserProfileApi.getHouse(companyId, houseId), onSuccess, onError)
	}

	fun getStatistic(companyId: Int, onSuccess: (response: StatisticResponse) -> Unit,
					 onError: (errorMessage: String, errorCode: Int) -> Unit): Single<StatisticResponse> {

		return initRequestSingleForResult(UserProfileApi.getStatistic(companyId), onSuccess, onError)
	}


	fun getVacancy(vacancyId: Int,
				   onSuccess: (response: VacancyResponse) -> Unit,
				   onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacancyResponse> {

		val companyId = PaperIO.companyData?.companyId ?: -1

		return initRequestSingleForResult(UserProfileApi.getVacancy(companyId, vacancyId), onSuccess, onError)
	}

	fun participateVacancy(vacancyId: Int,
						   onSuccess: (response: ParticipateResponse) -> Unit,
						   onError: (errorMessage: String, errorCode: Int) -> Unit): Single<ParticipateResponse> {
		return initRequestSingleForResult(UserProfileApi.participateVacancy(vacancyId), onSuccess, onError)
	}

	fun getFavoriteVacancy(id: Int,
						   onSuccess: (response: VacancyResponse) -> Unit,
						   onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacancyResponse> {
		return initRequestSingleForResult(UserProfileApi.getFavoriteVacancy(id), onSuccess, onError)
	}

	fun getVacancies(pageSize: Int, page: Int,
                     onSuccess: (response: VacanciesListResponse) -> Unit,
					 onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacanciesListResponse> {
		val companyId = PaperIO.companyData?.companyId ?: -1
		return initRequestSingleForResult(UserProfileApi.getVacanciesPagination(companyId, pageSize, page), onSuccess, onError)
	}

	fun getVacanciesWithFilter(time: Int, onSuccess: (response: VacanciesListResponse) -> Unit,
								onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacanciesListResponse> {

		return initRequestSingleForResult(UserProfileApi.getVacanciesWithFilter(time, 100), onSuccess, onError)
	}

	fun loadMoreVacanciesWithFilter(page: Int, filter: ModelFilter, onSuccess: (response: VacanciesListResponse) -> Unit,
									onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacanciesListResponse> {

		return initRequestSingleForResult(UserProfileApi.loadMoreVacanciesWithFilter(filter.type_employment, filter.hasHouse, filter.timePublish,
			filter.type_sort, filter.order, filter.expectedWage, 20, page), onSuccess, onError)
	}


	fun loadMoreVacanciesWithFilterTwo(page: Int, time: Int, onSuccess: (response: VacanciesListResponse) -> Unit,
									onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacanciesListResponse> {

		return initRequestSingleForResult(UserProfileApi.loadMoreVacanciesWithFilter(time, 100), onSuccess, onError)
	}

	fun getVacanciesForExecutor(pageSize: Int = Constants.vacancyItemLimit, page: Int = 0,onSuccess: (response: VacanciesListResponse) -> Unit,
					 onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacanciesListResponse> {
		return initRequestSingleForResult(UserProfileApi.getVacanciesForExecutor(pageSize, page), onSuccess, onError)
	}

	fun getFavoriteVacanciesForExecutor(onSuccess: (response: GetFavoriteVacanciesResponse) -> Unit,
								onError: (errorMessage: String, errorCode: Int) -> Unit): Single<GetFavoriteVacanciesResponse> {
		return initRequestSingleForResult(UserProfileApi.getFavoriteVacanciesForExecutor(), onSuccess, onError)
	}

	fun getVacancyForExecutor(id_vacancy: Int, onSuccess: (response: VacancyResponseDetail) -> Unit,
							  onError: (errorMessage: String, errorCode: Int) -> Unit): Single<VacancyResponseDetail> {
		return initRequestSingleForResult(UserProfileApi.getVacancyForExecutor(id_vacancy), onSuccess, onError)
	}

	fun getRequestsVacancy(
		id_vacancy: Int, onSuccess: (response: GetRequestsVacancyResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit): Single<GetRequestsVacancyResponse> {

		return initRequestSingleForResult(UserProfileApi.getVacancyRequests(id_vacancy), onSuccess, onError)
	}

	fun getPreliminaryVacancy(onSuccess: (response: PreliminaryVacancyResponse) -> Unit,
							  onError: (errorMessage: String, errorCode: Int) -> Unit): Single<PreliminaryVacancyResponse> {
		return initRequestSingleForResult(UserProfileApi.getPreliminaryVacancy(), onSuccess, onError)
	}

	fun applyVacancy(id: Int, onSuccess: (response: ApplyVacancyResponse) -> Unit,
					 onError: (errorMessage: String, errorCode: Int) -> Unit): Single<ApplyVacancyResponse> {
		return initRequestSingleForResult(UserProfileApi.applyVacancy(id), onSuccess, onError)
	}

	fun rejectVacancy(id: Int, onSuccess: (response: ApplyVacancyResponse) -> Unit,
					 onError: (errorMessage: String, errorCode: Int) -> Unit): Single<ApplyVacancyResponse> {
		return initRequestSingleForResult(UserProfileApi.rejectVacancy(id), onSuccess, onError)
	}

	fun addVacancyFavorite(request: AddFavoriteVacancyExecutorRequest,
						   onSuccess: (response: AddFavoriteVacancyResponse) -> Unit,
						   onError: (errorMessage: String, errorCode: Int) -> Unit): Single<AddFavoriteVacancyResponse> {
		return initRequestSingleForResult(UserProfileApi.addFavoriteVacancyExecutor(request), onSuccess, onError)
	}

    fun deleteFavoriteVacancy(id: Int,
                              onSuccess: (response: DeleteFavoriteVacancyResponse) -> Unit,
                              onError: (errorMessage: String, errorCode: Int) -> Unit): Single<DeleteFavoriteVacancyResponse> {
		return initRequestSingleForResult(UserProfileApi.deleteFavoriteVacancyExecutor(id), onSuccess, onError)
    }

	fun deleteVacancy(vacancyId: Int,
					  onSuccess: (response: DeleteVacancyResponse) -> Unit,
					  onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<DeleteVacancyResponse> {
		val companyId = PaperIO.companyData?.companyId ?: 0
		return initRequestSingleForResult(UserProfileApi.deleteVacancy(companyId, vacancyId), onSuccess, onError)
	}

	fun getNews(onSuccess:(NewsResponse) -> Unit,
				onError: (errorMessage: String, errorCode: Int) -> Unit): Single<NewsResponse> {
		return initRequestSingleForResult(UserProfileApi.getNews(), onSuccess, onError)
	}

	fun getUrgerCenter(onSuccess:(CenterUrgerResponse) -> Unit,
				onError: (errorMessage: String, errorCode: Int) -> Unit): Single<CenterUrgerResponse> {
		return initRequestSingleForResult(UserProfileApi.getUrgerCenter(), onSuccess, onError)
	}

	fun translateMessage(message:String,
						 pair:String,
						 onSuccess: (response: TranslateResponse) -> Unit,
						 onError: (errorMessage: String, errorCode: Int) -> Unit
						 ):Single<TranslateResponse> {
		return initRequestSingleForResult(UserProfileApi.translate(message, pair), onSuccess, onError)
	}

	fun addFeedback(email:String, text: String,
					onSuccess: (response: AddFeedbackFromExecutorResponse) -> Unit,
					onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<AddFeedbackFromExecutorResponse> {

		return initRequestSingleForResult(UserProfileApi.addFeedback(RequestBody.create(
			"multipart/form-data".toMediaTypeOrNull(), email), RequestBody.create("multipart/form-data".toMediaTypeOrNull(), text)), onSuccess,  onError)
	}

	fun addFeedbackFromCompany(id: Int, rating:Int, text: String,
					onSuccess: (response: FeedbackFromCompanyResponse) -> Unit,
					onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<FeedbackFromCompanyResponse> {

		return initRequestSingleForResult(UserProfileApi.addFeedbackFromCompany(id, rating, RequestBody.create(
			"multipart/form-data".toMediaTypeOrNull(), text)), onSuccess,  onError)
	}

	fun addFeedbackFromExecutor(id: Int, rating:Int, text: String,
							   onSuccess: (response: FeedbackFromExecutorResponse) -> Unit,
							   onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<FeedbackFromExecutorResponse> {

		return initRequestSingleForResult(UserProfileApi.addFeedbackFromExecutor(id, rating, RequestBody.create(
			"multipart/form-data".toMediaTypeOrNull(), text)), onSuccess,  onError)
	}

	fun getFeedbackExecutor(onSuccess: (response: GetFeedbackExecutorResponse) -> Unit,
							onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<GetFeedbackExecutorResponse> {
		return initRequestSingleForResult(UserProfileApi.getFeedbackExecutor(), onSuccess, onError)
	}

	fun getFeedbackCompany(onSuccess: (response: GetFeedbackCompanyResponse) -> Unit,
							onError: (errorMessage: String, errorCode: Int) -> Unit
	):Single<GetFeedbackCompanyResponse> {
		return initRequestSingleForResult(UserProfileApi.getFeedbackCompany(), onSuccess, onError)
	}

	fun getAllInfoExecutor(id: Int,
		onSuccess: (response: AllInfoExecutorResponse) -> Unit,
		onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<AllInfoExecutorResponse> {
		return initRequestSingleForResult(UserProfileApi.getAllInfoExecutor(id), onSuccess, onError)
	}

	private fun <T, V> initRequestSingleForResult(
			request: Single<T>,
			onSuccess: (response: V) -> Unit,

			onError: (errorMessage: String, errorCode: Int) -> Unit
	): Single<T> {
		val callback = CallbackWrapper<T, V>(WeakReference(context), retrofit, onSuccess, onError)
		val requestSingle = request.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.doOnSuccess {
					callback.onSuccess(it)
				}
				.doOnError {
					callback.onError(it)
				}
		callback.request = requestSingle

		return requestSingle
	}

	fun zip(requests: ArrayList<Single<*>>, onLoadingEnded: () -> Unit): Disposable {
		return Single.zip(requests) { result -> }.subscribe({ onLoadingEnded() }, {})
	}

}