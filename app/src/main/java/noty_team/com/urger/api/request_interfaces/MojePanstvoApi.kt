package noty_team.com.urger.api.request_interfaces

import io.reactivex.Single
import noty_team.com.urger.utils.api.response.register_company_api_moje_panstvo.CompanyNameResponce
import retrofit2.http.GET
import retrofit2.http.Query

interface MojePanstvoApi {
	@GET("dane/krs_podmioty.json?")
	fun getCompanyName(@Query("conditions[krs_podmioty.nip]") nip:String ): Single<CompanyNameResponce>
}