package noty_team.com.urger.api.retrofit

import android.content.Context
import io.reactivex.Single
import io.reactivex.observers.DisposableSingleObserver
import noty_team.com.urger.utils.api.response.BaseResponse
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.lang.ref.WeakReference

class CallbackWrapper<T, V>(
        private val contextWeakReference: WeakReference<Context>? = null,
        private val retrofit: Retrofit,
        private var onSuccessResponse:(response: V) -> Unit,
        private var onError:(errorMessage: String, errorCode: Int) -> Unit) : DisposableSingleObserver<T>(){

    companion object {
        const val ERROR_INTERNET_CONNECTION_CODE = 600
        const val ERROR_CONVERTING_DATA_CODE = 601
        const val ERROR_UNEXPECTED_CODE = 666
    }

    var request: Single<T>? = null
    override fun onSuccess(response: T) {

        onSuccessResponse(response as V)

    }

    override fun onError(t: Throwable) {
        val errorMessage: String
        val errorCode: Int
        if (t is IOException) {
            errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_no_intenet_connection)?:*/"Проверьте интернет соединение"
            errorCode = ERROR_INTERNET_CONNECTION_CODE
        } else if (t is HttpException) {
            val errorWrapper = ErrorWrapper()
            val error = t.response()?.let { errorWrapper.parseError(retrofit, it) }

            if (error != null) {
                errorMessage = error.error
                errorCode = t.code()
            }
            else {
                errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_unexpected)?:*/t.message().toString()
                errorCode = ERROR_UNEXPECTED_CODE
            }

        } else if (t is IllegalStateException) {
            errorMessage = "Error converting data"
            errorCode = ERROR_CONVERTING_DATA_CODE
        } else {
            errorMessage = /*contextWeakReference?.get()?.getString(R.string.error_unexpected)?:*/t.message.toString()
            errorCode = ERROR_UNEXPECTED_CODE
        }
        onError(errorMessage, errorCode)
    }

    private inner class ErrorWrapper {
        fun parseError(retrofit: Retrofit, response: Response<*>): BaseResponse? {
            val converter: Converter<ResponseBody, BaseResponse> = retrofit
                    .responseBodyConverter(BaseResponse::class.java, emptyArray())
            try {

                response.errorBody()?.let {
                    return converter.convert(it)
                }
            } catch (e: Exception) {
                return BaseResponse()
            }
            return BaseResponse()
        }
    }
}