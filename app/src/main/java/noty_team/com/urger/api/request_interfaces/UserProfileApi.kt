package noty_team.com.urger.api.request_interfaces

import io.reactivex.Single
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.request.create_vacancy.CreateVacancyRequest
import noty_team.com.urger.utils.api.request.edit_company_position.EditCompanyPositionRequest
import noty_team.com.urger.utils.api.request.edit_user_personal_info.EditEmployerInfoRequest
import noty_team.com.urger.utils.api.request.edit_user_personal_info.EditUserPersonalInfoRequest
import noty_team.com.urger.utils.api.request.edit_user_personal_info.FcmRequest
import noty_team.com.urger.utils.api.request.update_validate_company.UpdateValidateCompanyRequest
import noty_team.com.urger.utils.api.response.add_document.create_document.CreateDocumentExecutorResponse
import noty_team.com.urger.utils.api.response.add_document.create_document.CreateDocumentResponse
import noty_team.com.urger.utils.api.response.all_info.AllInfoExecutorResponse
import noty_team.com.urger.utils.api.response.callback.AddFeedbackFromExecutorResponse
import noty_team.com.urger.utils.api.response.can_edit_personal_data.CanEditPersonalData
import noty_team.com.urger.utils.api.response.car.AddCarExecutorResponse
import noty_team.com.urger.utils.api.response.car.DeleteAutoExecutorResponse
import noty_team.com.urger.utils.api.response.car.GetAutosExecutorResponse
import noty_team.com.urger.utils.api.response.car.GetModelCarResponse
import noty_team.com.urger.utils.api.response.categories_response.CategoryResponse
import noty_team.com.urger.utils.api.response.center.CenterUrgerResponse
import noty_team.com.urger.utils.api.response.chat.TranslateResponse
import noty_team.com.urger.utils.api.response.comapny_photo.CompanyPhotoResponse
import noty_team.com.urger.utils.api.response.company.GetCompanyInfoResponse
import noty_team.com.urger.utils.api.response.create_house.CreateHouseResponce
import noty_team.com.urger.utils.api.response.create_vacancy.CreateVacancyResponse
import noty_team.com.urger.utils.api.response.currency.CurrencyResponse
import noty_team.com.urger.utils.api.response.delete_document_response.DeleteDocumentResponse
import noty_team.com.urger.utils.api.response.delete_house.DeleteHouseResponse
import noty_team.com.urger.utils.api.response.delete_photo.DeletePhotoResponse
import noty_team.com.urger.utils.api.response.delete_vacancy.DeleteVacancyResponse
import noty_team.com.urger.utils.api.response.edit_executor.EditExecutorPersonalInfoResponse
import noty_team.com.urger.utils.api.response.edit_user_personal_info.EditUserPersonalInfoResponse
import noty_team.com.urger.utils.api.response.experience_executor.AddExperienceExecutorResponse
import noty_team.com.urger.utils.api.response.experience_executor.DeleteExperienceResponse
import noty_team.com.urger.utils.api.response.experience_executor.get.GetExperiencesListExecutorResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.AddFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.DeleteFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.favorite.GetFavoriteVacanciesResponse
import noty_team.com.urger.utils.api.response.feedback.FeedbackFromCompanyResponse
import noty_team.com.urger.utils.api.response.feedback.FeedbackFromExecutorResponse
import noty_team.com.urger.utils.api.response.feedback.get.employer.GetFeedbackCompanyResponse
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.get_all_house.AllHouseResponse
import noty_team.com.urger.utils.api.response.get_document.GetDocumentsExecutorResponse
import noty_team.com.urger.utils.api.response.get_document.GetDocumentsPaginationResponse
import noty_team.com.urger.utils.api.response.get_employer.GetEmployerInfoResponse
import noty_team.com.urger.utils.api.response.get_executor.GetExecutorInfoResponse
import noty_team.com.urger.utils.api.response.get_house.GetHouseResponse
import noty_team.com.urger.utils.api.response.get_user_personal_avatar.UserPersonalAvatarResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyResponse
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.PreliminaryVacancyResponse
import noty_team.com.urger.utils.api.response.load_background_response.LoadBackgroundResponse
import noty_team.com.urger.utils.api.response.load_photo.LoadPhotoResponse
import noty_team.com.urger.utils.api.response.news.NewsResponse
import noty_team.com.urger.utils.api.response.portfolio.AddPortfolioExecutorResponse
import noty_team.com.urger.utils.api.response.portfolio.GetPortfolioListExecutorResponse
import noty_team.com.urger.utils.api.response.statistic.StatisticResponse
import noty_team.com.urger.utils.api.response.update_validate_company.UpdateValidateCompanyResponse
import noty_team.com.urger.utils.api.response.upload_company_logo.UploadCompanyLogoResponse
import noty_team.com.urger.utils.api.response.vacancy.ParticipateResponse
import noty_team.com.urger.utils.api.response.vacancy.apply.ApplyVacancyResponse
import noty_team.com.urger.utils.api.response.vacancy.detailExecutor.VacancyResponseDetail
import noty_team.com.urger.utils.api.response.vacancy.requests.GetRequestsVacancyResponse
import noty_team.com.urger.utils.api.response.wage_executor.GetWageResponse
import noty_team.com.urger.utils.api.response.wage_type.WageTypeResponse
import noty_team.com.urger.utils.api.response.work_schedule.WorkScheduleResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


@JvmSuppressWildcards
interface UserProfileApi {

	@Multipart
	@POST("user/{id}/avatar")
	fun updateUserPersonalAvatar(
			@Path("id") userId: Int,
			@Part filePart: MultipartBody.Part
	): Single<LoadPhotoResponse>



	@GET("user/{id}/avatar")
	fun getUserPersonalAvatar(
		@Path("id") userId: Int
	): Single<UserPersonalAvatarResponse>

	@Multipart
	@POST("user/{id}/background")
	fun updateUserPersonalBackground(
			@Path("id") userId: Int,
			@Part filePart: MultipartBody.Part
	): Single<LoadBackgroundResponse>

	@PUT("executor/update")
	fun editUserPersonalInfo(
		@Body userPersonalInfo: EditUserPersonalInfoRequest
	): Single<EditExecutorPersonalInfoResponse>


	@PATCH("employer/{id}")
	fun updateFcmEmployer(
		@Path("id") id: Int,
		@Body fcm: FcmRequest
	): Single<GetEmployerInfoResponse>

	@PUT("executor/update")
	fun updateFcmExecutor(
		@Body fcm: FcmRequest
	): Single<EditExecutorPersonalInfoResponse>


	@GET("user/can-update")
	fun isCanEditPersonalInfo(): Single<CanEditPersonalData>

	@GET("employer/{id}")
	fun getEmployerInfo(@Path("id") userId: Int): Single<GetEmployerInfoResponse>

	@GET("user/executor/{id}")
	fun getExecutorInfo(@Path("id") userId: Int): Single<GetExecutorInfoResponse>

	@PATCH("company/validated-company/{id}")
	fun updateValidateCompany(
			@Path("id") id: Int,
			@Body description: UpdateValidateCompanyRequest
	): Single<UpdateValidateCompanyResponse>

	@GET("company/validated-company/{id}")
	fun getInfoCompany(
		@Path("id") id: Int
	): Single<GetCompanyInfoResponse>

	@PATCH("employer/{id}")
	fun editInfoEmployer(
		@Path("id") id: Int,
		@Body info: EditEmployerInfoRequest
	): Single<GetEmployerInfoResponse>



	@Multipart
	@POST("company/{id}/logo")
	fun uploadCompanyLogo(
			@Path("id") companyId: Int,
			@Part filePart: MultipartBody.Part
	): Single<UploadCompanyLogoResponse>

	@PATCH("employer/{id}")
	fun editEmployerPosition(
			@Path("id") id: Int,
			@Body editCompanyPosition: EditCompanyPositionRequest
	): Single<EditUserPersonalInfoResponse>

	@GET("company/validated-company/{companyId}/document?")
	fun getDocumentPagination(
			@Path("companyId") companyId: Int,
			@Query("pagination[pageSize]") pageSize: Int,
			@Query("pagination[page]") page: Int
	): Single<GetDocumentsPaginationResponse>

	@GET("user_document")
	fun getDocumentExecutorPagination(): Single<GetDocumentsExecutorResponse>

	@Multipart
	@POST("company/validated-company/{companyId}/document")
	fun loadDocument(
			@Path("companyId") companyId: Int,
			@Part("name") name: String,
			@Part fileBitmap: MultipartBody.Part,
			@Part filePDF: MultipartBody.Part
	): Single<CreateDocumentResponse>

	@Multipart
	@POST("user_document/create")
	fun loadDocumentExecutor(
		@Part("name") name: RequestBody,
		@Part fileBitmap: MultipartBody.Part,
		@Part filePDF: MultipartBody.Part
	): Single<CreateDocumentExecutorResponse>


	@DELETE("company/validated-company/{companyId}/document/{documentId}")
	fun deleteDocument(
			@Path("companyId") companyId: Int,
			@Path("documentId") documentId: Int
	): Single<DeleteDocumentResponse>

	@DELETE("user_document/{documentId}/delete")
	fun deleteDocumentExecutor(
		@Path("documentId") documentId: Int
	): Single<DeleteDocumentResponse>


	@Multipart
	@POST("company/validated-company/{companyId}/photo")
	fun addCompanyPhoto(
			@Path("companyId") companyId: Int,
			@Part vararg filePart: MultipartBody.Part
	): Single<CompanyPhotoResponse>

	@GET("company/validated-company/{companyId}/photo")
	fun getPhotosCompany(@Path("companyId") companyId: Int): Single<CompanyPhotoResponse>


	@DELETE("company/validated-company/{companyId}/photo/{photo_name}")
	fun deleteCompanyPhoto(
			@Path("companyId") companyId: Int,
			@Path("photo_name") photoName: String
	): Single<DeletePhotoResponse>

	@GET("company/validated-company/{companyId}/house")
	fun getHousingPagination(
			@Path("companyId") companyId: Int,
			@Query("pagination[pageSize]") pageSize: Int,
			@Query("pagination[page]") page: Int
	): Single<AllHouseResponse>

	@Multipart
	@POST("company/validated-company/{companyId}/house")
	fun createHouse(
			@Path("companyId") companyId: Int,
			@PartMap createHouseRequest: Map<String, Any>,
			@Part("description") descr: RequestBody,
			@Part vararg filePart: MultipartBody.Part
	): Single<CreateHouseResponce>


	@DELETE("company/validated-company/{companyId}/house/{houseId}")
	fun deleteHouse(@Path("companyId") companyId: Int,
					@Path("houseId") houseId: Int
	): Single<DeleteHouseResponse>


	@GET("company/validated-company/{companyId}/house/{houseId}")
	fun getHouse(@Path("companyId") companyId: Int,
				 @Path("houseId") houseId: Int
	): Single<GetHouseResponse>

	@GET("currency")
	fun getCurrency(): Single<CurrencyResponse>

	@GET("company/validated-company/{companyId}/statistics")
	fun getStatistic(@Path("companyId") companyId: Int): Single<StatisticResponse>


	@GET("company/validated-company/{companyId}/vacancy/category")
	fun getCategoriesList(@Path("companyId") companyId: Int): Single<CategoryResponse>


	@GET("company/validated-company/{companyId}/vacancy/wage-type")
	fun getWageType(@Path("companyId") companyId: Int): Single<WageTypeResponse>


	@GET("company/validated-company/{companyId}/vacancy/work-schedule")
	fun getWorkSchedule(@Path("companyId") companyId: Int): Single<WorkScheduleResponse>


	@GET("company/validated-company/{companyId}/vacancy/{vacancyId}")
	fun getVacancy(@Path("companyId") companyId: Int,
				   @Path("vacancyId") vacancyId: Int
	): Single<VacancyResponse>

	@POST("user_vacancy/{vacancyId}/create")
	fun participateVacancy(@Path("vacancyId") vacancyId: Int
	): Single<ParticipateResponse>

	@GET("favourite_vacancy/{id}/view")
	fun getFavoriteVacancy(@Path("id") id: Int): Single<VacancyResponse>

	@GET("company/validated-company/{companyId}/vacancy")
	fun getVacanciesPagination(@Path("companyId") companyId: Int,
		@Query("pagination[pageSize]") pageSize: Int,
		@Query("pagination[page]") page: Int
	): Single<VacanciesListResponse>


	@GET("user_vacancy")
	fun getVacanciesForExecutor(
		@Query("pagination[pageSize]") pageSize: Int,
		@Query("pagination[page]") page: Int
	): Single<VacanciesListResponse>


	@GET("user_vacancy")
	fun getVacanciesWithFilter(@Query("employment") employment: String?,
							   @Query("house") house: Int?,
							   @Query("time") time: Int?,
							   @Query("type_sort") type_sort: String?,
							   @Query("order") order: String?,
							   @Query("wage") wage: Int?,
							   @Query("pagination[pageSize]") pageSize: Int,
							   @Query("pagination[page]") page: Int
							   ): Single<VacanciesListResponse>

	@GET("user_vacancy")
	fun loadMoreVacanciesWithFilter(@Query("employment") employment: String?,
							   @Query("house") house: Int?,
							   @Query("time") time: Int?,
							   @Query("type_sort") type_sort: String?,
							   @Query("order") order: String?,
							   @Query("wage") wage: Int?,
							   @Query("pagination[pageSize]") pageSize: Int,
							   @Query("pagination[page]") page: Int
	): Single<VacanciesListResponse>

	@GET("user_vacancy")
	fun getVacanciesWithFilter(@Query("time") time: Int?,
							   @Query("pagination[pageSize]") pageSize: Int
	): Single<VacanciesListResponse>

	@GET("user_vacancy")
	fun loadMoreVacanciesWithFilter(@Query("time") time: Int?,
									@Query("pagination[pageSize]") page: Int
	): Single<VacanciesListResponse>

	@GET("favourite_vacancy")
	fun getFavoriteVacanciesForExecutor(): Single<GetFavoriteVacanciesResponse>

	@GET("user_vacancy/{id_vacancy}/view")
	fun getVacancyForExecutor(@Path("id_vacancy") vacancyId: Int) : Single<VacancyResponseDetail>

	@GET("employer_vacancy/vacancy")
	fun getVacancyRequests(@Query("id") vacancyId: Int): Single<GetRequestsVacancyResponse>


	@POST("favourite_vacancy/create")
	fun addFavoriteVacancyExecutor(@Body body: AddFavoriteVacancyExecutorRequest): Single<AddFavoriteVacancyResponse>

	@DELETE("favourite_vacancy/{id_to_delete}/delete")
	fun deleteFavoriteVacancyExecutor(@Path("id_to_delete") id: Int): Single<DeleteFavoriteVacancyResponse>

	@POST("company/validated-company/{companyId}/vacancy")
	fun createVacancy(@Path("companyId") companyId: Int,
					  @Body body: CreateVacancyRequest): Single<CreateVacancyResponse>

	@PUT("company/validated-company/{companyId}/vacancy/{vacancyId}")
	fun editVacancy(@Path("companyId") companyId: Int,
					@Path("vacancyId") vacancyId: Int,
					@Body body: CreateVacancyRequest): Single<CreateVacancyResponse>

	@DELETE("company/validated-company/{companyId}/vacancy/{vacancyId}")
	fun deleteVacancy(@Path("companyId") companyId: Int,
					  @Path("vacancyId") vacancyId: Int): Single<DeleteVacancyResponse>

	@GET("news/index")
	fun getNews():Single<NewsResponse>

	@Multipart
	@POST("expiriences/create")
	fun addExperienceExecutor(
		@Part("company_name") company_name: RequestBody,
		@Part("city") city: String,
		@Part("position") position: RequestBody,
		@Part("period")period: String,
		@Part logo: MultipartBody.Part):Single<AddExperienceExecutorResponse>

	@GET("expiriences")
	fun getExperiencesListExecutor(
		@Query("pagination[pageSize]") pageSize: Int,
		@Query("pagination[page]") page: Int
	): Single<GetExperiencesListExecutorResponse>

	@DELETE("expiriences/{id}/delete")
	fun deleteExperienceExecutor(@Path("id") id: Int) : Single<DeleteExperienceResponse>

	@Multipart
	@POST("portfolio/create")
	fun addPortfolioExecutor(
		@Part("name") position: RequestBody,
		@Part("description")description: RequestBody,
		@Part vararg file: MultipartBody.Part):Single<AddPortfolioExecutorResponse>


	@GET("portfolio")
	fun getPortfolioListExecutor(
		@Query("pagination[pageSize]") pageSize: Int,
		@Query("pagination[page]") page: Int
	): Single<GetPortfolioListExecutorResponse>

	@DELETE("portfolio/{id}/delete")
	fun deletePortfolioExecutor(@Path("id") id: Int) : Single<DeleteExperienceResponse>

	@Multipart
	@POST("auto/create")
	fun addCarExecutor(
		@Part("model") model: RequestBody,
		@Part("issued")issued: RequestBody,
		@Part file: MultipartBody.Part,
		@Part("color") color: String,
		@Part("fuel")fuel: RequestBody
    ):Single<AddCarExecutorResponse>

	@GET("https://vpic.nhtsa.dot.gov/api/vehicles/GetAllMakes?format=json")
	fun getModelCar():Single<GetModelCarResponse>


	@GET("auto")
	fun getAutoListExecutor(
		@Query("pagination[pageSize]") pageSize: Int,
		@Query("pagination[page]") page: Int
	): Single<GetAutosExecutorResponse>

	@DELETE("auto/{id}/delete")
	fun deleteCarExecutor(@Path("id") id: Int) : Single<DeleteAutoExecutorResponse>

	@GET("executor_profile")
	fun getWageExecutor() : Single<GetWageResponse>

	@PUT("executor_profile/update")
	fun updateWageSallary(@Query("sallary") wage: Int): Single<GetWageResponse>

	@GET("employer_vacancy")
	fun getPreliminaryVacancy():Single<PreliminaryVacancyResponse>

	@POST("employer_vacancy/apply")
	fun applyVacancy(@Query("id") id: Int): Single<ApplyVacancyResponse>

	@POST("employer_vacancy/reject")
	fun rejectVacancy(@Query("id") id: Int): Single<ApplyVacancyResponse>

	@GET("https://api.mymemory.translated.net/get?de=klondak10@gmail.com")
	fun translate(@Query("q") message: String,
				  @Query("langpair") fromTo: String):Single<TranslateResponse>

	@Multipart
	@POST("feedback/create")
	fun addFeedback(
		@Part("email") email: RequestBody,
		@Part("text") text: RequestBody
	): Single<AddFeedbackFromExecutorResponse>

	@Multipart
	@POST("employer_vacancy/finish")
	fun addFeedbackFromCompany(
		@Query("id") id: Int,
		@Part("employer_rate") rating: Int,
		@Part("employer_review") text: RequestBody
	): Single<FeedbackFromCompanyResponse>

	@Multipart
	@POST("user_vacancy/{id}/review")
	fun addFeedbackFromExecutor(
		@Path("id") id: Int,
		@Part("executor_rate") rating: Int,
		@Part("executor_review") text: RequestBody
	): Single<FeedbackFromExecutorResponse>

	@GET("executor_profile/reviews")
	fun getFeedbackExecutor():Single<GetFeedbackExecutorResponse>

	@GET("employer_vacancy/reviews")
	fun getFeedbackCompany():Single<GetFeedbackCompanyResponse>

	@GET("user/executor/{id}")
	fun getAllInfoExecutor(@Path("id") id: Int): Single<AllInfoExecutorResponse>

	@GET("center/index")
	fun getUrgerCenter(): Single<CenterUrgerResponse>

}