package noty_team.com.urger.api.request_interfaces

import io.reactivex.Single
import noty_team.com.urger.utils.api.request.create_company.CreateCompanyRequest
import noty_team.com.urger.utils.api.request.create_executor.CreateExecutorRequest
import noty_team.com.urger.utils.api.response.sms_checked.SmsCheckResponse
import noty_team.com.urger.utils.api.request.sms_phone_validation.UserPhoneValidationRequest
import noty_team.com.urger.utils.api.request.refresh_token_request.RefreshTokenRequest
import noty_team.com.urger.utils.api.response.create_employer_company.CreateEmployerCompanyResponse
import noty_team.com.urger.utils.api.response.create_executor_user.CreateExecutorUserResponse
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.api.response.nip.GetCompanyNameByNipResponse
import noty_team.com.urger.utils.api.response.refresh_token.RefreshTokenResponse
import noty_team.com.urger.utils.api.response.register_company.CreateEmployerResponse
import noty_team.com.urger.utils.api.response.validate_phone.ValidatePhoneResponse
import retrofit2.http.*

interface AuthorizationApi {

	@POST("token/refresh-token")
	fun refreshToken(@Body token: RefreshTokenRequest): Single<RefreshTokenResponse>

	@POST("sms-code/send-code")
	fun sendPhone(@Body body: UserPhoneValidationRequest): Single<ValidatePhoneResponse>

	@POST("sms-code/validate-code")
	fun checkSMS(@Body body: UserPhoneValidationRequest): Single<SmsCheckResponse>

	@POST("employer")
	fun createCompany(@Body body: CreateCompanyRequest): Single<CreateEmployerCompanyResponse>

	@POST("user/executor")
	fun createExecutor(@Body body: CreateExecutorRequest): Single<CreateExecutorUserResponse>

	@GET("category")
	fun getCategory(@Header("language")lang: String): Single<CityResponse>

	@GET("company/nip/{nip}")
	fun getCompanyNameByNip(@Path("nip") nip:String):Single<GetCompanyNameByNipResponse>

}