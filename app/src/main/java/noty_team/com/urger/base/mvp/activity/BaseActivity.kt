package noty_team.com.urger.base.mvp.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.ConfigurationCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleRegistryOwner
import com.google.android.gms.security.ProviderInstaller
import com.google.android.gms.security.ProviderInstaller.ProviderInstallListener
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.navigation.NavigationView
import com.squareup.otto.Bus
import com.zeugmasolutions.localehelper.LocaleHelper
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegate
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.urger.R
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.dagger.components.FragmentComponent
import noty_team.com.urger.dagger.modules.FragmentModule
import noty_team.com.urger.utils.cicerone.BackButtonListener
import noty_team.com.urger.utils.cicerone.Screens
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.*
import java.util.*
import javax.inject.Inject


abstract class BaseActivity : AppCompatActivity() {

	private val localeDelegate: LocaleHelperActivityDelegate = LocaleHelperActivityDelegateImpl()

	override fun getDelegate() = localeDelegate.getAppCompatDelegate(super.getDelegate())

	override fun attachBaseContext(newBase: Context) {
		super.attachBaseContext(localeDelegate.attachBaseContext(newBase))
	}


	override fun createConfigurationContext(overrideConfiguration: Configuration): Context {
		val context = super.createConfigurationContext(overrideConfiguration)
		return LocaleHelper.onAttach(context)
	}

	override fun getApplicationContext(): Context =
		localeDelegate.getApplicationContext(super.getApplicationContext())

	fun updateLocale(locale: Locale) {
		localeDelegate.setLocale(this, locale)
	}

	val currentLanguage
		get() = ConfigurationCompat.getLocales(resources.configuration)[0]

	lateinit var fragmentComponent: FragmentComponent

	@Inject
	lateinit var context: Context

	@Inject
	lateinit var subscriptions: CompositeDisposable

	lateinit var bus: Bus

	// lateinit var navigationBackManager: BackNavigator

	private lateinit var infoDialog: AlertDialog

	private lateinit var loadingDialog: Dialog

	private lateinit var deleteDialog: Dialog

	//цицерон бля
	lateinit var mainRouter: Router

	lateinit var navigatorHolder: NavigatorHolder

	val ciceroneNavigator = object : SupportAppNavigator(this, R.id.main_fragment_container) {
		override fun applyCommands(commands: Array<Command>) {
			super.applyCommands(commands)
			supportFragmentManager.executePendingTransactions()
		}
	}

	fun setCounterNewMessage(newMess: String, bottom_nav: BottomNavigationView) {
		val bottomNavigationMenuView = bottom_nav.findViewById(R.id.bn_chats) as BottomNavigationItemView
		val badge = LayoutInflater.from(context).inflate(R.layout.counter_message, bottomNavigationMenuView, false)
	    val tv = badge.findViewById(R.id.tv_badge) as TextView

		tv.text = newMess

		bottomNavigationMenuView.addView(badge)

	}


	abstract fun layout(): Int


	abstract fun initialization()

//	private fun registerBroadcastReceiver() {
//
//     val theFilter = IntentFilter()
//    /** System Defined Broadcast */
//    theFilter.addAction(Intent.ACTION_SCREEN_ON)
//    theFilter.addAction(Intent.ACTION_SCREEN_OFF)
//    theFilter.addAction(Intent.ACTION_USER_PRESENT)
//
//    val screenOnOffReceiver = object : BroadcastReceiver() {
//		override fun onReceive(context: Context?, intent: Intent?) {
//			val strAction = intent!!.action
//
//			val myKM =  context!!.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
//			if(theFilter.matchAction(strAction) || strAction!! == Intent.ACTION_SCREEN_OFF || strAction == Intent.ACTION_SCREEN_ON)
//				if(!myKM.inKeyguardRestrictedInputMode())  {
//					if (PaperIO.userRole == UserRole.COMPANY) {
//						CompanyActivity.start(this@BaseActivity)
//						finish()
//					} else {
//						ExecutorActivity.start(this@BaseActivity)
//						finish()
//					}
//					Log.i("TEST", "off")
//					if(!ChatSocket.provideSocketChat().connected()) {
//						Log.i("TEST", "off 2")
//						ChatSocket.provideSocketChat().connect()
//					} else {
//						Log.i("TEST", "off 3")
//					}
//				}
//		}
//    }
//
//    applicationContext.registerReceiver(screenOnOffReceiver, theFilter)
//	}


	companion object {
		lateinit  var appContext: Context
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		appContext = this

		ProviderInstaller.installIfNeededAsync(this, object : ProviderInstallListener {
			override fun onProviderInstalled() {}
			override fun onProviderInstallFailed(i: Int, intent: Intent?) {}
		})
		localeDelegate.onCreate(this)
		initCicerone()

		initFragmentComponent()

		bus = Bus()

		initDialog()
		intiLoadingDialog()

		if (layout() != 0) {
			setContentView(layout())
			initialization()
		}
	}


	private fun initCicerone() {
		val cicerone = Cicerone.create()
		mainRouter = cicerone.router
		navigatorHolder = cicerone.navigatorHolder
	}

	fun deleteDialog(dialogText: String, delete: () -> Unit) {
		deleteDialog = Dialog(this)

		deleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		deleteDialog.setContentView(R.layout.delete_document_dialog)


		val cancelButton = deleteDialog.findViewById<Button>(R.id.cancel_button_delete_document)
		val deleteButton = deleteDialog.findViewById<Button>(R.id.delete_document)
		deleteDialog.findViewById<TextView>(R.id.delete_dialog_text).text = dialogText

		cancelButton.setOnClickListener {
			deleteDialog.dismiss()
		}

		deleteButton.setOnClickListener {

			delete()
			deleteDialog.dismiss()

		}

		deleteDialog.show()

	}


	//цицерон бля
	private fun ciceroneBackPressedLogic() {

		val fm = supportFragmentManager
		var fragment: Fragment? = null
		val fragments = fm.fragments
		if (fragments != null) {
			for (f in fragments) {
				if (f.isVisible) {
					fragment = f
					break
				}
			}
		}

		if (fragment != null
				&& fragment is BackButtonListener
				&& (fragment as BackButtonListener).onBackPressed()
		) {
		} else {
			mainRouter.exit()
		}
	}

	fun <P : BasePresenter> replaceFragment(fragment: BaseFragment<P>) {
		ciceroneNavigator.applyCommands(arrayOf(Replace(Screens.FragmentScreen(fragment))))
	}

	fun <P : BasePresenter> addFragment(fragment: BaseFragment<P>) {
		ciceroneNavigator.applyCommands(arrayOf(Forward(Screens.FragmentScreen(fragment))))
	}

	fun <P : BasePresenter> newRootFragment(fragment: BaseFragment<P>) {
		ciceroneNavigator.applyCommands(arrayOf(BackTo(null), Replace(Screens.FragmentScreen(fragment))))
	}

	fun exit() {
		ciceroneNavigator.applyCommands(arrayOf(Back()))
	}

	override fun onResume() {
		localeDelegate.onResumed(this)
		navigatorHolder.setNavigator(ciceroneNavigator)
		super.onResume()
	}

	override fun onPause() {
		localeDelegate.onPaused()
		navigatorHolder.removeNavigator()
		super.onPause()
	}

	fun toggleKeyboard(show: Boolean) {
		val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		if (!show)
			inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
		else
			inputMethodManager.toggleSoftInputFromWindow(
					window.decorView.windowToken,
					InputMethodManager.SHOW_FORCED,
					0
			)
	}

	private fun intiLoadingDialog() {
		loadingDialog = Dialog(this)

		loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		loadingDialog.setContentView(R.layout.base_loading_dialog)


		loadingDialog.setOnCancelListener {
			onBackPressed()
			isShowLoadingDialog(false)
		}
		loadingDialog.setCanceledOnTouchOutside(false)
	}

	fun initFragmentComponent() {
		if (!::fragmentComponent.isInitialized) {
			fragmentComponent = AppUrger[this].appComponent.plus(FragmentModule(this))
			fragmentComponent.inject(this)
		}
	}

	open fun onBackStackChanged(stackSize: Int) {}

	override fun onBackPressed() {

		val drawer = getDrawerLayout() ?: null

		if (drawer != null) {
			when {
				drawer.isDrawerOpen(GravityCompat.START) -> drawer.closeDrawer(GravityCompat.START)
				else -> ciceroneBackPressedLogic()
			}
		} else {
			ciceroneBackPressedLogic()
		}

	}

	fun freeMemory() {
		System.runFinalization()
		Runtime.getRuntime().gc()
		System.gc()
	}


	private fun initDialog() {
		val builder = AlertDialog.Builder(this)
		builder.setTitle("Test")
				.setMessage("hello")
				.setNegativeButton("OK")
				{ _, _ -> this.hideDialog() }


		infoDialog = builder.create()
	}


	fun showDialogWithError() {
		infoDialog.show()
	}

	fun showShortToast(massage: String) {
		Toast.makeText(this, massage, Toast.LENGTH_SHORT).show()
	}

	fun showLongToast(massage: String) {
		Toast.makeText(this, massage, Toast.LENGTH_LONG).show()
	}

	fun hideDialog() {
		if (infoDialog.isShowing)
			infoDialog.dismiss()
	}

	fun isShowLoadingDialog(isShow: Boolean) {
		if (isShow) {
			loadingDialog.show()
		} else {
			loadingDialog.dismiss()
		}
	}

	fun getBottomSheetDialog() =
        BottomSheetDialog(
            this,
            R.style.BottomDialogTheme
        )

	abstract fun getDrawerLayout(): DrawerLayout?

	abstract fun getNavigationView(): NavigationView?

	abstract fun getBottomNavigationView(): BottomNavigationView?

	abstract fun openDrawerLayout()



	override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
		val v = currentFocus

		if (v != null &&
			(ev.action == MotionEvent.ACTION_UP || ev.action == MotionEvent.ACTION_MOVE) &&
			v is EditText
		//	!v::class.getName().startsWith("android.webkit.")
		) {
			val scrcoords = IntArray(2)
			v.getLocationOnScreen(scrcoords)
			val x = ev.rawX + v.left - scrcoords[0]
			val y = ev.rawY + v.top - scrcoords[1]
			if (x < v.left || x > v.right || y < v.top || y > v.bottom)
				hideKeyboard(this)
		}
		return super.dispatchTouchEvent(ev)
	}


	fun hideKeyboard(activity: AppCompatActivity?) {
		if (activity != null && activity.window != null && activity.window.decorView != null) {
			val imm =
				activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
			imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
		}
	}


}