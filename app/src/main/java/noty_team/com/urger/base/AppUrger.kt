package noty_team.com.urger.base

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.security.ProviderInstaller
import com.google.android.libraries.places.api.Places
import com.google.firebase.analytics.FirebaseAnalytics
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.zeugmasolutions.localehelper.LocaleHelper
import com.zeugmasolutions.localehelper.LocaleHelperApplicationDelegate
import io.paperdb.Paper
import noty_team.com.urger.BuildConfig
import noty_team.com.urger.dagger.components.AppComponent
import noty_team.com.urger.dagger.components.DaggerAppComponent
import noty_team.com.urger.dagger.modules.AppModule
import noty_team.com.urger.dagger.modules.RetrofitApiModule
import noty_team.com.urger.utils.cicerone.LocalCiceroneHolder
import java.util.*


class AppUrger : Application() {

    private val localeAppDelegate = LocaleHelperApplicationDelegate()

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(localeAppDelegate.attachBaseContext(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeAppDelegate.onConfigurationChanged(this)
    }

    override fun getApplicationContext(): Context =
        LocaleHelper.onAttach(super.getApplicationContext())


    companion object {
        val localCiceroneHolder: LocalCiceroneHolder by lazy {
            LocalCiceroneHolder()
        }

        lateinit var instance: AppUrger
            private set

        operator fun get(activity: AppCompatActivity): AppUrger {
            return activity.application as AppUrger
        }

        operator fun get(context: Context): AppUrger {
            return context as AppUrger
        }

    }

    val appComponent: AppComponent by lazy {
        initDagger()
    }

    private fun initDagger(): AppComponent =
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .retrofitApiModule(RetrofitApiModule())
            .build()

    override fun onCreate() {
        super.onCreate()
        instance = this
        Places.initialize(this, BuildConfig.API_KEY_PLACE)
        RxPaparazzo.register(this)
        Paper.init(applicationContext)
        FirebaseAnalytics.getInstance(this)
    }

}