package noty_team.com.urger.base.mvp.fragment

interface BaseContract {
	interface View

	interface Presenter {
		fun clearDisposable()
		fun onDestroy()
	}
}