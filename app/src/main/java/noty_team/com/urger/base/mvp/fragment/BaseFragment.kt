package noty_team.com.urger.base.mvp.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleRegistryOwner
import com.google.android.material.navigation.NavigationView
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.urger.base.mvp.DrawerListener
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.utils.cicerone.BackButtonListener
import noty_team.com.urger.utils.cicerone.RouterProvider
import noty_team.com.urger.utils.cicerone.Screens
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import javax.inject.Inject

abstract class BaseFragment<P : BasePresenter> : Fragment(), BackButtonListener {

    @Inject
    lateinit var baseContext: Context

    @Inject
    lateinit var baseActivity: BaseActivity

    @Inject
    lateinit var subscriptions: CompositeDisposable

    var drawerListener = DrawerListener.drawerListener

    protected var rootView: View? = null

    var isVisible: (fragment: Fragment) -> Boolean = { true }

    open var presenter: P? = null

    @LayoutRes
    protected abstract fun layout(): Int

    protected abstract fun initialization(view: View, isFirstInit: Boolean)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            (context as BaseActivity).fragmentComponent.inject(this as BaseFragment<BasePresenter>)
//            fragmentManager = context.fragmentManager
        } catch (e: UninitializedPropertyAccessException) {
            (context as BaseActivity).initFragmentComponent()
            context.fragmentComponent.inject(this as BaseFragment<BasePresenter>)
//            fragmentManager = context.fragmentManager
        }

        baseActivity = (context as BaseActivity)

        if (baseActivity is DrawerListener) {
            drawerListener = baseActivity as DrawerListener
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = if (layout() != 0)
            inflater.inflate(layout(), container, false)
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroy() {
        baseActivity.toggleKeyboard(false)
        presenter?.onDestroy()
        subscriptions.clear()
        presenter?.clearDisposable()
        super.onDestroy()
    }

    fun getNavigationView(): NavigationView? = baseActivity.getNavigationView()

    fun bottomBarIsVisible(visible: Boolean) {
        if (visible) {
            baseActivity.getBottomNavigationView()?.visibility = View.VISIBLE
        } else {
            baseActivity.getBottomNavigationView()?.visibility = View.GONE
        }
    }

    fun View.getClick(durationMillis: Long = 600, onClick: (view: View) -> Unit) {
        RxView.clicks(this)
            .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
            .subscribe({ _ ->
                onClick(this)
            }, {

            })
            .also { subscriptions.add(it) }
    }

    fun deleteDialog(textDeletedDialog: String, delete: () -> Unit) =
        baseActivity.deleteDialog(textDeletedDialog, delete)

    fun isShowLoadingDialog(isShow: Boolean) = baseActivity.isShowLoadingDialog(isShow)

    fun showShortToast(toasMassage: String) = baseActivity.showShortToast(toasMassage)

    fun showLongToast(toasMassage: String) = baseActivity.showLongToast(toasMassage)


    //цицерон бля
    override fun onBackPressed(): Boolean {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().exit()
        else baseActivity.exit()
        return true
    }

    fun getScreenRouter(): Router {
        return (parentFragment as RouterProvider).getRouter()
    }

    fun <P : BasePresenter> replaceFragment(fragment: BaseFragment<P>) {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().replaceScreen(
            Screens.FragmentScreen(
                fragment
            )
        )
        else baseActivity.replaceFragment(fragment)
    }

    fun <P : BasePresenter> addFragment(fragment: BaseFragment<P>) {

        val fm = baseActivity.supportFragmentManager
        val fragments = fm.fragments
        var isRouterFound = false
        fragments.forEach {
            if (it is RouterProvider) {
                isRouterFound = true
                it.getRouter().navigateTo(Screens.FragmentScreen(fragment))
                return
            }
        }
        if (!isRouterFound) baseActivity.addFragment(fragment)
    }

    fun <P : BasePresenter> newRootFragment(fragment: BaseFragment<P>) {
        if (parentFragment != null && parentFragment is RouterProvider) getScreenRouter().newRootScreen(
            Screens.FragmentScreen(
                fragment
            )
        )
        else baseActivity.newRootFragment(fragment)
    }

    //цицерон бля
    fun navigateTo(clazz: Class<*>) {
        val fm = baseActivity.supportFragmentManager
        val fragments = fm.fragments
        var isRouterFound = false
        fragments.forEach {


         val d = it.fragmentManager?.fragments

            d?.forEach {
                if (it.javaClass == clazz && it is RouterProvider) {
                    isRouterFound = true
                    it.getRouter().navigateTo(Screens.FragmentScreen(it as BaseFragment<BasePresenter>))
                    return
                }
            }
        }
        /*if (!isRouterFound) baseActivity.addFragment(fragment)*/
    }

}