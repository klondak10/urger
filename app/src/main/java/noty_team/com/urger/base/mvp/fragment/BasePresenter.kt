package noty_team.com.urger.base.mvp.fragment

import android.content.Context
import android.widget.Toast
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiAuthorization
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiEmployer
import noty_team.com.urger.base.AppUrger
import javax.inject.Inject

open class BasePresenter(val context: Context) : BaseContract.View, BaseContract.Presenter {

    @Inject
    lateinit var apiAuthorization: UrgerRetrofitApiAuthorization

    @Inject
    lateinit var apiEmployer: UrgerRetrofitApiEmployer

    @Inject
    lateinit var subscriptions: CompositeDisposable

    init {
        AppUrger[context].appComponent.inject(this)
    }

    override fun onDestroy() {
        subscriptions.clear()
    }

    override fun clearDisposable() {
        subscriptions.clear()
    }

    fun <T> Single<T>.callRequest() {
        this.subscribe({}, {

        }).also { subscriptions.add(it) }
    }

    fun Disposable.addToDisposables() {
        subscriptions.add(this)
    }

    fun <T> Single<T>.addRuntimeEnvironment(): Single<T> {
        return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    fun showLongToast(massage: String) {
        Toast.makeText(context, massage, Toast.LENGTH_LONG).show()
    }

    fun showShortToast(massage: String) {
        Toast.makeText(context, massage, Toast.LENGTH_SHORT).show()
    }
}