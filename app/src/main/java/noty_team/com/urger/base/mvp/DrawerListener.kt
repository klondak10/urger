package noty_team.com.urger.base.mvp

interface DrawerListener {
	fun onDrawerAvatarChanged()
	fun onDrawerBackgroundChanged()
	fun onDrawerUserInfoChanged()

	companion object {
		val drawerListener: DrawerListener = object : DrawerListener {
			override fun onDrawerAvatarChanged() {}
			override fun onDrawerBackgroundChanged() {}
			override fun onDrawerUserInfoChanged() {}
		}

	}
}