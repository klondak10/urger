package noty_team.com.urger.utils.api.response.vacancy.requests

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class AutoItem(

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("fuel")
	val fuel: String? = null,

	@field:SerializedName("model")
	val model: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("issued")
	val issued: String? = null
)