package noty_team.com.urger.utils.api.response.edit_executor

import android.util.Log
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class EditExecutorPersonalInfoResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse() {
	fun parseUserInfo() {
		if (data?.executor?.firstName != null && data?.executor?.lastName != null) {
			var userData = PaperIO.executorData
			userData?.firstName = data?.executor?.firstName
			userData?.lastName = data?.executor?.lastName
			userData?.email = data?.executor?.email
			userData?.position = data?.executor.position
			PaperIO.executorData = userData
		}

	}
}