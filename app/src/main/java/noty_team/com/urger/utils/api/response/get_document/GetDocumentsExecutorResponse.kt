package noty_team.com.urger.utils.api.response.get_document

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.BaseResponse

data class GetDocumentsExecutorResponse(
    @field:SerializedName("data")
    val data: List<Dat>? = null

) : BaseResponse() {
    fun map(): ArrayList<DocumentItem> {
        return data?.flatMap {
            arrayListOf(
                DocumentItem(
                    id = it.documentId?:-1,
                    image = it.preview?:"",
                    file = it.file?:"",
                    documentName = it.name?:""
                )
            )
        } as ArrayList<DocumentItem>
    }
}

data class Dat(
    @field:SerializedName("preview")
    val preview: String? = null,

    @field:SerializedName("file")
    val file: String? = null,

    @field:SerializedName("user_id")
    val ownerId: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val documentId: Int? = null
)