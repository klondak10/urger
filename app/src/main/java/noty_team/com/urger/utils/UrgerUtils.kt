package noty_team.com.urger.utils

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.ArrayRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import noty_team.com.urger.R
import noty_team.com.urger.base.AppUrger
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.util.*


fun getStringArray(@ArrayRes stringArray: Int, c: Context?): Array<out String> =
    c?.resources?.getStringArray(stringArray)!!

fun convertPixelsToDp(px: Float, context: Context?): Float {
    return if (context != null) {
        val resources = context.resources
        val metrics = resources.displayMetrics
        px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    } else {
        val metrics = Resources.getSystem().displayMetrics
        px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}

fun convertDpToPixel(dp: Float, context: Context?): Float {
    return if (context != null) {
        val resources = context.resources
        val metrics = resources.displayMetrics
        dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    } else {
        val metrics = Resources.getSystem().displayMetrics
        dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }
}

fun hideSoftKeyBoard(context: Context, view: View) {
    try {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    } catch (e: Exception) {
        // TODO: handle exception
        e.printStackTrace()
    }
}

fun createDrawableFromView(context: AppCompatActivity, viewId: Int): Bitmap {
    val view =
        (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
            viewId,
            null
        )
    val displayMetrics = DisplayMetrics()
    context.windowManager.defaultDisplay
        .getMetrics(displayMetrics)
    view.layoutParams = ViewGroup.LayoutParams(
        ViewGroup.LayoutParams.WRAP_CONTENT,
        ViewGroup.LayoutParams.WRAP_CONTENT
    )
    view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
    view.layout(
        0, 0, displayMetrics.widthPixels,
        displayMetrics.heightPixels
    )
    view.buildDrawingCache()
    val bitmap = Bitmap.createBitmap(
        view.measuredWidth,
        view.measuredHeight, Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    view.draw(canvas)
    return bitmap
}


fun setPhoto(imageView: ImageView, url: String?) {
    if (url != "" && url != null)
        Glide.with(imageView.context)
            .load(url)
             .apply(
                RequestOptions()
                    .placeholder(ContextCompat.getDrawable(imageView.context, R.drawable.ic_placeholder))
                    .override(720, 720)
                    .encodeQuality(100)
                    .fitCenter()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
            )
            .into(imageView)


}

fun waitUntilViewDrawn(view: View, onViewDrawn: () -> Unit) {
    view.viewTreeObserver.addOnGlobalLayoutListener(object :
        ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            onViewDrawn()
        }
    })


}

fun recyclerViewVertical(recyclerView: RecyclerView) {
    val layoutManager: RecyclerView.LayoutManager =
        LinearLayoutManager(recyclerView.context)
    recyclerView.layoutManager = layoutManager
}

fun bitmapToFile(context: Context, bitmap: Bitmap): File {
    // Get the context wrapper
    val wrapper = ContextWrapper(context)

    // Initialize a new file instance to saveWage bitmap object
    var file = wrapper.getDir("Images", Context.MODE_PRIVATE)
    file = File(file, "${UUID.randomUUID()}.jpg")

    try {
        // Compress the bitmap and saveWage in jpg format
        val stream: OutputStream = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        stream.flush()
        stream.close()
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return file
}

private fun getJson(data: HashMap<String, Any>): RequestBody {
    return RequestBody.create(
        "multipart/form-data; charset=utf-8".toMediaTypeOrNull(),
        (JSONObject(data as Map<*, *>?)).toString()
    )
}
