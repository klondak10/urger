package noty_team.com.urger.utils.api.response.create_vacancy

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItem


data class CreateVacancyResponse(

	@field:SerializedName("data")
	val data: VacancyItem? = null

): BaseResponse()