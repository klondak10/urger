package noty_team.com.urger.utils.api.response.statistic

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("vacancies_count")
	val vacanciesCount: Int? = null,

	@field:SerializedName("houses_count")
	val housesCount: Int? = null,

	@field:SerializedName("documents_count")
	val documentsCount: Int? = null
)