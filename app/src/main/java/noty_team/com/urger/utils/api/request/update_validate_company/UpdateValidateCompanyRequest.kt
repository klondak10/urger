package noty_team.com.urger.utils.api.request.update_validate_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class UpdateValidateCompanyRequest(
	val about: String? = null
)