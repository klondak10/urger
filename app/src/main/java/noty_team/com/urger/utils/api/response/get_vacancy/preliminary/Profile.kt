package noty_team.com.urger.utils.api.response.get_vacancy.preliminary

data class Profile(
	val userId: Int? = null,
	val id: Int? = null,
	val sallary: Int? = null
)
