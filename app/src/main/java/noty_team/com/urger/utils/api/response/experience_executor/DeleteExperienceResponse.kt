package noty_team.com.urger.utils.api.response.experience_executor

import com.google.gson.annotations.SerializedName

data class DeleteExperienceResponse(
	@field:SerializedName("status_message")
	val statusMessage: String? = null,
	@field:SerializedName("status_code")
	val statusCode: Int? = null,
	@field:SerializedName("data")
	val data: String? = null,
	@field:SerializedName("error")
	val error: String? = null
)
