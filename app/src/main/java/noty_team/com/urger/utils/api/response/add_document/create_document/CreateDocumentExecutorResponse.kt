package noty_team.com.urger.utils.api.response.add_document.create_document

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.BaseResponse

data class CreateDocumentExecutorResponse(
    @field:SerializedName("data")
    val data: Datta? = null

) : BaseResponse(){
    fun map(): DocumentItem {
        return DocumentItem(
            id = data?.documentId ?: -1,
            image = data?.preview ?: "",
            documentName = data?.name ?: "",
            file = data?.file ?: ""
        )
    }
}

data class Datta(
    @field:SerializedName("preview")
    val preview: String? = null,

    @field:SerializedName("file")
    val file: String? = null,

    @field:SerializedName("user_id")
    val userId: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val documentId: Int? = null

)