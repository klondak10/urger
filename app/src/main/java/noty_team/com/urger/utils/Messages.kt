package noty_team.com.urger.utils


import com.google.gson.annotations.SerializedName

data class Messages(
    @field: SerializedName("message")
    val messages: ArrayList<Message>? = null
)