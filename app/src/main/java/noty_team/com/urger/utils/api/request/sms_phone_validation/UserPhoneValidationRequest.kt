package noty_team.com.urger.utils.api.request.sms_phone_validation

data class UserPhoneValidationRequest(var phone: String? = null, var code:Int? = null)