package noty_team.com.urger.utils.api.response.statistic

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class StatisticResponse(
	@field:SerializedName("data")
	val data: Data? = null
):BaseResponse()