package noty_team.com.urger.utils.api.response.get_vacancy.preliminary

import android.util.Log
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItemLocation

data class PreliminaryVacancyResponse(
	val statusMessage: String? = null,
	val statusCode: Int? = null,
	val data: List<DataItem>? = null,
	val error: String? = null
) {
	fun mapLocation():ArrayList<VacancyItemLocation> {
		val filterList = data!!.filter { dataItem -> dataItem.vacancy?.status==0 }
		return filterList.flatMap { vacancy ->
			arrayListOf(VacancyItemLocation(
				idVacancy = vacancy.id ?: 0,
				phone = vacancy.user?.phone ?: "",
				lastName = vacancy.user?.lastName ?: "",
				id = vacancy.user?.id ?: 0,
				avatar = vacancy.user?.avatar,
				firstName = vacancy.user?.firstName,
				lat = vacancy.vacancy?.lat,
				lng = vacancy.vacancy?.lng,
				auto = vacancy.auto,
				categoryName = vacancy.vacancy?.categoryName,
				parentCategory = vacancy.vacancy?.parentCategory,
				rate = vacancy.rate
			))

		} as ArrayList
	}
}
