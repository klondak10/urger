package noty_team.com.urger.utils.api.response.nip

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class GetCompanyNameByNipResponse(
		@field:SerializedName("data")
		val data: DataCompanyName? = null) : BaseResponse()