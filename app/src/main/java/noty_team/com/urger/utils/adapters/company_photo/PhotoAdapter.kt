package noty_team.com.urger.utils.adapters.company_photo

import android.view.View
import kotlinx.android.synthetic.main.item_company_photo.*
import noty_team.com.urger.R
import noty_team.com.urger.ui.adapters.recycler.BaseAdapter
import noty_team.com.urger.utils.setPhoto

class PhotoAdapter(
		list: ArrayList<CompanyPhotoItem>,
		val onClick: (indexPhoto: Int) -> Unit = {},
		val onLongClick: (companyItem: CompanyPhotoItem, indexPhoto: Int) -> Unit = { _cpi, _index -> }
) :
		BaseAdapter<CompanyPhotoItem, PhotoAdapter.ViewHolder>(list) {


	override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

	override fun getItemViewType(position: Int) = R.layout.item_company_photo

	inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

		init {
			itemView.setOnClickListener {
				onClick(position)
			}
			itemView.setOnLongClickListener {

				onLongClick(list[position], position)

				return@setOnLongClickListener true
			}
		}

		override fun bind(pos: Int) {
			val item = list[position]
			setPhoto(company_photo_item, item.image)
		}
	}

	fun getItemsList() = list
}