package noty_team.com.urger.utils.api.response.create_executor_user

import com.google.gson.annotations.SerializedName


data class DataExecutor(
    @field:SerializedName("access_token")
    val accessToken: String? = null,

    @field:SerializedName("refresh_token")
    val refreshToken: String? = null,

    @field:SerializedName("executor")
    val executor: Executor? = null
)