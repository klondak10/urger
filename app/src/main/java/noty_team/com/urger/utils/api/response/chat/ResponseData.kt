package noty_team.com.urger.utils.api.response.chat

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResponseData(

	@field:SerializedName("translatedText")
	val translatedText: String? = null,

	@field:SerializedName("match")
	val match: Double? = null
)