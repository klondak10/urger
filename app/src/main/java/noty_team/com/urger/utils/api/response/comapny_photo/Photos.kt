package noty_team.com.urger.utils.api.response.comapny_photo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Photos(

		@field:SerializedName("photos")
		val photos: List<String>? = null
)