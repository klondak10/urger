package noty_team.com.urger.utils.api.response.get_all_house

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("cost_of_rent")
	val costOfRent: Int? = null,

	@field:SerializedName("id")
	val houseId: Int? = null,

	@field:SerializedName("rooms")
	val rooms: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("validated_company_id")
	val validatedCompanyId: Int? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("address_place_id")
	val addressPlaceId: String? = null,

	@field:SerializedName("available")
	val available: Boolean? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("currency_of_rent")
	val currencyOfRent: String? = null,

	@field:SerializedName("photos")
	val photos: List<String?>? = null,

	@field:SerializedName("lat")
	val lat: String? = null
)