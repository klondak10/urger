package noty_team.com.urger.utils;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import noty_team.com.urger.utils.adapters.BaseUiPaginationItem;

public class Room extends BaseUiPaginationItem implements Parcelable {


    @SerializedName("room_id")
    @Expose
    public int roomId;
    @SerializedName("userId")
    @Expose
    public int userId;
    @SerializedName("text")
    @Expose
    public String text;
    @SerializedName("newmess")
    @Expose
    public int newMess;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("avatar")
    @Expose
    public String avatar;


    protected Room(Parcel in) {
        text = in.readString();
        roomId = in.readInt();
        name = in.readString();
        newMess = in.readInt();
        avatar = in.readString();
        date = in.readString();
        status = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(text);
        dest.writeInt(roomId);
        dest.writeString(name);
        dest.writeInt(newMess);
        dest.writeString(avatar);
        dest.writeString(date);
        dest.writeInt(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Room> CREATOR = new Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel in) {
            return new Room(in);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return roomId == room.roomId &&
                newMess == room.newMess &&
                status == room.status &&
                Objects.equals(text, room.text) &&
                Objects.equals(name, room.name) &&
                Objects.equals(avatar, room.avatar) &&
                Objects.equals(date, room.date);
    }

    @Override
    public int hashCode() {

        return Objects.hash(text, roomId,name, newMess, avatar, date, status);
    }
}
