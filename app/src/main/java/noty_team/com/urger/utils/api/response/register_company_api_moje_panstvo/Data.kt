package noty_team.com.urger.utils.api.response.register_company_api_moje_panstvo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("krs_podmioty.person_id")
	val krsPodmiotyPersonId: List<String?>? = null,

	@field:SerializedName("krs_podmioty.email")
	val krsPodmiotyEmail: String? = null,

	@field:SerializedName("krs_podmioty.adres_kraj")
	val krsPodmiotyAdresKraj: String? = null,

	@field:SerializedName("krs_podmioty.knf_ostrzezenie_id")
	val krsPodmiotyKnfOstrzezenieId: String? = null,

	@field:SerializedName("krs_podmioty.liczba_zmian")
	val krsPodmiotyLiczbaZmian: String? = null,

	@field:SerializedName("krs_podmioty.nip")
	val krsPodmiotyNip: String? = null,

	@field:SerializedName("krs_podmioty.liczba_nadzorcow")
	val krsPodmiotyLiczbaNadzorcow: String? = null,

	@field:SerializedName("krs_podmioty.cel_dzialania")
	val krsPodmiotyCelDzialania: Any? = null,

	@field:SerializedName("krs_podmioty.www")
	val krsPodmiotyWww: String? = null,

	@field:SerializedName("krs_podmioty.data_uprawomocnienia_wykreslenia")
	val krsPodmiotyDataUprawomocnieniaWykreslenia: Any? = null,

	@field:SerializedName("krs_podmioty.sposob_reprezentacji")
	val krsPodmiotySposobReprezentacji: String? = null,

	@field:SerializedName("krs_podmioty.wartosc_kapital_zakladowy")
	val krsPodmiotyWartoscKapitalZakladowy: Int? = null,

	@field:SerializedName("krs_podmioty.nazwa_skrocona")
	val krsPodmiotyNazwaSkrocona: String? = null,

	@field:SerializedName("krs_podmioty.adres_poczta")
	val krsPodmiotyAdresPoczta: String? = null,

	@field:SerializedName("krs_podmioty.data_dokonania_wpisu")
	val krsPodmiotyDataDokonaniaWpisu: String? = null,

	@field:SerializedName("krs_podmioty.wojewodztwo_id")
	val krsPodmiotyWojewodztwoId: String? = null,

	@field:SerializedName("krs_podmioty.id")
	val krsPodmiotyId: String? = null,

	@field:SerializedName("krs_podmioty.powiat_id")
	val krsPodmiotyPowiatId: String? = null,

	@field:SerializedName("krs_podmioty.dotacje_ue_beneficjent_id")
	val krsPodmiotyDotacjeUeBeneficjentId: String? = null,

	@field:SerializedName("krs_podmioty.gmina_id")
	val krsPodmiotyGminaId: String? = null,

	@field:SerializedName("krs_podmioty.gpw")
	val krsPodmiotyGpw: Boolean? = null,

	@field:SerializedName("krs_podmioty.regon")
	val krsPodmiotyRegon: String? = null,

	@field:SerializedName("krs_podmioty.opp")
	val krsPodmiotyOpp: String? = null,

	@field:SerializedName("krs_podmioty.miejscowosc_id")
	val krsPodmiotyMiejscowoscId: String? = null,

	@field:SerializedName("krs_podmioty.data_zawieszenia_dzialanosci")
	val krsPodmiotyDataZawieszeniaDzialanosci: Any? = null,

	@field:SerializedName("krs_podmioty.nazwa")
	val krsPodmiotyNazwa: String? = null,

	@field:SerializedName("krs_podmioty.adres_kod_pocztowy")
	val krsPodmiotyAdresKodPocztowy: String? = null,

	@field:SerializedName("krs_podmioty.data_rejestracji")
	val krsPodmiotyDataRejestracji: String? = null,

	@field:SerializedName("krs_podmioty.liczba_jedynych_akcjonariuszy")
	val krsPodmiotyLiczbaJedynychAkcjonariuszy: String? = null,

	@field:SerializedName("krs_podmioty.file_id")
	val krsPodmiotyFileId: String? = null,

	@field:SerializedName("krs_podmioty.wykreslony")
	val krsPodmiotyWykreslony: String? = null,

	@field:SerializedName("krs_podmioty.data_rejestracji_przedsiebiorcy")
	val krsPodmiotyDataRejestracjiPrzedsiebiorcy: String? = null,

	@field:SerializedName("krs_podmioty.liczba_akcji_wszystkich_emisji")
	val krsPodmiotyLiczbaAkcjiWszystkichEmisji: String? = null,

	@field:SerializedName("krs_podmioty.wartosc_nominalna_akcji")
	val krsPodmiotyWartoscNominalnaAkcji: Int? = null,

	@field:SerializedName("krs_podmioty.forma_prawna_str")
	val krsPodmiotyFormaPrawnaStr: String? = null,

	@field:SerializedName("krs_podmioty.gpw_spolka_id")
	val krsPodmiotyGpwSpolkaId: String? = null,

	@field:SerializedName("krs_podmioty.liczba_oddzialow")
	val krsPodmiotyLiczbaOddzialow: String? = null,

	@field:SerializedName("krs_podmioty.data_wykreslenia_stowarzyszenia")
	val krsPodmiotyDataWykresleniaStowarzyszenia: Any? = null,

	@field:SerializedName("krs_podmioty.liczba_wspolnikow")
	val krsPodmiotyLiczbaWspolnikow: String? = null,

	@field:SerializedName("krs_podmioty.sygnatura_akt")
	val krsPodmiotySygnaturaAkt: String? = null,

	@field:SerializedName("krs_podmioty.data_wpisu_opp")
	val krsPodmiotyDataWpisuOpp: Any? = null,

	@field:SerializedName("krs_podmioty.forma_prawna_typ_id")
	val krsPodmiotyFormaPrawnaTypId: String? = null,

	@field:SerializedName("krs_podmioty.wartosc_kapital_docelowy")
	val krsPodmiotyWartoscKapitalDocelowy: Int? = null,

	@field:SerializedName("krs_podmioty.liczba_zmian_umow")
	val krsPodmiotyLiczbaZmianUmow: String? = null,

	@field:SerializedName("krs_podmioty.data_wyrejestrowania_przedsiebiorcy")
	val krsPodmiotyDataWyrejestrowaniaPrzedsiebiorcy: Any? = null,

	@field:SerializedName("krs_podmioty.akcept")
	val krsPodmiotyAkcept: String? = null,

	@field:SerializedName("krs_podmioty.nazwa_organu_reprezentacji")
	val krsPodmiotyNazwaOrganuReprezentacji: String? = null,

	@field:SerializedName("krs_podmioty.adres_miejscowosc")
	val krsPodmiotyAdresMiejscowosc: String? = null,

	@field:SerializedName("krs_podmioty.wczesniejsza_rejestracja_str")
	val krsPodmiotyWczesniejszaRejestracjaStr: String? = null,

	@field:SerializedName("krs_podmioty.rejestr")
	val krsPodmiotyRejestr: String? = null,

	@field:SerializedName("krs_podmioty.wartosc_czesc_kapitalu_wplaconego")
	val krsPodmiotyWartoscCzescKapitaluWplaconego: Int? = null,

	@field:SerializedName("krs_podmioty.rejestr_stowarzyszen")
	val krsPodmiotyRejestrStowarzyszen: String? = null,

	@field:SerializedName("krs_podmioty.adres")
	val krsPodmiotyAdres: String? = null,

	@field:SerializedName("krs_podmioty.empty")
	val krsPodmiotyEmpty: String? = null,

	@field:SerializedName("krs_podmioty.deleted_on_request")
	val krsPodmiotyDeletedOnRequest: String? = null,

	@field:SerializedName("krs_podmioty.adres_ulica")
	val krsPodmiotyAdresUlica: String? = null,

	@field:SerializedName("krs_podmioty.data_rejestracji_stowarzyszenia")
	val krsPodmiotyDataRejestracjiStowarzyszenia: Any? = null,

	@field:SerializedName("krs_podmioty.nieprzedsiebiorca")
	val krsPodmiotyNieprzedsiebiorca: String? = null,

	@field:SerializedName("krs_podmioty.oznaczenie_sadu")
	val krsPodmiotyOznaczenieSadu: String? = null,

	@field:SerializedName("krs_podmioty.kod_pocztowy_id")
	val krsPodmiotyKodPocztowyId: String? = null,

	@field:SerializedName("krs_podmioty.nazwa_organu_nadzoru")
	val krsPodmiotyNazwaOrganuNadzoru: String? = null,

	@field:SerializedName("krs_podmioty.siedziba")
	val krsPodmiotySiedziba: String? = null,

	@field:SerializedName("krs_podmioty.liczba_reprezentantow")
	val krsPodmiotyLiczbaReprezentantow: String? = null,

	@field:SerializedName("krs_podmioty.data_sprawdzenia")
	val krsPodmiotyDataSprawdzenia: String? = null,

	@field:SerializedName("krs_podmioty.ostatni_wpis_id")
	val krsPodmiotyOstatniWpisId: String? = null,

	@field:SerializedName("krs_podmioty.adres_numer")
	val krsPodmiotyAdresNumer: String? = null,

	@field:SerializedName("krs_podmioty.liczba_czlonkow_komitetu_zal")
	val krsPodmiotyLiczbaCzlonkowKomitetuZal: String? = null,

	@field:SerializedName("krs_podmioty.liczba_emisji_akcji")
	val krsPodmiotyLiczbaEmisjiAkcji: String? = null,

	@field:SerializedName("krs_podmioty.krs")
	val krsPodmiotyKrs: String? = null,

	@field:SerializedName("krs_podmioty.numer_wpisu")
	val krsPodmiotyNumerWpisu: String? = null,

	@field:SerializedName("krs_podmioty.data_ostatni_wpis")
	val krsPodmiotyDataOstatniWpis: String? = null,

	@field:SerializedName("krs_podmioty.liczba_prokurentow")
	val krsPodmiotyLiczbaProkurentow: String? = null,

	@field:SerializedName("krs_podmioty.forma_prawna_id")
	val krsPodmiotyFormaPrawnaId: String? = null,

	@field:SerializedName("krs_podmioty.liczba_dzialalnosci")
	val krsPodmiotyLiczbaDzialalnosci: String? = null,

	@field:SerializedName("krs_podmioty.adres_lokal")
	val krsPodmiotyAdresLokal: String? = null,

	@field:SerializedName("krs_podmioty.twitter_account_id")
	val krsPodmiotyTwitterAccountId: String? = null,

	@field:SerializedName("krs_podmioty.firma")
	val krsPodmiotyFirma: String? = null,

	@field:SerializedName("krs_podmioty.data_wznowienia_dzialanosci")
	val krsPodmiotyDataWznowieniaDzialanosci: Any? = null,

	@field:SerializedName("krs_podmioty.umowa_spolki_cywilnej")
	val krsPodmiotyUmowaSpolkiCywilnej: String? = null,

	@field:SerializedName("krs_podmioty.wartosc_nominalna_podwyzszenia_kapitalu")
	val krsPodmiotyWartoscNominalnaPodwyzszeniaKapitalu: Int? = null
)