package noty_team.com.urger.utils.api.response.get_executor

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(
		@field:SerializedName("executor")
		val executor: Executor? = null,

		@field:SerializedName("rate")
		val rate: Float? = null
)