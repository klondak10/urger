package noty_team.com.urger.utils.api.response.get_employer

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("validated_company")
	val validatedCompany: ValidatedCompany? = null,

	@field:SerializedName("employer")
	val employer: Employer? = null,

	@field:SerializedName("rate")
	val rate: Float? = null
)