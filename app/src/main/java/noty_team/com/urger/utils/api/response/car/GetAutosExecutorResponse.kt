package noty_team.com.urger.utils.api.response.car

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class GetAutosExecutorResponse(

    @field:SerializedName("status_message")
	val statusMessage: String? = null,

    @field:SerializedName("status_code")
	val statusCode: Int? = null,

    @field:SerializedName("data")
	val data: List<DataItemGetCars>? = null,

    @field:SerializedName("error")
	val error: String? = null
)