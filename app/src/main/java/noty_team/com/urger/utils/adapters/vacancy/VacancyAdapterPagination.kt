package noty_team.com.urger.utils.adapters.vacancy

import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import kotlinx.android.synthetic.main.item_vacancy.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination

class VacancyAdapterPagination(
    list: ArrayList<VacancyPaginationItem>,
    recyclerView: RecyclerView,
    var onLoadData: (offset: Int) -> Unit = {},
    var onClick: (data: VacancyPaginationItem) -> Unit = {},
    var onLongClick: (data: VacancyPaginationItem) -> Unit = {},
    var callbackChatEmpty:(Boolean) -> Unit = {}
) : BaseAdapterPagination<VacancyPaginationItem>(list, recyclerView), Filterable {

	var filterList:ArrayList<VacancyPaginationItem> = list

	override fun getFilter(): Filter {

		return object : Filter() {

			override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
				if(filterList.isEmpty()) {
					callbackChatEmpty(false)
				} else {
					callbackChatEmpty(true)
				}
				filterList = results!!.values as ArrayList<VacancyPaginationItem>
				notifyDataSetChanged()
			}

			override fun performFiltering(constraint: CharSequence?): FilterResults {

				val queryString = constraint?.toString()!!.toLowerCase()
				val filterResults = FilterResults()
				filterResults.values = if (queryString==null || queryString.isEmpty())
					list
				else
					list.filter {
						it.category.toLowerCase().contains(queryString) || it.parent_category.toLowerCase().contains(queryString)
					}
				return filterResults
			}

		}
	}

	override fun getItemCount()= filterList.size

	override val itemLayoutResourceId = R.layout.item_vacancy

	override fun onInit(pos: Int, viewHolder: MainViewHolder) {

		viewHolder.apply {
			try {
				main_container_vacancy.setOnClickListener {
					onClick(list[adapterPosition])
				}
				main_container_vacancy.setOnLongClickListener {
					onLongClick(list[adapterPosition])
					return@setOnLongClickListener true
				}
			} catch (e: Exception) {}
		}
	}

	override fun onBind(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			val spannable = SpannableString(list[pos].category + ", " + list[pos].parent_category)
			spannable.setSpan(
				StyleSpan(Typeface.BOLD),
				0, list[pos].category.length,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
			)


			vacancy_company_name_position.text = spannable
			vacancy_location.text = list[pos].address
			vacancy_type_employment.text = list[pos].workSchedule
			vacancy_price.text = "${list[pos].price} ${list[pos].currencySymbol}"
			vacancy_type_employment.text = list[pos].workSchedule
			existHouse.visibility = if(list[pos].houseId != null) View.VISIBLE else View.GONE
		}
	}

	override fun onDataLoaded(listSize: Int) {
		onLoadData(listSize)
	}

	fun deleteVacancy(id: Int): Boolean {
		var indexToDelete = -1
		try {
			list.forEachIndexed { index, item ->
				if (item.vacancyId == id){
					indexToDelete = index
					return@forEachIndexed
				}
			}
			return if (indexToDelete >= 0) {
				list.removeAt(indexToDelete)
				notifyItemRemoved(indexToDelete)
				true
			} else false
		}catch (e: java.lang.Exception){ return false}
	}

	fun editItem(data: VacancyPaginationItem){
		list.forEachIndexed { index, item ->
			if (item.vacancyId == data.vacancyId){
				list[index] = data
				notifyItemChanged(index)
				return@forEachIndexed
			}
		}
	}
}
