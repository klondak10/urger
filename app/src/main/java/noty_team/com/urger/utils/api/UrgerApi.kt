package noty_team.com.urger.utils.api

import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.BuildConfig
import noty_team.com.urger.api.request_interfaces.AuthorizationApi
import noty_team.com.urger.api.request_interfaces.UserProfileApi
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.net.Socket
import java.security.SecureRandom
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


object UrgerApi {


	val retrofit: Retrofit by lazy {
		create()
	}

	fun create(): Retrofit {
		val naiveTrustManager = object : X509TrustManager {
			override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
			override fun checkClientTrusted(certs: Array<X509Certificate>, authType: String) = Unit
			override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) = Unit
		}

		val insecureSocketFactory = SSLContext.getInstance("TLSv1.2").apply {
			val trustAllCerts = arrayOf<TrustManager>(naiveTrustManager)
			init(null, trustAllCerts, SecureRandom())
		}.socketFactory

		val interceptor = HttpLoggingInterceptor()
		interceptor.level = HttpLoggingInterceptor.Level.BODY
		val client = OkHttpClient.Builder()
		client.readTimeout(60, TimeUnit.SECONDS)
		client.writeTimeout(60, TimeUnit.SECONDS)
		client.connectTimeout(60, TimeUnit.SECONDS)
		client.addInterceptor(interceptor)
		client.sslSocketFactory(insecureSocketFactory, naiveTrustManager)
		client.hostnameVerifier { hostname, session -> true }
		client.addInterceptor { chain ->
			val request = chain.request()
			val newRequest: Request

			val headers = request.newBuilder()

			headers.addHeader("Content-type", "application/json")
			headers.addHeader("Authorization", "Bearer " + PaperIO.accessToken)

			newRequest = headers.build()

			chain.proceed(newRequest)
		}

		return Retrofit.Builder()
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create())
				.client(client.build())
				.baseUrl(BuildConfig.API_END_POINT)
				.build()
	}

	val AUTH_API: AuthorizationApi by lazy {
		retrofit.create(AuthorizationApi::class.java)
	}

	val USER_PROFILE_API: UserProfileApi by lazy {
		retrofit.create(UserProfileApi::class.java)
	}
}