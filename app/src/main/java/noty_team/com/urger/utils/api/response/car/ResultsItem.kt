package noty_team.com.urger.utils.api.response.car

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ResultsItem(

	@field:SerializedName("Make_ID")
	val makeID: Int? = null,

	@field:SerializedName("Make_Name")
	val makeName: String? = null
)