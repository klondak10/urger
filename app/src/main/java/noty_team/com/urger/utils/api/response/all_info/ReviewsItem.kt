package noty_team.com.urger.utils.api.response.all_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ReviewsItem(

	@field:SerializedName("employer_date")
	val employerDate: String? = null,

	@field:SerializedName("employer_rate")
	val employerRate: Int? = null,

	@field:SerializedName("employer_review")
	val employerReview: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null

)