package noty_team.com.urger.utils.api.response.edit_executor

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("user")
	val executor: User? = null
)