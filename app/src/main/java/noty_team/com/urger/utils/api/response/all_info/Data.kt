package noty_team.com.urger.utils.api.response.all_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("reviews")
	val reviews: List<ReviewsItem?>? = null,

	@field:SerializedName("rate")
	val rate: String? = null,

	@field:SerializedName("documents")
	val documents: List<DocumentsItem?>? = null,

	@field:SerializedName("executor")
	val executor: Executor? = null,

	@field:SerializedName("autos")
	val autos: List<AutosItem?>? = null,

	@field:SerializedName("expiriences")
	val expiriences: List<ExpiriencesItem?>? = null,

	@field:SerializedName("portfolios")
	val portfolios: List<PortfoliosItem>? = null
)