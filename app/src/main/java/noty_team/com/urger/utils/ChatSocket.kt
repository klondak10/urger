package noty_team.com.urger.utils


import android.util.Log
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.Polling
import kotlinx.android.synthetic.main.main_content.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import okhttp3.OkHttpClient
import org.json.JSONObject
import java.net.URISyntaxException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object ChatSocket {

    private var chat: Socket? = null

    fun provideSocketChat(): Socket {
        if (chat != null) {
            return chat as Socket
        }

        val myHostnameVerifier = HostnameVerifier { _, _ ->
            return@HostnameVerifier true
        }

        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager
        {
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}

            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }
        })

        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, trustAllCerts, null)

        val okHttpClient = OkHttpClient.Builder()
            .hostnameVerifier(myHostnameVerifier)
            .sslSocketFactory(sslContext.socketFactory, trustAllCerts[0] as X509TrustManager)
            .connectTimeout(20, TimeUnit.SECONDS)
            .writeTimeout(1, TimeUnit.MINUTES)
            .readTimeout(1, TimeUnit.MINUTES)
            .build()

        val options = IO.Options()
        options.transports = arrayOf("polling")
        options.reconnectionDelay = 60000
        options.reconnection = true
        options.callFactory = okHttpClient
        options.webSocketFactory = okHttpClient

        try {
            val userId: Int = if(PaperIO.userRole == UserRole.COMPANY) {
                PaperIO.employerData!!.userId!!
            } else {
                PaperIO.executorData!!.userId!!
            }
            chat = IO.socket("https://urger.eu?userId=${userId}",options)

        } catch (e: URISyntaxException) {
            e.printStackTrace()
        }
        chat!!.connect()
        return chat as Socket
    }
}
