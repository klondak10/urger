package noty_team.com.urger.utils.api.request.refresh_token_request

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class RefreshTokenRequest(

    @field:SerializedName("refresh_token")
    val refreshToken: String? = null

)