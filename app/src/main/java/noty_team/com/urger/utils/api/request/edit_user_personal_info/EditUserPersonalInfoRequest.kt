package noty_team.com.urger.utils.api.request.edit_user_personal_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import okhttp3.RequestBody

@Generated("com.robohorse.robopojogenerator")
data class EditUserPersonalInfoRequest(

		@field:SerializedName("first_name")
		val firstName: String? = null,

		@field:SerializedName("last_name")
		val lastName: String? = null,

		@field:SerializedName("email")
		val email: String? = null,

		@field:SerializedName("category_id")
		val categoryId: Array<Int>? = null,

		@field:SerializedName("position")
		val position: String? = null

)