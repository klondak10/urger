package noty_team.com.urger.utils.api.response.register_company_api_moje_panstvo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CompanyNameResponce(

	@field:SerializedName("Took")
	val took: Int? = null,

	@field:SerializedName("Links")
	val links: Links? = null,

	@field:SerializedName("Count")
	val count: Int? = null,

	@field:SerializedName("Dataobject")
	val dataobject: List<DataobjectItem?>? = null
)