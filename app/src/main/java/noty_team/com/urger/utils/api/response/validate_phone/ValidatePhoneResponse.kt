package noty_team.com.urger.utils.api.response.validate_phone

import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.sms_checked.DataItem

data class ValidatePhoneResponse(
    @field:SerializedName("data")
    var data: String? = null
) : BaseResponse()