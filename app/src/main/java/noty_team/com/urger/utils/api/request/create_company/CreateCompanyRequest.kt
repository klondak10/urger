package noty_team.com.urger.utils.api.request.create_company

data class CreateCompanyRequest(
    var phone: String? = null, var first_name: String? = null,
    var last_name: String? = null, var nip: String? = null, var email: String? = null,
    var company_name: String? = null, var address: String? = null
)