package noty_team.com.urger.utils.api.response

import com.google.gson.annotations.SerializedName

open class BaseResponse(
    @field:SerializedName("status_code")
    var statusCode: Int = 0,
    @field:SerializedName("status_massage")
    var statusMassage: String = "",
    @field:SerializedName("error")
    var error: String = ""
)