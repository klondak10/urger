package noty_team.com.urger.utils.api.response.currency

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class CurrencyItem(

    @field:SerializedName("currency_number")
    val currencyNumber: Int? = null,

    @field:SerializedName("id")
    val currencyId: Int? = null,

    @field:SerializedName("currency_code")
    val currencyCode: String? = null,

    @field:SerializedName("currency_symbol")
    val currencySymbol: String? = null
)