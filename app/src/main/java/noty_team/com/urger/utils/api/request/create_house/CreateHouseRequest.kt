package noty_team.com.urger.utils.api.request.create_house

import okhttp3.RequestBody

data class CreateHouseRequest(var address: String = "", var rooms: Int = 0, var cost_of_rent: Int = 0,
							  var currency_of_rent: Int = -1, var address_place_id: String = "",
							  var lat: String = "", var lng: String = "", var available: Boolean = false)