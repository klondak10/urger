package noty_team.com.urger.utils.api.response.currency

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CurrencyResponse(

		@field:SerializedName("data")
		val data: ArrayList<CurrencyItem> = arrayListOf()
) : BaseResponse() {
	fun mapToList(): ArrayList<String> {
		var listCurrency = ArrayList<String>()

		data?.forEach {
			listCurrency.add(it?.currencyCode ?: "")
		}


		return listCurrency
	}

	fun mapToHashMap(): HashMap<Int, String> {
		var listCurrency = HashMap<Int, String>()

		data?.forEach {
			if (it?.currencyId != null)
				listCurrency.put(it.currencyId, it.currencyCode ?: "")
		}

		return listCurrency
	}
}