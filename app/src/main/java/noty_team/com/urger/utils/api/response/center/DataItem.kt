package noty_team.com.urger.utils.api.response.center

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("schedule")
	val schedule: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("lon")
	val lon: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("services")
	val services: ArrayList<Services>? = null

)
data class Services(
	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("image")
	val image: String? = null
): BaseUiPaginationItem()