package noty_team.com.urger.utils.api.response.load_background_response

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

@Generated("com.robohorse.robopojogenerator")
data class LoadBackgroundResponse(

		@field:SerializedName("data")
		val data: BackgroundData? = null

) : BaseResponse() {
	fun parseImageUrl() {
		if (data?.image != null) {
			if(PaperIO.userRole == UserRole.COMPANY) {
				if (PaperIO.employerData != null) {
					val user = PaperIO.employerData
					user?.background = data.image

					PaperIO.employerData = user
				} else {

				}
			} else {
				if (PaperIO.executorData != null) {
					val user = PaperIO.executorData
					user?.background = data.image

					PaperIO.executorData = user
				}
			}
		}
	}
}