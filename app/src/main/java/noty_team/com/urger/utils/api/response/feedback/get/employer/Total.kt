package noty_team.com.urger.utils.api.response.feedback.get.employer

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Total(

	@field:SerializedName("rate")
	val rate: Float? = null,

	@field:SerializedName("count")
	val count: Int? = null
)