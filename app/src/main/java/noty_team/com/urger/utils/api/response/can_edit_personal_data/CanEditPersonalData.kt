package noty_team.com.urger.utils.api.response.can_edit_personal_data

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CanEditPersonalData(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse()