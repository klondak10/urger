package noty_team.com.urger.utils.api.response.delete_photo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class DeletePhotoResponse(

		@field:SerializedName("data")
		val data: String? = null

) : BaseResponse()