package noty_team.com.urger.utils.api.response.vacancy.detailExecutor

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

data class VacancyResponseDetail(
    @field:SerializedName("data")
    var data: VacancyItemDetail

) : BaseResponse()