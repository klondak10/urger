package noty_team.com.urger.utils.api.response.create_house

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

		@field:SerializedName("cost_of_rent")
		val costOfRent: Int? = null,

		@field:SerializedName("id")
		val houseId: Int? = null,

		@field:SerializedName("rooms")
		val rooms: Int? = null,

		@field:SerializedName("address")
		val address: String? = null,

		@field:SerializedName("validated_company_id")
		val validatedCompanyId: String? = null,

		@field:SerializedName("city_place_id")
		val cityPlaceId: String? = null,

		@field:SerializedName("address_place_id")
		val addressPlaceId: String? = null,

		@field:SerializedName("available")
		val available: Boolean? = null,

		@field:SerializedName("country_place_id")
		val countryPlaceId: String? = null,

		@field:SerializedName("currency_of_rent")
		val currencyOfRent: String? = null,

		@field:SerializedName("description")
		val description: String? = null,

		@field:SerializedName("photos")
		val photos: List<Any?>? = null,

		@field:SerializedName("lat")
		val lat: String? = null,

		@field:SerializedName("lng")
		val lng: String? = null
)