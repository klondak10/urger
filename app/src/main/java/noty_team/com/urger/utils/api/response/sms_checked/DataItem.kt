package noty_team.com.urger.utils.api.response.sms_checked

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class DataItem(
        @field:SerializedName("access_token")
        val accessToken: String? = null,

        @field:SerializedName("refresh_token")
        val refreshToken: String? = null,

        @field:SerializedName("sign_up")
        val signUp: Boolean? = null,

        @field:SerializedName("user")
        val user: UserItem? = null

)