package noty_team.com.urger.utils.api.response.update_validate_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class UpdateValidateCompanyResponse(
		@field:SerializedName("data")
		val data: Data? = null
) : BaseResponse() {
	fun updateDescription(position: String) {

		var company = PaperIO.companyData
		company?.description = data?.about ?: ""
		company?.position = position

		PaperIO.companyData = company
	}
}