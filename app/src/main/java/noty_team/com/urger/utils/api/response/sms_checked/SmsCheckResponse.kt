package noty_team.com.urger.utils.api.response.sms_checked

import android.util.Log
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.BaseResponse

class SmsCheckResponse(
		@field:SerializedName("data")
		var data: DataItem? = null
) : BaseResponse() {

	fun parseToken() {
		PaperIO.refreshToken = data?.refreshToken ?: ""
		PaperIO.accessToken = data?.accessToken ?: ""
	}

	fun parseUserData() {
		Log.i("TEST", "parse sms response")
		PaperIO.employerData = UserUrgerData(
				data?.user?.userId,
				data?.user?.phone,
				data?.user?.firstName,
				data?.user?.lastName,
				data?.user?.email,
				data?.user?.type,
				data?.user?.avatar,
				data?.user?.background
		)

		if (data?.user?.type == "1") {
			PaperIO.userRole = UserRole.COMPANY
		} else {
			PaperIO.userRole = UserRole.EXECUTOR
		}
	}

	fun canUpdateProfile() {
		if (data?.user?.canUpdate != null) {
			PaperIO.canEditProfile = data?.user?.canUpdate ?: false
		}else{
			PaperIO.canEditProfile = data?.user?.canUpdate ?: false
		}
	}

	fun isNeedSignUp() {
		if (data?.signUp != false) {
			PaperIO.signUp = Authorization.NEED_SING_UP
		}else if (data?.signUp == true){
			PaperIO.signUp = Authorization.ALREADY_LOGIN
		}
	}
}

