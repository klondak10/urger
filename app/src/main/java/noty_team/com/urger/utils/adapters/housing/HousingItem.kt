package noty_team.com.urger.utils.adapters.housing

import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class HousingItem(var houseId: Int = -1,
                       var preview: String = "",
                       var description: String ="",
                       var address: String = "") :
    BaseUiPaginationItem()