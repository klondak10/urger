package noty_team.com.urger.utils.api.response.wage_type

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class WageTypeResponse(

        @field:SerializedName("data")
		val data: ArrayList<WageItem> = arrayListOf()

) : BaseResponse()