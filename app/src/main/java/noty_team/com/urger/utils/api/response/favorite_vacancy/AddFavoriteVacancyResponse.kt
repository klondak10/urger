package noty_team.com.urger.utils.api.response.favorite_vacancy

data class AddFavoriteVacancyResponse(
	val statusMessage: String? = null,
	val statusCode: Int? = null,
	val data: Data? = null,
	val error: String? = null
)
