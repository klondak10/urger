package noty_team.com.urger.utils.adapters

import noty_team.com.urger.utils.Constants

open  class BaseUiPaginationItem (var itemPaginationType: Constants.PaginationItemType = Constants.PaginationItemType.MAIN_ITEM) :
    Comparable<BaseUiPaginationItem> {
    override fun compareTo(other: BaseUiPaginationItem): Int {
        return itemPaginationType.compareTo(other.itemPaginationType)
    }
}