package noty_team.com.urger.utils.api.response.get_city

import com.google.gson.annotations.SerializedName

data class CityResponse(
	@field:SerializedName("status_message")val statusMessage: String? = null,
	@field:SerializedName("status_code")val statusCode: Int? = null,
	@field:SerializedName("data")val data: List<DataItem?>? = null,
	@field:SerializedName("error")val error: String? = null
)
