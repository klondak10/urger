package noty_team.com.urger.utils.adapters.vacancy_nested

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.View
import kotlinx.android.synthetic.main.item_vacancy.*
import noty_team.com.urger.ui.adapters.recycler.BaseAdapter
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem

class VacancyNestedAdapter(
    listVacancyItems: ArrayList<VacancyPaginationItem>,
    var onClick: (data: VacancyPaginationItem) -> Unit = {},
    var onLongClick: (data: VacancyPaginationItem) -> Unit = {}
) : BaseAdapter<VacancyPaginationItem, VacancyNestedAdapter.ViewHolder>(listVacancyItems) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = noty_team.com.urger.R.layout.item_vacancy

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {

            main_container_vacancy.setOnClickListener {
                onClick(list[position])
            }
            main_container_vacancy.setOnLongClickListener {
                onLongClick(list[position])
                return@setOnLongClickListener true
            }
        }

        override fun bind(pos: Int) {

            val spannable = SpannableString(list[pos].vacancy)
            spannable.setSpan(
                StyleSpan(Typeface.BOLD),
                0, list[pos].vacancy.length/* + 1*/,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            existHouse.visibility = if(list[pos].houseId != null) View.VISIBLE else View.GONE
            vacancy_company_name_position.text = spannable
            vacancy_location.text = list[pos].address
            vacancy_type_employment.text = list[pos].workSchedule
            vacancy_price.text = "${list[pos].price} ${list[pos].currencySymbol}"
        }
    }

    fun deleteVacancy(id: Int): Boolean {
        var indexToDelete = -1
        try {
            list.forEachIndexed { index, item ->
                if (item.vacancyId == id) {
                    indexToDelete = index
                    return@forEachIndexed
                }
            }
            return if (indexToDelete >= 0) {
                list.removeAt(indexToDelete)
                notifyItemRemoved(indexToDelete)
                true
            } else false
        } catch (e: java.lang.Exception) {
            return false
        }
    }

    fun editItem(data: VacancyPaginationItem) {
        list.forEachIndexed { index, item ->
            if (item.vacancyId == data.vacancyId) {
                list[index] = data
                notifyItemChanged(index)
                return@forEachIndexed
            }
        }
    }
}

