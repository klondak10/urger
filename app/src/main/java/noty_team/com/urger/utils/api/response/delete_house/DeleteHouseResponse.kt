package noty_team.com.urger.utils.api.response.delete_house

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class DeleteHouseResponse(

		@field:SerializedName("data")
		val data: String? = null

) : BaseResponse()