package noty_team.com.urger.utils.api.response.car

data class DeleteAutoExecutorResponse(
	val statusMessage: String? = null,
	val statusCode: Int? = null,
	val data: String? = null,
	val error: String? = null
)
