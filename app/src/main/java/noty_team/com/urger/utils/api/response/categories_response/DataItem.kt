package noty_team.com.urger.utils.api.response.categories_response

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

		@field:SerializedName("parent_category_id")
		var parentCategoryId: String = "",

		@field:SerializedName("id")
		var categoryId: String = "",

		@field:SerializedName("name")
		var name: String = "",

		@field:SerializedName("subcategories")
		var subcategories: ArrayList<DataItem> = arrayListOf(),

		var isChecked: Boolean = false
)