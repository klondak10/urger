package noty_team.com.urger.utils

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Message(
    @SerializedName("message_id")
    @Expose
    var message_id: Int? = null,
    @SerializedName("text")
    @Expose
    var text: String? = null,
    @SerializedName("name")
    @Expose
    var nickname: String? = null,
    @SerializedName("avatar")
    @Expose
    var avatar: String? = null,
    @SerializedName("photo")
    @Expose
    var photo: String? = null,
    @SerializedName("status")
    @Expose
    var statusMessage: Int = 0,
    @SerializedName("id")
    @Expose
    var userId: Int = 0,
    @SerializedName("date")
    @Expose
    var date: String? = null
)