package noty_team.com.urger.utils.api.response.edit_user_personal_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Employer(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("type")
	val type: Int? = null

)