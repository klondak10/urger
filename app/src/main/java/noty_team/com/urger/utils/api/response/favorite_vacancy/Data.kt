package noty_team.com.urger.utils.api.response.favorite_vacancy

data class Data(
	val userId: Int? = null,
	val vacancyId: String? = null,
	val id: Int? = null
)
