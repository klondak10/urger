package noty_team.com.urger.utils.api.response.edit_user_personal_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("validated_company")
	val validatedCompany: ValidatedCompany? = null,

	@field:SerializedName("employer")
	val employer: Employer? = null
)