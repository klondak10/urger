package noty_team.com.urger.utils.api.response.favorite_vacancy

data class DeleteFavoriteVacancyResponse(
	val statusMessage: String? = null,
	val statusCode: Int? = null,
	val data: Any? = null,
	val error: String? = null
)
