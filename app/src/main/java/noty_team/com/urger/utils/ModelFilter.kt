package noty_team.com.urger.utils

import android.os.Parcel
import android.os.Parcelable

data class ModelFilter(var type_sort:String? = null, var order:String = "wage", var city: String = "", var expectedWage: Int? = null,
                       var timePublish: Int? = null, var type_employment: String? = null, var hasHouse: Int? = 0): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type_sort)
        parcel.writeString(order)
        parcel.writeString(city)
        expectedWage?.let { parcel.writeInt(it) }
        timePublish?.let { parcel.writeInt(it) }
        parcel.writeString(type_employment)
        parcel.writeInt(hasHouse?:0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelFilter> {
        override fun createFromParcel(parcel: Parcel): ModelFilter {
            return ModelFilter(parcel)
        }

        override fun newArray(size: Int): Array<ModelFilter?> {
            return arrayOfNulls(size)
        }
    }

}