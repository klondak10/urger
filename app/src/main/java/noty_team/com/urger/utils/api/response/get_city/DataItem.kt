package noty_team.com.urger.utils.api.response.get_city

import com.google.gson.annotations.SerializedName

data class DataItem(
	@field:SerializedName("parent_category_id")val parentCategoryId: Int? = null,
	@field:SerializedName("name")val name: String? = null,
	@field:SerializedName("id")val id: String? = null,
	@field:SerializedName("subcategories")val subcategories: List<SubcategoriesItem?>? = null
)
