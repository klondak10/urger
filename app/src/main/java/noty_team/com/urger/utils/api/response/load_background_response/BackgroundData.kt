package noty_team.com.urger.utils.api.response.load_background_response


import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class BackgroundData(

		@field:SerializedName("image")
		val image: String? = null
)