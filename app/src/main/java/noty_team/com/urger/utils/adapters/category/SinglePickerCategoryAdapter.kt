package noty_team.com.urger.utils.adapters.category

import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation.RELATIVE_TO_SELF
import android.view.animation.RotateAnimation
import android.widget.CheckedTextView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.thoughtbot.expandablecheckrecyclerview.CheckableChildRecyclerViewAdapter
import com.thoughtbot.expandablecheckrecyclerview.models.CheckedExpandableGroup
import com.thoughtbot.expandablecheckrecyclerview.viewholders.CheckableChildViewHolder
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder
import noty_team.com.urger.R
import noty_team.com.urger.utils.api.response.get_city.SubcategoriesItem

class SinglePickerCategoryAdapter(var list: List<SingleCategory>?, var callback: OnChooseSubcategoryCallback,var onClickPos:()->Unit) : CheckableChildRecyclerViewAdapter<CategoryViewHolder, SinglePickSubcategoryViewHolder>(list) {

    private var items = getItemCount() - 1

    override fun onBindCheckChildViewHolder(
        holder: SinglePickSubcategoryViewHolder?,
        flatPosition: Int,
        group: CheckedExpandableGroup?,
        childIndex: Int
    ) {

        val subcategory:SubcategoriesItem = group!!.items.get(childIndex) as SubcategoriesItem
        subcategory.name?.let { holder?.setSubcategoryName(it) }
    }

    override fun onCreateCheckChildViewHolder(
        parent: ViewGroup?,
        viewType: Int
    ): SinglePickSubcategoryViewHolder {
        val view = LayoutInflater.from(parent?.context)!!.inflate(R.layout.list_item_subcategory, parent, false)
        return SinglePickSubcategoryViewHolder(view, callback)
    }

    override fun onCreateGroupViewHolder(parent: ViewGroup?, viewType: Int): CategoryViewHolder {
        val view = LayoutInflater.from(parent?.context)!!.inflate(R.layout.list_item_category, parent, false)
        return CategoryViewHolder(view, onClickPos)
    }

    override fun onBindGroupViewHolder(
        holder: CategoryViewHolder?,
        flatPosition: Int,
        group: ExpandableGroup<*>?
    ) {

        group?.let { holder?.setCategoryTitle(it) }
    }
}

class CategoryViewHolder(var view: View,val onClickPos:()->Unit) : GroupViewHolder(view) {
    private var categoryName: TextView? = null
    private var arrow: ImageView? = null
    private var categoryLayout: LinearLayout? = null

    init  {
        categoryLayout = view.findViewById(R.id.layout_category) as LinearLayout
        categoryName = view.findViewById<View>(R.id.list_item_category_name) as TextView
        arrow = view.findViewById<View>(R.id.list_item_category_arrow) as ImageView
    }

    fun setCategoryTitle(genre: ExpandableGroup<*>) {

        categoryName?.text = genre.title
    }

    override fun expand() {
        animateExpand()
        onClickPos()
    }

    override fun collapse() {
        animateCollapse()
    }

    private fun animateExpand() {
        val rotate = RotateAnimation(360f, 180f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        arrow?.animation = rotate
    }

    private fun animateCollapse() {
        val rotate = RotateAnimation(180f, 360f, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 300
        rotate.fillAfter = true
        arrow?.animation = rotate
    }
}

class SinglePickSubcategoryViewHolder(var view: View, callback: OnChooseSubcategoryCallback) : CheckableChildViewHolder(view) {

    private var  childCheckedTextView: CheckedTextView? = null

    init {
        childCheckedTextView = view.findViewById<View>(R.id.list_item_subcategory_name) as CheckedTextView
        childCheckedTextView!!.setOnClickListener {
            callback.onChooseSubcategory(childCheckedTextView?.text.toString())
        }
    }

    override fun getCheckable(): CheckedTextView? {
        return childCheckedTextView
    }

    fun setSubcategoryName(subcategoryName: String) {
        childCheckedTextView?.text = subcategoryName

    }
}



interface OnChooseSubcategoryCallback {
    fun onChooseSubcategory(name: String)
    fun clickOnLastGroup(position: Int)
}
