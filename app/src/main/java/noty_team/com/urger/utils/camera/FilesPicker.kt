package noty_team.com.urger.utils.camera

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import com.google.android.material.bottomsheet.BottomSheetDialog
import androidx.core.content.ContextCompat
import android.widget.LinearLayout
import android.widget.Toast
import com.creativechintak.multiimagepicker.builder.MultiImagePicker
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.yalantis.ucrop.UCrop
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import noty_team.com.urger.R
import java.io.File

object FilesPicker {
	const val maxSizeInBytes = 2097152L

	fun createImagePickDialog(
        activity: AppCompatActivity, isMultipleImages: Boolean = false,
        onImageLoaded: (fileData: FileData) -> Unit = {},
        onFilesLoaded: (fileDataList: ArrayList<FileData>) -> Unit = {},
        onFilesLoadError: (errorMessage: String) -> Unit = {}
	) {

		val bottomDialog = BottomSheetDialog(activity)
		bottomDialog.setContentView(R.layout.bottom_dialog_photo)


		val galleryButton = bottomDialog.findViewById<LinearLayout>(R.id.bottom_dialog_gallery)

		galleryButton?.setOnClickListener { _ ->
			if (!isMultipleImages) pickFromGallery(activity, onImageLoaded)
			else pickPhotoFiles(activity, onFilesLoaded, onFilesLoadError)

			bottomDialog.dismiss()
		}

		bottomDialog.show()
	}

	private fun getCropOptions(activity: AppCompatActivity): UCrop.Options {
		val options = UCrop.Options()
		options.setStatusBarColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark))
		options.setToolbarColor(ContextCompat.getColor(activity, R.color.colorWhite))
		options.setToolbarTitle("Edit photo")

		//TODO make toolbar text color

		return options
	}

	@SuppressLint("CheckResult")
	private fun pickFromGallery(activity: AppCompatActivity, onImageLoaded: (fileData: FileData) -> Unit) {
		RxPaparazzo.single(activity)
				.setMultipleMimeType("image/jpeg", "image/jpg", "image/png")
				.setMaximumFileSizeInBytes(maxSizeInBytes)
				.crop(getCropOptions(activity))
				.usingGallery()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe({ response ->
					run {
						if (checkResultCode(activity, response.resultCode())) {
							setPhotoToImage(response.data(), onImageLoaded)
						}
					}
				}, {

				})
	}

	@SuppressLint("CheckResult")
	private fun pickPhoto(activity: AppCompatActivity, onImageLoaded: (fileData: FileData) -> Unit) {
		RxPaparazzo.single(activity)
				.setMaximumFileSizeInBytes(maxSizeInBytes)
				.crop(getCropOptions(activity))
				.usingCamera()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe({ response ->
					run {
						if (checkResultCode(activity, response.resultCode())) {
							setPhotoToImage(response.data(), onImageLoaded)
							Toast.makeText(activity, "test", Toast.LENGTH_LONG).show()
						}

					}
				}, {
					Toast.makeText(activity, it.localizedMessage, Toast.LENGTH_LONG).show()
					var i = 0
				})
	}

	@SuppressLint("CheckResult")
	private fun pickPhotoFiles(
        activity: AppCompatActivity, onFilesLoaded: (fileDataList: ArrayList<FileData>) -> Unit,
        onFilesLoadError: (errorMessage: String) -> Unit
	) {


		RxPaparazzo
				.multiple(activity)
				.setMultipleMimeType("image/jpeg", "image/jpg", "image/png")
				.useDocumentPicker()
				.usingFiles()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe({ response ->
					run {
						if (checkResultCode(activity, response.resultCode())) {

							var data = response.data()
							onFilesLoaded(response.data().toCollection(ArrayList()))
						} else onFilesLoadError("")
					}
				}, {
					onFilesLoadError(it.message.toString())
				})
	}

	private fun setPhotoToImage(data: FileData, onImageLoaded: (fileData: FileData) -> Unit) {
		val imgFile = File(data.file.absolutePath)
		if (imgFile.exists()) {
			onImageLoaded(data)
		}
	}

	fun pickFiles(activity: AppCompatActivity, isMultiple: Boolean, showDialog: (isShow: Boolean) -> Unit, onFilesLoaded: (fileDataList: ArrayList<FileData>) -> Unit): Disposable {
		showDialog(true)
		if (isMultiple) {
			return RxPaparazzo
					.multiple(activity)
					.setMultipleMimeType(
						"image/jpeg",
				    	"image/jpg",
						"image/png"  // .jpeg & .jpg & .png
					)
					.useDocumentPicker()
					.usingFiles()
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe { response ->
						if (checkResultCode(activity, response.resultCode())) {
							onFilesLoaded(response.data().toCollection(ArrayList()))
						} else {
							showDialog(false)
						}
					}
		} else {
			return RxPaparazzo
					.single(activity)
					.setMultipleMimeType()
					.useDocumentPicker()
					.usingFiles()
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe { response ->
						if (checkResultCode(activity, response.resultCode())) {
							onFilesLoaded(arrayListOf(response.data()))
						} else {
							showDialog(false)
						}
					}
		}
	}

	private fun checkResultCode(activity: AppCompatActivity, code: Int): Boolean {
		when {
			code == RxPaparazzo.RESULT_DENIED_PERMISSION -> {}
			code == RxPaparazzo.RESULT_DENIED_PERMISSION_NEVER_ASK -> showPermissionDialog(activity)
			code != AppCompatActivity.RESULT_OK -> {}
		}
		return code == AppCompatActivity.RESULT_OK
	}

	private fun showPermissionDialog(activity: AppCompatActivity) {
		val builder = AlertDialog.Builder(activity)
		builder.setTitle(activity.getString(R.string.error_permission_title))
				.setMessage(activity.getString(R.string.error_permission_message))
				.setCancelable(false)
				.setNegativeButton(activity.getString(R.string.error_permission_cancel_button))
				{ _, _ -> }
		val errorDialog = builder.create()
		errorDialog.setButton(
				AlertDialog.BUTTON_POSITIVE,
				activity.getString(R.string.error_permission_ok_button)
		) { _, _ ->
			showUserDidNotGrantPermissionsNeverAsk(activity.applicationContext)
		}
		errorDialog.show()
	}

	private fun showUserDidNotGrantPermissionsNeverAsk(context: Context) {
		val intent = Intent()
		intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
		val uri = Uri.fromParts("package", context.packageName, null)
		intent.data = uri
		context.startActivity(intent)
	}
}