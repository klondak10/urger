package noty_team.com.urger.utils.adapters.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import noty_team.com.urger.R
import noty_team.com.urger.utils.Room
import noty_team.com.urger.utils.setPhoto


class AdapterRoomChat(
    private val mContext: Context,
    private val mDataSet: ArrayList<Room>,
    var onClickChat: (item: Room) -> Unit = {},
    var callbackEmptyList: (Boolean) -> Unit = {}
) : RecyclerView.Adapter<AdapterRoomChat.ViewHolder>(), Filterable {

    var filterList:ArrayList<Room> = mDataSet

    fun setNewList(list: ArrayList<Room>) {
        mDataSet.clear()
        mDataSet.addAll(list)
        notifyDataSetChanged()
    }

    override fun getFilter(): Filter {

        return object : Filter() {

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {


                filterList = results!!.values as ArrayList<Room>
                if(filterList.isEmpty()) {
                    callbackEmptyList(true)
                } else {
                    callbackEmptyList(false)
                }
                notifyDataSetChanged()
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {

                val queryString = constraint?.toString()!!.toLowerCase()
                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    mDataSet
                else
                    mDataSet.filter {
                        it.name.toLowerCase().contains(queryString)
                    }

                return filterResults
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_chat, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = mDataSet[position]
        holder.bind(data, mContext)
    }


    override fun getItemCount(): Int {
        return mDataSet.size
    }

    fun setList(dataSet: List<Room>) {
        mDataSet.clear()
        mDataSet.addAll(dataSet)
        filterList = dataSet as ArrayList<Room>
        notifyDataSetChanged()
    }

    inner class ViewHolder internal constructor(itemView: View) :
       RecyclerView.ViewHolder(itemView) {

        private var container: CardView = itemView.findViewById(R.id.layout_chat)
        private var name: TextView = itemView.findViewById(R.id.name_tv)
        private var time: TextView = itemView.findViewById(R.id.time_tv)
        private var text: TextView = itemView.findViewById(R.id.text_tv)
        private  var indicator: ImageView = itemView.findViewById(R.id.indicator_viewed)
        private  var countMsg: TextView = itemView.findViewById(R.id.count_msg)
        private  var avatar: ImageView = itemView.findViewById(R.id.avatar)


        fun bind(data: Room, mContext: Context) {
            container.setOnClickListener {
                onClickChat(mDataSet[position])
            }
            name.text = data.name
            text.text = data.text.replace("\'","")
            val times = data.date.substring(data.date.indexOf('T') + 1, data.date.indexOf('.') - 3)
            time.text = times
            setPhoto(avatar, data.avatar)

            if(data.newMess != 0) {
                indicator.setImageDrawable(itemView.context.resources.getDrawable(R.drawable.count_message))
                countMsg.text = data.newMess.toString()
            } else {
                if(data.status == 0) {
                    indicator.setImageDrawable(itemView.context.resources.getDrawable(R.drawable.ic_check_symbol))
                } else if(data.status == 1) {
                    indicator.setImageDrawable(itemView.context.resources.getDrawable(R.drawable.ic_double_check))
                }
            }

        }
    }
}
