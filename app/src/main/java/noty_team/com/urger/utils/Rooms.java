package noty_team.com.urger.utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Objects;


public class Rooms {
    @SerializedName("rooms")
    @Expose
    public ArrayList<Room> rooms = null;

    public int newMessages() {
        int count = 0;
        for (Room room : rooms)
            count += room.newMess;
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rooms rooms1 = (Rooms) o;
        return Objects.equals(rooms, rooms1.rooms);
    }

    @Override
    public int hashCode() {

        return Objects.hash(rooms);
    }
}
