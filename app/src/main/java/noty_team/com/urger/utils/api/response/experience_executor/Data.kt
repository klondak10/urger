package noty_team.com.urger.utils.api.response.experience_executor

import com.google.gson.annotations.SerializedName

data class Data(
	@field:SerializedName("period")
	val period: String? = null,
	@field:SerializedName("company_logo")
	val companyLogo: String? = null,
	@field:SerializedName("user_id")
	val userId: Int? = null,
	@field:SerializedName("city")
	val city: String? = null,
	@field:SerializedName("company_name")
	val companyName: String? = null,
	@field:SerializedName("position")
	val position: String? = null,
	@field:SerializedName("id")
	val id: Int? = null
)
