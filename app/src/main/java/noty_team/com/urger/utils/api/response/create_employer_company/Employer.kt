package noty_team.com.urger.utils.api.response.create_employer_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Employer(

		@field:SerializedName("phone")
		val phone: String? = null,

		@field:SerializedName("background")
		val background: String? = null,

		@field:SerializedName("last_name")
		val lastName: String? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("avatar")
		val avatar: String? = null,

		@field:SerializedName("position")
		val position: String? = null,

		@field:SerializedName("type")
		val type: Int? = null,

		@field:SerializedName("create_at")
		val createAt: String? = null,

		@field:SerializedName("first_name")
		val firstName: String? = null,

		@field:SerializedName("email")
		val email: String? = null,

		@field:SerializedName("can_update")
		val canUpdate: Boolean? = null
)