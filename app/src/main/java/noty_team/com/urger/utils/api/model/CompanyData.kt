package noty_team.com.urger.utils.api.model

data class CompanyData(var companyId: Int, var name: String = "", var city: String = "",
					   var position: String = "", var description: String = "",
					   var nip: String? = "", var logo: String? = "")