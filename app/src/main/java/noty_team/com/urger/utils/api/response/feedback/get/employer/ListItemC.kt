package noty_team.com.urger.utils.api.response.feedback.get.employer

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ListItemC(

	@field:SerializedName("executor_date")
	val executorDate: String? = null,

	@field:SerializedName("executor_rate")
	val executorRate: String? = null,

	@field:SerializedName("executor_review")
	val executorReview: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null
)