package noty_team.com.urger.utils.api.request.create_executor

import com.google.gson.annotations.SerializedName

data class CreateExecutorRequest(
    var phone: String? = null, var first_name: String? = null,
    var last_name: String? = null, var email: String? = null, var birthday: String? = null,
    @field:SerializedName("searched_position")
    val data: Position? = null
)

data class Position(
    var category_id: Array<Int>? = null, var city_place_id: String? = null
)