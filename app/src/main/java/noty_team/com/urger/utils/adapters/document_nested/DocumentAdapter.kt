package noty_team.com.urger.utils.adapters.document_nested

import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_document.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.setPhoto

class DocumentAdapter(
    listDocumentItems: ArrayList<DocumentItem>,
    recyclerView: RecyclerView,
    val onClickItem: (documentItem: DocumentItem) -> Unit = {},
    val onLongClick: (companyItem: DocumentItem) -> Unit = {},
    var onLoadData: (offset: Int) -> Unit = {}
) : BaseAdapterPagination<DocumentItem>(listDocumentItems, recyclerView) {
    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            itemView.setOnClickListener {
                onClickItem(list[position])
            }
            itemView.setOnLongClickListener {

                onLongClick(list[position])
                return@setOnLongClickListener true
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            setPhoto(document_image, list[pos].image)
            document_name.text = list[pos].documentName.replace("super", "").replace("document", "").replace("contract", "").replace("\\n", " ")
        }
    }

    override val itemLayoutResourceId: Int = R.layout.item_document
}