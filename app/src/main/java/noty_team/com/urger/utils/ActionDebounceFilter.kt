package noty_team.com.urger.utils

class ActionDebounceFilter(val time: Long = 200) {
    private var lastClickedTime = 0L
    private val currentTimeMs: Long
        get()= System.currentTimeMillis()

    fun filterAction(): Boolean {
        return if (currentTimeMs - lastClickedTime >= time) {
            lastClickedTime = currentTimeMs
            true
        } else {
            false
        }
    }
}