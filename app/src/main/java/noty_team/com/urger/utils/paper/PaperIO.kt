package noty_team.com.masterovik.utils.paper

import io.paperdb.Paper
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.model.CompanyData
import noty_team.com.urger.utils.api.model.Document
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class PaperIO {
    companion object {
        private const val USER_ROLE = "USER_ROLE"
        const val USER_PHONE_NUMBER = "PHONE_NUMBER"
        private const val REFRESH_TOKEN = "REFRESH_TOKEN"
        private const val ACCESS_TOKEN = "ACCESS_TOKEN"
        private const val SIGN_UP = "SIGN_UP"
        private const val USER_INFO_Е = "USER_INFO"
        private const val USER_INFO_EXECUTOR = "EXECUTOR_INFO"
        private const val USER_AVATAR = "USER_AVATAR"
        private const val COMPANY_INFO = "COMPANY_INFO"
        private const val CAN_EDIT_PROFILE = "CAN_EDIT_PROFILE"
        private const val USER_BACKGROUND = "USER_BACKGROUND"
        private const val DOCUMENTS = "DOCUMENTS"
        private const val ID_COMPANY = "ID_COMPANY"
        private const val POSITION_EXECUTOR = "POSITION_EXECUTOR"
        private const val POSITION_EXECUTOR_IDS = "POSITION_EXECUTOR_IDS"


        var positionExecutor: String?
            get() {
                return Paper.book().read(POSITION_EXECUTOR)
            }
            set(value) {
                Paper.book().write(POSITION_EXECUTOR, value)
            }

        var positionIdsExecutor: Array<Int>?
            get() {
                return Paper.book().read(POSITION_EXECUTOR_IDS, null)
            }
            set(value) {
                Paper.book().write(POSITION_EXECUTOR_IDS, value)
            }

        var idCompany: Int?
            get() {
                return Paper.book().read(ID_COMPANY)
            }
            set(value) {
                Paper.book().write(ID_COMPANY, value)
            }

        var userRole: UserRole?
            get() {
                return Paper.book().read(USER_ROLE)
            }
            set(value) {
                Paper.book().write(USER_ROLE, value)
            }

        var userPhoneNumber: String
            get() {
                return Paper.book().read(USER_PHONE_NUMBER)
            }
            set(value) {
                Paper.book().write(USER_PHONE_NUMBER, value)
            }

        var refreshToken: String
            get() {
                return Paper.book().read(REFRESH_TOKEN) ?: ""
            }
            set(value) {
                Paper.book().write(REFRESH_TOKEN, value)
            }

        var accessToken: String
            get() {
                return Paper.book().read(ACCESS_TOKEN) ?: ""
            }
            set(value) {
                Paper.book().write(ACCESS_TOKEN, value)
            }

        var signUp: Authorization?
            get() {
                return Paper.book().read(SIGN_UP)
            }
            set(value) {
                Paper.book().write(SIGN_UP, value)
            }

        var employerData: UserUrgerData?
            get() {
                return Paper.book().read(USER_INFO_Е)
            }
            set(value) {
                Paper.book().write(USER_INFO_Е, value)
            }

        var executorData: UserUrgerData?
            get()  {
                return Paper.book().read(USER_INFO_EXECUTOR)
            }
            set(value) {
                Paper.book().write(USER_INFO_EXECUTOR, value)
            }

        var companyData: CompanyData?
            get() {
                return Paper.book().read(COMPANY_INFO)
            }
            set(value) {
                Paper.book().write(COMPANY_INFO, value)
            }


        var canEditProfile: Boolean
            get() {
                return Paper.book().read(CAN_EDIT_PROFILE)
            }
            set(value) {
                Paper.book().write(CAN_EDIT_PROFILE, value)
            }

        var documents: ArrayList<Document?>?
            get() {
                return Paper.book().read(DOCUMENTS)
            }
            set(value) {
                Paper.book().write(DOCUMENTS, value)
            }
    }
}