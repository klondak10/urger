package noty_team.com.urger.utils.api.response.get_document

import android.util.Log
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class GetDocumentsPaginationResponse(

		@field:SerializedName("data")
		val data: ArrayList<DataItem>? = null

) : BaseResponse() {

	fun maps(): ArrayList<DocumentItem> {


		return data?.flatMap {
			arrayListOf(

				DocumentItem(
					id = it.documentId?:-1,
					image = it.preview?:"",
					file = it.file?:"",
					documentName = it.name?:""
				)


			)

		}as ArrayList<DocumentItem>


	}


	fun map(): ArrayList<DocumentItem> {


				return data?.flatMap {
					arrayListOf(

						DocumentItem(
							id = it.documentId?:-1,
							image = it.preview?:"",
							file = it.file?:"",
							documentName = it.name?:""
						)


					)

				}?.filter { it -> it.documentName.contains("contract") } as ArrayList<DocumentItem>


	}

	fun mapPre(): ArrayList<DocumentItem> {

		return data?.flatMap {
			arrayListOf(

				DocumentItem(
					id = it.documentId?:-1,
					image = it.preview?:"",
					file = it.file?:"",
					documentName = it.name?:""
				)


			)

		}?.filter { it -> it.documentName.contains("document") } as ArrayList<DocumentItem>
	}
}

