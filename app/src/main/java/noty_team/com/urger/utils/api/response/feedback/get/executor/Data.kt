package noty_team.com.urger.utils.api.response.feedback.get.executor

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

    @field:SerializedName("total")
	val total: Total? = null,

    @field:SerializedName("list")
	val list: List<ListItem>
)