package noty_team.com.urger.utils.api.response.update_validate_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Company(

	@field:SerializedName("company_id")
	val companyId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("normalized_name")
	val normalizedName: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("create_at")
	val createAt: String? = null
)