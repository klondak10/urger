package noty_team.com.urger.utils.api.response.edit_user_personal_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class EditUserPersonalInfoResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse() {
//	fun parseUserInfo() {
//		if (data?.employer?.user?.firstName != null && data?.employer?.user?.lastName != null) {
//			var userData = PaperIO.employerData
//			userData?.firstName = data.employer.user.firstName
//			userData?.lastName = data.employer.user.lastName
//
//			PaperIO.employerData = userData
//		}
//	}

	fun parsePosition(){
		var userData = PaperIO.companyData

		userData?.position = data?.employer?.position ?: "123"

		PaperIO.companyData = userData

	}
}