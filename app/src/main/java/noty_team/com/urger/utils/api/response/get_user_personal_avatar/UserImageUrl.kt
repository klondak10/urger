package noty_team.com.urger.utils.api.response.get_user_personal_avatar

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class UserImageUrl(
		@field:SerializedName("image")
		val image: String? = null
)