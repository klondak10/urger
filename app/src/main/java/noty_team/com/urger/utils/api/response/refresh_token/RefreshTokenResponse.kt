package noty_team.com.urger.utils.api.response.refresh_token

import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse

data class RefreshTokenResponse(

		@field:SerializedName("status_message")
		val statusMessage: String? = null,

		@field:SerializedName("status_code")
		val statusCode: Int? = null,

		@field:SerializedName("data")
		val data: UserTokenData? = null,

		@field:SerializedName("error")
		val error: String? = null
) {
	fun parseToken() {
		if (data?.refreshToken != null) PaperIO.refreshToken = data.refreshToken

		if (data?.accessToken != null) PaperIO.accessToken = data.accessToken
	}
}