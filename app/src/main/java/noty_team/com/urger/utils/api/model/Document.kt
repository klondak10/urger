package noty_team.com.urger.utils.api.model

data class Document(var id: Int?, var name: String?, var document: String?)