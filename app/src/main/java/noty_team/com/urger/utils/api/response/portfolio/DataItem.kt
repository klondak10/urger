package noty_team.com.urger.utils.api.response.portfolio

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.BaseUiPaginationItem
import java.io.Serializable

data class DataItem(
    @field:SerializedName("file")
    val file: String? = null,
    @field:SerializedName("user_id")
    val userId: Int? = null,
    @field:SerializedName("name")
    val name: String? = null,
    @field:SerializedName("description")
    val description: String? = null,
    @field:SerializedName("id")
    val id: Int? = null
): BaseUiPaginationItem(), Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(file)
        parcel.writeValue(userId)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeValue(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DataItem> {
        override fun createFromParcel(parcel: Parcel): DataItem {
            return DataItem(parcel)
        }

        override fun newArray(size: Int): Array<DataItem?> {
            return arrayOfNulls(size)
        }
    }
}