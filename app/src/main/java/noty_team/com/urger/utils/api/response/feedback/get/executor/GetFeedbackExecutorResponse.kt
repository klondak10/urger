package noty_team.com.urger.utils.api.response.feedback.get.executor

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class GetFeedbackExecutorResponse(

    @field:SerializedName("status_message")
	val statusMessage: String? = null,

    @field:SerializedName("status_code")
	val statusCode: Int? = null,

    @field:SerializedName("data")
	val data: Data? = null,

    @field:SerializedName("error")
	val error: String? = null
)