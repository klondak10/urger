package noty_team.com.urger.utils.api.response.favorite_vacancy.favorite

import com.google.gson.annotations.SerializedName

data class DataItem(
	var id: Int? = null,
	@field:SerializedName("vacancy_id") var vacancy_id: Int? = null,
	var category_name: String? = null,
	var parent_category: String? = null,
	var position: String? = null,
	var wage: Int? = null,
	var wage_currency: String? = null,
	var work_schedule: String? = null,
	var address: String? = null,
	var house_id: Int? = null
)
