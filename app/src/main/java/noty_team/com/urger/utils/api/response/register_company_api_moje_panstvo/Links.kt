package noty_team.com.urger.utils.api.response.register_company_api_moje_panstvo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Links(

	@field:SerializedName("self")
	val self: String? = null
)