package noty_team.com.urger.utils.api.response.sms_checked.test

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("access_token")
	val accessToken: String? = null,

	@field:SerializedName("refresh_token")
	val refreshToken: String? = null,

	@field:SerializedName("sign_up")
	val signUp: Boolean? = null,

	@field:SerializedName("user")
	val user: User? = null
)