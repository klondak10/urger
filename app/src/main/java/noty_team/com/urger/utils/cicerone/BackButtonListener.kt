package noty_team.com.urger.utils.cicerone

interface BackButtonListener {
    fun onBackPressed(): Boolean
}