package noty_team.com.urger.utils.api.response.vacancy

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataParticipate(

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("vacancy_id")
	val vacancyId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null
)