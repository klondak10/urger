package noty_team.com.urger.utils.api.response.get_vacancy.preliminary

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class DataItem(
	val auto: Boolean? = null,
	val profile: Profile? = null,
	val id: Int? = null,
	val user: User? = null,
	val vacancy: Vacancy? = null,
	val status: Int? = null,
	@field:SerializedName("user_rate") val rate: Rate? = null
): BaseUiPaginationItem()

data class Rate(
	@field:SerializedName("count") val count: Int? = null,
	@field:SerializedName("rate") val rate: Float? = null
)