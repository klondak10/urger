package noty_team.com.urger.utils.api

import noty_team.com.urger.api.request_interfaces.MojePanstvoApi
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object MojePanstvoApi {

	private val API_END_POINT = "https://api-v3.mojepanstwo.pl/"

	val retrofit: Retrofit by lazy {
		create()
	}

	fun create(): Retrofit {
		val interceptor = HttpLoggingInterceptor()
		interceptor.level = HttpLoggingInterceptor.Level.BODY
		val client = OkHttpClient.Builder()
		client.readTimeout(60, TimeUnit.SECONDS)
		client.writeTimeout(60, TimeUnit.SECONDS)
		client.connectTimeout(60, TimeUnit.SECONDS)
		client.addInterceptor(interceptor)
		client.addInterceptor { chain ->
			val request = chain.request()
			val newRequest: Request

			newRequest = request.newBuilder()
					.addHeader("Content-type", "application/json")
					.build()

			chain.proceed(newRequest)
		}

		val retrofit = Retrofit.Builder()
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.addConverterFactory(GsonConverterFactory.create())
				.client(client.build())
				.baseUrl(API_END_POINT)
				.build()
		return retrofit
	}


	val mojePanstvoApi: MojePanstvoApi by lazy {
		retrofit.create(MojePanstvoApi::class.java)
	}
}