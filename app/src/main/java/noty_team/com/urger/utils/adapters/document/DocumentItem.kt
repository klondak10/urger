package noty_team.com.urger.utils.adapters.document

import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class DocumentItem( var id :Int = 0,
						 var image: String = "",
						 var documentName: String = "",
						 var file:String =""
): BaseUiPaginationItem()
