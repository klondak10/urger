package noty_team.com.urger.utils.api.response.create_executor_user

import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.BaseResponse

data class CreateExecutorUserResponse(

    @field:SerializedName("data")
    val data: DataExecutor? = null


) : BaseResponse() {
    fun parseToken() {
        PaperIO.refreshToken = data?.refreshToken ?: ""
        PaperIO.accessToken = data?.accessToken ?: ""
    }

    fun parseUser() {

        PaperIO.executorData = UserUrgerData(
            data?.executor?.id ?: -1,
            data?.executor?.phone,
            data?.executor?.firstName,
            data?.executor?.lastName,
            data?.executor?.email,
            data?.executor?.type.toString(),
            data?.executor?.avatar,
            data?.executor?.background,
            data?.executor?.birthday,
            position = data?.executor?.position
        )
    }

    fun isCanUpdate() {
        PaperIO.canEditProfile = data?.executor?.canUpdate ?: false
    }
}
