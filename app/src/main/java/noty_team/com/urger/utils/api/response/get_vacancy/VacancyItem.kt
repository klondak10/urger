package noty_team.com.urger.utils.api.response.get_vacancy

import android.util.Log
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem

@Generated("com.robohorse.robopojogenerator")
data class VacancyItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("work_schedule")
	val workSchedule: String? = null,

	@field:SerializedName("short_description")
	val shortDescription: String? = null,

	@field:SerializedName("requirements")
	val requirements: String? = null,

	@field:SerializedName("we_offer")
	val weOffer: String? = null,

	@field:SerializedName("house_id")
	val houseId: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("validated_company_id")
	val validatedCompanyId: Int? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("address_place_id")
	val addressPlaceId: String? = null,

	@field:SerializedName("wage_currency")
	val wageCurrency: String? = null,

	@field:SerializedName("number_of_employees")
	val numberOfEmployees: Int? = null,

	@field:SerializedName("full_description")
	val fullDescription: String? = null,

	@field:SerializedName("responsibilities")
	val responsibilities: String? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("additionally")
	val additionally: String? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("wage")
	val wage: Int? = null,

	@field:SerializedName("wage_type")
	val wageType: String? = null,

	@field:SerializedName("category_name")
	val category_name: String? = null,

	@field:SerializedName("parent_category")
	val parent_category: String? = null,

	@field:SerializedName("created_at")
	val created_at: String? = null,

	@field:SerializedName("city_place_id")
	val city_id: String? = null,

	@field:SerializedName("company_logo")
	val company_logo: String? = null,

	@field:SerializedName("company_name")
	val company_name: String? = null,

	@field:SerializedName("company_about")
	val company_about: String? = null
){
	fun map(): VacancyPaginationItem {
		val schedule: String = when(workSchedule) {
			"full_time" -> "Полная занятость"
			"part_time" -> "Частичная занятость"
			"temporary_job" -> "Временная работа"
			else -> "Контракт"
		}
        return VacancyPaginationItem(
                vacancyId = id?:0,
                vacancy = position?:"",
                category = category_name?:"",
				parent_category = parent_category?:"",
                address = address?:"",
                workSchedule = schedule,
                price = wage?:0,
				currencySymbol = wageCurrency?:"",
				houseId = houseId,
				companyName = company_name?:"",
				companyLogo = company_logo?:"",
				companyDescription = company_about?:""
        )
    }
}