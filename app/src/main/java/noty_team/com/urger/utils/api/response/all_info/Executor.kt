package noty_team.com.urger.utils.api.response.all_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Executor(

	@field:SerializedName("birthday")
	val birthday: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("socketId")
	val socketId: String? = null,

	@field:SerializedName("fcm")
	val fcm: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("background")
	val background: String? = null,

	@field:SerializedName("online")
	val online: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("position")
	val position: Any? = null,

	@field:SerializedName("create_at")
	val createAt: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("can_update")
	val canUpdate: Boolean? = null
)