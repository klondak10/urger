package noty_team.com.urger.utils.api.response.vacancy.requests

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Vacancy(

	@field:SerializedName("work_schedule")
	val workSchedule: String? = null,

	@field:SerializedName("short_description")
	val shortDescription: String? = null,

	@field:SerializedName("we_offer")
	val weOffer: String? = null,

	@field:SerializedName("category_name")
	val categoryName: String? = null,

	@field:SerializedName("validated_company_id")
	val validatedCompanyId: Int? = null,

	@field:SerializedName("address_place_id")
	val addressPlaceId: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("full_description")
	val fullDescription: String? = null,

	@field:SerializedName("responsibilities")
	val responsibilities: String? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("updated_at")
	val updatedAt: Any? = null,

	@field:SerializedName("parent_category")
	val parentCategory: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("wage")
	val wage: Int? = null,

	@field:SerializedName("requirements")
	val requirements: String? = null,

	@field:SerializedName("house_id")
	val houseId: Any? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("wage_currency")
	val wageCurrency: String? = null,

	@field:SerializedName("number_of_employees")
	val numberOfEmployees: Int? = null,

	@field:SerializedName("city_place_id")
	val cityPlaceId: Any? = null,

	@field:SerializedName("additionally")
	val additionally: String? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("wage_type")
	val wageType: String? = null
)