package noty_team.com.urger.utils.adapters.housing

import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_housing.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.setPhoto

class HousingAdapterPagination(
    list: ArrayList<HousingItem>,
    recyclerView: RecyclerView,
    var onLoadData: (offset: Int) -> Unit = {},
    var onClick: (data: HousingItem) -> Unit = {},
    var onLongClick: (documentItem: HousingItem) -> Unit = {}
) : BaseAdapterPagination<HousingItem>(list, recyclerView) {

	override val itemLayoutResourceId = R.layout.item_housing

	override fun onInit(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			try {
				itemView.setOnClickListener {
					onClick(list[position])
				}

				itemView.setOnLongClickListener {

					onLongClick(list[position])

					return@setOnLongClickListener true
				}

			} catch (e: Exception) {
			}
		}
	}

	override fun onBind(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {

			setPhoto(item_image_house, list[pos].preview)
			housing_item_location_tv.text = list[pos].address
			housing_description_tv.text = list[pos].description
		}
	}

	override fun onDataLoaded(listSize: Int) {
		onLoadData(listSize)
	}
}