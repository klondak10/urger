package noty_team.com.urger.utils.api.response.get_vacancy

import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.Rate

data class VacancyItemLocation(
    val idVacancy: Int? = null,
    val phone: String? = null,
    val lastName: String? = null,
    val id: Int? = null,
    val avatar: String? = null,
    val firstName: String? = null,
    val lat: String? = null,
    val lng: String? = null,
    val auto: Boolean? = null,
    val categoryName: String? = null,
    val parentCategory: String? = null,
    val rate: Rate? = null
)