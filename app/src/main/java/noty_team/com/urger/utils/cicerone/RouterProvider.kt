package noty_team.com.urger.utils.cicerone

import ru.terrakok.cicerone.Router

interface RouterProvider {
    fun getRouter(): Router
}