package noty_team.com.urger.utils.api.response.get_employer

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ValidatedCompany(

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("nip")
	val nip: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("about")
	val about: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("create_at")
	val createAt: String? = null,

	@field:SerializedName("employer_id")
	val employerId: Int? = null
)