package noty_team.com.urger.utils.adapters.vacancy

import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class VacancyPaginationItem(
        var vacancyId: Int = -1,
        var idCompany: String = "",
        var vacancy: String ="",
        var category: String = "",
        var parent_category: String = "",
        var address: String = "",
        var workSchedule: String = "",
        var price: Int = 0,
        var currencySymbol: String = "",
        var houseId: Int? = null,
        var companyLogo: String? = null,
        var companyName: String? = null,
        var companyDescription: String? = null
) : BaseUiPaginationItem()