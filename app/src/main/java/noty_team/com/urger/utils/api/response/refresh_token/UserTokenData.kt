package noty_team.com.urger.utils.api.response.refresh_token

import com.google.gson.annotations.SerializedName

data class UserTokenData(
    @field:SerializedName("access_token")
    val accessToken: String? = null,

    @field:SerializedName("refresh_token")
    val refreshToken: String? = null
)