package noty_team.com.urger.utils.api.response.experience_executor.get

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.document.DocumentItem

data class GetExperiencesListExecutorResponse(
	@field:SerializedName("status_message")
	val statusMessage: String? = null,
	@field:SerializedName("status_code")
	val statusCode: Int? = null,
	@field:SerializedName("data")
	val data: List<DataItem?>? = null,
	@field:SerializedName("error")
	val error: String? = null
){
	fun map(): ArrayList<DataItem> {
		return data?.flatMap {
			arrayListOf(
				DataItem(
					period = it?.period?:"",
					companyLogo = it?.companyLogo?:"",
					city = it?.city?:"",
					userId = it?.userId?:-1,
					companyName = it?.companyName?:"",
					position = it?.position?:"",
					id = it?.id?:-1
				)
			)
		} as ArrayList<DataItem>
	}
}
