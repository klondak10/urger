package noty_team.com.urger.utils.api.response.create_house

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CreateHouseResponce(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse()