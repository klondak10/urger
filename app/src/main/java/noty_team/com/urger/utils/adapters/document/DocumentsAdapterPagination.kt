package noty_team.com.urger.utils.adapters.document

import androidx.recyclerview.widget.RecyclerView
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_company.*
import kotlinx.android.synthetic.main.item_document.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.setPhoto

class DocumentsAdapterPagination(
    list: ArrayList<DocumentItem>,
    recyclerView: RecyclerView,
    var onLoadData: (offset: Int) -> Unit = {},
    var onClickPosition: (data: DocumentItem, pos:Int) -> Unit = { data: DocumentItem, pos: Int -> },
    var onClick: (data: DocumentItem) -> Unit = {},
    var onLongClick: (documentItem: DocumentItem) -> Unit = {}
) : BaseAdapterPagination<DocumentItem>(list, recyclerView) {

	override val itemLayoutResourceId = R.layout.item_document

	override fun onInit(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			try {
				itemView.setOnClickListener {
					onClick(list[position])
					onClickPosition (list[position], adapterPosition)
				}

				itemView.setOnLongClickListener {

					onLongClick(list[position])

					return@setOnLongClickListener true
				}

			} catch (e: Exception) {
			}
		}
	}

	override fun onBind(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			loadImg(list[pos].image, document_image)
			document_name.text = list[pos].documentName.replace("super", "").replace("document", "").replace("contract", "").replace("\\n", " ")
		}
	}

	private fun loadImg(image: String, avatarView: ImageView) {
		setPhoto(avatarView, image)
	}

	override fun onDataLoaded(listSize: Int) {
		onLoadData(listSize)
	}
}