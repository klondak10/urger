package noty_team.com.urger.utils.api.response.portfolio

import com.google.gson.annotations.SerializedName

data class Data(
	@field:SerializedName("file")
	val file: String? = null,
	@field:SerializedName("user_id")
	val userId: Int? = null,
	@field:SerializedName("name")
	val name: String? = null,
	@field:SerializedName("description")
	val description: String? = null,
	@field:SerializedName("id")
	val id: Int? = null
)
