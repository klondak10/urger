package noty_team.com.urger.utils

import android.content.Context
import android.graphics.Canvas
import com.google.android.material.appbar.AppBarLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import android.util.AttributeSet
import android.util.TypedValue
import java.lang.ref.WeakReference

class ControllableAppBarLayout : AppBarLayout, AppBarLayout.OnOffsetChangedListener {
    private var mBehavior: AppBarLayout.Behavior? = null
    private var mParent: WeakReference<CoordinatorLayout>? = null
    private var mQueuedChange = ToolbarChange.NONE
    private var mAfterFirstDraw = false
    var state: State? = null
        private set
    private var onStateChangeListener: OnStateChangeListener? = null

    constructor(context: Context) : super(context) {}

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (layoutParams !is CoordinatorLayout.LayoutParams || parent !is CoordinatorLayout) {
            throw IllegalStateException(
                "ControllableAppBarLayout must be a direct child of CoordinatorLayout."
            )
        }
        mParent = WeakReference<CoordinatorLayout>(parent as CoordinatorLayout)
        addOnOffsetChangedListener(this)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        if (mBehavior == null) {
            mBehavior = (layoutParams as CoordinatorLayout.LayoutParams).behavior as AppBarLayout.Behavior?
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        if (r - l > 0 && b - t > 0 && mAfterFirstDraw && mQueuedChange != ToolbarChange.NONE) {
            analyzeQueuedChange()
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (!mAfterFirstDraw) {
            mAfterFirstDraw = true
            if (mQueuedChange != ToolbarChange.NONE) {
                analyzeQueuedChange()
            }
        }
    }

    @Synchronized
    private fun analyzeQueuedChange() {
        when (mQueuedChange) {
            ControllableAppBarLayout.ToolbarChange.COLLAPSE -> performCollapsingWithoutAnimation()
            ControllableAppBarLayout.ToolbarChange.COLLAPSE_WITH_ANIMATION -> performCollapsingWithAnimation()
            ControllableAppBarLayout.ToolbarChange.EXPAND -> performExpandingWithoutAnimation()
            ControllableAppBarLayout.ToolbarChange.EXPAND_WITH_ANIMATION -> performExpandingWithAnimation()
        }

        mQueuedChange = ToolbarChange.NONE
    }

    @JvmOverloads
    fun collapseToolbar(withAnimation: Boolean = false) {
        mQueuedChange = if (withAnimation) ToolbarChange.COLLAPSE_WITH_ANIMATION else ToolbarChange.COLLAPSE
        requestLayout()
    }

    @JvmOverloads
    fun expandToolbar(withAnimation: Boolean = false) {
        mQueuedChange = if (withAnimation) ToolbarChange.EXPAND_WITH_ANIMATION else ToolbarChange.EXPAND
        requestLayout()
    }

    private fun performCollapsingWithoutAnimation() {
        mParent?.get()?.let {
            mBehavior?.onNestedPreScroll(it, this, it, 0, height, intArrayOf(0, 0))
        }
    }

    private fun performCollapsingWithAnimation() {
        mParent?.get()?.let {
            mBehavior?.onNestedFling(it, this, it, 0f, height.toFloat(), true)
        }
    }

    private fun performExpandingWithoutAnimation() {
        mParent?.get()?.let {
            mBehavior?.topAndBottomOffset = 0
        }
    }

    private fun performExpandingWithAnimation() {
        mParent?.get()?.let {
            mBehavior?.onNestedFling(it, this, it, 0f, (-height * 5).toFloat(), false)
        }
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout, i: Int) {
        val tv = TypedValue()
        var toolBarHeight = 0
        if (context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            toolBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
        }
        if (i <= -appBarLayout.totalScrollRange + toolBarHeight) {
            if (onStateChangeListener != null && state != State.COLLAPSED) {
                onStateChangeListener?.onStateChange(State.COLLAPSED)
            }
            state = State.COLLAPSED
        } else {
            if (onStateChangeListener != null && state != State.EXPANDED) {
                onStateChangeListener?.onStateChange(State.EXPANDED)
            }
            state = State.EXPANDED
        } /*else {
            if (onStateChangeListener != null && state != State.IDLE) {
                onStateChangeListener?.onStateChange(State.IDLE)
            }
            state = State.IDLE
        }*/
    }

    fun setOnStateChangeListener(listener: OnStateChangeListener) {
        this.onStateChangeListener = listener
    }

    interface OnStateChangeListener {
        fun onStateChange(toolbarChange: State)
    }

    enum class State {
        COLLAPSED,
        EXPANDED,
        IDLE
    }

    private enum class ToolbarChange {
        COLLAPSE,
        COLLAPSE_WITH_ANIMATION,
        EXPAND,
        EXPAND_WITH_ANIMATION,
        NONE
    }
}