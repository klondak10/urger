package noty_team.com.urger.utils.api.response.register_company_api_moje_panstvo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class DataobjectItem(

	@field:SerializedName("score")
	val score: Any? = null,

	@field:SerializedName("mp_url")
	val mpUrl: String? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("global_id")
	val globalId: String? = null,

	@field:SerializedName("schema_url")
	val schemaUrl: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("dataset")
	val dataset: String? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null
)