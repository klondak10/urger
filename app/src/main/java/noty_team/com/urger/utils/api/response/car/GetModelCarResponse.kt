package noty_team.com.urger.utils.api.response.car


import com.google.gson.annotations.SerializedName


data class GetModelCarResponse(

	@field:SerializedName("Message")
	val message: String? = null,

	@field:SerializedName("Results")
	val results: List<ResultsItem>? = null,

	@field:SerializedName("Count")
	val count: Int? = null) {

	fun map(): ArrayList<String> {
		val list = ArrayList<String>()
		results!!.forEach {
			list.add(it.makeName!!)
		}
		return list
	}
}