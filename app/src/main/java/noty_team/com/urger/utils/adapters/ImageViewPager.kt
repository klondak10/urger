package noty_team.com.urger.utils.adapters

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.davemorrissey.labs.subscaleview.ImageSource
import kotlinx.android.synthetic.main.view_pager_image.view.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.waitUntilViewDrawn


class ImageViewPager(val context: Context, val imageLinkList: ArrayList<CompanyPhotoItem>, private val isLoadFromGallery: Boolean = false)
	: PagerAdapter() {


	override fun instantiateItem(collection: ViewGroup, position: Int): Any {
		val currentImageLink = imageLinkList[position].image
		val inflater = LayoutInflater.from(context)

		val layout = inflater.inflate(R.layout.view_pager_image, collection, false) as ViewGroup

		if (currentImageLink.isNotEmpty()) {
			waitUntilViewDrawn(layout.view_pager_image) {

				layout.loading_spinner.visibility = View.VISIBLE

				if (isLoadFromGallery) {

					layout.view_pager_image.setImage(ImageSource.bitmap(BitmapFactory.decodeFile(currentImageLink)))
					layout.loading_spinner?.visibility = View.GONE

				} else {
					while (!layout.view_pager_image.hasImage()) {
						Glide.with(context)
							.asBitmap().load(currentImageLink)
							.listener(object : RequestListener<Bitmap?> {

								override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap?>?, isFirstResource: Boolean): Boolean {
									return false
								}

								override fun onResourceReady(
									resource: Bitmap?,
									model: Any?,
									target: Target<Bitmap?>?,
									dataSource: DataSource?,
									isFirstResource: Boolean
								): Boolean {
									try {
										if (resource != null) {
											if (!layout.view_pager_image.hasImage())
												layout.view_pager_image.setImage(ImageSource.bitmap(resource))
										}
									} catch (e: Exception) { } catch (e: IllegalStateException) { }
									return  false
								}
							}).submit()



					}


					try {
						layout.loading_spinner?.visibility = View.GONE
					} catch (e: Exception) {

					} catch (e: IllegalStateException) {

					}
				}


			}
		}
		collection.addView(layout)

		return layout

	}

	override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
		collection.removeView(view as View)
	}

	override fun getCount(): Int {
		return imageLinkList.size
	}

	override fun isViewFromObject(view: View, `object`: Any): Boolean {
		return view == `object`
	}
}