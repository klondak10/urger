package noty_team.com.urger.utils.api.model

import com.google.android.gms.maps.model.LatLng

data class PlacesItem(
		val address: String = "",
		val placeId: String = ""
)