package noty_team.com.urger.utils.api.response.get_vacancy.preliminary

import android.os.Parcel
import android.os.Parcelable

data class User(
	val birthday: String? = null,
	val phone: String? = null,
	val background: String? = null,
	val lastName: String? = null,
	val id: Int? = null,
	val avatar: String? = null,
	val position: String? = null,
	val createAt: String? = null,
	val type: Int? = null,
	val firstName: String? = null,
	val email: String? = null,
	val canUpdate: Boolean? = null
) : Parcelable {
	constructor(parcel: Parcel) : this(
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.readString(),
		parcel.readString(),
		parcel.readValue(Int::class.java.classLoader) as? Int,
		parcel.readString(),
		parcel.readString(),
		parcel.readValue(Boolean::class.java.classLoader) as? Boolean
	) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(birthday)
		parcel.writeString(phone)
		parcel.writeString(background)
		parcel.writeString(lastName)
		parcel.writeValue(id)
		parcel.writeString(avatar)
		parcel.writeString(position)
		parcel.writeString(createAt)
		parcel.writeValue(type)
		parcel.writeString(firstName)
		parcel.writeString(email)
		parcel.writeValue(canUpdate)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<User> {
		override fun createFromParcel(parcel: Parcel): User {
			return User(parcel)
		}

		override fun newArray(size: Int): Array<User?> {
			return arrayOfNulls(size)
		}
	}
}
