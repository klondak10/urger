package noty_team.com.urger.utils.api.response.get_vacancy

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class VacancyResponse(
	@field:SerializedName("data")
	var data: VacancyItem = VacancyItem()

) : BaseResponse()