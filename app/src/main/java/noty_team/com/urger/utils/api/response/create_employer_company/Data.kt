package noty_team.com.urger.utils.api.response.create_employer_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("access_token")
	val accessToken: String? = null,

	@field:SerializedName("refresh_token")
	val refreshToken: String? = null,

	@field:SerializedName("validated_company")
	val validatedCompany: ValidatedCompany? = null,

	@field:SerializedName("employer")
	val employer: Employer? = null
)