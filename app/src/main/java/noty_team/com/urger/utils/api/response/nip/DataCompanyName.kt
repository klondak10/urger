package noty_team.com.urger.utils.api.response.nip

import com.google.gson.annotations.SerializedName
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class DataCompanyName(
		@field:SerializedName("address")
		val address: String? = null,

		@field:SerializedName("name")
		val companyName: String? = null
)