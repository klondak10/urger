package noty_team.com.urger.utils.api.response.get_house

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class GetHouseResponse(

	@field:SerializedName("data")
	val data: Data? = null

): BaseResponse(){

	fun map(): DataMapResponse {
		val data = DataMapResponse()
		data.houseId =  data?.houseId ?: 0
		data.address = data?.address ?: ""
		data.available = data?.available ?: false
		data.lat = data?.lat ?: ""
		data.lng = data?.lng ?: ""
		data.rooms = data?.rooms ?: 0
		data.photos = data?.photos ?: arrayListOf()
		data.description = data?.description ?: ""
		data.costOfRent = data?.costOfRent ?: 0
		data.currencyOfRent = data?.currencyOfRent ?: ""
		return data
	}

	fun mapToHousingItem(): HousingItem {
		return HousingItem(
				houseId = data?.houseId?:-1,
				preview = if ( data?.photos?.isNotEmpty() == true) data.photos[0] else "",
				description = data?.description?:"",
				address = data?.address?:""
		)
	}
}