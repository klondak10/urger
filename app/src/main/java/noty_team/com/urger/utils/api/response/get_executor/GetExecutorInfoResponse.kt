package noty_team.com.urger.utils.api.response.get_executor

import android.util.Log
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.model.CompanyData
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.BaseResponse
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class GetExecutorInfoResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse() {

	fun parseUser() {

		PaperIO.executorData = UserUrgerData(
				data?.executor?.id ?: -1,
				data?.executor?.phone,
				data?.executor?.firstName,
				data?.executor?.lastName,
				data?.executor?.email,
				data?.executor?.type.toString(),
				data?.executor?.avatar,
				data?.executor?.background,
				data?.executor?.birthday,
				rate = data?.rate ?: 0.0F,
				position = data?.executor?.position
		)

	}
}