package noty_team.com.urger.utils.api.request.edit_user_personal_info

import com.google.gson.annotations.SerializedName

data class EditEmployerInfoRequest(

    @field:SerializedName("first_name")
    val firstName: String? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null
)