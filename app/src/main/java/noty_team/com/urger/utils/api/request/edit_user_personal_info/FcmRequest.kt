package noty_team.com.urger.utils.api.request.edit_user_personal_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class FcmRequest(

		@field:SerializedName("fcm")
		val fcm: String? = null

)