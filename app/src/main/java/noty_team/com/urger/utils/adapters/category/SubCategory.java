package noty_team.com.urger.utils.adapters.category;

import android.os.Parcel;
import android.os.Parcelable;

public class SubCategory implements Parcelable {

  private String name;
  private boolean isFavorite;

  public SubCategory(String name, boolean isFavorite) {
    this.name = name;
    this.isFavorite = isFavorite;
  }

  protected SubCategory(Parcel in) {
    name = in.readString();
  }

  public String getName() {
    return name;
  }

  public boolean isFavorite() {
    return isFavorite;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof SubCategory)) return false;

    SubCategory artist = (SubCategory) o;

    if (isFavorite() != artist.isFavorite()) return false;
    return getName() != null ? getName().equals(artist.getName()) : artist.getName() == null;

  }

  @Override
  public int hashCode() {
    int result = getName() != null ? getName().hashCode() : 0;
    result = 31 * result + (isFavorite() ? 1 : 0);
    return result;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(name);
  }

  @Override
  public int describeContents() {
    return 0;
  }

  public static final Creator<SubCategory> CREATOR = new Creator<SubCategory>() {
    @Override
    public SubCategory createFromParcel(Parcel in) {
      return new SubCategory(in);
    }

    @Override
    public SubCategory[] newArray(int size) {
      return new SubCategory[size];
    }
  };
}

