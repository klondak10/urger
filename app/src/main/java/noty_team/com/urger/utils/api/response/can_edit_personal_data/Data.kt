package noty_team.com.urger.utils.api.response.can_edit_personal_data

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("can_update")
	val canUpdate: Boolean? = null
)