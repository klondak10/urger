package noty_team.com.urger.utils.api.response.update_validate_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("validated_company_id")
	val validatedCompanyId: Int? = null,

	@field:SerializedName("nip")
	val nip: String? = null,

	@field:SerializedName("id")/*company_*/
	val companyId: Int? = null,

	@field:SerializedName("about")
	val about: String? = null,

	@field:SerializedName("company")
	val company: Company? = null
)