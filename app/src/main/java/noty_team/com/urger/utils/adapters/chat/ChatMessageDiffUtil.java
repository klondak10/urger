package noty_team.com.urger.utils.adapters.chat;

import androidx.recyclerview.widget.DiffUtil;
import java.util.ArrayList;

import noty_team.com.urger.utils.Message;

public class ChatMessageDiffUtil extends DiffUtil.Callback {

    private final ArrayList<Message> oldList;
    private final ArrayList<Message> newList;

    public ChatMessageDiffUtil(ArrayList<Message> oldList, ArrayList<Message> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int i, int i1) {
        Message oldMsg = oldList.get(i);
        Message newMsg;
        try {
            newMsg = newList.get(i);
        } catch (IndexOutOfBoundsException e ) {
            newMsg = oldList.get(i);
        }
        return oldMsg.getMessage_id().equals(newMsg.getMessage_id());
    }

    @Override
    public boolean areContentsTheSame(int i, int i1) {
        Message oldMsg = oldList.get(i);
        Message newMsg;
        try {
            newMsg = newList.get(i);
        } catch (IndexOutOfBoundsException e ) {
            newMsg = oldList.get(i);
        }
        return oldMsg.getStatusMessage() == newMsg.getStatusMessage();
    }
}
