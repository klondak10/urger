package noty_team.com.urger.utils.api.response.get_vacancy

import android.util.Log
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.currency.CurrencyItem
import noty_team.com.urger.utils.api.response.work_schedule.ScheduleItem

@Generated("com.robohorse.robopojogenerator")
data class VacanciesListResponse(

		@field:SerializedName("data")
		var data: ArrayList<VacancyItem> = arrayListOf()

) : BaseResponse() {

    fun map(scheduleTypes: ArrayList<ScheduleItem>,
            currencyTypes: ArrayList<CurrencyItem>): ArrayList<VacancyPaginationItem> {
        return data.flatMap { vacancy ->

            var currencySymbol = ""
            currencyTypes.forEach { item ->
                if (item.currencyCode == vacancy.wageCurrency){
                    currencySymbol = item.currencySymbol?:""
                }
            }

            val schedule: String = when(vacancy.workSchedule) {
                "full_time" -> "Полная занятость"
                "part_time" -> "Частичная занятость"
                "temporary_job" -> "Временная работа"
                else -> "Контракт"
            }

            arrayListOf(
                VacancyPaginationItem(
                        vacancyId = vacancy.id?:-1,
                        idCompany = vacancy.validatedCompanyId.toString(),
                        vacancy = vacancy.position?:"",
                        category = vacancy.category_name?:"",
                        parent_category = vacancy.parent_category?:"",
                        address = vacancy.address?:"",
                        workSchedule = schedule,
                        price = vacancy.wage?:0,
                        currencySymbol = vacancy.wageCurrency?:"EUR",
                        houseId = vacancy.houseId
                )
        ) } as ArrayList
    }

    fun map(): ArrayList<VacancyPaginationItem> {
        return data.flatMap { vacancy ->

            val schedule: String = when(vacancy.workSchedule) {
                "full_time" -> "Полная занятость"
                "part_time" -> "Частичная занятость"
                "temporary_job" -> "Временная работа"
                else -> "Контракт"
            }

            arrayListOf(
                VacancyPaginationItem(
                    vacancyId = vacancy.id?:-1,
                    idCompany = vacancy.validatedCompanyId.toString(),
                    vacancy = vacancy.position?:"",
                    category = vacancy.category_name?:"",
                    parent_category = vacancy.parent_category?:"",
                    address = vacancy.address?:"",
                    workSchedule = schedule,
                    price = vacancy.wage?:0,
                    currencySymbol = vacancy.wageCurrency?:"EUR",
                    houseId = vacancy.houseId
                )
            ) } as ArrayList
    }

}