package noty_team.com.urger.utils.api.model

data class UserUrgerData(var userId: Int? = null,
						 var phone: String? = "",
						 var firstName: String? = "",
						 var lastName: String? = "",
						 var email: String? = "",
						 var type: String? = "",
						 var avatar: String? = "",
						 var background: String? = "",
						 var birthday: String? = "",
						 var expectedWage: Int? = 0,
						 var rate: Float? = 0.0F,
						 var position: String? = "")