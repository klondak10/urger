package noty_team.com.urger.utils.api.response.get_document

import com.google.gson.annotations.SerializedName

data class DataItem(

	@field:SerializedName("preview")
	val preview: String? = null,

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("owner_id")
	val ownerId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val documentId: Int? = null,

	@field:SerializedName("owner_type")
	val ownerType: String? = null
)

