package noty_team.com.urger.utils.api.request.create_vacancy

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class CreateVacancyRequest(

	@field:SerializedName("work_schedule")
	val workSchedule: String? = null,

	@field:SerializedName("short_description")
	val shortDescription: String? = null,

	@field:SerializedName("requirements")
	val requirements: String? = null,

	@field:SerializedName("we_offer")
	val weOffer: String? = null,

	@field:SerializedName("house_id")
	val houseId: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("address_place_id")
	val addressPlaceId: String? = null,

	@field:SerializedName("wage_currency")
	val wageCurrency: String? = null,

	@field:SerializedName("number_of_employees")
	val numberOfEmployees: Int? = null,

	@field:SerializedName("full_description")
	val fullDescription: String? = null,

	@field:SerializedName("responsibilities")
	val responsibilities: String? = null,

	@field:SerializedName("category_id")
	val categoryId: Int? = null,

	@field:SerializedName("additionally")
	val additionally: String? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("wage")
	val wage: Int? = null,

	@field:SerializedName("wage_type")
	val wageType: String? = null
)