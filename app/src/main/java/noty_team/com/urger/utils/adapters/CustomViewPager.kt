package noty_team.com.urger.utils.adapters

import android.content.Context
import androidx.viewpager.widget.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var enableds: Boolean = false

    init {
        this.enableds = true
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return this.enableds && super.onTouchEvent(event)

    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return this.enableds && super.onInterceptTouchEvent(event)

    }

    fun setPagingEnabled(enabled: Boolean) {
        this.enableds = enabled
    }
}