package noty_team.com.urger.utils.api.response.car

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class DataItemAddCar(
	@field: SerializedName("file")
	val file: String? = null,
	@field: SerializedName("color")
	val color: String? = null,
	@field: SerializedName("user_id")
	val userId: Int? = null,
	@field: SerializedName("fuel")
	val fuel: String? = null,
	@field: SerializedName("model")
	val model: String? = null,
	@field: SerializedName("id")
	val id: Int? = null,
	@field: SerializedName("issued")
	val issued: String? = null
)

data class DataItemGetCars(

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("color")
	val color: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("fuel")
	val fuel: String? = null,

	@field:SerializedName("model")
	val model: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("issued")
	val issued: String? = null
): BaseUiPaginationItem()

