package noty_team.com.urger.utils

import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FetchPlaceRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.PlacesClient
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import noty_team.com.urger.utils.api.model.PlacesItem
import java.util.*

fun getAddresses(query: String, geoDataClient: PlacesClient,
				 singleResult: Boolean, searchCity: Boolean = false,
				 onResultFound: (place: PlacesItem) -> Unit,
				 onResultsNotFound: () -> Unit): Disposable {

	val token = AutocompleteSessionToken.newInstance()

	val request = FindAutocompletePredictionsRequest.builder()

			.setTypeFilter(if (searchCity) TypeFilter.CITIES else TypeFilter.ADDRESS)
			.setSessionToken(token)
			.setQuery(query)
			.build()


	val task = geoDataClient.findAutocompletePredictions(request)

	return Observable.fromCallable { Tasks.await(task) }
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
			.subscribe(
					{ result ->
						if (result.autocompletePredictions.isNotEmpty()) {
							result.autocompletePredictions.forEachIndexed { index, prediction ->
								/*if (singleResult && index == 0 || !singleResult) {
									val fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.ADDRESS)
									prediction?.placeId?.let { placeId ->
										val placeRequest = FetchPlaceRequest.builder(placeId, fields).build()
										geoDataClient.fetchPlace(placeRequest)
												.addOnFailureListener {
													onResultsNotFound()
												}
												.addOnCompleteListener {
													it?.result?.place?.let { place ->
														val address = place?.address
														val latLng = place?.latLng
														if (address != null && latLng != null)
															onResultFound(PlacesItem(address, latLng, placeId))
													}
												}
									}
								}*/
								prediction?.let {
									onResultFound(PlacesItem(prediction?.getFullText(null).toString(), prediction?.placeId
											?: ""))
								} ?: onResultsNotFound()

							}
						} else {
							onResultsNotFound()
						}
					}, {
				onResultsNotFound()
			}
			)

}