package noty_team.com.urger.utils.api.response.favorite_vacancy.favorite


data class GetFavoriteVacanciesResponse(
	val statusMessage: String? = null,
	val statusCode: Int? = null,
	val data: List<DataItem> = arrayListOf(),
	val error: String? = null
) {
	fun map():ArrayList<FavoriteVacancyItem> {
		return data.flatMap {
			val schedule: String = when(it.work_schedule) {
				"full_time" -> "Полная занятость"
				"part_time" -> "Частичная занятость"
				"temporary_job" -> "Временная работа"
				else -> "Контракт"
			}
			arrayListOf(
				FavoriteVacancyItem(
					id = it.id ?: -1,
					vacancy_id = it.vacancy_id ?: -1,
					category_name = it.category_name ?: "",
					parent_category = it.parent_category ?: "",
					position = it.position ?: "",
					wage = it.wage ?: -1,
					wage_currency = it.wage_currency ?: "",
					work_schedule = schedule,
					address = it.address ?: "",
					house_id = it.house_id
				)
			)} as ArrayList<FavoriteVacancyItem>
	}
}
