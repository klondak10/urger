package noty_team.com.urger.utils.api.response.vacancy.requests

import android.os.Parcelable
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

@Generated("com.robohorse.robopojogenerator")
data class DataItem(

	@field:SerializedName("auto")
	val auto: Boolean = false,

	@field:SerializedName("profile")
	val profile: Profile? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("user")
	val user: User? = null,

	@field:SerializedName("vacancy")
	val vacancy: Vacancy? = null
): BaseUiPaginationItem()