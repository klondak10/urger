package noty_team.com.urger.utils.api.response.load_photo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(
	@field:SerializedName("image")
	val image: String? = null
)