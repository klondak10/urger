package noty_team.com.urger.utils.api.response.add_document.create_document

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CreateDocumentResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse(){
	fun map(): DocumentItem{
		return DocumentItem(
			id = data?.documentId ?: -1,
			image = data?.preview ?: "",
			documentName = data?.name ?: "",
			file = data?.file ?: ""
		)
	}
}