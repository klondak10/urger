package noty_team.com.urger.utils.api.response.register_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.model.CompanyData
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CreateEmployerResponse(

    @field:SerializedName("data")
    val data: Data? = null


    ) : BaseResponse() {

    fun parseToken(){
        PaperIO.refreshToken = data?.refreshToken ?:""
        PaperIO.accessToken = data?.accessToken ?:""
    }

    fun parseCompany() {
        PaperIO.companyData = CompanyData(data?.validatedCompany?.companyId ?: -1,
            data?.validatedCompany?.company?.name ?: "def",
            data?.validatedCompany?.address ?: "undef",
            data?.employer?.position ?: "Some position",
            data?.validatedCompany?.about ?: " ",
            data?.validatedCompany?.nip ?: "",
            data?.validatedCompany?.company?.logo ?: ""
        )

    }

    fun parseUser() {
        PaperIO.employerData = UserUrgerData(
            data?.employer?.userId ?: -1,
            data?.employer?.user?.phone,
            data?.employer?.user?.firstName,
            data?.employer?.user?.lastName,
            data?.employer?.user?.email,
            data?.employer?.user?.type,
            data?.employer?.user?.avatar,
            data?.employer?.user?.background
        )
    }

    fun isCanUpdate() {
        PaperIO.canEditProfile = data?.employer?.user?.canUpdate ?: false
    }

}