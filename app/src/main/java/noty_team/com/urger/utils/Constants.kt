package noty_team.com.urger.utils

object Constants {
    var DISPOSABLE_ERROR = "Ошибка запроса"

    enum class PaginationItemType {
        MAIN_ITEM,
        LOADING_ITEM,
        ERROR_ITEM;
    }

    const val documentItemLimit = 11
    const val vacancyItemLimit = 10
}