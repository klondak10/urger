package noty_team.com.urger.utils.adapters.chat

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_chat_image_companion.view.*
import kotlinx.android.synthetic.main.item_chat_image_my.view.*
import kotlinx.android.synthetic.main.item_msg.view.*
import kotlinx.android.synthetic.main.item_msg_my.view.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.Message
import noty_team.com.urger.utils.setPhoto
import org.json.JSONException
import java.lang.IndexOutOfBoundsException
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat


class ChatAdapter(var context: Context, var mData: ArrayList<Message>, var userId: Int, var companionId: Int,
                  val clickImageCallback: (url: String) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var inflater: LayoutInflater = LayoutInflater.from(context)

    var formatter = SimpleDateFormat("yyyy-MM-dd")
    var currentDate = formatter.format(324234234432) //System.currentMiles()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            1 -> {
                view = inflater.inflate(R.layout.item_msg_my, parent, false)
                ViewHolderMsgMy(view)
            }
            2 -> {
                view = inflater.inflate(R.layout.item_msg, parent, false)
                ViewHolderMessage(view)
            }
            3 -> {
                view = inflater.inflate(R.layout.item_chat_image_companion, parent, false)
                ViewHolderImageCompanion(view)
            }
            else -> { /*4*/
                view = inflater.inflate(R.layout.item_chat_image_my, parent, false)
                ViewHolderImageMy(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            1 -> {
                val itemMsgMe = holder as ViewHolderMsgMy
                val msg = mData[position]
                itemMsgMe.bind(msg, position)
            }
            2 -> {
                val itemMsg = holder as ViewHolderMessage
                val msg = mData[position]
                itemMsg.bind(msg, position)
            }
            3 -> {
                val itemMsgMe = holder as ViewHolderImageCompanion
                val msg = mData[position]

                itemMsgMe.bind(msg, clickImageCallback, position)
            }
            4 -> {
                val itemMsgMe = holder as ViewHolderImageMy
                val msg = mData[position]
                itemMsgMe.bind(msg, clickImageCallback, position)
            }
        }
    }

    fun setData(message: Message) {
        mData.add(message)
        notifyDataSetChanged()
    }

    fun set(list: ArrayList<Message>) {
        mData.clear()
        mData.addAll(list)
        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        val m = mData[position]
        try {
            if (m.userId == userId && m.text != null) {
                return 1
            } else if(m.userId == companionId && m.text != null)  {
                return 2
            } else if(m.userId == companionId && m.text == null) {
                return 3
            }else if(m.userId == userId && m.text == null){
                return 4
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return 1
    }

    internal inner class ViewHolderMessage(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(msg: Message, position: Int) {

            itemView.message_companion.text = msg.text.toString().replace("\'","")
            itemView.time_message_companion.text = convertTime(msg.date!!)

            val date = convertDate(msg.date!!)
            if(mData.size == 1) {
                bindDate(date, itemView.txt_date_companion)
            } else if(mData.size > 1) {
                try {
                    if(formatter.parse(mData[position].date).after(formatter.parse(mData[position - 1].date))) {
                        bindDate(date, itemView.txt_date_companion)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    bindDate(date, itemView.txt_date_companion)
                }
            }
           }
        }


    internal inner class ViewHolderImageCompanion(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(msg: Message, clickImageCallback:(String) -> Unit, position: Int) {

            setPhoto(itemView.image_companion, msg.photo.toString())
            itemView.image_time_companion.text = convertTime(msg.date!!)
            val date = convertDate(msg.date!!)
            if(mData.size == 1) {
                bindDate(date, itemView.image_date_companion)
            } else if(mData.size > 1) {
                try {
                    if(formatter.parse(mData[position].date).after(formatter.parse(mData[position - 1].date))) {
                        bindDate(date, itemView.image_date_companion)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    bindDate(date, itemView.image_date_companion)
                }
            }



            itemView.setOnClickListener{clickImageCallback(msg.photo!!)}
        }
    }

    internal inner class ViewHolderImageMy(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(msg: Message, clickImageCallback:(String) -> Unit, position: Int) {

            setPhoto(itemView.image_my, msg.photo.toString())
            itemView.image_time_my.text = convertTime(msg.date!!)
            val date = convertDate(msg.date!!)
            if(mData.size == 1) {
                bindDate(date, itemView.image_date_my)
            } else if(mData.size > 1) {
                try {
                    if(formatter.parse(mData[position].date).after(formatter.parse(mData[position - 1].date))) {
                        bindDate(date, itemView.image_date_my)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    bindDate(date, itemView.image_date_my)
                }
            }
            itemView.setOnClickListener{clickImageCallback(msg.photo!!)}
        }
    }

    internal inner class ViewHolderMsgMy(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bind(msg: Message, position: Int) {

            itemView.message_my.text = msg.text.toString().replace("\'","")
            itemView.time_message_my.text = convertTime(msg.date!!)

            val date = convertDate(msg.date!!)
            if(mData.size == 1) {
                bindDate(date, itemView.txt_date_my)
            } else if(mData.size > 1) {
                try {
                    if(formatter.parse(mData[position].date).after(formatter.parse(mData[position - 1].date))) {
                        bindDate(date, itemView.txt_date_my)
                    }
                } catch (e: IndexOutOfBoundsException) {
                    bindDate(date, itemView.txt_date_my)
                }
            }

            if(msg.statusMessage == 0) {
                itemView.time_message_my.setCompoundDrawablesWithIntrinsicBounds(null, null, context.resources.getDrawable(
                    R.drawable.ic_check_symbol), null)
            } else if(msg.statusMessage == 1) {
                itemView.time_message_my.setCompoundDrawablesWithIntrinsicBounds(null, null, context.resources.getDrawable(
                    R.drawable.ic_double_check), null)
            }
        }
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    private fun convertDate(date: String) =  date.substring(0, date.indexOf('T'))
    private fun convertTime(date: String) =  date.substring(date.indexOf('T') + 1, date.indexOf('.') - 3)

    @SuppressLint("SetTextI18n")
    private fun bindDate(date: String, view: TextView) {

       view.visibility = View.VISIBLE
       view.text = "${date.substring(date.length - 2, date.length)} ${getMonth(date.substring(5, 7).toInt())} ${date.substring(0, 4)}"
       currentDate = date

    }

    private fun getMonth(month: Int): String {
        return DateFormatSymbols().months[month - 1]
    }
}