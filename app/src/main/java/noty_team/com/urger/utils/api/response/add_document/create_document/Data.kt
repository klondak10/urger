package noty_team.com.urger.utils.api.response.add_document.create_document

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("preview")
	val preview: String? = null,

	@field:SerializedName("file")
	val file: String? = null,

	@field:SerializedName("owner_id")
	val ownerId: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val documentId: Int? = null,

	@field:SerializedName("owner_type")
	val ownerType: String? = null
)

