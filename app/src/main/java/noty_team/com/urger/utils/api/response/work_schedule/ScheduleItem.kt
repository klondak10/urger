package noty_team.com.urger.utils.api.response.work_schedule

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class ScheduleItem(

		@field:SerializedName("key")
		val key: String = "",

		@field:SerializedName("value")
		val value: String = ""

)