package noty_team.com.urger.utils.api.response.upload_company_logo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class UploadCompanyLogoResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse() {
	fun parseCompanyLogo() {
		val company = PaperIO.companyData
		company?.logo = data?.logo

		PaperIO.companyData = company
	}
}