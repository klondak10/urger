package noty_team.com.urger.utils.api.response.get_employer

import android.util.Log
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.model.CompanyData
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class GetEmployerInfoResponse(

	@field:SerializedName("data")
	val data: Data? = null
):BaseResponse(){

	fun parseCompany() {
		PaperIO.companyData = CompanyData(data?.validatedCompany?.id ?: -1,
				data?.validatedCompany?.name ?: "def",
				data?.validatedCompany?.address ?: "undef",
				data?.employer?.position ?: "Some position",
				data?.validatedCompany?.about ?: " ",
				data?.validatedCompany?.nip ?: "",
				data?.validatedCompany?.logo ?: ""
		)

	}

	fun parseUser() {
		PaperIO.employerData = UserUrgerData(
				data?.employer?.id ?: -1,
				data?.employer?.phone,
				data?.employer?.firstName,
				data?.employer?.lastName,
				data?.employer?.email,
				data?.employer?.type.toString(),
				data?.employer?.avatar,
				data?.employer?.background,
				rate = data?.rate ?: 0.0F
		)
	}

	fun isCanUpdate() {
		PaperIO.canEditProfile = data?.employer?.canUpdate ?: false
	}

}