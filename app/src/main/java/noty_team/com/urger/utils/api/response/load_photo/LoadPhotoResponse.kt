package noty_team.com.urger.utils.api.response.load_photo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

@Generated("com.robohorse.robopojogenerator")
data class LoadPhotoResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse() {
	fun parseImageUrl() {

		if (data?.image != null) {
			if(PaperIO.userRole == UserRole.COMPANY) {
				val user = PaperIO.employerData
				user?.avatar = data.image

				PaperIO.employerData = user
			} else {
				val user = PaperIO.executorData
				user?.avatar = data.image
				PaperIO.executorData = user
			}
		}
	}
}