package noty_team.com.urger.utils.api.response.sms_checked

enum class UserRole {
    COMPANY, EXECUTOR
}