package noty_team.com.urger.utils.api.response.get_user_personal_avatar

import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import javax.annotation.Generated


@Generated("com.robohorse.robopojogenerator")
data class UserPersonalAvatarResponse(

		@field:SerializedName("data")
		val data: UserImageUrl? = null

) : BaseResponse() {
	fun parseImageUrl() {

		if (data?.image != null) {
			if(PaperIO.userRole == UserRole.COMPANY) {
				if (PaperIO.employerData != null) {
					val user = PaperIO.employerData
					user?.avatar = data.image

					PaperIO.employerData = user
				} else {

				}
			} else {
				if (PaperIO.executorData != null) {
					val user = PaperIO.executorData
					user?.avatar = data.image

					PaperIO.executorData = user
				}
			}
		}
	}
}