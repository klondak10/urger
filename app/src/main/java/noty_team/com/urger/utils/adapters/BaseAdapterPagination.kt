package noty_team.com.urger.utils.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.list_item_error.*
import noty_team.com.urger.R
import noty_team.com.urger.ui.adapters.items_adapter.PushItem
import noty_team.com.urger.ui.adapters.recycler.BaseAdapter
import noty_team.com.urger.utils.Constants
import java.util.*
import kotlin.NoSuchElementException
import kotlin.collections.ArrayList

abstract class BaseAdapterPagination<T : BaseUiPaginationItem>(
		list: ArrayList<T>,
		private var recyclerView: RecyclerView
) : BaseAdapter<T, BaseAdapter.BaseViewHolder>(list) {

	override fun getItemViewType(position: Int): Int {

		return when (list[position].itemPaginationType) {
			Constants.PaginationItemType.MAIN_ITEM -> itemLayoutResourceId
			Constants.PaginationItemType.LOADING_ITEM -> R.layout.list_item_loading
			Constants.PaginationItemType.ERROR_ITEM -> R.layout.list_item_error
		}
	}

	override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
		holder.bind(position)
	}

	override fun createViewHolder(view: View, layoutId: Int): BaseViewHolder {
		return when (layoutId) {
			R.layout.list_item_loading -> LoadingViewHolder(view)
			R.layout.list_item_error -> ErrorViewHolder(view)
			else -> MainViewHolder(view)
		}
	}

	abstract fun onDataLoaded(listSize: Int)
	abstract fun onInit(pos: Int, viewHolder: MainViewHolder)
	abstract fun onBind(pos: Int, viewHolder: MainViewHolder)
	abstract val itemLayoutResourceId: Int

	inner class MainViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {
		init {
			try {
				onInit(adapterPosition, this)
			} catch (e: Exception) {
			}
		}

		override fun bind(pos: Int) {
			onBind(pos, this)

			if (pos == itemCount - 1 && !isLoading() && !isError())
				addElements()
		}
	}


	private var endOfList = false
	private var isLoadingStarted = false

	fun addElements() {
		if (!endOfList) {
			val itemsCount = list.filter { it.itemPaginationType == Constants.PaginationItemType.MAIN_ITEM }
			showLoading()
			onDataLoaded(itemsCount.size)
		}
	}

	open fun onNewDataReceived(data: ArrayList<T>) {
		if (isLoadingStarted) {
			clear()
			isLoadingStarted = false
		}
		clear()
		hideLoading()
		endOfList = data.size == 0
		addAll(data.iterator())
	}

	open fun onNewDataReceivedWithReverse(data: ArrayList<T>, limit: Int) {
		if (isLoadingStarted) {
			clear()
			isLoadingStarted = false
		}
		clear()
		hideLoading()
		endOfList = data.size < limit
		data.reverse()
        addAllList(data)
	}

	fun onNewDataReceived(data: ArrayList<T>, limit: Int) {
		if (isLoadingStarted) {
			clear()
			isLoadingStarted = false
		}
		clear()
		hideLoading()
		endOfList = data.size < limit
		addAllList(data)
	}

	fun onDeleteItem(position: Int) {
		notifyItemRemoved(position)
	}

	fun getSizeList():Int {
		return list.size
	}

	fun addAllList(data: ArrayList<T>) {
		list.addAll(data)
		notifyDataSetChanged()
	}

	override fun addAll(addIterator: Iterator<T>) {
		while (addIterator.hasNext()) {
			val item = addIterator.next()

			if (!list.contains(item))
				list.add(item)

			notifyItemInserted(list.size - 1)
		}
	}

	private fun addItem(item: T) {
		recyclerView.post {
			list.add(item)
			notifyItemInserted(list.size - 1)
			if (isError()) recyclerView.smoothScrollToPosition(itemCount)
		}
	}

	private fun isLoading(): Boolean {
		return try {
			return list.last().itemPaginationType == Constants.PaginationItemType.LOADING_ITEM
		} catch (e: NoSuchElementException) {
			false
		}
	}

	private fun isError(): Boolean {
		return try {
			list.last().itemPaginationType == Constants.PaginationItemType.ERROR_ITEM
		} catch (e: NoSuchElementException) {
			false
		}
	}

	private fun isError(index: Int): Boolean {
		return try {
			list[index].itemPaginationType == Constants.PaginationItemType.ERROR_ITEM
		} catch (e: NoSuchElementException) {
			false
		} catch (e: IndexOutOfBoundsException) {
			false
		}
	}

	private fun showLoading() {
		if (isError()) {
			try {
				remove(list.last())
			} catch (e: NoSuchElementException) {
			}
		}
		if (!isLoading()) {
//			Log.i("TEST", "add load item")
//			val item = BaseUiPaginationItem(Constants.PaginationItemType.LOADING_ITEM) as T
//			addItem(item)
		} else {

		}
	}

	fun addNewItemInStart(item: T) {
		hideLoading()

		recyclerView.post {
			list.add(0, item)
			notifyItemInserted(0)
			if (isError()) {
				recyclerView.smoothScrollToPosition(0)
			}
		}
	}

	private fun hideLoading() {
		if (isLoading()) {
			try {
				remove(list.last())
			} catch (e: NoSuchElementException) {
			}
		}
	}

	fun showError() {
		if (isLoading()) {
			try {
				remove(list.last())
			} catch (e: NoSuchElementException) {
			}
		}

		hideError()

		if (!isError()) {
			val item = BaseUiPaginationItem(Constants.PaginationItemType.ERROR_ITEM) as T
			addItem(item)
			addAll(arrayListOf(item).iterator())
		}
	}

	private fun hideError() {
		if (isError()) {
			try {
				remove(list.last())
			} catch (e: NoSuchElementException) {
			}
		}
	}

	fun onListRefresh() {
		list.clear()
		hideLoading()
		hideError()
		endOfList = false
		isLoadingStarted = true
		notifyDataSetChanged()
	}

	inner class LoadingViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {
		override fun bind(pos: Int) {
			val context = itemView.context
		}
	}

	inner class ErrorViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {
		override fun bind(pos: Int) {
			retry_button.setOnClickListener { addElements() }
			horizontal_divider.visibility = View.GONE
		}
	}
}