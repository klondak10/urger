package noty_team.com.urger.utils.api.response.register_company

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Employer(

	@field:SerializedName("company_id")
	val companyId: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("user")
	val user: User? = null,

	@field:SerializedName("employer_id")
	val employerId: Int? = null
)