package noty_team.com.urger.utils.api.response.chat

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class TranslateResponse(

	@field:SerializedName("responderId")
	val responderId: String? = null,

	@field:SerializedName("quotaFinished")
	val quotaFinished: Boolean? = null,

	@field:SerializedName("exception_code")
	val exceptionCode: Any? = null,

	@field:SerializedName("responseDetails")
	val responseDetails: String? = null,

	@field:SerializedName("responseData")
	val responseData: ResponseData? = null,

	@field:SerializedName("responseStatus")
	val responseStatus: Int? = null,

	@field:SerializedName("matches")
	val matches: List<MatchesItem?>? = null,

	@field:SerializedName("mtLangSupported")
	val mtLangSupported: Any? = null
)