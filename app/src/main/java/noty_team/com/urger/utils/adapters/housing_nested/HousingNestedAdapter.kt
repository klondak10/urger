package noty_team.com.urger.utils.adapters.housing_nested

import android.view.View
import kotlinx.android.synthetic.main.item_housing.*
import noty_team.com.urger.R
import noty_team.com.urger.ui.adapters.recycler.BaseAdapter
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.setPhoto

class HousingNestedAdapter (
    listDocumentItems: ArrayList<HousingItem>,
    val onClickItem: (housingItem: HousingItem) -> Unit = {},
    val onLongClick: (housingItem: HousingItem) -> Unit = {}
) :
    BaseAdapter<HousingItem, HousingNestedAdapter.ViewHolder>(listDocumentItems) {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.item_housing

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {

        init {
            itemView.setOnClickListener {
                onClickItem(list[position])
            }
            itemView.setOnLongClickListener {

                onLongClick(list[position])

                return@setOnLongClickListener true
            }
        }

        override fun bind(pos: Int) {
            setPhoto(item_image_house, list[pos].preview)
            housing_item_location_tv.text = list[pos].address
            housing_description_tv.text = list[pos].description
        }
    }
}