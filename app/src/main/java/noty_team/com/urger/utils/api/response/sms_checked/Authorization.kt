package noty_team.com.urger.utils.api.response.sms_checked

enum class Authorization {
    NEED_LOGIN_OR_SING_UP, NEED_SING_UP, ALREADY_LOGIN, CHOOSE_ROLE, CHECK_SMS
}