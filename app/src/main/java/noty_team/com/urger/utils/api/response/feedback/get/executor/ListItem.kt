package noty_team.com.urger.utils.api.response.feedback.get.executor

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class ListItem(

	@field:SerializedName("employer_rate")
	val employerRate: String? = null,

	@field:SerializedName("employer_review")
	val employerReview: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("employer_date")
	val executor_date: String
)