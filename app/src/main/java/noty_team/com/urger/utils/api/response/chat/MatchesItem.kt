package noty_team.com.urger.utils.api.response.chat

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class MatchesItem(

	@field:SerializedName("reference")
	val reference: Any? = null,

	@field:SerializedName("usage-count")
	val usageCount: Int? = null,

	@field:SerializedName("create-date")
	val createDate: String? = null,

	@field:SerializedName("subject")
	val subject: String? = null,

	@field:SerializedName("segment")
	val segment: String? = null,

	@field:SerializedName("translation")
	val translation: String? = null,

	@field:SerializedName("match")
	val match: Double? = null,

	@field:SerializedName("last-update-date")
	val lastUpdateDate: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("last-updated-by")
	val lastUpdatedBy: String? = null,

	@field:SerializedName("created-by")
	val createdBy: String? = null,

	@field:SerializedName("quality")
	val quality: String? = null
)