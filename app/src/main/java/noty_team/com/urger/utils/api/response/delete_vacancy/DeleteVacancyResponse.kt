package noty_team.com.urger.utils.api.response.delete_vacancy

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItem

@Generated("com.robohorse.robopojogenerator")
data class DeleteVacancyResponse(

	@field:SerializedName("data")
	val data: String = ""

): BaseResponse()