package noty_team.com.urger.utils.api.response.comapny_photo

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CompanyPhotoResponse(

		@field:SerializedName("data")
		val data: Photos? = null

) : BaseResponse() {
	fun map(): ArrayList<CompanyPhotoItem> {

		val list = ArrayList<CompanyPhotoItem>()

		data?.photos?.forEach {
			list.add(CompanyPhotoItem(it))
		}

		return list
	}
}