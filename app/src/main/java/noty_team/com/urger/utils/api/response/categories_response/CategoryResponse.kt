package noty_team.com.urger.utils.api.response.categories_response

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class CategoryResponse(

        @field:SerializedName("data")
		val data: ArrayList<DataItem> = arrayListOf()

) : BaseResponse()