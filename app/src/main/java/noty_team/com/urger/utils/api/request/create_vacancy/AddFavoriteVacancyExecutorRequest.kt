package noty_team.com.urger.utils.api.request.create_vacancy

import com.google.gson.annotations.SerializedName

data class AddFavoriteVacancyExecutorRequest(@field:SerializedName("vacancy_id")
                                          val vacancy_id: Int? = null)