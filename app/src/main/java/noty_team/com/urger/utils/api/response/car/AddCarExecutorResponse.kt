package noty_team.com.urger.utils.api.response.car

import com.google.gson.annotations.SerializedName

data class AddCarExecutorResponse(
	@field: SerializedName("status_message")
	val statusMessage: String? = null,
	@field: SerializedName("status_code")
	val statusCode: Int? = null,
	@field: SerializedName("data")
	val data: DataItemAddCar? = null,
	@field: SerializedName("error")
	val error: String? = null
)
