package noty_team.com.urger.utils.api.response.wage_type

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class WageItem(

		@field:SerializedName("key")
		val key: String = "",

		@field:SerializedName("value")
		val value: String = ""

)