package noty_team.com.urger.utils.api.response.favorite_vacancy.favorite

import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class FavoriteVacancyItem(
    var id: Int = -1,
    var vacancy_id: Int = -1,
    var category_name: String ="",
    var parent_category: String = "",
    var position: String = "",
    var wage: Int = -1,
    var wage_currency: String = "",
    var work_schedule: String = "",
    var address: String = "",
    var house_id: Int? = null
) : BaseUiPaginationItem()