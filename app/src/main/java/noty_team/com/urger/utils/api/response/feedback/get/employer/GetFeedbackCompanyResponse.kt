package noty_team.com.urger.utils.api.response.feedback.get.employer

import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.feedback.get.executor.ListItem
import noty_team.com.urger.utils.api.response.feedback.get.executor.Total
import javax.annotation.Generated

@Generated("com.robohorse.robopojogenerator")
data class GetFeedbackCompanyResponse(

	@field:SerializedName("status_message")
	val statusMessage: String? = null,

	@field:SerializedName("status_code")
	val statusCode: Int? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: String? = null
) {
	fun map(): GetFeedbackExecutorResponse {

		val list: ArrayList<ListItem> = arrayListOf()
		data!!.list!!.forEach {
			list.add(ListItem(it.executorRate, it.executorReview, it.lastName, it.id, it.firstName, it.avatar, it.executorDate!!))
		}
		return GetFeedbackExecutorResponse(
					statusMessage = statusMessage,
					statusCode = statusCode,
					data = noty_team.com.urger.utils.api.response.feedback.get.executor.Data(Total(data.total!!.rate, data.total.count), list),
					error = error
				)
	}
}