package noty_team.com.urger.utils.adapters.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.thoughtbot.expandablecheckrecyclerview.models.SingleCheckExpandableGroup;

import java.util.List;

public class SingleCategory extends SingleCheckExpandableGroup implements Parcelable {

    public SingleCategory(String title, List items) {
        super(title, items);

    }

    protected SingleCategory(Parcel in) {
        super(in);

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SingleCategory> CREATOR = new Creator<SingleCategory>() {
        @Override
        public SingleCategory createFromParcel(Parcel in) {
            return new SingleCategory(in);
        }

        @Override
        public SingleCategory[] newArray(int size) {
            return new SingleCategory[size];
        }
    };
}
