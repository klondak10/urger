package noty_team.com.urger.utils.adapters.vacancy



import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.View
import android.widget.Filter
import android.widget.Filterable
import kotlinx.android.synthetic.main.item_vacancy.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.sms_checked.UserRole


class VacancyBottomAdapter(
    list: ArrayList<VacancyPaginationItem>,
    recyclerView: RecyclerView,
    var onLoadData: (offset: Int) -> Unit = {},
    var onClick: (data: VacancyPaginationItem) -> Unit = {},
    var onLongClick: (data: VacancyPaginationItem) -> Unit = {}
) : BaseAdapterPagination<VacancyPaginationItem>(list, recyclerView) {

    override fun getItemCount(): Int {
        return list.size
    }

    override val itemLayoutResourceId = noty_team.com.urger.R.layout.item_vacancy
    override fun onInit(pos: Int, viewHolder: MainViewHolder) {

        if(PaperIO.userRole == UserRole.EXECUTOR) {
            //viewHolder.hot_vacancy.visibility = View.GONE
            //viewHolder.edit_vacancy.visibility = View.GONE
        }

        viewHolder.apply {
            try {
                main_container_vacancy.setOnClickListener {
                    onClick(list[adapterPosition])
                }

                main_container_vacancy.setOnLongClickListener {
                    onLongClick(list[adapterPosition])
                    return@setOnLongClickListener true
                }
            } catch (e: Exception) {}
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            val spannable = SpannableString(list[pos].category + ", " + list[pos].parent_category)
            existHouse.visibility = if(list[pos].houseId != null) View.VISIBLE else View.GONE
            spannable.setSpan(
                StyleSpan(Typeface.BOLD),
                0, list[pos].category.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            vacancy_company_name_position.text = spannable

            vacancy_location.text = list[pos].address
            vacancy_type_employment.text = list[pos].workSchedule
            vacancy_price.text = "${list[pos].price} ${list[pos].currencySymbol}"
            vacancy_type_employment.text = list[pos].workSchedule

            // Save/restore the open/close state.
            // You need to provide a String id which uniquely defines the data object.
        }
    }

    fun addNewVacancies(item: ArrayList<VacancyPaginationItem>) {
        list.clear()
        list.addAll(item)
        notifyDataSetChanged()
    }

    fun loadMoreVacancies(item: ArrayList<VacancyPaginationItem>) {

        list.addAll(item)
        notifyDataSetChanged()
    }

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }
}
