package noty_team.com.urger.utils.api.response.work_schedule

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class WorkScheduleResponse(

        @field:SerializedName("data")
		val data: ArrayList<ScheduleItem> = arrayListOf()

) : BaseResponse()