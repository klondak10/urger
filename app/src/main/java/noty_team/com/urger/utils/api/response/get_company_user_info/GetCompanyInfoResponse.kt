package noty_team.com.urger.utils.api.response.get_company_user_info

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.BaseResponse

@Generated("com.robohorse.robopojogenerator")
data class GetCompanyInfoResponse(

		@field:SerializedName("data")
		val data: Data? = null

) : BaseResponse()