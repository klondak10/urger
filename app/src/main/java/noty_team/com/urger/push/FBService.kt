package noty_team.com.urger.push

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.paperdb.Paper
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.ui.activity.splash.SplashActivity
import noty_team.com.urger.ui.adapters.items_adapter.PushItem
import java.text.SimpleDateFormat
import java.util.*


class FBService : FirebaseMessagingService() {

    var listPush:ArrayList<PushItem> = arrayListOf()

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        /**Creates an explicit intent for an Activity in your app */
        var resultIntent = Intent()
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if(remoteMessage.data.get("type").toString() == "4") {

            resultIntent = Intent(this, SplashActivity::class.java)
            val id = remoteMessage.data.get("id")!!
            PaperIO.idCompany = id.toInt()
        } else {
            resultIntent = Intent(this, SplashActivity::class.java)
        }
        val resultPendingIntent = PendingIntent.getActivity(
            this,
            0 /* Request code */, resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val mBuilder = NotificationCompat.Builder(this, "Default")
        mBuilder.color = ContextCompat.getColor(this, R.color.colorRedRippleDarkTransparent)
        mBuilder.setSmallIcon(R.mipmap.logo)
        mBuilder.setLargeIcon(
            BitmapFactory.decodeResource(
                this.resources,
                R.mipmap.logo
            )
        )
        mBuilder.setContentTitle(remoteMessage.data.get("title").toString())
        mBuilder.setContentText(remoteMessage.data.get("body").toString())
        mBuilder.setAutoCancel(true)
        mBuilder.setContentIntent(resultPendingIntent)

        val mNotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel =
                NotificationChannel("Default channel", "NOTIFICATION_CHANNEL_NAME", importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = resources.getColor(android.R.color.transparent)
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                300,
                200,
                400
            )
            mBuilder.setChannelId("Default channel")
            mNotificationManager.createNotificationChannel(notificationChannel)
        }
        val notification = mBuilder.build();

        mNotificationManager.notify(0 /* Request Code */, notification)

        val notifications: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val r = RingtoneManager.getRingtone(applicationContext, notifications)
        r.play()

        val sdf = SimpleDateFormat("dd-MM-yyyy HH:mm")
        val currentDate = sdf.format(Date())

        if (remoteMessage.data.get("title").toString().contains("Новое сообщение")) {

            if (Paper.book().exist("listPush"+PaperIO.userPhoneNumber)){
                listPush = Paper.book().read("listPush"+PaperIO.userPhoneNumber)
            }
            listPush.add(PushItem(
                true,
                remoteMessage.data.get("title").toString(),
                remoteMessage.data.get("body").toString(),
                currentDate
            ))


        }else{

            if (Paper.book().exist("listPush"+PaperIO.userPhoneNumber)){
                listPush = Paper.book().read("listPush"+PaperIO.userPhoneNumber)
            }

            listPush.add(PushItem(
                false,
                remoteMessage.data.get("title").toString(),
                remoteMessage.data.get("body").toString(),
                currentDate
            ))

        }
        Paper.book().write("number"+PaperIO.userPhoneNumber,PaperIO.userPhoneNumber)
        Paper.book().write("listPush"+PaperIO.userPhoneNumber, listPush)


    }


}