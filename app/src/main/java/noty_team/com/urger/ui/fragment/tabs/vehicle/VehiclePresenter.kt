package noty_team.com.urger.ui.fragment.tabs.vehicle

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.response.car.DataItemGetCars

class VehiclePresenter(context: Context): BasePresenter(context) {

    fun getCarsPagination(listSize: Int,
                               onDocumentsSuccess: (cars: List<DataItemGetCars>) -> Unit) {
        val page = listSize / Constants.documentItemLimit
            apiEmployer.getCarsListExecutor( Constants.documentItemLimit, page, {
                    if(it.statusCode == 200 || it.statusCode == 201) {
                        onDocumentsSuccess(it.data!!)
                    }
                }, { errorMessage, errorCode ->
                    showShortToast("$errorCode $errorMessage")
                }).callRequest()

    }


    fun deleteCar(carId: Int, isDeleteCar: (isSuccess: Boolean) -> Unit) {

            apiEmployer.deleteCarExecutor(carId, {
                if (it.statusCode == 200 || it.statusCode == 201) {
                    isDeleteCar(true)
                } else {
                    isDeleteCar(false)
                }
            }, { errorMessage, errorCode ->
                showShortToast("$errorCode $errorMessage")
            }).callRequest()

    }
}