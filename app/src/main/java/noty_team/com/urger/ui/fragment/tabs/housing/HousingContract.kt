package noty_team.com.urger.ui.fragment.tabs.housing

import noty_team.com.urger.utils.adapters.housing.HousingItem

interface HousingContract {
	interface Presenter {
		fun getHousing(listSize: Int,
					   onDocumentsSuccess: (documents: ArrayList<HousingItem>) -> Unit,
					   showToasMassage: (massage: String) -> Unit)

		fun deleteHouse(houseId: Int, showToasMassage: (massage: String) -> Unit, onSuccess: (isDeleted: Boolean) -> Unit)
	}
}