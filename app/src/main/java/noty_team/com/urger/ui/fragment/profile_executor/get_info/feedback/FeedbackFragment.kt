package noty_team.com.urger.ui.fragment.profile_executor.get_info.feedback

import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_info_executor.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.FeedbackAdapter
import noty_team.com.urger.ui.fragment.profile_executor.DetailProfileExecutorPresenter
import noty_team.com.urger.utils.api.response.all_info.ReviewsItem
import noty_team.com.urger.utils.api.response.feedback.get.executor.ListItem

class FeedbackFragment : BaseFragment<DetailProfileExecutorPresenter>() {
	private lateinit var adapterFeedback: FeedbackAdapter
	var data: List<ReviewsItem?> = listOf()
	companion object {

		fun newInstance(): FeedbackFragment {
			return FeedbackFragment()
		}

	}


	override fun layout() = R.layout.fragment_info_executor

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = DetailProfileExecutorPresenter(baseContext)
		if(data.isNullOrEmpty()) {
			empty_list.visibility = View.VISIBLE
		} else {
			initAdapter(convertData())
		}
	}

	private fun initAdapter(list:ArrayList<ListItem>) {
		adapterFeedback = FeedbackAdapter(list, baseActivity)
		recycler_list.layoutManager =
            LinearLayoutManager(baseActivity)
		recycler_list.adapter = adapterFeedback
	}

	private fun convertData():ArrayList<ListItem> {
		val list: ArrayList<ListItem> = arrayListOf()
		data.forEach {
			val item = ListItem(it!!.employerRate.toString(), it.employerReview, it.lastName, "0", it.firstName, it.avatar, it.employerDate.toString())
			list.add(item)
		}
		return list
	}
}