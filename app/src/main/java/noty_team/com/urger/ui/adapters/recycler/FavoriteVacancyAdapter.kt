package noty_team.com.urger.ui.adapters.recycler

import android.graphics.Typeface.BOLD
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.View
import kotlinx.android.synthetic.main.item_favorite_vacancy.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.favorite_vacancy.favorite.FavoriteVacancyItem

class FavoriteVacancyAdapter(list: ArrayList<FavoriteVacancyItem>,
                             recyclerView: RecyclerView,
                             val onClick: (id: Int) -> Unit = {},
                             var onSwipeToDelete: (id: Int) -> Unit = {},
                             var onLoadData: (offset: Int) -> Unit = {}) :
	BaseAdapterPagination<FavoriteVacancyItem>(list, recyclerView) {
	override fun onDataLoaded(listSize: Int) {
		onLoadData(listSize)
	}

	override fun onInit(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			try {
				main_container__favorite_vacancy.setOnClickListener {
					onClick(list[adapterPosition].vacancy_id)
				}
				delete_from_fav.setOnClickListener {
					onSwipeToDelete(list[adapterPosition].id)
					list.removeAt(adapterPosition)
					onDeleteItem(adapterPosition)
				}

			} catch (e: Exception) {}
		}
	}

	override fun onBind(pos: Int, viewHolder: MainViewHolder) {
		val item = list[pos]
		viewHolder.apply {
			var spannable = SpannableString(item.category_name + ", " + item.parent_category)
			spannable.setSpan(
				StyleSpan(BOLD),
				0, item.category_name.length + 1,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
			)
			exist_house?.visibility = if(item.house_id!= null) View.VISIBLE else View.GONE
			favorite_vacancy_company_name_position.text = spannable
			favorite_vacancy_location.text = item.address
			favorite_vacancy_type_employment.text = item.work_schedule
			favorite_vacancy_price.text = "${item.wage} ${item.wage_currency}"
		}

	}

	override val itemLayoutResourceId: Int = R.layout.item_favorite_vacancy

}