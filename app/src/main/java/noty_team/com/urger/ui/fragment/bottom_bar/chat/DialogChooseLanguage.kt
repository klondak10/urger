package noty_team.com.urger.ui.fragment.bottom_bar.chat

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.util.Log
import io.paperdb.Paper
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R


class DialogChooseLanguage : DialogFragment() {
    val DATA = "items"

    val SELECTED = "selected"

    var callbackChooseLanguage: (pos: Int) -> Unit = {}
    var position: Int = 6

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val bundle = arguments

        val dialog = AlertDialog.Builder(activity)
        dialog.setTitle(getString(R.string.please_select))
        dialog.setPositiveButton(R.string.cancel, PositiveButtonClickListener())

        val list = bundle!!.get(DATA) as List<String>

        val cs = list.toTypedArray()
        dialog.setSingleChoiceItems(cs, position, selectItemListener)

        return dialog.create()
    }

    internal inner class PositiveButtonClickListener : DialogInterface.OnClickListener {
        override fun onClick(dialog: DialogInterface, which: Int) {
            dialog.cancel()
            dialog.dismiss()
        }
    }

    private var selectItemListener: DialogInterface.OnClickListener =
        DialogInterface.OnClickListener { dialog, which ->
            run {
                dialog?.dismiss()
                dialog.cancel()
                callbackChooseLanguage(which)
            }
    }
}