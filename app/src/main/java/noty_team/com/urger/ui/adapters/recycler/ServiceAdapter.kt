package noty_team.com.urger.ui.adapters.recycler

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_notification.*
import kotlinx.android.synthetic.main.item_servise.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.center.Services

class ServiceAdapter(list: ArrayList<Services>,
                     recyclerView: RecyclerView,
                     var onLoadData: (offset: Int) -> Unit = {}) :
	BaseAdapterPagination<Services>(list, recyclerView) {

	override fun onDataLoaded(listSize: Int) {
		onLoadData(listSize)
	}

	override fun onInit(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			try {

				itemView.setOnLongClickListener {
					return@setOnLongClickListener true
				}

			} catch (e: Exception) {}
		}
	}

	override fun onBind(pos: Int, viewHolder: MainViewHolder) {
		val item = list[pos]
		viewHolder.apply {


			if (item.name!!.contains("-")){
				name_service.text = item.name.substring(0,item.name.lastIndexOf("-"))
				name_service_sub.text = item.name.substring(item.name.indexOf("-"),item.name.length-1).replace(",","\n\n-")
			}else{
				name_service.text = item.name
				name_service_sub.text = ""
			}
			Glide.with(image_service.context)
				.load("https://${item.image}")
				.into(image_service)
		}

	}

	override val itemLayoutResourceId: Int = R.layout.item_servise

}