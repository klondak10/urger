package noty_team.com.urger.ui.adapters.view_pager


import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import android.view.ViewGroup
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.adapters.CustomViewPager


abstract class BasePagerAdapter(val fm: FragmentManager,
                                val pager: CustomViewPager,
                                val fragments: ArrayList<BaseFragment<*>>) : FragmentPagerAdapter(fm) /*FragmentPagerAdapter(fm)*/ {
	val context: Context = pager.context

	private fun getVisibleCallback(fragment: Fragment): (fragment: Fragment) -> Boolean {
		return {
			val fragmentIndex = fragments.indexOf(fragment)
			fragmentIndex == pager.currentItem
		}
	}

	init {
		fragments.forEach {
			it.isVisible = getVisibleCallback(it)
		}
	}

	override fun instantiateItem(container: ViewGroup, position: Int): Any {
		val fragment = super.instantiateItem(container, position) as BaseFragment<*>
		fragments[position] = fragment
		return fragment
	}

	fun destroyAllFragment() {
		fragments.forEach {
			fm.beginTransaction().remove(it).commit()
		}
		fragments.clear()
		notifyDataSetChanged()

	}

	override fun getItemPosition(`object`: Any): Int {
		return PagerAdapter.POSITION_NONE
	}

	override fun getItem(position: Int) = fragments[position]

	override fun getCount() = fragments.size

	override fun getPageTitle(position: Int) = getTitle(position)

	abstract fun getTitle(pageIndex: Int): String
}