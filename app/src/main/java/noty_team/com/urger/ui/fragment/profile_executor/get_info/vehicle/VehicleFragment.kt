package noty_team.com.urger.ui.fragment.profile_executor.get_info.vehicle

import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_info_executor.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.VehicleAdapter
import noty_team.com.urger.ui.fragment.profile_executor.DetailProfileExecutorPresenter
import noty_team.com.urger.utils.api.response.all_info.AutosItem
import noty_team.com.urger.utils.api.response.car.DataItemGetCars

class VehicleFragment : BaseFragment<DetailProfileExecutorPresenter>() {
	var data: List<AutosItem?> = listOf()
	lateinit var adapterVehicle: VehicleAdapter
	companion object {

		fun newInstance(): VehicleFragment {
			return VehicleFragment()
		}

	}

	override fun layout() = R.layout.fragment_info_executor

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = DetailProfileExecutorPresenter(baseContext)
		if(data.isNullOrEmpty()) {
			empty_list.visibility = View.VISIBLE
		} else {
			initAdapter(convertData())
		}
	}

	private fun initAdapter(list: ArrayList<DataItemGetCars>) {
		recycler_list.layoutManager =
            LinearLayoutManager(baseActivity)
		adapterVehicle = VehicleAdapter(
			list, baseActivity, recycler_list
		)
		recycler_list.adapter = adapterVehicle
	}

	private fun convertData(): ArrayList<DataItemGetCars> {
		val list: ArrayList<DataItemGetCars> = arrayListOf()
		data.forEach {
			val item = DataItemGetCars(it!!.file, it.color, it.userId, it.fuel, it.model, it.id, it.issued)
			list.add(item)
		}
		return list
	}
}