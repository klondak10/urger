package noty_team.com.urger.ui.fragment.bottom_bar.container

import android.os.Bundle
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.bottom_bar.ContainerFragment
import noty_team.com.urger.ui.fragment.profile_company.ProfileCompanyFragment
import noty_team.com.urger.ui.fragment.profile_executor.ProfileExecutorFragment
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.cicerone.Screens

class ProfileFragmentContainer : ContainerFragment() {

    companion object {
        fun getNewInstance(name: String): ProfileFragmentContainer {
            val fragment = ProfileFragmentContainer()

            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.setArguments(arguments)

            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(
                if (PaperIO.userRole == UserRole.COMPANY) Screens.FragmentScreen(ProfileCompanyFragment.newInstance())
                else Screens.FragmentScreen(ProfileExecutorFragment.newInstance())
            )
        }
    }
}