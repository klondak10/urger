package noty_team.com.urger.ui.fragment.login.choose_role.company

import noty_team.com.urger.utils.api.request.create_company.CreateCompanyRequest

interface CompanyInfoContract {
    interface Presenter {
        fun createCompanyRequest(
            createCompanyRequest: CreateCompanyRequest,
            openAppCallback: (isOpen: Boolean) -> Unit
        )
    }
}