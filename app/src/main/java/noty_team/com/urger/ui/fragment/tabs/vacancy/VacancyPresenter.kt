package noty_team.com.urger.ui.fragment.tabs.vacancy

import android.content.Context
import android.util.Log
import io.reactivex.Single
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.currency.CurrencyResponse
import noty_team.com.urger.utils.api.response.delete_vacancy.DeleteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.AddFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.api.response.work_schedule.WorkScheduleResponse

class VacancyPresenter(context: Context): BasePresenter(context){

    private var page: Int = 1

    fun getVacancies(listSize: Int, onSuccess:(response: VacanciesListResponse)->Unit, onError:()->Unit){
        val page = listSize / Constants.documentItemLimit
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getVacancies(Constants.vacancyItemLimit, page, {
                onSuccess(it)
            }, { errorMessage, errorCode ->
                onError()

                //showLongToast(errorMessage)
            }).callRequest()
        } else {
            apiEmployer.getVacanciesForExecutor(Constants.vacancyItemLimit, page,{
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun addFavoriteVacancy(request: AddFavoriteVacancyExecutorRequest, onSuccess:(response: AddFavoriteVacancyResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
        } else {
            apiEmployer.addVacancyFavorite(request, {
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun deleteVacancy(vacancyId: Int, onSuccess:(response: DeleteVacancyResponse)->Unit, onError:()->Unit){
        apiEmployer.deleteVacancy(vacancyId,{
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getWorkSchedule(onSuccess:(response: WorkScheduleResponse)->Unit, onError:()->Unit) {
        apiEmployer.getWorkSchedule({
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getCurrency(onSuccess:(response: CurrencyResponse)->Unit, onError:()->Unit) {
        apiEmployer.getCurrency({
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun loadMoreVacancy(onSuccess:(response: VacanciesListResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getVacancies(Constants.vacancyItemLimit, page, {
                onSuccess(it)
                ++page
            }, { errorMessage, errorCode ->
                onError()

            }).callRequest()
        } else {
            apiEmployer.getVacanciesForExecutor(Constants.vacancyItemLimit, page,{
                onSuccess(it)
                ++page
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }
}