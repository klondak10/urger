package noty_team.com.urger.ui.fragment.tabs.housing.house_detail

import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.api.response.get_house.GetHouseResponse

interface HouseDetailContract {
    interface Presenter {
        fun getHouseItem(houseId: Int, companyId: Int, showToast: (it: String) -> Unit, onDataLoad: (house: DataMapResponse) -> Unit)
    }
}