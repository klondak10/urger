package noty_team.com.urger.ui.fragment.bottom_bar.container

import android.os.Bundle
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.bottom_bar.ContainerFragment
import noty_team.com.urger.ui.fragment.bottom_bar.notification.NotificationFragment
import noty_team.com.urger.utils.cicerone.Screens

class NotificationFragmentContainer : ContainerFragment() {

    companion object {
        fun getNewInstance(name: String): NotificationFragmentContainer {
            val fragment = NotificationFragmentContainer()

            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.setArguments(arguments)

            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(Screens.FragmentScreen(NotificationFragment.newInstance()))
        }
    }
}