package noty_team.com.urger.ui.activity.splash

import android.content.Intent
import android.util.Log
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.push.FBService
import noty_team.com.urger.ui.activity.FeedbackExecutorActivity
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.ui.activity.login.LoginActivity
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class SplashActivity : BaseActivity() {

    lateinit var splashPresenter: SplashContract.Presenter

    override fun layout() = R.layout.activity_splash

    override fun initialization() {

            splashPresenter = SplashPresenter(context)

            splashPresenter.serverRequest {
                if (it) openApp()
                else openLoginApp()

        }
    }

    private fun openLoginApp() {
        LoginActivity.start(this)
        finish()
    }

    private fun openApp() {

        if(PaperIO.idCompany != null && PaperIO.idCompany != 0 ) {
            val intent = Intent(this, FeedbackExecutorActivity::class.java)
            startActivity(intent)
        } else {
            if (PaperIO.userRole == UserRole.COMPANY) {
                CompanyActivity.start(this)
                finish()
            } else {
                ExecutorActivity.start(this)
                finish()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        splashPresenter.dispose()
    }

    override fun getDrawerLayout() = null
    override fun getNavigationView() = null
    override fun getBottomNavigationView() = null
    override fun openDrawerLayout() {}

}