package noty_team.com.urger.ui.fragment.tabs.company

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.drjacky.imagepicker.ImagePicker
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_company.*
import kotlinx.android.synthetic.main.fragment_profile_nested_company_section.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.company.company_photo.CompanyPhotoFragment
import noty_team.com.urger.ui.fragment.tabs.company.edit_profile.company_profile_info.CompanyProfileInfoFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.PhotoAdapter
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.camera.FilesPicker
import java.io.File

class CompanyFragment : BaseFragment<CompanyPresenter>() {

	private var fileData = ArrayList<File>()
	companion object {
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.companyData"
		private const val ID_COMPANY = "companyId"
		private const val NAME_COMPANY = "companyName"
		private const val LOGO_COMPANY = "companyName"
		private const val ABOUT_COMPANY = "companyName"
		var isNeedUpdateProfile = false
		var isNeedUpdateCompanyLogo = true

		fun newInstance(isShowToolbar: Boolean, companyId: Int = PaperIO.companyData!!.companyId): CompanyFragment {

			return CompanyFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
					putInt(ID_COMPANY, companyId)
				}
			}
		}

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}

	private lateinit var photoAdapter: PhotoAdapter

	override fun layout() = R.layout.fragment_company

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = CompanyPresenter(baseContext)

		if(PaperIO.userRole == UserRole.EXECUTOR) {
			add_photo_company.visibility = View.GONE
		}

		if (isFirstInit) {
			isNeedUpdateProfile = false
			initData()
			initCompanyLogo()
		} else {
			if (isNeedUpdateProfile || isNeedUpdateCompanyLogo) {
				isNeedUpdateProfile = false
				initData()
			} else {
				initAction()
				initCompanyData()
			}
		}

		global_company_description.setOnClickListener {
			if(PaperIO.userRole == UserRole.COMPANY) {
				addFragment(CompanyProfileInfoFragment.newInstance())
			}
		}
	}

	private fun initData() {
		initCompanyRecycler()
		initToolbar()
		initAction()
		initCompanyData()
	}

	private fun initCompanyRecycler() {

		recycler_company_photo.layoutManager = LinearLayoutManager(baseActivity, RecyclerView.HORIZONTAL, false)
		recycler_company_photo.isNestedScrollingEnabled = false

		loadCompanyPhotoList()

		this.photoAdapter = PhotoAdapter(arrayListOf(), onClick = { index ->

			addFragment(CompanyPhotoFragment.newInstance().apply {
				this.index = index
				listImages = photoAdapter.getItemsList()
			})

		}, onLongClick = { _it, _index ->
			run {

				deleteDialog(getString(R.string.delete_company_image_text)) {
					val photoName = _it.image.substring(_it.image.indexOfLast { it == '/' } + 1)
					presenter?.deleteCompanyPhoto(photoName, {
						showShortToast(it)
					}, {
						if (it) {
							NestedProfileCompanyFragment.isNeedUpdate = true
							isNeedUpdateProfile = true

							photoAdapter.remove(_it)
							showShortToast(resources.getString(R.string.photo_success_delete))

						} else {
							showShortToast(getString(R.string.error_delete))

						}
					})
				}
			}
		})


		recycler_company_photo.adapter = this.photoAdapter
	}


	private fun loadCompanyPhotoList() {
		val companyId = arguments?.getInt(ID_COMPANY) ?: PaperIO.companyData!!.companyId
		presenter?.getCompanyPhotos(companyId, {
			this.photoAdapter.addAll(it)
		}, {
			baseActivity.showShortToast(Constants.DISPOSABLE_ERROR)
		})
	}

	private fun initCompanyData() {

		if (isNeedUpdateCompanyLogo) {
			initCompanyLogo()
			isNeedUpdateCompanyLogo = false
		}

		//initCompanyLogo()
		initCompanyInfo()
		initCountDataCompany()
	}

	private fun initCountDataCompany() {
		val companyId = arguments?.getInt(ID_COMPANY) ?: PaperIO.companyData!!.companyId
		presenter?.getStatistic(companyId, {
			showShortToast(it)
		}, { countDocument, countHousing, countVacancies ->

			company_count_document.text = countDocument
			company_count_vacancy.text = countVacancies
			company_count_housing.text = countHousing

		})
	}

	@SuppressLint("SetTextI18n")
	private fun initCompanyInfo() {
		//init company name
		//global_company_name.text = "in " + PaperIO.companyData?.name

		toolbar_text.text = "${PaperIO.employerData!!.firstName} ${PaperIO.employerData!!.lastName}"

		//initPosition
        if (Paper.book().exist("about")){
			global_company_description.text = Paper.book().read("about")
			Paper.book().delete("about")
		}else{
			global_company_description.text = PaperIO.companyData?.description
		}

		//location
		global_company_location.text = PaperIO.companyData?.city

	}

	private fun initCompanyLogo() {

		if (PaperIO.companyData?.logo != null && PaperIO.companyData?.logo != "") {
			company_logo_img.visibility = View.VISIBLE
			company_logo_tv.visibility = View.GONE
			setPhoto(company_logo_img, PaperIO.companyData?.logo ?: "")
		} else {
			company_logo_img.visibility = View.GONE
			company_logo_tv.visibility = View.VISIBLE
			company_logo_tv.text = PaperIO.companyData?.name?.substring(0, 1)
		}
	}

	private fun initToolbar() {

		if (isShowToolbar(arguments)) {
			company_fragment_toolbar_container.visibility = View.VISIBLE
		} else {
			company_fragment_toolbar_container.visibility = View.GONE
		}
	}

	private fun initAction() {

		nb_fragment_company.setOnClickListener {
			onBackPressed()
		}


		company_personal_info.setOnClickListener {
			if(PaperIO.userRole == UserRole.COMPANY) {
				addFragment(CompanyProfileInfoFragment.newInstance())
			}
		}

		add_photo_company.getClick {
			showDialogChoose().show()
		}

	}

	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				fileData.addAll(mutableListOf(File(uri.path.toString())))
				presenter?.addCompanyPhotos(fileData,
					dataList = {
						showShortToast(resources.getString(R.string.add_photo_company_success))
						NestedProfileCompanyFragment.isNeedUpdate = true
						isNeedUpdateProfile = true
						photoAdapter.addAllInStart(it)
						recycler_company_photo.smoothScrollToPosition(0)
					})


			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				fileData.addAll(mutableListOf(File(uri.path.toString())))
				presenter?.addCompanyPhotos(fileData,
					dataList = {
						showShortToast(resources.getString(R.string.add_photo_company_success))
						NestedProfileCompanyFragment.isNeedUpdate = true
						isNeedUpdateProfile = true
						photoAdapter.addAllInStart(it)
						recycler_company_photo.smoothScrollToPosition(0)
					})

			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}


	private fun setPhoto(imageId: ImageView, url: String) {
		if (url != "")
			Glide.with(baseActivity)
					.load(url)
					.apply(
							RequestOptions()
									.override(250, 250)
									.encodeQuality(50)
									.fitCenter()
									.skipMemoryCache(true)
									.diskCacheStrategy(DiskCacheStrategy.NONE)
					)
					.into(imageId)
	}
}