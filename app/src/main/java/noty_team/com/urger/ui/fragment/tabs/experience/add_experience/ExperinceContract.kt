package noty_team.com.urger.ui.fragment.tabs.experience.add_experience

import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem

interface ExperinceContract {
	interface View
	interface Presenter {
		fun getExperincePagination(listSize: Int, onExperinceSuccess: (documents: ArrayList<DataItem>) -> Unit)

	}
}