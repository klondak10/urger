package noty_team.com.urger.ui.fragment.tabs.company.edit_profile.edit_personal_data

interface EditPersonalDataContract {
    interface Presenter {
        fun editUserInfo(
            firstName: String,
            lastName: String,
            email: String,
            idCategory: Array<Int>,
            position: String,
            isSuccess: (isUserEdit: Boolean) -> Unit
        )
    }
}