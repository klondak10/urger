package noty_team.com.urger.ui.fragment.profile_company.get_info.tabs

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_houses.*
import kotlinx.android.synthetic.main.fragment_housing.*
import kotlinx.android.synthetic.main.fragment_housing.empty_list
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.get_info.DetailProfileCompany
import noty_team.com.urger.ui.fragment.tabs.housing.HousingPresenter
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.HouseDetailFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.housing.HousingAdapterPagination

class InfoHouseFragment : BaseFragment<InfoHousePresenter>() {


    companion object {

        fun newInstance(companyId: Int): InfoHouseFragment {
            return InfoHouseFragment().apply {
                arguments = Bundle().apply {
                    putInt(DetailProfileCompany.ID_COMPANY, companyId)
                }
            }
        }
    }

    private lateinit var housingAdapterPagination: HousingAdapterPagination

    override fun layout() = R.layout.fragment_houses

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = InfoHousePresenter(baseContext)
        initRecycler()
        getHousing(0)

    }

    private fun getHousing(size: Int) {
        val id = arguments!![DetailProfileCompany.ID_COMPANY] as Int
        presenter?.getHousing(id, size, { response ->

            if (response.isEmpty()) {
                empty_list_tv.visibility = View.VISIBLE
            } else {
                empty_list_tv.visibility = View.GONE
            }
            housingAdapterPagination.onNewDataReceived(response, Constants.documentItemLimit)
        }, {
            //showShortToast(it)
        })
    }

    private fun initRecycler() {

        recycler_houses.layoutManager =
            LinearLayoutManager(baseActivity)

        housingAdapterPagination = HousingAdapterPagination(
            arrayListOf(), recycler_houses,

            onClick = { it_ ->

                run {
                    addFragment(HouseDetailFragment.newInstance().apply {
                        houseId = it_.houseId
                    })
                }
            }
        )

        recycler_houses.adapter = housingAdapterPagination

    }
}