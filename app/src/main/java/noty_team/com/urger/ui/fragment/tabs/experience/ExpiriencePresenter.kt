package noty_team.com.urger.ui.fragment.tabs.experience

import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.fragment.tabs.experience.add_experience.ExperinceContract
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem

class ExpiriencePresenter(context: Context): BasePresenter(context), ExperinceContract.Presenter{



    override fun getExperincePagination(listSize: Int,
        onExperinceSuccess: (documents: ArrayList<DataItem>) -> Unit)

    {
        val page = listSize / Constants.documentItemLimit

            apiEmployer.getExperincePagination(
                 Constants.documentItemLimit, page, {

                    onExperinceSuccess(it.map())

                }, { errorMessage, errorCode ->

                }).callRequest()

    }

    fun deleteExperienceExecutor(id: Int, isSuccessDelete: (b: Boolean) -> Unit) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.deleteExperienceExecutor(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        isSuccessDelete(true)
                    } else {
                        isSuccessDelete(false)
                        showShortToast(it.error!!)
                    }
                }, {

                })
        )
    }
}