package noty_team.com.urger.ui.fragment.tabs.experience

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_experience.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.ExperienceAdapter
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.experience.add_experience.AddExperienceFragment
import noty_team.com.urger.utils.api.response.experience_executor.AddExperienceExecutorResponse
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem


class ExperienceFragment : BaseFragment<ExpiriencePresenter>() {

	lateinit var adapterExperienceAdapter: ExperienceAdapter

	var onAddExperienceObservable: Observable<Unit>? = null

	companion object {
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.experience"

		fun newInstance(isShowToolbar: Boolean): ExperienceFragment {
			return ExperienceFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
				}
			}
		}

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}

	override fun layout() = R.layout.fragment_experience

	override fun initialization(view: View, isFirstInit: Boolean) {

		presenter = ExpiriencePresenter(baseContext)
		recycler_experience.layoutManager =
            LinearLayoutManager(baseActivity)
		adapterExperienceAdapter = ExperienceAdapter(
			arrayListOf(), baseActivity, recycler_experience,
			onLongClick = { _it ->
					run {
						deleteDialog(getString(R.string.delete_experience)) {
							adapterExperienceAdapter.remove(_it)
							deleteExperience(_it)
						}
					}
				}
			)
		recycler_experience.adapter = adapterExperienceAdapter

		if (isShowToolbar(arguments)) {
			experience_fragment_toolbar_container.visibility = View.VISIBLE
			experience_fragment_fab.show()
		}

		nb_fragment_executor.setOnClickListener {
			onBackPressed()
	//		backNavigator().navigateBack()
		}
		experience_fragment_fab.setOnClickListener {
			addFragment(AddExperienceFragment.newInstance())
			//navigator().navigateToFragment(AddExperienceFragment.newInstance(), null, true)
		}

		getExperiencesList(0)
	}

	private fun deleteExperience(_it: DataItem) {
		presenter?.deleteExperienceExecutor(_it.id!!) {
			if(it) {
				showShortToast(getString(R.string.success_delete))
			}
			if(adapterExperienceAdapter.itemCount == 0) {
				empty_list.visibility = View.VISIBLE
			}
		}
	}

	var callbackAddExperience: (item: AddExperienceExecutorResponse) -> Unit = {
		//isCallback = true
		onAddExperienceObservable = Observable.fromCallable { addNewExperience(it) }
		empty_list?.visibility = View.GONE
	}

	private fun addNewExperience(experienceItem: AddExperienceExecutorResponse) {
		val item = DataItem(experienceItem.data!!.period, experienceItem.data.companyLogo, experienceItem.data.city, experienceItem.data.userId, experienceItem.data.companyName, experienceItem.data.id, experienceItem.data.position)
		adapterExperienceAdapter.addNewItemInStart(item)

		NestedProfileCompanyFragment.isNeedUpdate = true
	}

	private fun getExperiencesList(size: Int) {
		presenter?.getExperincePagination(size) {
			if(it.isEmpty()) {
				empty_list.visibility = View.VISIBLE
			} else {
				empty_list.visibility = View.GONE
			}
			adapterExperienceAdapter.onNewDataReceived(it)
		}
	}
}