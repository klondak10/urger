package noty_team.com.urger.ui.fragment.tabs.feedback

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_feedback.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.FeedbackAdapter
import noty_team.com.urger.utils.api.response.feedback.get.executor.ListItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.text.SimpleDateFormat


class FeedbackFragment : BaseFragment<FeedbackPresenter>() {

	private var typeSorting: String = ""
	private lateinit var adapterFeedback: FeedbackAdapter
	val parser =  SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	val formatter = SimpleDateFormat("dd-MM-yyyy")
	var  currentList: ArrayList<ListItem> = arrayListOf()
	companion object {
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.feedback"

		fun newInstance(isShowToolbar: Boolean): FeedbackFragment {
			return FeedbackFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
				}
			}
		}

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}


	override fun layout() = noty_team.com.urger.R.layout.fragment_feedback

	override fun initialization(view: View, isFirstInit: Boolean) {

		presenter = FeedbackPresenter(baseContext)
		adapterFeedback = FeedbackAdapter(arrayListOf(), baseActivity)
		recycler_feedback.layoutManager =
            LinearLayoutManager(baseActivity)
		recycler_feedback.adapter = adapterFeedback

		if (isShowToolbar(arguments)) {
			feedback_fragment_toolbar_container.visibility = View.VISIBLE
			space_feedback.visibility = View.VISIBLE
		} else {
			feedback_fragment_toolbar_container.visibility = View.GONE
			space_feedback.visibility = View.GONE
		}

		nb_fragment_feedback.setOnClickListener {
			onBackPressed()
		}

		initSpinner()
		getFeedback()
	}

	private fun initSpinner() {
		val dataAdapter =
			ArrayAdapter<String>(
				baseContext,
				noty_team.com.urger.R.layout.item_feedback_spinner,
				resources.getStringArray(noty_team.com.urger.R.array.filter_feedback)
			)
		dataAdapter.setDropDownViewResource(noty_team.com.urger.R.layout.layout_spinner)
		spinner_sorting.adapter = dataAdapter

		spinner_sorting.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
			override fun onNothingSelected(parent: AdapterView<*>?) {
				typeSorting = resources.getStringArray(noty_team.com.urger.R.array.filter_feedback)[0]
				//(parent!!.getChildAt(0) as TextView).setTextColor(Color.RED)
			}

			override fun onItemSelected(
				parent: AdapterView<*>?,
				view: View?,
				position: Int,
				id: Long
			) {
				typeSorting = resources.getStringArray(noty_team.com.urger.R.array.filter_feedback)[position]
				//(parent?.getChildAt(0) as TextView).setTextColor(Color.RED)
				if(position == 1) {
					currentList = ArrayList(currentList.sortedByDescending {
						formatter.format(parser.parse(it.executor_date))
					})
					adapterFeedback.setData(currentList)
				} else {
					currentList = ArrayList(currentList.sortedWith(compareByDescending(ListItem::employerRate)))
					adapterFeedback.setData(currentList)
				}
			}
		}
	}

	private fun getFeedback() {

		if (PaperIO.userRole == UserRole.EXECUTOR) {
			presenter?.getFeedbacksExecutor {

				if (!it.data!!.list.isNullOrEmpty()) {
					currentList = ArrayList(it.data.list.sortedWith(compareByDescending(ListItem::employerRate)))
					adapterFeedback.setData(currentList)
				}

				ratingBar.rating = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toFloat()

				count_ratings.text = "${it.data.total.count} review"
				rating_tv.text = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toString()
			}
		} else {
			    if (Paper.book().exist("feedback")){
					presenter?.getFeedbacksExecutor {

						if (!it.data!!.list.isNullOrEmpty()) {
							empty_list.visibility = View.GONE
							currentList = ArrayList(it.data.list.sortedWith(compareByDescending(ListItem::employerRate)))
							adapterFeedback.setData(currentList)
						}

						ratingBar.rating = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toFloat()

						count_ratings.text = "${it.data.total.count} review"
						rating_tv.text = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toString()
					}
					Paper.book().delete("feedback")
				}else {

				presenter?.getFeedbackCompany {
					if (!it.data!!.list.isNullOrEmpty()) {
						empty_list.visibility = View.GONE
						currentList = ArrayList(it.data.list)
						adapterFeedback.setData(currentList)
					}

					ratingBar.rating = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toFloat()
					count_ratings.text = "${it.data.total.count} review"

					rating_tv.text = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toString()
				}
			}
		}

		if (adapterFeedback.itemCount > 0) {
			empty_list.visibility = View.GONE
		} else {
			empty_list.visibility = View.VISIBLE
		}
	}
}