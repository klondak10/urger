package noty_team.com.urger.ui.fragment.tabs.housing.house_detail

import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_house_detail.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.tabs.company.company_photo.CompanyPhotoFragment
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.adapters.company_photo.PhotoAdapter
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import java.util.*
import java.util.concurrent.TimeUnit

class HouseDetailFragment : BaseFragment<HouseDetailPresenter>() {

	companion object {
		fun newInstance() = HouseDetailFragment()
	}

	var houseId = 0
	var companyId = 0

	private lateinit var photoAdapter: PhotoAdapter

	var mMapView: MapView? = null

	var mListImages = ArrayList<CompanyPhotoItem>()

	private var googleMap: GoogleMap? = null

	override fun layout() = R.layout.fragment_house_detail


	override fun initialization(view: View, isFirstInit: Boolean) {
		bottomBarIsVisible(true)

		if (isFirstInit) {
			MapsInitializer.initialize(baseContext)

			presenter = HouseDetailPresenter(baseContext)


			mMapView = housing_detail_map_view
			mMapView?.onCreate(arguments)

			getHousing()
		}
	}

	private fun getHousing() {
		isShowLoadingDialog(true)
		presenter?.getHouseItem(houseId, companyId, {
			showShortToast(it)
		}, {
			initHousingData(it)
		})
	}

	private fun initHousingData(house: DataMapResponse) {
		house_detail_address.text = house.address

		val stringRooms = resources.getQuantityString(R.plurals.rooms_plurals, house.rooms)

		house_detail_price.text = house.cost_of_rent.toString().plus(" ")
		house_detail_currency_symbol.text = house.currency_of_rent

		house_detail_count_rooms.text = "${house.rooms} $stringRooms"

		house_detail_description.text = house.description

		val list = ArrayList<CompanyPhotoItem>()

		if (house.photos.isNotEmpty()) {

			setPhotorrr(house_detail_full_image, house.photos[0])

			house.photos.forEachIndexed { index, image ->
				if (image != "")
					mListImages.add(CompanyPhotoItem(image))

				if (index > 0) {
					if (image != "")
						list.add(CompanyPhotoItem(image))
				}
			}
		}

		if (list.size > 0) {
			initRecycler(list)
		} else {
			initRecycler(arrayListOf())
		}

		initMap(house.lat, house.lng)

		initAction()
	}

	private fun initMap(lat: String, lng: String) {
		if (lat != "" && lng != "") {
			mMapView?.getMapAsync { mMap ->
				googleMap = mMap as GoogleMap
				val metro = LatLng(lat.toDouble(), lng.toDouble())

				val cameraPosition = CameraPosition.Builder().target(metro).zoom(16f).build()

				googleMap?.addMarker(MarkerOptions().position(metro).title("Marker"))
				googleMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
			}
		}
	}

	private fun initAction() {
		nb_house_detail.setOnClickListener {
			onBackPressed()
		}

		house_detail_full_image.setOnClickListener {
			addFragment(CompanyPhotoFragment.newInstance().apply {
				this.index = 0
				Log.d("myLogs","mListImages "+mListImages)
				listImages = mListImages
			})
		}


		presenter?.subscriptions?.add(
				Observable.interval(1500, TimeUnit.MILLISECONDS, Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe({ isShowLoadingDialog(false) }, {})
		)


	}

	private fun initRecycler(list: ArrayList<CompanyPhotoItem>) {


		recycler_house_detail.layoutManager = LinearLayoutManager(baseActivity, RecyclerView.HORIZONTAL, false)

		photoAdapter = PhotoAdapter(list, onClick = { photoIndex ->

			addFragment(CompanyPhotoFragment.newInstance().apply {
				this.index = photoIndex + 1
				listImages = mListImages
			})


		})

		recycler_house_detail.adapter = photoAdapter
	}

	override fun onResume() {
		super.onResume()
		mMapView?.onResume()
	}

	override fun onPause() {
		super.onPause()
		mMapView?.onPause()
	}

	override fun onDestroy() {
		super.onDestroy()
		mMapView?.onDestroy()
	}

	override fun onLowMemory() {
		super.onLowMemory()
		mMapView?.onLowMemory()
	}


	fun setPhotorrr(imageView: ImageView, url: String) {
		if (url != "")
			Glide.with(imageView.context)
					.load(url)
					.apply(
							RequestOptions()
									.override(500, 500)
									.encodeQuality(50)
									.fitCenter()
									.skipMemoryCache(true)
									.diskCacheStrategy(DiskCacheStrategy.NONE)
					)
					.into(imageView)
	}


}