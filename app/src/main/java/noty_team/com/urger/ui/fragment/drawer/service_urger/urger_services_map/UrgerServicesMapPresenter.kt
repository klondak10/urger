package noty_team.com.urger.ui.fragment.drawer.service_urger.urger_services_map

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.center.CenterUrgerResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.news.NewsResponse

class UrgerServicesMapPresenter(context: Context): BasePresenter(context) {

    fun getCenterUrger(onSuccess:(response: CenterUrgerResponse)->Unit) {
        apiEmployer.getUrgerCenter({
            if(it.statusCode == 200) {
                onSuccess(it)
            } else {
                showShortToast("get Urger " + it.error)
            }

        }, {errorMessage, errorCode ->
            showShortToast("get Urger $errorMessage")
        }).callRequest()
    }

}