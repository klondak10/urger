package noty_team.com.urger.ui.fragment.bottom_bar.chat

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.content.FileProvider.getUriForFile
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import io.paperdb.Paper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_executor.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_bottom_choose_take_photo.*
import kotlinx.android.synthetic.main.fragment_concrete_chat.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.BuildConfig
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.FragmentImageViewer
import noty_team.com.urger.utils.ChatSocket
import noty_team.com.urger.utils.Message
import noty_team.com.urger.utils.Messages
import noty_team.com.urger.utils.adapters.chat.ChatAdapter
import noty_team.com.urger.utils.adapters.chat.ChatMessageDiffUtil
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.setPhoto
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import io.reactivex.Observable

class DialogChatFragment : BaseFragment<ConcreteChatPresenter>() {

    private var currentPosChooseDialog = 6
    private var currentData: ArrayList<Message> = arrayListOf()
    lateinit var mPhotoFile: File
    lateinit var imageUri: Uri
    lateinit var adapterChat: ChatAdapter

    private var roomId: Int = 0
    private var userId: Int = 0
    private var companionId: Int = 0
    private var isInitialized: Boolean = true

    internal var chatSocket = ChatSocket.provideSocketChat()
    var compositeDisposable: CompositeDisposable? = null

    companion object {

        private const val REQUEST_GALLERY = 100
        private const val REQUEST_CAMERA = 200

        fun newInstance(userId: Int, name: String, avatar: String?): DialogChatFragment {
            return DialogChatFragment().apply {
                arguments = Bundle().apply {
                    putInt("companionId", userId)
                    putString("name", name)
                    putString("avatar", avatar)
                }
            }
        }
    }

    override fun layout() = R.layout.fragment_concrete_chat


    override fun initialization(view: View, isFirstInit: Boolean) {
        isShowLoadingDialog(true)
        //PaperIO.chatIsOpen = true
        currentPosChooseDialog = Paper.book().read("language" + PaperIO.userPhoneNumber, 6) as Int
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        companionId = requireArguments()["companionId"] as Int
        //PaperIO.idFromChat = companionId

        if(PaperIO.userRole == UserRole.COMPANY) {
            userId = PaperIO.employerData!!.userId!!
        } else {
            userId = PaperIO.executorData!!.userId!!
        }
        val name = (requireArguments()["name"] as String)
        //PaperIO.nameFromChat = name
        var avatar = ""
        try {
            avatar = requireArguments()["avatar"] as String
            //PaperIO.avatarFromChat = avatar
        } catch (e: NullPointerException){}

        initCompanion(name, avatar)

        bottomBarIsVisible(false)
        presenter = ConcreteChatPresenter(baseContext)


        bar_chat_back.getClick {

            requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
            bottomBarIsVisible(true)

            if(PaperIO.userRole == UserRole.COMPANY) {
                baseActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            }else {
                baseActivity.drawer_layout_executor.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            }

            onBackPressed()
        }

        show_dialog_choose.getClick {
            showBottomDialog()
        }

        send_message.getClick {
            sendMessage()
        }

        initAdapter()
        initSocket()
        initInputMessage()

        compositeDisposable?.add(Observable
            .interval(5, TimeUnit.SECONDS)
            .repeat()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {}, {

                chatSocket.open()
                chatSocket.connect()
                initSocket()
                initInputMessage()

            }))


        translate_button.getClick {
            val manager = fragmentManager
            val dialog = DialogChooseLanguage().apply {
                callbackChooseLanguage = this@DialogChatFragment.callbackChooseLanguage
                position = this@DialogChatFragment.currentPosChooseDialog
            }


            val bundle = Bundle().apply {
                putStringArrayList(dialog.DATA, getItems())     // Require ArrayList
                putInt(dialog.SELECTED, this@DialogChatFragment.currentPosChooseDialog)
            }

            dialog.arguments = bundle
            dialog.show(manager!!, "Dialog")
        }

        message_et.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                sendMessage()
                return@OnEditorActionListener true
            }
            false
        })

    }

    override fun onDestroyView() {
        adapterChat.mData.clear()
        adapterChat.notifyDataSetChanged()
        adapterChat == null
        super.onDestroyView()
    }

    override fun onBackPressed(): Boolean {
        //PaperIO.chatIsOpen = false
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        bottomBarIsVisible(true)
        if(PaperIO.userRole == UserRole.COMPANY) {
            baseActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }else {
            baseActivity.drawer_layout_executor.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
        return super.onBackPressed()
    }

    @SuppressLint("NotifyDataSetChanged")
    var callbackChooseLanguage: (pos: Int) -> Unit = {
        if(currentPosChooseDialog != it) {
            currentPosChooseDialog = it
            val pair = when (it) {
                0 -> "ru|pl"
                1 -> "ru|de"
                2 -> "ru|en"
                3 -> "pl|ru"
                4 -> "de|ru"
                5 -> "en|ru"
                else -> "" //по умолчанию
            }
            Paper.book().write("language" + PaperIO.userPhoneNumber, it)
            if (pair.isNotEmpty()) {

                isShowLoadingDialog(true)

                currentData.clear()
                currentData.addAll(adapterChat.mData)
                presenter?.translateMessage(adapterChat.mData, pair, userId) { msg, index ->

                adapterChat.mData[index] = msg
                adapterChat.notifyItemChanged(index)
                    isShowLoadingDialog(false)
                }

            } else {
                isShowLoadingDialog(false)
                adapterChat.mData.clear()
                adapterChat.mData.addAll(currentData)
                adapterChat.notifyDataSetChanged()
            }
        }
    }

    private fun getItems(): ArrayList<String>? {
        return (resources.getStringArray(R.array.language)).toCollection(ArrayList())
    }

    private fun initAdapter(list:ArrayList<Message> = arrayListOf()) {
        val layoutManager = LinearLayoutManager(
            baseContext,
            LinearLayoutManager.VERTICAL,
            false
        )
        layoutManager.stackFromEnd  = true
        recycler_chat.layoutManager = layoutManager
        adapterChat = ChatAdapter(baseContext, list, userId, companionId) {
            val fragmentExpectedWage = FragmentImageViewer.newInstance(it)
            fragmentExpectedWage.show(baseActivity.supportFragmentManager, "dialog_expected_wage")
        }

        recycler_chat.adapter = adapterChat
    }

    private fun showBottomDialog() {
        val choosingDialog = baseActivity.getBottomSheetDialog()
        choosingDialog.setContentView(R.layout.dialog_bottom_choose_take_photo)
        choosingDialog.show()

        choosingDialog.load_from_gallery.getClick {
            val takeImageIntent = Intent(Intent.ACTION_PICK)
            takeImageIntent.type = "image/*"
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            takeImageIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(takeImageIntent, REQUEST_GALLERY)
            choosingDialog.dismiss()
        }

        choosingDialog.take_photo.getClick {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(baseActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_CAMERA)
                } else {
                    takePhoto()
                }
            } else {

                takePhoto()
            }
            choosingDialog.dismiss()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_GALLERY -> if (resultCode == AppCompatActivity.RESULT_OK) {
                val imageUri = data!!.data
                val bitmap = MediaStore.Images.Media.getBitmap(baseActivity.contentResolver, imageUri)
                val obj = JSONObject()
                obj.put("photo", getBaseFromBitmap(bitmap))
                obj.put("userFrom", userId)
                obj.put("userTo", companionId)
                chatSocket.emit("message", obj)
            } else {
                showShortToast(baseContext.resources.getString(R.string.error_pick_image))
            }
            REQUEST_CAMERA -> if (resultCode == AppCompatActivity.RESULT_OK) {
                val file = File(Environment.getExternalStorageDirectory().path, "photo.jpg")
                val uri = Uri.fromFile(file)
                //val imageUri = data!!.getData()
                val bitmap = MediaStore.Images.Media.getBitmap(baseActivity.contentResolver, uri)
                val obj = JSONObject()
                obj.put("photo", getBaseFromBitmap(bitmap))
                obj.put("userFrom", userId)
                obj.put("userTo", companionId)
                chatSocket.emit("message", obj)
            } else {
                showShortToast(baseContext.resources.getString(R.string.error_take_photo))
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                takePhoto()
            } else {
                showShortToast(baseContext.resources.getString(R.string.error_permission_camera))
            }
        }
    }

    @SuppressLint("SdCardPath", "QueryPermissionsNeeded")
    private fun takePhoto() {

        if(Build.VERSION.SDK_INT>=24){
            try{
                val m = StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            }catch(e:Exception){ e.printStackTrace() }
        }

        val intent =  Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if(intent.resolveActivity(baseContext.packageManager) != null) {
            val photo = createImageFiles()
            val uri  = Uri.parse("file:///sdcard/photo.jpg")
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            imageUri = if(Build.VERSION.SDK_INT  >= Build.VERSION_CODES.N) {
                getUriForFile(baseContext, BuildConfig.APPLICATION_ID + ".provider", photo)
            } else {
                Uri.fromFile(photo)
            }
            startActivityForResult(intent, REQUEST_CAMERA)
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun createImageFiles(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
        val mFileName = "JPEG_" + timeStamp + "_"
        val storageDir = baseActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(mFileName,  /* prefix */".jpg",         /* suffix */storageDir      /* directory */)
    }

    private fun initSocket() {

        val jsonRoom = JSONObject()
        jsonRoom.put("userFrom", userId)
        jsonRoom.put("userTo", companionId)
        chatSocket.emit("getRoom", jsonRoom).on("getRoom") {
            roomId = (it[0] as JSONObject).getInt("roomId")
            baseActivity.runOnUiThread {
                companion_name?.text = (it[0] as JSONObject).getString("name")
                if(executor_avatar != null) {
                    setPhoto(executor_avatar, (it[0] as JSONObject).getString("avatar"))
                }
                val jsonObject = JSONObject()
                jsonObject.put("userId", userId)
                jsonObject.put("roomId", roomId)
                chatSocket.emit("joinToRoom", jsonObject)
                chatSocket.on("message", OnNewMessageListener())
                chatSocket.on("writes", OnTypingListener())
                chatSocket.on("mread", OnReadingListener())
            }}
    }

    private fun setPhoto(imageView: ImageView, url: String?) {
        if (url != "" && url != null)
            Glide.with(imageView.context)
                .load(url)
                .apply(
                    RequestOptions()
                        .placeholder(ContextCompat.getDrawable(imageView.context, R.drawable.ic_user))
                        .override(720, 720)
                        .encodeQuality(100)
                        .fitCenter()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                )
                .into(imageView)


    }

    private fun sendMessage() {
        if (message_et.text.toString().isEmpty()
            || message_et.text.toString().replace("\n", " ").isEmpty()
            || message_et.text.toString().replace(" ", "").isEmpty()) {
            message_et.text.clear()
            return
        }
        var msg = ""
        val lines: Array<String> = message_et.text.toString().split(System.getProperty("line.separator")).toTypedArray()
        var lastIndex = -1
        var startIndex = -1
        for (i in lines.indices) {
            if (lines[i].replace("\n", "").isNotEmpty()) {
                lastIndex = i
                if (startIndex == -1) startIndex = i
            }
        }
        if (startIndex == -1 && lastIndex == -1) {
            message_et.text.clear()
            return
        }
        for (i in startIndex..lastIndex) {
            msg += if (lines[i] == "" && msg[msg.length - 1] != '\n') "\n\n" else if (lines[i] == "" && msg[msg.length - 1] == '\n') "\n" else lines[i]
        }
        if (msg.isEmpty() ||
            msg.replace("\n", " ").isEmpty() ||
            msg.replace(" ", "").isEmpty()
        ) {
            message_et.text.clear()
            return
        }
        var message = JSONObject()
        try {
            message.put("userFrom", userId)
            message.put("userTo", companionId)
            message.put("text", msg)
            chatSocket.emit("message", message)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        message_et.setText("")
    }

    private fun initInputMessage() {
        message_et.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().isNotEmpty()) {
                    val onTyping = JSONObject()
                    try {
                        onTyping.put("roomId", roomId)
                        onTyping.put("userId", userId)
                        chatSocket.emit("writes", onTyping)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    inner class OnTypingListener : Emitter.Listener {

        override fun call(vararg args: Any?) {
            var name = ""
            if (args[0] != null) {
                val id = (args[0] as JSONObject).getInt("id")
                if (id == companionId) {
                    name = (args[0] as JSONObject).getString("name")
                    try {
                        baseActivity.runOnUiThread {

                            if (layout_indicator != null) {
                                layout_indicator.visibility = View.VISIBLE
                                companion_write_tv.text = name
                                Handler().postDelayed({
                                    layout_indicator.visibility = View.GONE
                                }, 3000)
                            }
                        }
                    } catch (e: NullPointerException) {
                    }
                }
            }
        }
    }

    inner class OnNewMessageListener : Emitter.Listener {

        override fun call(vararg args: Any?) {
            if(args[0] != null) {
                val messages = Gson().fromJson(args[0].toString(), Messages::class.java) as Messages
                if (!messages.messages.isNullOrEmpty()) {
                    try{
                        if (messages.messages[messages.messages.size - 1].userId == companionId) {
                            baseActivity.runOnUiThread {
                                if (layout_indicator != null) {
                                    layout_indicator.visibility = View.GONE
                                }
                            }
                        }
                    }catch (e : IndexOutOfBoundsException){}

                    if (isInitialized) {

                        baseActivity.runOnUiThread {
                            when (currentPosChooseDialog) {
                                0 -> {
                                    presenter!!.translateMessage(messages.messages, "ru|pl", userId) { msg, index ->
                                        messages.messages[index] = msg
                                        initAdapter(messages.messages)
                                    }
                                }
                                1 -> {
                                    presenter!!.translateMessage(messages.messages, "ru|de", userId) { msg, index ->
                                        messages.messages[index] = msg
                                        initAdapter(messages.messages)
                                    }
                                }
                                2 -> {
                                    presenter!!.translateMessage(messages.messages, "ru|en", userId) { msg, index ->
                                        messages.messages[index] = msg
                                        initAdapter(messages.messages)
                                    }
                                }
                                3 -> {
                                    presenter!!.translateMessage(messages.messages, "pl|ru", userId) { msg, index ->
                                        messages.messages[index] = msg
                                        initAdapter(messages.messages)
                                    }
                                }
                                4 -> {
                                    presenter!!.translateMessage(messages.messages, "de|ru", userId) { msg, index ->
                                        messages.messages[index] = msg
                                        initAdapter(messages.messages)
                                    }
                                }
                                5 -> {
                                    presenter!!.translateMessage(messages.messages, "en|ru", userId) { msg, index ->
                                        messages.messages[index] = msg
                                        initAdapter(messages.messages)
                                    }
                                }
                                6 -> {
                                    adapterChat.mData = messages.messages
                                }
                            }


                            if (recycler_chat!=null){
                                adapterChat.notifyDataSetChanged()
                                recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                isInitialized = false
                                currentData.clear()
                                currentData.addAll(messages.messages)
                            }
                            isShowLoadingDialog(false)
                        }
                    } else {
                        baseActivity.runOnUiThread {
                            try {
                                currentData.add(messages.messages[0])

                                when (currentPosChooseDialog) {
                                    0 -> {
                                        presenter!!.translateLastMessage(messages.messages[0], "ru|pl", userId) {
                                            adapterChat.setData(it)
                                            recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                            isShowLoadingDialog(false)
                                        }
                                    }
                                    1 -> {
                                        presenter!!.translateLastMessage(messages.messages[0], "ru|de", userId) {
                                            adapterChat.setData(it)
                                            recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                            isShowLoadingDialog(false)
                                        }
                                    }
                                    2 -> {
                                        presenter!!.translateLastMessage(messages.messages[0], "ru|en", userId) {
                                            adapterChat.setData(it)
                                            recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                            isShowLoadingDialog(false)
                                        }
                                    }
                                    3 -> {
                                        presenter!!.translateLastMessage(messages.messages[0], "pl|ru", userId) {
                                            adapterChat.setData(it)
                                            recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                            isShowLoadingDialog(false)
                                        }
                                    }
                                    4 -> {
                                        presenter!!.translateLastMessage(messages.messages[0], "de|ru", userId) {
                                            adapterChat.setData(it)
                                            recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                            isShowLoadingDialog(false)
                                        }
                                    }
                                    5 -> {
                                        presenter!!.translateLastMessage(messages.messages[0], "en|ru", userId) {
                                            adapterChat.setData(it)
                                            recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                            isShowLoadingDialog(false)
                                        }
                                    }
                                    6 -> {
                                        adapterChat.setData(messages.messages[0])
                                        recycler_chat.scrollToPosition(adapterChat.itemCount - 1)
                                        isShowLoadingDialog(false)
                                    }
                                }

                            } catch (e: NullPointerException) {
                            }
                        }
                    }
                } else {
                    isShowLoadingDialog(false)
                }
            }
        }
    }

    inner class OnReadingListener : Emitter.Listener {

        override fun call(vararg args: Any?) {

            if(args[0] != null) {

                try {
                    val messages = Gson().fromJson(args[0].toString(), Messages::class.java) as Messages
                    if(adapterChat.mData.isNotEmpty()) {
                        val chatMessageDiffUtil = ChatMessageDiffUtil(adapterChat.mData, messages.messages)
                        val messageDiffUtil = DiffUtil.calculateDiff(chatMessageDiffUtil)
                        adapterChat.mData = messages.messages!!
                        baseActivity.runOnUiThread {
                            messageDiffUtil.dispatchUpdatesTo(adapterChat)
                        }
                    }

                }catch (e : IndexOutOfBoundsException){ }

            }

        }
    }

    private fun initCompanion(name: String, avatar: String?) {

        companion_name.text = name
        if(avatar != null) {
            if(avatar.length > 1) {
                setPhoto(executor_avatar, avatar.replace("\\/", "\\"))
            }
        }
    }


    private fun getBaseFromBitmap(bitmap: Bitmap): String {

        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.WEBP, 100, byteArrayOutputStream)
        return "data:image/jpeg;base64," + Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)

    }
}
