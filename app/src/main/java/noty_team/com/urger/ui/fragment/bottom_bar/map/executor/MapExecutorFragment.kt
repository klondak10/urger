package noty_team.com.urger.ui.fragment.bottom_bar.map.executor

import android.graphics.Typeface
import com.google.android.material.bottomsheet.BottomSheetBehavior
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.MotionEvent
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.fragment_map_executor.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItem


class MapExecutorFragment : BaseFragment<MapExecutorPresenter>(), GoogleMap.OnMarkerClickListener {

	companion object {
		fun newInstance() = MapExecutorFragment()
	}

	private var firstLocation: LatLng? = null
	lateinit var data: ArrayList<VacancyItem>
	lateinit var currentVacancy: VacancyItem
	private lateinit var mBottomSheetBehavior: BottomSheetBehavior<*>

	var mMapView: MapView? = null

	private var googleMap: GoogleMap? = null

	override fun layout() = R.layout.fragment_map_executor

	override fun initialization(view: View, isFirstInit: Boolean) {
		drawer_map_executor.setOnClickListener {
			baseActivity.openDrawerLayout()
		}

		if (isFirstInit) {

			initMap()

			mBottomSheetBehavior = BottomSheetBehavior.from(bottom_marker_info)

			bottom_marker_info.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
				when (motionEvent.action) {
					MotionEvent.ACTION_UP -> { }
				}
				return@OnTouchListener true
			})

			more_button.setOnClickListener {
				mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

				addFragment(VacancyDetailFragmnet.newInstance(currentVacancy.id!!))
				//navigator().navigateToFragment(BottomVacancyDetailFragment.newInstance(), null, true)
			}
		}
	}

	private fun initMap() {
		mMapView = map_view
		mMapView?.onCreate(arguments)

		try {
			MapsInitializer.initialize(baseActivity)
		} catch (e: Exception) {
			e.printStackTrace()
		}

		mMapView?.getMapAsync { mMap ->
			googleMap = mMap

			googleMap?.setOnMapClickListener {
				mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
			}

			googleMap?.setOnMarkerClickListener(this)

			getVacanciesLocation()
		}

	}

	private fun getVacanciesLocation() {
		presenter = MapExecutorPresenter(baseContext)
		presenter?.getVacancies {
			if(!it.data.isNullOrEmpty()) {
				data = it.data
				it.data.forEach {
					// 52.2322 21.0083 центр Варшавы
					val location = LatLng(it.lat?.toDouble() ?: 52.2322, it.lng?.toDouble() ?: 21.0083)
					val markerOptions = MarkerOptions().title(it.position)
						.snippet("${it.category_name}\n${it.parent_category}")
						.icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_map))
					googleMap?.addMarker(markerOptions.position(location))!!.setTag(it.id)
					if (firstLocation == null) {
						firstLocation = location
						val cameraPosition = CameraPosition.Builder().target(firstLocation!!).zoom(14f).build()
						googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
					}
				}
			}
		}

		// For zooming automatically to the location of the marker
		if(firstLocation == null) {

			val cameraPosition = CameraPosition.Builder().target(LatLng(52.757322, 19.259165)).zoom(7f).build()
			googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
		}
	}


	override fun onMarkerClick(marker: Marker): Boolean {
		mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

		data.forEach {
			if(marker.tag.toString().toInt() == it.id) {
				currentVacancy = it
			}
		}
		if(currentVacancy != null) {

			val spannable = SpannableString(currentVacancy.category_name + ", " + currentVacancy.parent_category)
			spannable.setSpan(
				StyleSpan(Typeface.BOLD),
				0, currentVacancy.category_name!!.length,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
			)
			vacancy_detail_company_name_position.text = spannable
			exist_house.visibility = if(currentVacancy.houseId != null) View.VISIBLE else View.GONE
			vacancy_detail_location.text = currentVacancy.address
			when(currentVacancy.workSchedule) {
				"full_time" -> vacancy_detail_type_employment.text = getString(R.string.full_employment)
				"part_time" -> vacancy_detail_type_employment.text = getString(R.string.part_time)
				"temporary_job" -> vacancy_detail_type_employment.text = getString(R.string.temporary_job)
				else -> vacancy_detail_type_employment.text = getString(R.string.contract)
			}
			wage.text = "${currentVacancy.wage} ${currentVacancy.wageCurrency}"
			description_vacancy.text = currentVacancy.shortDescription
		}

		return true
	}

	override fun onResume() {
		super.onResume()
		mMapView?.onResume()
	}

	override fun onPause() {
		super.onPause()
		mMapView?.onPause()
	}

	override fun onDestroy() {
		super.onDestroy()
		mMapView?.onDestroy()
	}

	override fun onLowMemory() {
		super.onLowMemory()
		mMapView?.onLowMemory()
	}
}