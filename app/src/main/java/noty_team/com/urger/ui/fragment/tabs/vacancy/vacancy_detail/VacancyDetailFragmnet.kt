package noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail

import android.graphics.Typeface
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.transition.Fade
import android.transition.TransitionManager
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.google.gson.Gson
import io.socket.emitter.Emitter
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_vacancy_detail.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.ui.adapters.recycler.VacancyDetailAdapter
import noty_team.com.urger.ui.fragment.bottom_bar.chat.DialogChatFragment
import noty_team.com.urger.ui.fragment.profile_company.get_info.DetailProfileCompany
import noty_team.com.urger.ui.fragment.profile_executor.get_info.DetailProfileExecutor
import noty_team.com.urger.ui.fragment.tabs.company.CompanyFragment
import noty_team.com.urger.ui.fragment.tabs.contract.document_detail.FragmentDocumentDetails
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.HouseDetailFragment
import noty_team.com.urger.utils.ChatSocket
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.Rooms
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.document.DocumentsAdapterPagination
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItem
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.User
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.setPhoto
import org.json.JSONObject
import java.text.DateFormatSymbols


class VacancyDetailFragmnet : BaseFragment<VacancyDetailPresenter>() {

    lateinit var adapterRequestVacancy: VacancyDetailAdapter
	private var detailIsVisible = false
	private lateinit var documentsAdapterPagination: DocumentsAdapterPagination
	private val listDocuments = ArrayList<DocumentItem>()
	private var roomsSocket = ChatSocket.provideSocketChat()

	var companyId = 0

	companion object {
		private const val VACANCY_ID_KEY = "VACANCY_ID_KEY"
		fun newInstance(id: Int = -1, type: TypeVacancy = TypeVacancy.NORMAL) = VacancyDetailFragmnet().apply {
			arguments = Bundle().apply {
				putInt(VACANCY_ID_KEY, id)
				putSerializable("type", type)
			}
		}

		fun getVacancyId(arguments: Bundle?) = arguments?.getInt(VACANCY_ID_KEY, -1) ?: -1

	}

	private fun initSocket() {

		if(!roomsSocket.connected())
			roomsSocket.connect()
		val jsonObject = JSONObject()
		if(PaperIO.userRole == UserRole.COMPANY) {
			jsonObject.put("userId", PaperIO.employerData!!.userId)
		} else {
			jsonObject.put("userId", PaperIO.executorData!!.userId)
		}
		roomsSocket.emit("getRooms", jsonObject).on("getRooms", OnChatsListener())
	}
	inner class OnChatsListener : Emitter.Listener {
		override fun call(vararg args: Any?) {
			val rooms = Gson().fromJson(args[0].toString(), Rooms::class.java) as Rooms
			if(!rooms.rooms.isNullOrEmpty()) {
				baseActivity.runOnUiThread {

				}
			} else {
				baseActivity.runOnUiThread {

				}
			}
		}
	}

	override fun layout() = R.layout.fragment_vacancy_detail

	private fun getMonth(month: Int): String {
		return DateFormatSymbols().months[month - 1]
	}

	override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = VacancyDetailPresenter(baseContext)

		initSocket()

		isShowLoadingDialog(true)


		val vacancyId = getVacancyId(arguments)

		presenter?.getVacancy(vacancyId, (arguments?.get("type") as TypeVacancy),
		onDataLoad = {
			isShowLoadingDialog(false)

			companyId = it.data.validatedCompanyId!!

			if (PaperIO.userRole == UserRole.EXECUTOR){
				presenter?.getContractPagination(companyId,0) { response ->
					if(response.isNullOrEmpty()) {
						responded_text.visibility = View.GONE
					} else {
						responded_text.visibility = View.VISIBLE
					}
					documentsAdapterPagination.onNewDataReceived(response)

				}
			}
			screen_title_text_view.text = it.data?.category_name
			exist_house?.visibility = if(it.data.houseId != null) View.VISIBLE else View.GONE

			val spannable = SpannableString(it.data?.category_name + ", " + it.data?.parent_category)
			spannable.setSpan(
				StyleSpan(Typeface.BOLD),
				0, it.data?.category_name!!.length,
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
			)

			vacancy_detail_company_name_position.text = spannable
			vacancy_detail_location.text = it.data?.address

			val day = it.data.created_at!!.substring(8, 10)
			val month = it.data.created_at!!.substring(5, 7)
			house_detail_data.text = "$day ${getMonth(month.toInt())}"
			val wageCurrency = when(it.data.wageCurrency) {
				"1" -> "CZK"
				"2" -> "EUR"
				"4" -> "PLN"
				else -> "EUR"
			}
			house_detail_price.text = "${if (it.data.wage != null) it.data.wage.toString() else "0"} ${wageCurrency}"

			house_detail_description.text = it.data.shortDescription
			vacancy_detail_task.text = it.data.responsibilities
			house_detail_requirements.text = it.data.requirements
			house_detail_we_offer.text = it.data.weOffer

			vacancy_detail_avatar.setOnClickListener {

				if(PaperIO.userRole == UserRole.EXECUTOR) {
					addFragment(DetailProfileCompany.newInstance(companyId))
				}
			}

			//added full description and check additionally
			vacancy_detail_task_complete.text = it.data.fullDescription

			if (it.data.additionally?.isNotEmpty() == true) {
				house_detail_detail.text = it.data.additionally
			} else {
				house_detail_detail.visibility = View.GONE
				house_detail_text_detail.visibility = View.GONE
			}
			if(PaperIO.userRole == UserRole.COMPANY) {

				setPhoto(vacancy_detail_avatar, PaperIO.employerData?.avatar ?: "")
			} else {

				setPhoto(vacancy_detail_avatar, it.data.company_logo)
			}

			if (it.data.houseId != null) {
				presenter?.getHouseItem(it.data.houseId!!, companyId) {
					setPhoto(vacancy_detail_image_house, if (it.photos.isNotEmpty()) it.photos[0] else "")
					vacancy_detail_house_address.text = it.address
					vacancy_detail_house_description.text = it.description


					val houseID = it.houseId

					vacancy_detail_house_detail.setOnClickListener {

						addFragment(HouseDetailFragment.newInstance().apply {
							houseId = houseID
							companyId = this@VacancyDetailFragmnet.companyId
						})
					}

					isShowLoadingDialog(false)
				}
			} else {
				vacancy_detail_text_house.visibility = View.GONE
				vacancy_detail_house_detail.visibility = View.GONE
				isShowLoadingDialog(false)
			}
		},
		onError = {
			baseActivity.supportFragmentManager.popBackStack()
			isShowLoadingDialog(false)
		})

		if(PaperIO.userRole == UserRole.COMPANY) {
			presenter?.getRequestVacancy(vacancyId) {
				if(!it.data.isNullOrEmpty()) {

					adapterRequestVacancy.onNewDataReceived(
						ArrayList(it.data),
						Constants.documentItemLimit
					)
				} else {
					responded_text.visibility = View.GONE
				}
			}
			respond_vacancy.visibility = View.GONE
			initVacancyDetailRecycler()
			responded_text.text = getResources().getString(R.string.responded_vacancy)
		} else {
			initContract()
			responded_text.text = getResources().getString(R.string.txt_contract)

		}


		respond_vacancy?.getClick {

			presenter?.participateVacancy(vacancyId) {
				if(it) {
					showShortToast(getString(noty_team.com.urger.R.string.success_participate))
				}
                if (PaperIO.userRole == UserRole.COMPANY) {
                    CompanyActivity.start(baseActivity)
                    baseActivity.finish()
                } else {
                    ExecutorActivity.start(baseActivity)
                    baseActivity.finish()
                }
			}
		}


		seeMore.getClick {
			if(!detailIsVisible) {
				toggle(vacancy_detail_description, vacancy_detail_description, true)
//				seeMore_tv.text = resources.getString(noty_team.com.urger.R.string.close_more)
//				vacancy_detail_description.visibility = View.VISIBLE
				detailIsVisible = true
				seeMore_tv.text = baseContext.resources.getString(R.string.close_deailts)
			} else {
				toggle(vacancy_detail_description, vacancy_detail_description, false)
//				seeMore_tv.text = resources.getString(noty_team.com.urger.R.string.see_more)
//				vacancy_detail_description.visibility = View.GONE
				seeMore_tv.text = baseContext.resources.getString(R.string.show_deailts)
				detailIsVisible = false
			}
		}
		nb_vacancy_detail.setOnClickListener {
			onBackPressed()
		}
	}


	private fun toggle(view: View, viewGroup: ViewGroup, isShow: Boolean) {
		val transition = Fade()
		transition.setDuration(400)
		transition.addTarget(view)

		TransitionManager.beginDelayedTransition(viewGroup, transition)
		view.setVisibility(if (isShow) View.VISIBLE else View.GONE)
	}

	private fun initVacancyDetailRecycler() {
		recycler_vacancy_detail.layoutManager =
            LinearLayoutManager(baseActivity)
		adapterRequestVacancy = VacancyDetailAdapter(arrayListOf(), recycler_vacancy_detail,
			onSendMessage = {
				Paper.book().write("currentVacancyid", it.user!!.id!!)
				baseActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
				addFragment(DialogChatFragment.newInstance(Paper.book().read("currentVacancyid"), "${it.user.firstName} ${it.user.lastName}", "${it.user.avatar}"))
			},
			onOpenProfile = {
				val user = User(it.user!!.birthday, it.user!!.phone, it.user!!.background, it.user!!.lastName,
					it.user!!.id, it.user!!.avatar, it.user!!.position, it.user!!.createAt, it.user!!.type,
					it.user!!.firstName, it.user!!.email, false)
				addFragment(DetailProfileExecutor.newInstance(user, it.auto))
			})
        recycler_vacancy_detail.adapter = adapterRequestVacancy
	}

	private fun initContract() {


		val horizontalLayoutManagaer =
            LinearLayoutManager(
                baseActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
		recycler_vacancy_detail?.layoutManager = horizontalLayoutManagaer

		documentsAdapterPagination = DocumentsAdapterPagination(listDocuments, recycler_vacancy_detail,

			onClickPosition = { it_,position ->
				run {
					addFragment(FragmentDocumentDetails.newInstance().apply {
						fileName = it_.documentName
						addressLink = it_.image
						listDocument = listDocuments
						pos = position
					})
				}
			}

		)

		recycler_vacancy_detail.adapter = documentsAdapterPagination

	}
}

enum class TypeVacancy {
	FAVORITE, NORMAL
}