package noty_team.com.urger.ui.fragment.tabs.portfolio

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_portfolio.*
import kotlinx.android.synthetic.main.fragment_portfolio.empty_list
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.PortfolioAdapter
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.add_portfolio.AddPortfolioFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.partfolio_detail.PortfolioDetailFragment
import noty_team.com.urger.utils.api.response.portfolio.AddPortfolioExecutorResponse

class PortfolioFragment : BaseFragment<PortfolioPresenter>() {
	var onAddPortfolioObservable: Observable<Unit>? = null
	lateinit var adapterPortfolioFragment: PortfolioAdapter
	override fun onResume() {
		super.onResume()
		onAddPortfolioObservable?.subscribe()
	}
	companion object {
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.portfolio"

		fun newInstance(isShowToolbar: Boolean): PortfolioFragment {
			return PortfolioFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
				}
			}
		}

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}

	override fun layout() = R.layout.fragment_portfolio

	override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = PortfolioPresenter(baseContext)
		getPortfolioList(0)
		if (isShowToolbar(arguments)) {
			portfolio_fragment_toolbar_container.visibility = View.VISIBLE
			portfolio_fragment_fab.show()
		} else {
			portfolio_fragment_toolbar_container.visibility = View.GONE
			portfolio_fragment_fab.hide()
		}

		nb_fragment_portfolio.setOnClickListener {
			onBackPressed()
	//		backNavigator().navigateBack()
		}
		portfolio_fragment_fab.setOnClickListener {

			addFragment(AddPortfolioFragment.newInstance())
			//navigator().navigateToFragment(AddPortfolioFragment.newInstance(), null, true)
		}

		recycler_portfolio.layoutManager =
            GridLayoutManager(baseActivity, 2)
		adapterPortfolioFragment = PortfolioAdapter(
			arrayListOf(), baseActivity, recycler_portfolio,
			onLongClick = { _it ->
				run {
					deleteDialog(getString(R.string.delete_portfolio)) {
						adapterPortfolioFragment.remove(_it)
						presenter?.deletePortfolioExecutor(_it.id!!) {
							if(it) {
								showShortToast(getString(R.string.success_delete))
							} else {
								showShortToast(getString(R.string.failure_delete))
							}
							if(adapterPortfolioFragment.itemCount == 0) {
								empty_list.visibility = View.VISIBLE
							}
						}
					}
				}
			}, onClick = {
				addFragment(PortfolioDetailFragment.newInstance(it))
			})

		recycler_portfolio.adapter = adapterPortfolioFragment
	}

	private fun getPortfolioList(size: Int) {

		presenter?.getPortfolioPagination(size) {
			if(it.isEmpty()) {
				empty_list.visibility = View.VISIBLE
			} else {
				empty_list.visibility = View.GONE
			}

			adapterPortfolioFragment.onNewDataReceived(it)
		}
	}

	var callbackAddPortfolio :(item: AddPortfolioExecutorResponse) -> Unit = {
		onAddPortfolioObservable = Observable.fromCallable { addNewPortfolio(it) }
	}

	private fun addNewPortfolio(experienceItem: AddPortfolioExecutorResponse) {
		val item = noty_team.com.urger.utils.api.response.portfolio.DataItem()
		adapterPortfolioFragment.addNewItemInStart(item)
		empty_list?.visibility = View.GONE
		NestedProfileCompanyFragment.isNeedUpdate = true
	}

}