package noty_team.com.urger.ui.activity.splash

interface SplashContract {

    interface Presenter {
        fun dispose()
        fun serverRequest(chooseActivityCallback: (isUserExist: Boolean) -> Unit)
    }

}