package noty_team.com.urger.ui.fragment.profile_company

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.core.widget.NestedScrollView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.drjacky.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.fragment_profile_company.*
import kotlinx.android.synthetic.main.fragment_profile_company_pager.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.view_pager.MainPageAdapter
import noty_team.com.urger.ui.fragment.tabs.company.edit_profile.edit_personal_data.EditPersonalDataFragment
import noty_team.com.urger.ui.fragment.tabs.contract.ContractsFragment
import noty_team.com.urger.ui.fragment.tabs.documents.DocumentFragment
import noty_team.com.urger.ui.fragment.tabs.housing.HousingFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.VacancyFragment
import noty_team.com.urger.utils.ControllableAppBarLayout
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.cicerone.Screens
import noty_team.com.urger.utils.convertDpToPixel
import noty_team.com.urger.utils.setPhoto
import java.io.File

class ProfileCompanyFragment : BaseFragment<ProfileCompanyPresenter>() {

	lateinit var toolbar: Toolbar
	private lateinit var drawerButton: View
	var image = ""

	companion object {
		fun newInstance() = ProfileCompanyFragment()
		var IsNested = false
	}

	override fun layout() = R.layout.fragment_profile_company

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = ProfileCompanyPresenter(baseContext)

		toolbar = toolbar_main
		drawerButton = drawer_button
		initAction()

		if (isFirstInit) {
			replaceNested(IsNested)
			initUserInfo()
		}

		initBottomBar()
		appBarOffset()
		nestedScrollControll()

	}

	private fun nestedScrollControll() {
		nested_scroll_view_company?.setOnScrollChangeListener(
			NestedScrollView.OnScrollChangeListener
		{ nested, scrollX, scrollY, oldScrollX, oldScrollY ->

			if (scrollY > oldScrollY) {
				bottomBarIsVisible(false)
			}
			if (scrollY < oldScrollY) {
				bottomBarIsVisible(true)
			}

		})

	}

	private fun initBottomBar() {
		when (app_bar_layout.state) {
			ControllableAppBarLayout.State.COLLAPSED ->
				bottomBarIsVisible(false)

			ControllableAppBarLayout.State.EXPANDED ->
				bottomBarIsVisible(true)

			else ->
				bottomBarIsVisible(true)
		}
	}

	private fun initAction() {

		nested_replace.getClick {
			IsNested = !IsNested
			replaceNested(IsNested)
		}

		edit_personal_data_img.setOnClickListener {
			presenter?.isCanEditProfile {
				PaperIO.canEditProfile = it
				canEditProfile()
			}
		}

		//drawer
		drawerButton.setOnClickListener {
			baseActivity.openDrawerLayout()
		}

		// avatar
		avatar.setOnClickListener {
			image = "avatar"
			showDialogChoose().show()
		}

		profile_company_background.setOnClickListener {
			image = "background"
			showDialogChoose().show()
		}

	}

	private fun canEditProfile() {
		if (PaperIO.canEditProfile) {
			addFragment(EditPersonalDataFragment.newInstance().apply {
				callback = {
					initUserInfo()
				}
			})
		} else {
			initCanEditProfile()
			showLongToast(getString(R.string.not_edit_profile))
		}
	}

	private fun initUserInfo() {
		initCanEditProfile()
		setAvatar()
		setUserBackground(PaperIO.companyData?.logo)
		setUserPersonalInfo()
	}

	@SuppressLint("SetTextI18n")
	private fun setUserPersonalInfo() {

		user_name_company_profile.text = "${PaperIO.employerData?.firstName}  ${PaperIO.employerData?.lastName}"
		toolbar_text.text = "${PaperIO.employerData?.firstName}  ${PaperIO.employerData?.lastName}"
		drawerListener.onDrawerUserInfoChanged()

		company_name.text = PaperIO.companyData?.name ?: getString(R.string.some_corp)

		profile_company_user_rating.rating = PaperIO.employerData!!.rate ?: 0.0F
	}

	private fun initCanEditProfile() {
		if (PaperIO.canEditProfile) {
			edit_personal_data_img.setImageResource(R.mipmap.red_edit)
		} else {
			edit_personal_data_img.setImageResource(R.mipmap.gray_edit)
		}
	}

	private fun setAvatar() {
		if (PaperIO.employerData?.avatar != "" && PaperIO.employerData?.avatar != null) {
			avatar_img.visibility = View.VISIBLE
			avatar_tv.visibility = View.GONE

			setPhoto(avatar_img, PaperIO.employerData?.avatar ?: "")


		} else {
			avatar_img.visibility = View.GONE
			avatar_tv.visibility = View.VISIBLE


			val avatarLogoText =
					PaperIO.employerData?.firstName?.substring(0, 1) + PaperIO.employerData?.lastName?.substring(0, 1)
			avatar_tv.text = avatarLogoText

		}
		//drawerAvatar
		drawerListener.onDrawerAvatarChanged()
	}

	private fun setUserBackground(logo: String?) {
		if (PaperIO.companyData?.logo != "" && PaperIO.companyData?.logo  != null) {
			profile_company_background_img.visibility = View.VISIBLE
			profile_company_background_def.visibility = View.INVISIBLE

			setPhoto(profile_company_background_img, logo  ?: "")

		} else {
			profile_company_background_img.visibility = View.INVISIBLE
			profile_company_background_def.visibility = View.VISIBLE

		}
		drawerListener.onDrawerBackgroundChanged()
	}


	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				if (image == "avatar"){
					presenter?.updateAvatar(File(uri.path.toString()), {
						if (it) setAvatar()
					}, {
						showLongToast(it)
					})
				}else{
					presenter?.updateBackground(File(uri.path.toString()), {
							setUserBackground(it.data?.logo)
					}, {
						showLongToast(it)
					})
				}

			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				if (image == "avatar"){
					presenter?.updateAvatar(File(uri.path.toString()), {
						if (it) setAvatar()
					}, {
						showLongToast(it)
					})
				}else{
					presenter?.updateBackground(File(uri.path.toString()), {
						setUserBackground(it.data?.logo)
					}, {
						showLongToast(it)
					})
				}

			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}


	private fun setPhoto(imageId: ImageView, url: String) {
		if (url != "")
			Glide.with(baseActivity)
					.load(url)
					.apply(
							RequestOptions()
									.override(250, 250)
									.encodeQuality(50)
									.fitCenter()
									.skipMemoryCache(true)
									.diskCacheStrategy(DiskCacheStrategy.NONE)
					)
					.into(imageId)
	}

	private fun initTab() {

		main_tabLayout.setupWithViewPager(company_profile_view_pager)

		//fab logic
		if (PaperIO.userRole == UserRole.COMPANY) {

			right_icon.visibility = View.GONE

			main_tabLayout.addOnTabSelectedListener(object :
					TabLayout.OnTabSelectedListener {
				override fun onTabSelected(tab: TabLayout.Tab) {
					changeToolbarText(tab.position, app_bar_layout.state)
					showFab(tab.position)
				}

				override fun onTabUnselected(tab: TabLayout.Tab) {}
				override fun onTabReselected(tab: TabLayout.Tab) {}
			})

			fab_main.setOnClickListener {
				val adapter = company_profile_view_pager.adapter as MainPageAdapter
				val fragment = adapter.fragments[main_tabLayout.selectedTabPosition]
				when (fragment) {
					is ContractsFragment -> fragment.openAddDocumentsFragment()
					is DocumentFragment -> fragment.openAddDocumentsFragment()
					is VacancyFragment -> { fragment.openAddVacancyFragment() }
					is HousingFragment -> fragment.openAddHouseFragment()
				}
			}
		} else {
			// if else role
			edit_personal_data_img.visibility = View.GONE
		}
	}

	private fun showFab(tabIndex: Int) {
		when (tabIndex) {
			0,2,3,5 -> {
				fab_main.show()
			}
			else -> {
				fab_main.hide()
			}
		}
	}

//	private fun initToolbar() {
//		if (PaperIO.userRole == UserRole.COMPANY) {
//			//set count feedback
//			var mGalleryTextView =
//					MenuItemCompat.getActionView(baseActivity.getNavigationView()?.getMenu()!!.findItem(noty_team.com.urger.R.id.nav_responses)) as TextView
//			mGalleryTextView.setGravity(Gravity.CENTER_VERTICAL)
//			mGalleryTextView.setTypeface(null, Typeface.BOLD)
//			mGalleryTextView.setTextColor(ContextCompat.getColor(baseActivity, R.color.colorAccent))
//			mGalleryTextView.text = "100"
//		}
//	}

	private fun changeToolbarText(position: Int, state: ControllableAppBarLayout.State?) {
		if (state != ControllableAppBarLayout.State.EXPANDED) {
			toolbar_text.text = "${PaperIO.employerData!!.firstName} ${PaperIO.employerData!!.lastName}"
		}
		right_icon.visibility = if (position == 1) View.VISIBLE else View.GONE
	}

	private fun appBarOffset() {
		app_bar_layout.setOnStateChangeListener(object : ControllableAppBarLayout.OnStateChangeListener {
			override fun onStateChange(toolbarChange: ControllableAppBarLayout.State) {
				when (toolbarChange) {
					ControllableAppBarLayout.State.COLLAPSED -> {
						if (IsNested) {

							toolbar_text.text = user_name_company_profile.text
							right_icon.visibility = View.GONE
						} else changeToolbarText(main_tabLayout.selectedTabPosition, toolbarChange)
						val toolbarParams = toolbar_main.layoutParams
						toolbarParams.width = ActionBar.LayoutParams.MATCH_PARENT
						bottomBarIsVisible(false)
					}
					ControllableAppBarLayout.State.EXPANDED -> {
						toolbar_text.text = " "
						bottomBarIsVisible(true)
						val toolbarParams = toolbar_main.layoutParams
						toolbarParams.width = convertDpToPixel(56f, baseContext).toInt()
					}
					ControllableAppBarLayout.State.IDLE -> {
					}
				}
			}
		})
	}

	@SuppressLint("RestrictedApi")
	private fun replaceNested(isNested: Boolean) {
		if (isNested) {

			main_tab_container.visibility = View.GONE
			gray_space.visibility = View.VISIBLE
			fab_main.hide()
			nested_replace.setImageResource(R.mipmap.red_nested_icon)
			changeNestedFragment(Screens.NestedScreens.PROFILE_KOLBASA)
		} else {
			nested_replace.setImageResource(R.mipmap.list)
			main_tab_container.visibility = View.VISIBLE
			gray_space.visibility = View.GONE
			showFab(main_tabLayout.selectedTabPosition)
			changeNestedFragment(Screens.NestedScreens.PROFILE_PAGER) {
				initTab()
			}

		}
	}

	private fun changeNestedFragment(
			screen: Screens.NestedScreens,
			onFragmentCreated: (rootView: View) -> Unit = {}
	) {
		val tab: String = screen.name
		val fm = childFragmentManager
		var currentFragment: Fragment? = null
		val fragments = fm.fragments
		for (f in fragments) {
			if (f.isVisible) {
				currentFragment = f
				break
			}
		}
		val newFragment = fm.findFragmentByTag(tab)

		if (currentFragment != null && newFragment != null && currentFragment === newFragment) return

		val transaction = fm.beginTransaction()
		if (newFragment == null) {
			transaction.add(
					R.id.nested_main_container,
					Screens.NestedTabScreen(screen, onFragmentCreated).fragment,
					tab
			)
		}

		if (currentFragment != null) {
			transaction.detach(currentFragment)
		}

		if (newFragment != null) {
			transaction.attach(newFragment)
		}
		transaction.commitNow()
	}
}