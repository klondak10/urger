package noty_team.com.urger.ui.fragment.tabs.company

import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import java.io.File

interface CompanyContract {
	interface View
	interface Presenter {
		fun addCompanyPhotos(list: ArrayList<File>/*, showToast: (massage: String) -> Unit*/, dataList: (ArrayList<CompanyPhotoItem>) -> Unit)
		fun getCompanyPhotos(companyId: Int, dataList: (ArrayList<CompanyPhotoItem>) -> Unit, showToast: (massage: String) -> Unit)
		fun deleteCompanyPhoto(photoName: String, showToast: (massage: String) -> Unit, onDeleteSuccess: (isSuccess: Boolean) -> Unit)
		fun getStatistic(companyId: Int, showToast: (massage: String) -> Unit, onDataLoad: (countDocument: String, countHousing: String, countVacancy: String) ->Unit)
	}
}