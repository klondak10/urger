package noty_team.com.urger.ui.fragment.bottom_bar.service_delivery

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.response.delete_vacancy.DeleteVacancyResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class ServiceDeliveryPresenter(context: Context): BasePresenter(context) {
    private var page: Int = 1
    fun getVacancies(listSize: Int, onSuccess:(response: VacanciesListResponse)->Unit, onError:()->Unit){
        val page = listSize / Constants.documentItemLimit
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getVacancies(Constants.vacancyItemLimit, page, {
                onSuccess(it)
            }, { errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        } else {
            apiEmployer.getVacanciesForExecutor(Constants.vacancyItemLimit, 0,{
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun deleteVacancy(vacancyId: Int, onSuccess:(response: DeleteVacancyResponse)->Unit, onError:()->Unit){
        apiEmployer.deleteVacancy(vacancyId,{
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun loadMoreVacancy(onSuccess:(response: VacanciesListResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getVacancies(Constants.vacancyItemLimit, page, {
                onSuccess(it)
                ++page
            }, { errorMessage, errorCode ->
                onError()

            }).callRequest()
        } else {
            apiEmployer.getVacanciesForExecutor(Constants.vacancyItemLimit, page,{
                onSuccess(it)
                ++page
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }
}