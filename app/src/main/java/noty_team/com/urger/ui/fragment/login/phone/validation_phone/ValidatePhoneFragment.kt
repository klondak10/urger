package noty_team.com.urger.ui.fragment.login.phone.validation_phone

import android.content.Context
import android.telephony.TelephonyManager
import android.view.View
import android.widget.Toast
import androidx.core.os.ConfigurationCompat
import kotlinx.android.synthetic.main.fragment_validate_phone.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.login.phone.check_sms_code.CheckSMSFragment
import noty_team.com.urger.utils.api.request.sms_phone_validation.UserPhoneValidationRequest
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import java.util.*


class ValidatePhoneFragment : BaseFragment<ValidatePhonePresenter>() {

	companion object {
		fun newInstance() = ValidatePhoneFragment()
	}

	override fun layout() = R.layout.fragment_validate_phone

	override fun initialization(view: View, isFirstInit: Boolean) {
		baseActivity.toggleKeyboard(false)

		presenter = ValidatePhonePresenter(baseContext)

		PaperIO.signUp = Authorization.NEED_LOGIN_OR_SING_UP

		val tm = requireActivity().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
		val countryCodeValue = tm.networkCountryIso

		country_picker.setCountryForNameCode(countryCodeValue)

		ripple_button_login_send_sms.getClick(1000) {

			if (phone_number_registration.text.isNullOrEmpty()){
				Toast.makeText(requireContext(),getString(R.string.error_input_et),Toast.LENGTH_SHORT).show()
			}else{
				val phoneNumber = "+" + country_picker.fullNumber + phone_number_registration.text

				PaperIO.userPhoneNumber = phoneNumber

				presenter?.serverRequest(UserPhoneValidationRequest(phoneNumber), {
					if (it) {
						addFragment(CheckSMSFragment.newInstance())
					}
				}, {
					baseActivity.showShortToast(it)
				})
			}

		}

	}
}