package noty_team.com.urger.ui.fragment.tabs.housing

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_housing.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.housing.add_house.AddHouseFragment
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.HouseDetailFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.housing.HousingAdapterPagination
import noty_team.com.urger.utils.adapters.housing.HousingItem

class HousingFragment : BaseFragment<HousingPresenter>() {

    enum class ScreenType {
        REVIEW,
        RETURN
    }

    enum class ParentScreen {
        KOLBASA,
        PAGER
    }

    companion object {
        private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.housing"
        private const val SCREEN_TYPE = "SCREEN_TYPE"
        private const val PARENT_SCREEN_KEY = "PARENT_SCREEN"

        var isNeedUpdateHouse = false

        fun newInstance(
            isShowToolbar: Boolean, screenType: ScreenType = ScreenType.REVIEW,
            parentType: ParentScreen = ParentScreen.PAGER,
            returnCallback: (data: HousingItem) -> Unit = {}

        ): HousingFragment {
            return HousingFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
                    putSerializable(SCREEN_TYPE, screenType)
                    putSerializable(PARENT_SCREEN_KEY, parentType)
                    onReturn = returnCallback
                }
            }
        }

        fun getParentScreenType(args: Bundle?) = args?.getSerializable(PARENT_SCREEN_KEY) ?: ParentScreen.PAGER
        fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
        fun getScreenType(args: Bundle?) = args?.getSerializable(SCREEN_TYPE) ?: ScreenType.REVIEW
    }

    var isCallback = false
    var onAddHouseAdded: Observable<Unit>? = null
    var onReturn: (data: HousingItem) -> Unit = {}

    private lateinit var housingAdapterPagination: HousingAdapterPagination

    private val listHousing = ArrayList<HousingItem>()

    override fun layout() = R.layout.fragment_housing

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = HousingPresenter(baseContext)

        if (isFirstInit) {
            isNeedUpdateHouse = false
            getHousing(0)
            initToolbar()
            initAction()
            initRecycler()
        }
    }


    override fun onResume() {

        if (isNeedUpdateHouse && getParentScreenType(arguments) == ParentScreen.PAGER) {
            isNeedUpdateHouse = false
            housingAdapterPagination.clear()

            getHousing(0)
            initToolbar()
            initAction()
            initRecycler()

        }

        if (isCallback) {
            isCallback = false
            onAddHouseAdded?.subscribe()
            isNeedUpdateHouse = true
        }

        super.onResume()
    }

    var callbackAddHouse: (item: HousingItem) -> Unit = {
        isCallback = true
        isNeedUpdateHouse = false
        onAddHouseAdded = Observable.fromCallable { addNewHouse(it) }

    }

    private fun addNewHouse(housingItem: HousingItem) {

        housingAdapterPagination.addNewItemInStart(
            housingItem
        )
        empty_list?.visibility = View.GONE
        NestedProfileCompanyFragment.isNeedUpdate = true
    }

    private fun getHousing(size: Int) {
        presenter?.getHousing(size, { response ->

            if(response.isEmpty()) {
                empty_list.visibility = View.VISIBLE
            } else {
                empty_list.visibility = View.GONE
            }
            housingAdapterPagination.onNewDataReceived(response, Constants.documentItemLimit)
        }, {
            showShortToast(it)
        })
    }

    private fun initRecycler() {

        recycler_housing.layoutManager =
            LinearLayoutManager(baseActivity)

        housingAdapterPagination = HousingAdapterPagination(listHousing, recycler_housing,

            onClick = { it_ ->
                val screenType = getScreenType(arguments)

                when (screenType) {
                    ScreenType.REVIEW -> {
                        run {
                            addFragment(HouseDetailFragment.newInstance().apply {
                                houseId = it_.houseId
                            })
                        }
                    }
                    ScreenType.RETURN -> {
                        onReturn(it_)
                        onBackPressed()
                    }
                }
            },
            onLongClick = { _it ->
                run {

                    deleteDialog(getString(R.string.delete_hosing_item_text)) {
                        presenter?.deleteHouse(_it.houseId, {
                            showShortToast(resources.getString(R.string.uouse_success_delete))
                        }, {
                            if (it) {
                                housingAdapterPagination.remove(_it)
                                if(housingAdapterPagination.getSizeList() == 0) {
                                    empty_list.visibility = View.VISIBLE
                                } else {
                                    empty_list.visibility = View.GONE
                                }
                                showShortToast(resources.getString(R.string.success_delete))
                                NestedProfileCompanyFragment.isNeedUpdate = true
                                isNeedUpdateHouse = true
                            } else {
                                showShortToast("")
                            }
                        })
                    }
                }
            }
        )


        recycler_housing.adapter = housingAdapterPagination

    }

    private fun initAction() {
        nb_fragment_housing.setOnClickListener {
            onBackPressed()
        }

        housing_fragment_fab.setOnClickListener {
            openAddHouseFragment()
        }
    }

    private fun initToolbar() {

        if (isShowToolbar(arguments)) {
            housing_fragment_toolbar_container.visibility = View.VISIBLE
            housing_fragment_fab.show()
        } else {
            housing_fragment_toolbar_container.visibility = View.GONE
            housing_fragment_fab.hide()
        }
    }

    fun openAddHouseFragment() {
        addFragment(AddHouseFragment.newInstance().apply {
            callbackAddHouse = this@HousingFragment.callbackAddHouse
        })
    }
}