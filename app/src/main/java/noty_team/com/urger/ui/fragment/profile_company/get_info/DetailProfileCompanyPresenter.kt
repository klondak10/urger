package noty_team.com.urger.ui.fragment.profile_company.get_info

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.company.GetCompanyInfoResponse
import noty_team.com.urger.utils.api.response.get_employer.GetEmployerInfoResponse

class DetailProfileCompanyPresenter(context: Context) : BasePresenter(context) {


    fun getEmployerInfo(id: Int, isLoadData:(GetEmployerInfoResponse) -> Unit) {
        apiEmployer.getEmployerInfo(id, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                isLoadData(it)
            } else {
                showShortToast(it.error)
            }
        }, { errorMessage, errorCode ->

            showShortToast(errorMessage)

        }).callRequest()
    }

    fun getGetInfoCompany(companyId: Int, isLoadData:(GetCompanyInfoResponse) -> Unit) {
        apiEmployer.getInfoCompany(companyId, {

            if(it.statusCode == 200) {
                isLoadData(it)
            } else {
                showShortToast("getGetInfoCompany ${it.error}")
            }

        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }
}