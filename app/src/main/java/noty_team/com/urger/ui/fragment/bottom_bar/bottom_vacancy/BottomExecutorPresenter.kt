package noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.favorite_vacancy.AddFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.DeleteFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.news.NewsResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class BottomExecutorPresenter(context: Context): BasePresenter(context) {

    private var page: Int = 1
     var pageFilter = 1

    fun getVacancies(onSuccess:(response: VacanciesListResponse)->Unit, onError:()->Unit){

            apiEmployer.getVacanciesForExecutor(Constants.vacancyItemLimit, 0,{
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
            }).callRequest()

    }

    fun getVacanciesWithFilter(time: Int,
        onSuccess: (response: VacanciesListResponse) -> Unit,
        onError: () -> Unit) {
        apiEmployer.getVacanciesWithFilter(time, {
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()

    }
    fun loadMoreVacancy(onSuccess:(response: VacanciesListResponse)->Unit, onError:()->Unit) {

            apiEmployer.getVacanciesForExecutor(Constants.vacancyItemLimit, ++page,{
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                    showLongToast(errorMessage)
            }).callRequest()
        }

    fun addFavoriteVacancy(request: AddFavoriteVacancyExecutorRequest, onSuccess:(response: AddFavoriteVacancyResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
        } else {
            apiEmployer.addVacancyFavorite(request, {
                onSuccess(it)
            }, {errorMessage, errorCode ->

                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun deleteFavoriteVacancy(id: Int, onSuccess:(response: DeleteFavoriteVacancyResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
        } else {
            apiEmployer.deleteFavoriteVacancy(id, {
                onSuccess(it)
            }, {errorMessage, errorCode ->

                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun getNews(onLoadNews:(NewsResponse) -> Unit) {
        apiEmployer.getNews({
            if(it.statusCode == 200) {
                onLoadNews(it)
            } else {
                showShortToast("get News " + it.error)
            }

        }, {errorMessage, errorCode ->
            showShortToast("get News " + errorMessage)
        }).callRequest()
    }
}