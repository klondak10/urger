package noty_team.com.urger.ui.fragment.bottom_bar

import android.os.Bundle
import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import noty_team.com.urger.R
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.utils.cicerone.BackButtonListener
import noty_team.com.urger.utils.cicerone.LocalCiceroneHolder
import noty_team.com.urger.utils.cicerone.RouterProvider
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

open class ContainerFragment : Fragment(), RouterProvider, BackButtonListener {

    companion object {
        const val EXTRA_NAME = "tcf_extra_name"
    }

    private var navigator: Navigator? = null

    lateinit var ciceroneHolder: LocalCiceroneHolder

    open fun getContainerName(): String {
        return arguments?.getString(EXTRA_NAME)?:""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
//        AistApp.appComponent.inject(this)
        super.onCreate(savedInstanceState)

        ciceroneHolder = AppUrger.localCiceroneHolder
    }

    fun getCicerone(): Cicerone<Router> {
        return ciceroneHolder.getCicerone(getContainerName())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_container, container, false)
    }

    override fun onResume() {
        super.onResume()
        getCicerone().navigatorHolder.setNavigator(getNavigator())
    }

    override fun onPause() {
        getCicerone().navigatorHolder.removeNavigator()
        super.onPause()
    }

    private fun getNavigator(): Navigator {
        if (navigator == null) {
            navigator = SupportAppNavigator(activity, childFragmentManager, R.id.ftc_container)
        }
        return navigator as Navigator
    }

    override fun getRouter(): Router {
        return getCicerone().router
    }

    override fun onBackPressed(): Boolean {
        val fragment = childFragmentManager.findFragmentById(R.id.ftc_container)
        return if (fragment != null
            && fragment is BackButtonListener
            && (fragment as BackButtonListener).onBackPressed()) {
            true
        } else {
            (activity as RouterProvider).getRouter().exit()
            true
        }
    }
}