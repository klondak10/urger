package noty_team.com.urger.ui.fragment.login.phone.check_sms_code

import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.request.sms_phone_validation.UserPhoneValidationRequest
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class CheckSMSPresenter(context: Context) : BasePresenter(context), CheckSMSContract.Presenter {


	lateinit var showToastCallback: (massage: String) -> Unit
	private lateinit var isNeedSignUpCallback: (isSignUP: Boolean) -> Unit

	override fun serverRequest(
			smsCode: Int,
			isNeedSingUpCallback: (isSignUP: Boolean) -> Unit,
			showToast: (massage: String) -> Unit
	) {
		subscriptions.add(
				UrgerApi.AUTH_API.checkSMS(UserPhoneValidationRequest(PaperIO.userPhoneNumber, smsCode))
						.subscribeOn(Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe({
							this.showToastCallback = showToast
							this.isNeedSignUpCallback = isNeedSingUpCallback

							if (it.statusCode == 200 || it.statusCode == 201) {
								it.parseToken()
								it.isNeedSignUp()
								it.parseUserData()
								it.canUpdateProfile()

								if (PaperIO.signUp == Authorization.NEED_SING_UP) {

									isNeedSingUpCallback(true)
								} else {
									if (PaperIO.userRole == UserRole.COMPANY) {
										getEmployerInfo()
									} else {
										getExecutorInfo()
									}
								}

							} else {
								showToast(it.error)
							}

						}, {
							showToast(Constants.DISPOSABLE_ERROR)
						})
		)
	}

	fun getEmployerInfo() {

		apiEmployer.getEmployerInfo(PaperIO.employerData?.userId ?: -1, {

			if (it.statusCode == 200 || it.statusCode == 201) {
				it.isCanUpdate()
				it.parseCompany()
				it.parseUser()
				isNeedSignUpCallback(false)
			} else {
				showToastCallback(it.error)
			}
		}, { errorMessage, errorCode ->
			showToastCallback("error")
		}).callRequest()


	}

	private fun getExecutorInfo() {
		apiEmployer.getExecutorInfo(PaperIO.employerData?.userId ?: -1, {
			if (it.statusCode == 200 || it.statusCode == 201) {
				it.parseUser()
				isNeedSignUpCallback(false)
			} else {
				showToastCallback(it.error)
			}
		}, { errorMessage, errorCode ->
			showToastCallback("error")
		}).callRequest()
	}

}