package noty_team.com.urger.ui.fragment.profile_company.pager_company


import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_detail_company.*
import kotlinx.android.synthetic.main.fragment_profile_nested_detail_company.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.FeedbackAdapter
import noty_team.com.urger.ui.fragment.profile_company.get_info.DetailProfileCompany
import noty_team.com.urger.ui.fragment.tabs.company.company_photo.CompanyPhotoFragment
import noty_team.com.urger.ui.fragment.tabs.contract.document_detail.FragmentDocumentDetails
import noty_team.com.urger.ui.fragment.tabs.documents.document_detail.FragmentDocumentDetail
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.HouseDetailFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.PhotoAdapter
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.document.DocumentsAdapterPagination
import noty_team.com.urger.utils.adapters.housing.HousingAdapterPagination
import noty_team.com.urger.utils.api.response.feedback.get.executor.ListItem
import noty_team.com.urger.utils.setPhoto


class NestedProfileCompanyDetail : BaseFragment<NestedProfileCompanyDetailPresenter>() {

    private var companyId = 0
    private lateinit var photoAdapter: PhotoAdapter

    private lateinit var contractsAdapterPagination: DocumentsAdapterPagination
    private lateinit var documentsAdapterPagination: DocumentsAdapterPagination
    private lateinit var adapterFeedback: FeedbackAdapter
    private lateinit var housingAdapterPagination: HousingAdapterPagination
    private var listContracts = ArrayList<DocumentItem>()
    private var listDocuments = ArrayList<DocumentItem>()
    var listFeedback: ArrayList<ListItem> = arrayListOf()

    companion object {
        fun newInstance(companyId: Int): NestedProfileCompanyDetail {
            return NestedProfileCompanyDetail().apply {
                arguments = Bundle().apply {
                    putInt("companyId", companyId)
                }
            }
        }
    }

    override fun layout() = R.layout.fragment_profile_nested_detail_company

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = NestedProfileCompanyDetailPresenter(baseContext)
        companyId = arguments!!["companyId"] as Int
        bottomBarIsVisible(false)

        initInfo()

    }

    private fun initInfo() {

        getCompanyInfo()
        initCompanyRecycler()
        initCountDataCompany()
        initCompanyInfo()
        initCompanyLogo()

        initRecycler()
        getContracts(0)

        initRecyclerDocuments()
        getDocuments(0)

        initRecyclerFeedback()
        getFeedback()

        initRecyclerHouse()
        getHousing(0)
    }

    private fun getFeedback() {

        presenter?.getFeedbackCompany {

            if (!it.data!!.list.isNullOrEmpty()) {

                listFeedback =
                    ArrayList(it.data.list.sortedWith(compareByDescending(ListItem::employerRate)))
                adapterFeedback.setData(listFeedback)
            }

            ratingBar_nested_company.rating =
                (Math.round(it.data.total!!.rate!! * 10) / 10.0).toFloat()

            count_ratings_nested_company.text = "${it.data.total.count} review"
            rating_tv_nested_company.text =
                (Math.round(it.data.total!!.rate!! * 10) / 10.0).toString()

        }
    }

    private fun initRecyclerFeedback() {
        adapterFeedback = FeedbackAdapter(arrayListOf(), baseActivity)
        recycler_nested_company_feedback_detail.layoutManager =
            LinearLayoutManager(baseActivity)
        recycler_nested_company_feedback_detail.adapter = adapterFeedback
    }

    private fun initCompanyRecycler() {

        company_photo_nested_detail.layoutManager =
            LinearLayoutManager(
                baseActivity,
                LinearLayout.HORIZONTAL,
                false
            )
        company_photo_nested_detail.isNestedScrollingEnabled = false

        loadCompanyPhotoList()

        this.photoAdapter = PhotoAdapter(arrayListOf(), onClick = { index ->

            addFragment(CompanyPhotoFragment.newInstance().apply {
                this.index = index
                listImages = photoAdapter.getItemsList()
            })
        })

        company_photo_nested_detail.adapter = this.photoAdapter
    }

    private fun loadCompanyPhotoList() {

        presenter?.getCompanyPhotos(companyId, {
            this.photoAdapter.addAll(it)
            this.photoAdapter.notifyDataSetChanged()
        }, {
            baseActivity.showShortToast(Constants.DISPOSABLE_ERROR)
        })
    }

    private fun getCompanyInfo() {
        presenter?.getGetInfoCompany(companyId) {

            Paper.book().write("about", it.data?.about ?: "")
            nested_company_description.text = it.data?.about ?: ""
            Paper.book().write("feedback", "true")
            Paper.book().write("document", "true")

        }

        presenter?.getEmployerInfo(Paper.book().read("user_id_employer")) {

            if (it.data?.employer?.avatar != null) {
                setPhoto(company_avatar, it.data?.employer?.avatar)
            }
        }
    }

    private fun initCountDataCompany() {
        presenter?.getStatistic(companyId, {
            showShortToast(it)
        }, { countDocument, countHousing, countVacancies ->

            company_document_statistic_nested.text = countDocument
            company_vacancy_statistic_nested.text = countVacancies
            company_housing_statistic_nested.text = countHousing

        })
    }

    private fun initCompanyInfo() {
        //init company name
        nested_global_company_name.text = "in " + PaperIO.companyData?.name


        //initPosition
        if (PaperIO.companyData?.position != null && PaperIO.companyData?.position != "")
            nested_global_company_position.text = PaperIO.companyData?.position
        else nested_global_company_position.text = getString(R.string.some_position)

        //location
        nested_global_company_location.text = PaperIO.companyData?.city

    }

    private fun initCompanyLogo() {

        if (PaperIO.companyData?.logo != null && PaperIO.companyData?.logo != "") {
            nested_company_logo_img.visibility = View.VISIBLE
            nested_company_logo_tv.visibility = View.GONE

            setPhoto(nested_company_logo_img, PaperIO.companyData?.logo ?: "")

        } else {
            nested_company_logo_img.visibility = View.GONE
            nested_company_logo_tv.visibility = View.VISIBLE

            nested_company_logo_tv.text = PaperIO.companyData?.name?.substring(0, 1)
        }
    }

    private fun initRecycler() {

        recycler_nested_document_detail?.layoutManager =
            GridLayoutManager(baseActivity, 2)

        contractsAdapterPagination =
            DocumentsAdapterPagination(listContracts, recycler_nested_document_detail,

                onClickPosition = { it_, position ->
                    run {
                        addFragment(FragmentDocumentDetails.newInstance().apply {
                            fileName = it_.documentName
                            addressLink = it_.image
                            listDocument = listContracts
                            pos = position
                        })
                    }
                })

        recycler_nested_document_detail.adapter = contractsAdapterPagination

    }

    private fun getContracts(size: Int) {
        presenter?.getContractPagination(size, companyId) { response ->
            listContracts = response
            photoAdapter.notifyDataSetChanged()
        }
    }

    private fun initRecyclerDocuments() {

        recycler_nested_contracts?.layoutManager =
            GridLayoutManager(baseActivity, 2)

        documentsAdapterPagination =
            DocumentsAdapterPagination(listDocuments, recycler_nested_contracts,

                onClick = { it_ ->
                    run {
                        addFragment(FragmentDocumentDetail.newInstance().apply {
                            fileName = it_.documentName
                            addressLink = it_.file
                        })
                    }
                }
            )

        recycler_nested_contracts.adapter = documentsAdapterPagination
        documentsAdapterPagination.onListRefresh()
    }

    private fun getDocuments(size: Int) {
        presenter?.getDocumentsPagination(companyId, size) { response ->
            documentsAdapterPagination.onNewDataReceived(response)

            Paper.book().delete("document")
        }
    }

    private fun getHousing(size: Int) {
        val id = arguments!![DetailProfileCompany.ID_COMPANY] as Int
        presenter?.getHousing(id, size, { response ->

            housingAdapterPagination.onNewDataReceived(response, Constants.documentItemLimit)
        }, {
            //showShortToast(it)
        })
    }

    private fun initRecyclerHouse() {

        recycler_nested_housing_detail.layoutManager =
            LinearLayoutManager(baseActivity)

        housingAdapterPagination = HousingAdapterPagination(
            arrayListOf(), recycler_nested_housing_detail,

            onClick = { it_ ->

                run {
                    addFragment(HouseDetailFragment.newInstance().apply {
                        houseId = it_.houseId
                    })
                }
            }
        )

        recycler_nested_housing_detail.adapter = housingAdapterPagination

    }
}