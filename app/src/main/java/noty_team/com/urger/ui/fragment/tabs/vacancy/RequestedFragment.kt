package noty_team.com.urger.ui.fragment.tabs.vacancy

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_preliminary.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.adapters.recycler.RequestedAdapter
import noty_team.com.urger.ui.fragment.drawer.my_employee.preliminary.PreliminaryPresenter
import noty_team.com.urger.utils.Constants


class RequestedFragment : BaseFragment<PreliminaryPresenter>() {

    companion object {
        fun newInstance(id: Int):RequestedFragment {
            return RequestedFragment().apply {
                arguments = Bundle().apply {
                    putInt("id", id)
                }
            }
        }
    }

    lateinit var adapterRequested: RequestedAdapter
    override fun layout() = noty_team.com.urger.R.layout.fragment_preliminary

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = PreliminaryPresenter(baseContext)
        initAdapter()

        getPreliminaryVacancy()
    }

    private fun getPreliminaryVacancy() {
        presenter?.getCompaniesRequests {
            val id = arguments!!["id"]

            var filterList = it.data!!.filter { s -> s.user!!.id == id }.filter { i -> i.status == 0 }
            if(!filterList.isNullOrEmpty()) {
                adapterRequested.onNewDataReceived(ArrayList(filterList), Constants.documentItemLimit)
                empty_list_tv?.visibility = View.GONE
            } else {
                empty_list_tv?.visibility = View.VISIBLE
            }
        }
    }

    private fun initAdapter() {
        recycler_preliminary.layoutManager =
            LinearLayoutManager(baseActivity)
        adapterRequested = RequestedAdapter(arrayListOf(), recycler_preliminary, false,
            onLoadData = {
                getPreliminaryVacancy()
            },
            onClick = { it_ ->

                //replaceFragment(ContractFragment.newInstance(it_.id!!))
                showDialog(it_.id!!, it_.vacancy?.categoryName!!)
            })
        recycler_preliminary.adapter = adapterRequested
    }
    private fun showDialog(vacancyId: Int, name: String) {
        val vacancyDialog = Dialog(baseActivity)

        vacancyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        vacancyDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        vacancyDialog.setContentView(R.layout.dialog_apply_vacancy)

        val applyButton: Button? = vacancyDialog.findViewById(R.id.apply_button)

        val cancelButton: Button = vacancyDialog.findViewById(R.id.cancel_button)
        val title = vacancyDialog.findViewById(R.id.apply_dialog_text) as TextView
        title.text = "${resources.getString(R.string.confirm_apply, name)}"
        cancelButton.setOnClickListener {
            vacancyDialog.dismiss()
        }

        applyButton?.setOnClickListener {
            presenter?.applyVacancy(vacancyId) {
                if(it) {
                    showShortToast(getString(R.string.success_apply_executor))
                    CompanyActivity.start(baseActivity)
                    baseActivity.finish()
                } else {
                    showShortToast(getString(R.string.error_apply_executor))
                }
            }
            vacancyDialog.dismiss()
        }
        cancelButton?.setOnClickListener {
            vacancyDialog.dismiss()
        }

        vacancyDialog.show()
    }
}