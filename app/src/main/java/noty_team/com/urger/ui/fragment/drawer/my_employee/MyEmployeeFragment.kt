package noty_team.com.urger.ui.fragment.drawer.my_employee

import android.view.View
import kotlinx.android.synthetic.main.fragment_my_employee.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.view_pager.MyEmployeePageAdapter
import noty_team.com.urger.ui.fragment.drawer.my_employee.hired.HiredFragment
import noty_team.com.urger.ui.fragment.drawer.my_employee.preliminary.PreliminaryFragment
import noty_team.com.urger.utils.adapters.CustomViewPager
import ru.dimorinny.showcasecard.ShowCaseView
import ru.dimorinny.showcasecard.position.TopLeftToolbar
import ru.dimorinny.showcasecard.position.TopRightToolbar
import ru.dimorinny.showcasecard.radius.Radius

class MyEmployeeFragment : BaseFragment<MyEmployeePresenter>() {

    companion object {
        fun newInstance() = MyEmployeeFragment()
    }

    override fun layout() = R.layout.fragment_my_employee

    override fun initialization(view: View, isFirstInit: Boolean) {
        bottomBarIsVisible(false)
        if(isFirstInit){
            initTab()
        }


        drawer_my_employee.setOnClickListener {
            bottomBarIsVisible(true)
            onBackPressed()
        }

        /*ShowCaseView.Builder(activity)
            .withTypedPosition(TopLeftToolbar())
            .withTypedRadius(Radius(286f))
            .withContent(getString(R.string.show_case))
            .build().show(activity)*/
    }

    private fun initTab() {

        var viewPager = baseActivity.findViewById<CustomViewPager>(R.id.my_employee_viewPager)

        viewPager.adapter = MyEmployeePageAdapter(
            childFragmentManager, viewPager, arrayListOf(
                PreliminaryFragment(),
                HiredFragment()
            )
        )
        my_employee_tabLayout.setupWithViewPager(viewPager)
    }
}