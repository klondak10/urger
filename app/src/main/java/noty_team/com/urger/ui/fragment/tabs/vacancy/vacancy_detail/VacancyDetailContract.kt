package noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail

import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyResponse

interface VacancyDetailContract {
	interface Presenter {

		fun getVacancy(vacancyId: Int,typeVacancy: TypeVacancy, onDataLoad: (vacancyItem: VacancyResponse) -> Unit, onError: () -> Unit)
		fun getHouseItem(houseId: Int, companyId: Int, onDataLoad: (house: DataMapResponse) -> Unit)
	}
}