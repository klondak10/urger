package noty_team.com.urger.ui.fragment.profile_executor.get_info.documents

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_info_executor.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.get_info.DetailProfileCompany
import noty_team.com.urger.ui.fragment.profile_executor.DetailProfileExecutorPresenter
import noty_team.com.urger.ui.fragment.tabs.contract.document_detail.FragmentDocumentDetails
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.document.DocumentsAdapterPagination
import noty_team.com.urger.utils.api.response.all_info.DocumentsItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class DocumentsFragment : BaseFragment<DetailProfileExecutorPresenter>() {

	 var data: List<DocumentsItem?> = listOf()

	companion object {
		fun newInstance(companyId: Int = 0): DocumentsFragment {

			return DocumentsFragment().apply {
				arguments = Bundle().apply {
					putInt(DetailProfileCompany.ID_COMPANY, companyId)
				}
			}
		}
	}

	override fun layout() = R.layout.fragment_info_executor

	private lateinit var documentsAdapterPagination: DocumentsAdapterPagination

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = DetailProfileExecutorPresenter(baseContext)

		if(PaperIO.userRole == UserRole.EXECUTOR) {
			presenter?.getDocumentsCompanys(arguments?.get(DetailProfileCompany.ID_COMPANY) as Int) {
				initAdapter(it)
			}
		} else {
			initAdapter(convertData())
		}
	}

	private fun convertData(): ArrayList<DocumentItem> {
		val list: ArrayList<DocumentItem> = arrayListOf()
		data.forEach {
			val item = DocumentItem(it!!.id!!, it.preview!!, it.name!!, it.file!!)
			list.add(item)
		}
		return list
	}

	private fun initAdapter(list: ArrayList<DocumentItem>) {
		if(list.isNullOrEmpty()) {
			empty_list.visibility = View.VISIBLE
		}
		recycler_list?.layoutManager =
            GridLayoutManager(baseActivity, 2)

		documentsAdapterPagination = DocumentsAdapterPagination(list, recycler_list,


			onClick = { it_ ->


					run {
						addFragment(FragmentDocumentDetails.newInstance().apply {
							fileName = it_.documentName
							addressLink = it_.image

						})
					}


			}


		)

		recycler_list?.adapter = documentsAdapterPagination
	}
}