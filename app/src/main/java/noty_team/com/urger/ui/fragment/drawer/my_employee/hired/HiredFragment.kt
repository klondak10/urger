package noty_team.com.urger.ui.fragment.drawer.my_employee.hired

import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_hired.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.HiredAdapter
import noty_team.com.urger.ui.fragment.drawer.rating_feedback.RatingFeedbackFragment
import noty_team.com.urger.ui.fragment.profile_executor.get_info.DetailProfileExecutor
import noty_team.com.urger.utils.Constants

class HiredFragment : BaseFragment<HiredPresenter>() {

    lateinit var adapterHired: HiredAdapter

    override fun layout() = R.layout.fragment_hired

    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = HiredPresenter(baseContext)
        recycler_hired.layoutManager =
            LinearLayoutManager(baseActivity)
        initAdapter()

        getHiredVacancy()

            //navigator().navigateToFragment(RatingFeedbackFragment.newInstance(), null, true)

    }

    private fun getHiredVacancy() {
        presenter?.getCompaniesRequests {
            if(it.data != null) {

                var filterList = it.data.filter { i -> i.status == 2 }
                if(!filterList.isNullOrEmpty()) {
                    empty_list_tv.visibility = View.GONE
                }
                adapterHired.onNewDataReceived(ArrayList(filterList), Constants.documentItemLimit)
            }
        }
    }

    private fun initAdapter() {
        recycler_hired.layoutManager =
            LinearLayoutManager(baseActivity)
        adapterHired = HiredAdapter(arrayListOf(), recycler_hired,
            onLoadData = {
                getHiredVacancy()
            },
            onDeleteUser = { it_ ->
                replaceFragment(RatingFeedbackFragment.newInstance(it_.id!!))
            },
            onClick = {
                addFragment(DetailProfileExecutor.newInstance(it.user!!, it.auto!!))
            })
        recycler_hired.adapter = adapterHired
    }
}