package noty_team.com.urger.ui.fragment.bottom_bar.chat

import android.content.Context
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import io.paperdb.Paper
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_executor.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_chat.nb_bottom_chat_search_back
import kotlinx.android.synthetic.main.fragment_chat.search_toolbar_container
import kotlinx.android.synthetic.main.fragment_executor_bottom_vacancy_news.*
import kotlinx.android.synthetic.main.fragment_service_delivery.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.ChatSocket
import noty_team.com.urger.utils.Room
import noty_team.com.urger.utils.Rooms
import noty_team.com.urger.utils.adapters.chat.AdapterRoomChat
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import org.json.JSONObject

class ChatRoomsFragment : BaseFragment<ChatPresenter>() {

    private var roomsSocket = ChatSocket.provideSocketChat()
    lateinit var adapterRoomsChat: AdapterRoomChat
    private var chatRooms: List<Room> = listOf()

    companion object {
        fun newInstance() = ChatRoomsFragment()
    }

    override fun layout() = R.layout.fragment_chat

    override fun initialization(view: View, isFirstInit: Boolean) {

        
        if (isFirstInit){
            isShowLoadingDialog(true)
            initAdapter()
            initSocket()
        }


        drawer_chat.setOnClickListener {
            baseActivity.openDrawerLayout()
        }


        search_chat.getClick {
            toolbar_chat_et?.setText("")
            chat_toolbar.visibility = View.GONE
            search_toolbar_container.visibility = View.VISIBLE

            //val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

            //imm.showSoftInput(toolbar_vacancy_et, InputMethodManager.SHOW_IMPLICIT)

            toolbar_chat_et.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (p0!!.isNotEmpty()) {
                        //adapterRoomsChat.filter.filter(p0)
                        val list = chatRooms.filter {
                            it.name.toLowerCase().contains(p0.toString().toLowerCase())
                        }
                        if (list.isEmpty()) {
                            empty_list_search.visibility = View.VISIBLE
                        } else {
                            empty_list_search.visibility = View.GONE
                        }
                        adapterRoomsChat.setNewList(ArrayList(list))
                    } else {
                        adapterRoomsChat.setNewList(ArrayList(chatRooms))
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {


                }
            })
        }
        nb_bottom_chat_search_back.setOnClickListener {
            /// invisible keyboard
            baseActivity.toggleKeyboard(false)
            empty_list_search.visibility = View.GONE
            if (!chatRooms.isNullOrEmpty()) {
                adapterRoomsChat.setList(chatRooms)
            }
            chat_toolbar.visibility = View.VISIBLE
            search_toolbar_container.visibility = View.GONE
        }
    }

    private fun initAdapter() {
        recycler_rooms.layoutManager = LinearLayoutManager(baseActivity)
        adapterRoomsChat = AdapterRoomChat(baseContext, arrayListOf(),
            onClickChat = {
                if (PaperIO.userRole == UserRole.COMPANY) {
                    baseActivity.drawer_layout?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                } else {
                    baseActivity.drawer_layout_executor?.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                }

                addFragment(DialogChatFragment.newInstance(it.userId, it.name, it.avatar))
            }, callbackEmptyList = {
                if (it) {
                    empty_list_search.visibility = View.VISIBLE

                } else {
                    empty_list_search.visibility = View.GONE
                }
            })

        recycler_rooms.adapter = adapterRoomsChat
    }

    private fun initSocket() {

        if (!roomsSocket.connected())
            roomsSocket.connect()
        val jsonObject = JSONObject()
        if (PaperIO.userRole == UserRole.COMPANY) {
            jsonObject.put("userId", PaperIO.employerData!!.userId)
        } else {
            jsonObject.put("userId", PaperIO.executorData!!.userId)
        }
        roomsSocket.emit("getRooms", jsonObject).on("getRooms", OnChatsListener())
    }

    inner class OnChatsListener : Emitter.Listener {
        override fun call(vararg args: Any?) {
            val rooms = Gson().fromJson(args[0].toString(), Rooms::class.java) as Rooms

            isShowLoadingDialog(false)
            if (!rooms.rooms.isNullOrEmpty()) {
                baseActivity.runOnUiThread {
                    empty_list_tv?.visibility = View.GONE
                    search_chat?.visibility = View.VISIBLE
                    chatRooms = rooms.rooms
                    adapterRoomsChat.setList(rooms.rooms)
                }
            } else {
                baseActivity.runOnUiThread {
                    empty_list_tv?.visibility = View.VISIBLE
                }
            }
        }
    }
}