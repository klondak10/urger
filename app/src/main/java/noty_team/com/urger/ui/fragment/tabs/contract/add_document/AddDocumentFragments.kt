package noty_team.com.urger.ui.fragment.tabs.contract.add_document

import android.app.Activity
import android.app.Dialog
import android.graphics.Bitmap
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.drjacky.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.fragment_add_document.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.camera.FilesPicker.pickFiles
import ru.dimorinny.showcasecard.ShowCaseView
import ru.dimorinny.showcasecard.position.ViewPosition
import ru.dimorinny.showcasecard.radius.Radius
import java.io.File

class AddDocumentFragments : BaseFragment<AddDocumentPresenters>() {

	companion object {
		fun newInstance() = AddDocumentFragments()
	}

	var callbackAddDocument:(item: DocumentItem) -> Unit = {}


	override fun layout() = R.layout.fragment_add_contract

	override fun initialization(view: View, isFirstInit: Boolean) {
		bottomBarIsVisible(false)

		if (isFirstInit) {
			presenter = AddDocumentPresenters(baseContext)

			intiAction()
		} else {
			isShowLoadingDialog(false)
		}


		ShowCaseView.Builder(activity)
            .withTypedPosition(ViewPosition(choose_file))
            .withTypedRadius(Radius(286f))
            .withContent(getString(R.string.show_case_contract))
            .build().show(activity)

	}

	private fun intiAction() {
		navigate_back_add_document.getClick {
			onBackPressed()
		}

		confirm_add_document.getClick {

			val documentName = document_name.text.toString()

			if (documentName.trim().isEmpty())
				baseActivity.showShortToast(getString(R.string.input_file_name))
			else if (presenter?.isBitmapInitialize() != true)
				baseActivity.showShortToast(getString(R.string.load_pdf_file))
			else if (documentName.trim().isNotEmpty() && presenter?.isBitmapInitialize() == true) {

				isShowLoadingDialog(true)

				presenter?.addDocumentRequest(documentName, {
					if (!it) {
						isShowLoadingDialog(false)
					}
				}, { document ->
					showShortToast(resources.getString(R.string.add_document_success))
					isShowLoadingDialog(false)
					callbackAddDocument(document)
					onBackPressed()
//					if (PaperIO.userRole == UserRole.COMPANY) {
//						CompanyActivity.start(baseActivity)
//						baseActivity.finish()
//					} else {
//						ExecutorActivity.start(baseActivity)
//						baseActivity.finish()
//					}
				})
			} else {
				isShowLoadingDialog(false)
			}
		}

		choose_file.setOnClickListener {
			showDialogChoose().show()
		}
	}
	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!

				presenter?.getBitmapFromFileData(File(uri.path.toString())) {
					file_name_text_view?.text = uri.path?.let { it1 -> File(it1).name }
					loadImage(it)
				}

			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				presenter?.getBitmapFromFileData(File(uri.path.toString())) {
					file_name_text_view?.text = uri.path?.let { it1 -> File(it1).name }
					loadImage(it)
				}

			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}

	private fun loadImage(bitMap: Bitmap) {
		if (pdf_preview_image_view != null)
			Glide.with(this)
					.load(bitMap)
					.apply(
							RequestOptions()
									.encodeQuality(33)
									.fitCenter()
									.skipMemoryCache(true)
									.diskCacheStrategy(DiskCacheStrategy.NONE)
					)
					.into(pdf_preview_image_view)
	}

}