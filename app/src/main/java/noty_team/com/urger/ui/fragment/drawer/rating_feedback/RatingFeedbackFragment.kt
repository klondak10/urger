package noty_team.com.urger.ui.fragment.drawer.rating_feedback

import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.fragment_rating_feedback.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class RatingFeedbackFragment: BaseFragment<RatingFeedbackPresenter>() {

    companion object {
        fun newInstance(id: Int): RatingFeedbackFragment {
            return RatingFeedbackFragment().apply {
                arguments = Bundle().apply {
                    putInt("id", id)
                }
            }
        }
    }

    override fun layout() = R.layout.fragment_rating_feedback

    override fun initialization(view: View, isFirstInit: Boolean) {
        val id = arguments!!["id"] as Int

        presenter = RatingFeedbackPresenter(baseContext)

        close_button.setOnClickListener {
            onBackPressed()
        }
        drawer_feedback_done.getClick {
            if (validator()) {
                if (PaperIO.userRole == UserRole.COMPANY) {
                    presenter?.addFeedbackFromCompany(
                        id,
                        (simpleRatingBar.rating).toInt(),
                        text_tv.text.toString()
                    ) {
                        if (it) {
                            showShortToast(baseContext.resources.getString(R.string.success_feedback))
                        } else {
                            showShortToast(baseContext.resources.getString(R.string.failure_feedback))
                        }
                    }
                } else {
                    presenter?.addFeedbackFromExecutor(
                        id,
                        (simpleRatingBar.rating).toInt(),
                        text_tv.text.toString()
                    ) {
                        if (it) {
                            showShortToast(baseContext.resources.getString(R.string.success_feedback))
                        } else {
                            showShortToast(baseContext.resources.getString(R.string.failure_feedback))
                        }
                    }
                }
                if (PaperIO.userRole == UserRole.COMPANY) {
                    CompanyActivity.start(baseActivity)
                    baseActivity.finish()
                } else {
                    ExecutorActivity.start(baseActivity)
                    baseActivity.finish()
                }
            }
        }
    }

    private fun validator(): Boolean {
        if(simpleRatingBar.rating.toInt() == 0) {
            showShortToast(resources.getString(R.string.error_add_rate))
            return false
        } else if(text_tv.text.toString().length < 3) {
            showShortToast(resources.getString(R.string.error_add_review))
            return false
        }
        return true
    }
}