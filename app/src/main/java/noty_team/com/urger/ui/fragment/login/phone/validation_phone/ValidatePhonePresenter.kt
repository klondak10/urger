package noty_team.com.urger.ui.fragment.login.phone.validation_phone

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.request.sms_phone_validation.UserPhoneValidationRequest

class ValidatePhonePresenter(context: Context) : BasePresenter(context), ValidatePhoneContract.Presenter {


	override fun serverRequest(data: UserPhoneValidationRequest, openNextScreen: (isOpen: Boolean) -> Unit, showToast: (massage: String) -> Unit) {
		apiAuthorization.sendPhone(data, {
			openNextScreen(true)
		}, { errorMessage, errorCode ->

			showToast(errorMessage)

		}).callRequest()
	}
}