package noty_team.com.urger.ui.fragment.drawer.settings

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.request.edit_user_personal_info.FcmRequest
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class SettingPresenter(context: Context) : BasePresenter(context) {

    fun updateFcm(fcm: String, isSuccessUpdate:(Boolean) -> Unit) {
        if (PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.updateFcmEmployer(PaperIO.employerData!!.userId!!, FcmRequest(fcm), {
                if(it.statusCode == 200) {
                    isSuccessUpdate(true)
                }else {
                    isSuccessUpdate(false)
                }
            }, { errorMessage, errorCode ->
                showShortToast("SettingPresenter updateFcm enployer $errorMessage")
                isSuccessUpdate(false)
            }).callRequest()
        } else {

            apiEmployer.updateFcmExecutor(FcmRequest(fcm), {
                if(it.statusCode == 200) {
                    isSuccessUpdate(true)
                } else {
                    isSuccessUpdate(false)
                }
            }, { errorMessage, errorCode ->
                showShortToast("SettingPresenter updateFcm executor $errorMessage")
                isSuccessUpdate(false)
            }).callRequest()
        }
    }
}