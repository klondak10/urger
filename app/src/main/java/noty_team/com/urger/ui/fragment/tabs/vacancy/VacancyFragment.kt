package noty_team.com.urger.ui.fragment.tabs.vacancy

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_vacancy.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.ServiceDeliveryAdapter
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.add_vacancy.AddVacancyFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.currency.CurrencyItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.api.response.work_schedule.ScheduleItem


class VacancyFragment : BaseFragment<VacancyPresenter>() {

    enum class ParentScreen {
        KOLBASA,
        PAGER
    }

    var isLoading = false
    companion object {
        private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.vacancy"
        private const val PARENT_SCREEN_KEY = "PARENT_SCREEN"

        var isNeedUpdateVacancy = false

        fun newInstance(isShowToolbar: Boolean, parentType: ParentScreen = ParentScreen.PAGER): VacancyFragment {
            return VacancyFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(PARENT_SCREEN_KEY, parentType)
                    putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
                }
            }
        }

        fun getParentScreenType(args: Bundle?) = args?.getSerializable(PARENT_SCREEN_KEY) ?: ParentScreen.PAGER
        fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
    }


    override fun onResume() {
        if (isNeedUpdateVacancy && getParentScreenType(arguments) == ParentScreen.PAGER) {
            initView(true)
        }
        super.onResume()
    }

    override fun layout() = noty_team.com.urger.R.layout.fragment_vacancy

    private lateinit var vacancyAdapter: ServiceDeliveryAdapter

    private var scheduleTypes = arrayListOf<ScheduleItem>()
    private var currencyTypes = arrayListOf<CurrencyItem>()

    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = VacancyPresenter(baseContext)
        initView(isFirstInit)
//        recycler_vacancy.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//
//           override fun onScrollStateChanged(recyclerView: RecyclerView , newState: Int) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {
//                    Log.d("-----","end");
//
//                }
//            }
//        })
        recycler_vacancy.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
               val linearLayoutManager = recyclerView.getLayoutManager() as LinearLayoutManager
                if (!isLoading) {

                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == vacancyAdapter.itemCount - 1) {
                        isLoading = true
                        presenter?.loadMoreVacancy({
                            vacancyAdapter.addNewVacancies(it.map(scheduleTypes, currencyTypes))
                            isLoading = false
                        },{
                            isLoading = false
                        })
                    }
            }
                super.onScrolled(recyclerView, dx, dy)
        }})
    }


    private fun initView(isFirstInit: Boolean) {

        if (isFirstInit) {
            isNeedUpdateVacancy = false

            if (::vacancyAdapter.isInitialized) vacancyAdapter?.clear()

            initFABToolbar()
            initAction()

            getVacancies(0)

        }
    }

    private fun initAction() {
        nb_vacancy_fragment.getClick {
            onBackPressed()
        }

        vacancy_fragment_fab.getClick {
            openAddVacancyFragment()
        }
    }

    private fun initFABToolbar() {
        if (isShowToolbar(arguments)) {
            vacancy_fragment_fab.show()
            vacancy_fragment_toolbar_container.visibility = View.VISIBLE
        } else {
            vacancy_fragment_fab.hide()
            vacancy_fragment_toolbar_container.visibility = View.GONE
        }
    }

    fun openAddVacancyFragment() {
        addFragment(AddVacancyFragment.newInstance(AddVacancyFragment.ScreenType.CREATING) { data, isEdit ->
            if (!isEdit) addNewVacancy(data)
            else editVacancy(data)
        })

    }

    private fun getCurrency() {
        presenter?.getCurrency({

            currencyTypes = it.data

            getVacancies(0)

        }, {
            isShowLoadingDialog(false)
        })
    }

    private fun getVacancies(size: Int) {
        presenter?.getVacancies(size, {
            if(it.data.isEmpty()) {
                empty_list.visibility = View.VISIBLE
            } else {
                empty_list.visibility = View.GONE
            }
            initVacanciesAdapter(it.map(scheduleTypes, currencyTypes))
        }, {

        })
    }

    private fun addNewVacancy(vacancyItem: VacancyPaginationItem) {
        vacancyAdapter.addNewVacancy(vacancyItem)
        vacancyAdapter.notifyDataSetChanged()
    }


    private fun editVacancy(vacancyItem: VacancyPaginationItem) {
        val scheduleItems = scheduleTypes.filter { it.key == vacancyItem.workSchedule }
        val schedule: String = if (scheduleItems.isNotEmpty()) scheduleItems[0].value else ""
        vacancyItem.workSchedule = schedule
        vacancyAdapter.editItem(vacancyItem)
    }

    private fun initVacanciesAdapter(list: ArrayList<VacancyPaginationItem>) {
        recycler_vacancy.layoutManager =
            LinearLayoutManager(baseActivity)
        vacancyAdapter = ServiceDeliveryAdapter(list, baseContext,
            onLoadData = {
                getVacancies(it)
            },
            onClick = {
                addFragment(VacancyDetailFragmnet.newInstance(it.vacancyId))
            },
            onLongClick = { _it ->
                showDialog(_it.vacancyId)
            }
        )
        recycler_vacancy.adapter = vacancyAdapter
    }

    private fun showDialog(vacancyId: Int) {
        val vacancyDialog = Dialog(baseActivity)

        vacancyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        vacancyDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if(PaperIO.userRole == UserRole.COMPANY) {
            vacancyDialog.setContentView(noty_team.com.urger.R.layout.dialog_employer_vacancy)
        } else {
            vacancyDialog.setContentView(noty_team.com.urger.R.layout.dialog_executor_vacancy)
        }
        var editButton:Button? = null
        var deleteButton:Button? = null
        var addToFavButton:Button? = null
        try {
            editButton = vacancyDialog.findViewById(noty_team.com.urger.R.id.edit_vacancy_button)
        } catch(e:NullPointerException){}
        try {
            deleteButton = vacancyDialog.findViewById(noty_team.com.urger.R.id.delete_vacancy_button)
        } catch(e:NullPointerException){}
        try {
            addToFavButton = vacancyDialog.findViewById(noty_team.com.urger.R.id.add_to_favorite_button)
        } catch(e:NullPointerException){}

        val cancelButton:Button = vacancyDialog.findViewById(noty_team.com.urger.R.id.cancel_vacancy_button)

        cancelButton.setOnClickListener {
            vacancyDialog.dismiss()
        }

        editButton?.setOnClickListener {
            addFragment(AddVacancyFragment.newInstance(
                AddVacancyFragment.ScreenType.EDITING,
                vacancyId
            ) { data, isEdit ->
                if (!isEdit) addNewVacancy(data)
                else editVacancy(data)
            })
            vacancyDialog.dismiss()
        }
        deleteButton?.setOnClickListener {
            deleteVacancy(vacancyId)
            vacancyDialog.dismiss()
        }

        addToFavButton?.setOnClickListener {
            val request = AddFavoriteVacancyExecutorRequest(vacancyId)
            presenter?.addFavoriteVacancy(request, {response ->
                if(response.statusCode == 200) {
                    showShortToast(getString(noty_team.com.urger.R.string.vacancy_add_to_favorite))
                }
            }, {
                showShortToast(getString(noty_team.com.urger.R.string.error_add))
            })
            vacancyDialog.dismiss()
        }

        vacancyDialog.show()
    }

    private fun deleteVacancy(id: Int) {

        isShowLoadingDialog(true)
        presenter?.deleteVacancy(id, {
            isShowLoadingDialog(false)
            if (vacancyAdapter.deleteVacancy(id)) {
                isNeedUpdateVacancy = true
                if(vacancyAdapter.itemCount == 0) {
                    empty_list.visibility = View.VISIBLE
                } else {
                    empty_list.visibility = View.GONE
                }
                NestedProfileCompanyFragment.isNeedUpdate = true
                showShortToast(getString(noty_team.com.urger.R.string.success_delete))
            }
        }, {
            isShowLoadingDialog(false)
        })
    }
}