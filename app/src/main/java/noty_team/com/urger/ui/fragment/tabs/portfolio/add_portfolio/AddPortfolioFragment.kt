package noty_team.com.urger.ui.fragment.tabs.portfolio.add_portfolio

import android.app.Activity
import android.app.Dialog
import android.graphics.Bitmap
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.github.drjacky.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.fragment_add_portfolio.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.PortfolioImageAdapter
import noty_team.com.urger.utils.api.response.portfolio.AddPortfolioExecutorResponse
import noty_team.com.urger.utils.camera.FilesPicker
import java.io.File


class AddPortfolioFragment : BaseFragment<AddPortfolioPresenter>() {

    var isAdd: Boolean= false

    lateinit var adapterPortfolioImage: PortfolioImageAdapter
    var isSetLogo = false
    companion object {
        fun newInstance() = AddPortfolioFragment()
    }
    override fun layout() = R.layout.fragment_add_portfolio

    var callbackAddPortfolio: (item: AddPortfolioExecutorResponse) -> Unit = {}


    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = AddPortfolioPresenter(baseContext)

        adapterPortfolioImage = PortfolioImageAdapter(arrayListOf(), baseContext)
        val layoutManager = LinearLayoutManager(
            baseContext,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recycler_view_portfolio_image.layoutManager = layoutManager
        recycler_view_portfolio_image.adapter = adapterPortfolioImage

        nb_add_portfolio.setOnClickListener {
            onBackPressed()
            //backNavigator().navigateBack()
        }
        confirm_add_portfolio.setOnClickListener {
            if(validator() && !isAdd) {
                isAdd = true
                presenter?.addPortfolioExecutor(document_portfolio_name.text.toString(),
                    document_portfolio_description.text.toString(), {

                            showShortToast(getString(R.string.add_portfolio_success))
                            callbackAddPortfolio(it)
                            onBackPressed()
//                            if (PaperIO.userRole == UserRole.COMPANY) {
//                                CompanyActivity.start(baseActivity)
//                                baseActivity.finish()
//                            } else {
//                                ExecutorActivity.start(baseActivity)
//                                baseActivity.finish()
//                            }

                    })
            }
        }

        document_portfolio_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                name_item_portfolio.text = s.toString()
            }
        })


        add_new_image_portfolio.getClick {
           showDialogChoose().show()
        }
    }

    private fun showDialogChoose(): Dialog {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_choose)

        val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
        val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
        val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

        ivCross.setOnClickListener {
            dialog.dismiss()
        }
        btnGallery.setOnClickListener {
            dialog.dismiss()
            pickGalleryImage()
        }
        btnCamera.setOnClickListener {
            dialog.dismiss()
            pickCameraImage()
        }
        return dialog
    }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!
                presenter?.getBitmapFromFileData(File(uri.path.toString())) {

                    if(!isSetLogo) {
                        setLogoPortfolio(it)
                    }
                }

                adapterPortfolioImage.setNewItem(File(uri.path.toString()))

            } else parseError(it)
        }

    private fun pickCameraImage() {
        cameraLauncher.launch(
            ImagePicker.with(requireActivity())
                .crop()
                .cameraOnly()
                .maxResultSize(720, 1280, true)
                .createIntent()
        )
    }

    private fun pickGalleryImage() {
        galleryLauncher.launch(
            ImagePicker.with(requireActivity())
                .crop()
                .galleryOnly()
                .cropFreeStyle()
                .maxResultSize(720, 1280, true)
                .galleryMimeTypes( // no gif images at all
                    mimeTypes = arrayOf(
                        "image/png",
                        "image/jpg",
                        "image/jpeg"
                    )
                )
                .createIntent()
        )
    }

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!
                presenter?.getBitmapFromFileData(File(uri.path.toString())) {

                    if(!isSetLogo) {
                        setLogoPortfolio(it)
                    }
                }

                adapterPortfolioImage.setNewItem(File(uri.path.toString()))
            } else parseError(it)
        }


    private fun parseError(activityResult: ActivityResult) {
        if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(
                requireContext(),
                ImagePicker.getError(activityResult.data),
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun setLogoPortfolio(data: Bitmap) {

            portfolio_item.visibility = View.VISIBLE
            isSetLogo = true
            portfolio_logo.setImageBitmap(data)

    }

    private fun validator(): Boolean {
        val minLength = 2
        try {
            if(document_portfolio_name.text.trim().length < minLength) {
                showShortToast(getString(R.string.name_document_short))
                return false
            }

            if(document_portfolio_description.text.trim().length < minLength) {
                showShortToast(getString(R.string.description_short))
                return false
            }
            if(!presenter!!.isBitmapInitialize()) {
                showShortToast(getString(R.string.error_pick_image))
                return false
            }

        } catch (e: java.lang.Exception) {
            showLongToast(getString(R.string.error_load_data))
            return false
        }
        return true
    }
}