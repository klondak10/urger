package noty_team.com.urger.ui.fragment.drawer.service_urger.service_detail

import android.Manifest
import android.R.attr.phoneNumber
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_service_detail.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.ServiceAdapter
import noty_team.com.urger.ui.fragment.FragmentImageViewer
import noty_team.com.urger.utils.api.response.center.DataItem
import noty_team.com.urger.utils.api.response.center.Services


class ServiceUrgerFragment : BaseFragment<ServiceUrgerPresenter>() {

    var list = arrayListOf<String>()
    var adapterPreliminary: ServiceAdapter? = null
    var listService = arrayListOf<Services>()
    companion object {

        private const val DATA = "dataItem"

        fun newInstance(dataItem: String): ServiceUrgerFragment {
            return ServiceUrgerFragment().apply {
                arguments = Bundle().apply {
                      putString(DATA, dataItem)
                }
            }
        }

        fun getData(args: Bundle?): DataItem {
            val dataString = args?.getString(DATA) ?: ""
            if (dataString.isNotEmpty()) {
                Gson().fromJson(dataString, DataItem::class.java)?.let {
                    return it
                }
            }

            return DataItem()
        }
    }

    override fun layout() = R.layout.fragment_service_detail

    override fun initialization(view: View, isFirstInit: Boolean) {

        listService = getData(arguments).services!!

        val manager = LinearLayoutManager(baseActivity)
        list_service.layoutManager = manager
        adapterPreliminary = ServiceAdapter(arrayListOf(), list_service,
            onLoadData = {
                getPreliminaryVacancy()
            })
        list_service.adapter = adapterPreliminary

        getPreliminaryVacancy()

        txt_shedule.text = getData(arguments).schedule
        txt_street.text = getData(arguments).address

        if (getData(arguments).image!=null){
            val strs = getData(arguments).image!!.split(",").toTypedArray()
            if (strs.size==1){
                list.add(strs[0])
            }else{
                strs.forEach {
                    list.add(it)
                }
            }

            viewPager.setAdapter(
                MyPager(
                    baseContext,
                    list
                )
            )
        }else{

        }


        drawer_service_detail.setOnClickListener {
            onBackPressed()
            //backNavigator().navigateBack()
        }

        openDialer.getClick {
            checkPermission()
        }
        send_email.getClick {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",getData(arguments).email, null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
    }

    fun checkPermission() {
        if (ContextCompat.checkSelfPermission(baseActivity,
                Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), 42)
            }
        } else {
            // Permission has already been granted
            callPhone()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 42) {
            // If request is cancelled, the result arrays are empty.
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                // permission was granted, yay!
                callPhone()

            } else {
                // permission denied, boo! Disable the
                // functionality
            }
            return
        }
    }

    fun callPhone(){
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getData(arguments).phone))
        startActivity(intent)
    }

    private fun getPreliminaryVacancy() =   adapterPreliminary?.onNewDataReceived(ArrayList(listService), 100)

    inner class MyPager(
        private val context: Context,
        private val list: ArrayList<String>
    ) : PagerAdapter() {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(context).inflate(R.layout.item_photo_pager, null)
            val imageView = view.findViewById<ImageView>(R.id.Imageview)
            val main = view.findViewById<RelativeLayout>(R.id.main)

            main.getClick {
                val fragmentExpectedWage = FragmentImageViewer.newInstance("https://${list?.get(position)}")
                fragmentExpectedWage.show(baseActivity.supportFragmentManager, "dialog_expected_wage")
            }

            isShowLoadingDialog(true)
            Glide.with(context)
                .load("https://${list?.get(position)}")
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        isShowLoadingDialog(false)
                        return  false
                    }
                    override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        isShowLoadingDialog(false)
                        //do something when picture already loaded
                        return false
                    }
                })
                .into(imageView)

            container.addView(view)

            return view
        }

        override fun destroyItem(
            container: ViewGroup,
            position: Int,
            view: Any
        ) {
            container.removeView(view as View)
        }

        override fun getCount(): Int {
            return list.size
        }

        override fun isViewFromObject(
            view: View,
            `object`: Any
        ): Boolean {
            return `object` === view
        }

    }
}