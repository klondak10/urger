package noty_team.com.urger.ui.fragment.privacy


import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_privacy.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import java.net.CookieHandler
import java.net.CookiePolicy


class PrivacyFragment : BaseFragment<WebviewPresenter>() {

    override fun layout()= R.layout.fragment_privacy

    override fun initialization(view: View, isFirstInit: Boolean) {
        val url = requireArguments()["url"] as String
        initView(url)

        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    @SuppressLint("SetJavaScriptEnabled", "ObsoleteSdkInt")
    private fun initView(url: String) {

        webView.apply {

            settings.apply {
                javaScriptEnabled = true
                javaScriptCanOpenWindowsAutomatically = true
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    displayZoomControls = false
                }
                builtInZoomControls = true
                setSupportZoom(true)
                domStorageEnabled = true
            }

            webChromeClient = MyWebChromeClient()
            webViewClient = MyWebViewClient()
            CookieSyncManager.createInstance(requireActivity())
            // unrelated, just make sure cookies are generally allowed
            CookieManager.getInstance().setAcceptCookie(true)
            // magic starts here
            val coreCookieManager = WebkitCookieManagerProxy(null, CookiePolicy.ACCEPT_ALL)
            CookieHandler.setDefault(coreCookieManager)
            CookieManager.getInstance().setAcceptCookie(true)
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
            url.let { webView.loadUrl(it) }
        }


        webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                val uri = Uri.parse(url)
                return handleUri(uri)
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                val uri = request.url
                return handleUri(uri)
            }

            private fun handleUri(uri: Uri): Boolean {
                webView.loadUrl(uri.toString())
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {}

            override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                view.loadUrl(url)
            }

            override fun shouldInterceptRequest(view: WebView?, url: String?): WebResourceResponse? {
                    return  super.shouldInterceptRequest(view, url)
            }

        }
        webView.loadUrl(url)
    }

    inner class MyWebChromeClient : WebChromeClient() {

        override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
            AlertDialog.Builder(requireActivity())
                .setMessage(message)
                .setPositiveButton(R.string.yes) { _, _ -> result.confirm() }
                .create()
                .apply {
                    setCanceledOnTouchOutside(false)
                }.run {
                    show()
                }
            return true
        }

        override fun onJsConfirm(view: WebView, url: String, message: String, result: JsResult): Boolean {
            AlertDialog.Builder(requireActivity())
                .setMessage(message)
                .setPositiveButton(R.string.yes) { _, _ -> result.confirm() }
                .setNegativeButton(R.string.no) { _, _ -> result.cancel() }
                .create()
                .apply {
                    setCanceledOnTouchOutside(false)
                }.run {
                    show()
                }
            return true
        }

    }

    inner class MyWebViewClient : WebViewClient() {

        override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
            super.onReceivedError(view, errorCode, description, failingUrl)
            Log.d("myLogs", description.toString())
        }
        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            val view = view ?: return false
            val url = url ?: return false

            return when {
                url.endsWith(".mp4") -> {
                    Intent(Intent.ACTION_VIEW)
                        .setDataAndType(Uri.parse(url), "video/*")
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .let {
                            view.context.startActivity(it)
                        }
                    true
                }
                url.startsWith("tel:") || url.startsWith("sms:") || url.startsWith("smsto:")
                        || url.startsWith("mms:") || url.startsWith("mmsto:") || url.startsWith("mailto:") -> {

                    Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .let {
                            view.context.startActivity(it)
                        }
                    true
                }
                else -> super.shouldOverrideUrlLoading(view, url)
            }
        }
    }


    companion object {
        fun newInstance(url: String): PrivacyFragment {
            return PrivacyFragment().apply {
                arguments = Bundle().apply {
                    putString("url", url)
                }
            }
        }
    }
}
