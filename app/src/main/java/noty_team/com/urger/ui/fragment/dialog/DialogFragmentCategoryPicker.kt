package noty_team.com.urger.ui.fragment.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_category_picker.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.category.OnChooseSubcategoryCallback
import noty_team.com.urger.utils.adapters.category.SingleCategory
import noty_team.com.urger.utils.adapters.category.SinglePickerCategoryAdapter
import noty_team.com.urger.utils.api.response.get_city.SubcategoriesItem

class DialogFragmentCategoryPicker : DialogFragment(), OnChooseSubcategoryCallback {

    override fun clickOnLastGroup(position: Int) {
            if(position == listData!!.size - 1) {
                Log.i("TEST", "last + " +  listData!!.lastIndex)
                recycler_view_category.scrollToPosition(position)
            }

    }

    lateinit var callback: ChoosedSubCategory
    var listData: ArrayList<SingleCategory>? = arrayListOf()

    override fun onChooseSubcategory(name: String) {

        for (data in listData as ArrayList<SingleCategory>) {
            for (sub in data.items as List<SubcategoriesItem>) {
                if (sub.name.toString() == name) {
                    callback.pickSubcategory("${sub.id}", "${data.title}", name)
                    dismiss()
                }
            }
        }
    }

    companion object {
        fun newInstance(list: ArrayList<SingleCategory>): DialogFragmentCategoryPicker {
            val fragment = DialogFragmentCategoryPicker()
            var bundle = Bundle()
            bundle.putParcelableArrayList("data", list)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.dialog_category_picker, container, false)
        if (dialog != null && dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        val recyclerView = view.findViewById(R.id.recycler_view_category) as RecyclerView
        val layoutManager =
            LinearLayoutManager(activity)

        listData = arguments?.getParcelableArrayList("data")

        val adapter = SinglePickerCategoryAdapter(listData, this,
            onClickPos = {
            val layoutManager =
                LinearLayoutManager::class.java.cast(recyclerView.layoutManager)
            val totalItemCount = listData!!.size
            val lastVisible = layoutManager.findLastVisibleItemPosition()
            if (lastVisible+1 >= totalItemCount) {
                recyclerView.postDelayed(
                    {
                        recyclerView.scrollToPosition(listData!!.size+5)
                    },
                    100
                )
            }
        })
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter


        return view
    }

    fun setChoosedSubCategory(callbacks: ChoosedSubCategory) {
        callback = callbacks
    }
}

interface ChoosedSubCategory {
    fun pickSubcategory(id: String, categoryParent: String = "", name: String)
}