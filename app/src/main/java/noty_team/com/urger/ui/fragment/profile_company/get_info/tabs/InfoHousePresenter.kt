package noty_team.com.urger.ui.fragment.profile_company.get_info.tabs

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.fragment.tabs.housing.HousingContract
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.housing.HousingItem

class InfoHousePresenter(context: Context) : BasePresenter(context) {

    fun getHousing(
        id: Int = PaperIO.companyData?.companyId!!,
        listSize: Int,
        onHousingSuccess: (documents: ArrayList<HousingItem>) -> Unit,
        showToasMassage: (massage: String) -> Unit
    ) {
        val page = listSize / Constants.documentItemLimit
        apiEmployer.getHousingPagination(id, Constants.documentItemLimit, page, {

            if (it.statusCode == 200 || it.statusCode == 201) {

                var housinglist = ArrayList<HousingItem>()

                it.data?.forEach {
                    housinglist.add(
                        HousingItem(
                            houseId = it?.houseId ?: -1,
                            address = it?.address ?: "",
                            description = it?.description ?: "",
                            preview = if(it!!.photos.isNullOrEmpty()) {
                                ""
                            }else{
                                it.photos!![0]!!
                            }
                        )
                    )
                }
                onHousingSuccess(housinglist)

            } else {

                showToasMassage(it.error!!)
            }

        }, { errorMessage, errorCode ->
            showToasMassage(errorCode.toString())

        }).callRequest()

    }
}