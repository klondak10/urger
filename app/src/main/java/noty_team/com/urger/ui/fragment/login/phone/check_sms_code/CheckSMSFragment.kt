package noty_team.com.urger.ui.fragment.login.phone.check_sms_code

import android.view.View
import kotlinx.android.synthetic.main.fragment_check_sms.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.push.FBService
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.ui.fragment.login.choose_role.ChooseRoleFragment
import noty_team.com.urger.utils.api.response.sms_checked.UserRole


class CheckSMSFragment : BaseFragment<CheckSMSPresenter>() {

    companion object {
        fun newInstance() = CheckSMSFragment()
    }

    override fun layout() = R.layout.fragment_check_sms

    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = CheckSMSPresenter(baseContext)

        button_login_check_sms.getClick(1000) {
            val smsCode = firstPinView.text.toString()

            if (smsCode.length == 6) {

                presenter?.serverRequest(smsCode.toInt(), {
                    if (it) {
                        navigateChooseRole()
                    } else {
                        openApp()
                    }
                }, {
                    showShortToast(it)
                })
            } else {
                showShortToast("Еnter you sms code")
            }
        }

    }

    private fun navigateChooseRole() {

        addFragment(ChooseRoleFragment.newInstance())

        baseActivity.toggleKeyboard(false)
    }

    private fun openApp() {

        if (PaperIO.userRole == UserRole.COMPANY) {
            baseActivity.toggleKeyboard(false)
            CompanyActivity.start(baseActivity)
        } else {
            baseActivity.toggleKeyboard(false)
            ExecutorActivity.start(baseActivity)
        }
    }
}