package noty_team.com.urger.ui.adapters.recycler

import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.item_preliminary.*
import kotlinx.android.synthetic.main.item_preliminary.preliminary_rating
import kotlinx.android.synthetic.main.item_preliminary.preliminary_review
import kotlinx.android.synthetic.main.item_preliminary.preliminary_user_logo
import kotlinx.android.synthetic.main.item_preliminary.preliminary_user_name
import kotlinx.android.synthetic.main.item_preliminary_2.*
import kotlinx.android.synthetic.main.item_preliminary_2.image_exist_car
import kotlinx.android.synthetic.main.item_preliminary_2.preliminary_price
import kotlinx.android.synthetic.main.item_requested.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.DataItem
import noty_team.com.urger.utils.setPhoto

class RequestedAdapter(items: ArrayList<DataItem>,
                       recyclerView: RecyclerView,
                       isPreliminary: Boolean,
                       var onLoadData: (offset: Int) -> Unit = {},
                       var onCancel:(item: DataItem) -> Unit = {},
                       var onClick: (data: DataItem) -> Unit = {}) :
    BaseAdapterPagination<DataItem>(items, recyclerView)  {

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            itemView.setOnClickListener {
                onClick(list[position])
            }
            cancel_button?.setOnClickListener{
                onCancel(list[position])
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            if(item.user?.avatar != null) {
                setPhoto((preliminary_user_logo as ImageView), item.user?.avatar!!)
            }
            image_exist_car?.visibility = if(item.auto!!) View.VISIBLE else View.GONE
            preliminary_user_name.text = "${item.user!!.firstName} ${item.user!!.lastName}"
            preliminary_review.text =  "${item.rate!!.count?.toString() ?: 0} ${itemView.context.getString(R.string.reviewText)}"

            try {
                preliminary_price.text = "${item.profile!!.sallary} ${item.vacancy!!.wageCurrency}"
            }catch (e:KotlinNullPointerException){
                preliminary_price.visibility = View.GONE
            }


            val spannable = SpannableString(item.vacancy!!.categoryName + ", " + item.vacancy!!.parentCategory)
            spannable.setSpan(
                StyleSpan(Typeface.BOLD),
                0, item.vacancy!!.categoryName!!.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            vacancy_company_name_position.text = spannable
            vacancy_location.text = item.vacancy!!.address
            existHouse.visibility = if(item.vacancy!!.houseId != null) View.VISIBLE else View.GONE
            val schedule: String = when(item.vacancy!!.workSchedule) {
                "full_time" -> itemView.context.getString(R.string.full_employment)
                "part_time" -> itemView.context.getString(R.string.part_time)
                "temporary_job" -> itemView.context.getString(R.string.temporary_job)
                else -> itemView.context.getString(R.string.contract)
            }
            vacancy_type_employment.text = schedule
            vacancy_price.text = "${item.vacancy!!.wage} ${item.vacancy!!.wageCurrency}"
            preliminary_rating.rating = item.rate!!.rate ?: 0.0F

        }
    }

    override val itemLayoutResourceId =  R.layout.item_requested

}