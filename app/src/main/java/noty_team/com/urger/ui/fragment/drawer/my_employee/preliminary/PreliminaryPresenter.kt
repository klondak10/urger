package noty_team.com.urger.ui.fragment.drawer.my_employee.preliminary

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.PreliminaryVacancyResponse

class PreliminaryPresenter(context: Context): BasePresenter(context) {

    fun getCompaniesRequests(onLoadData:(data: PreliminaryVacancyResponse) -> Unit) {
        apiEmployer.getPreliminaryVacancy({
            if(it.statusCode == 200|| it.statusCode == 201) {
                onLoadData(it)
            } else {
                showShortToast(it.error!!)
            }
        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }

    fun rejectVacancy(id: Int, isSuccessReject:(b: Boolean) -> Unit) {
        apiEmployer.rejectVacancy(id, {
            if(it.statusCode == 200|| it.statusCode == 201) {
                isSuccessReject(true)
            } else if(it.statusCode == 500) {
                isSuccessReject(true)
            }
            else {
                showShortToast(it.error!!)
            }

        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }

    fun applyVacancy(id: Int, onSuccess:(isSuccess: Boolean) -> Unit) {
        apiEmployer.applyVacancy(id, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onSuccess(true)
            } else if(it.statusCode == 500) {
                onSuccess(true)
            } else {
                showShortToast(it.error!!)
                onSuccess(false)
            }

        }, {errorMessage, errorCode ->
            //showShortToast(errorMessage)
        }).callRequest()
    }
}