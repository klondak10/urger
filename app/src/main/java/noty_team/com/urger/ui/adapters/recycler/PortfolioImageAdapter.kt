package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import android.graphics.BitmapFactory
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import kotlinx.android.synthetic.main.item_portfolio_image.view.*
import java.io.File


class PortfolioImageAdapter(val items: ArrayList<File>, val context: Context) :
        RecyclerView.Adapter<PortfolioImageAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var portfolio_image = view.portfolio_image as ImageView
    }


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(
            noty_team.com.urger.R.layout.item_portfolio_image,
            parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var item = items.get(position)
        if (item.exists()) {
            val myBitmap = BitmapFactory.decodeFile(item.absolutePath)
            holder.portfolio_image.setImageBitmap(myBitmap)
        }
    }

    fun setNewItem(item: File) {
        items.add(item)
        notifyDataSetChanged()
    }
}