package noty_team.com.urger.ui.fragment.drawer.service_urger.urger_services_map

import android.annotation.SuppressLint
import com.google.android.material.bottomsheet.BottomSheetBehavior
import android.view.MotionEvent
import android.view.View
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapsInitializer
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_urger_services_map.*
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.drawer.service_urger.service_detail.ServiceUrgerFragment
import noty_team.com.urger.utils.api.response.center.DataItem
import noty_team.com.urger.utils.createDrawableFromView

class UrgerServicesMapFragment : BaseFragment<UrgerServicesMapPresenter>(), GoogleMap.OnMarkerClickListener {

    companion object {
        fun newInstance() = UrgerServicesMapFragment()
    }

    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<*>

    var mMapView: MapView? = null

    private var googleMap: GoogleMap? = null
    var pos:LatLng? = null

    override fun layout() = R.layout.fragment_urger_services_map

    @SuppressLint("ClickableViewAccessibility")
    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = UrgerServicesMapPresenter(baseContext)

        bottomBarIsVisible(false)
        if (isFirstInit) {
            nb_back_press.setOnClickListener {
                bottomBarIsVisible(true)
                onBackPressed()
            }
            mBottomSheetBehavior = BottomSheetBehavior.from(bottom_marker_info)

            bottom_marker_info.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
                when (motionEvent.action) {
                    MotionEvent.ACTION_UP -> {

                    }
                }
                return@OnTouchListener true
            })

            initMap()

        }
    }


    private fun initMap() {
        mMapView = map_view
        mMapView?.onCreate(arguments)

        try {
            MapsInitializer.initialize(requireActivity().applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        mMapView?.getMapAsync { mMap ->
            googleMap = mMap

            googleMap?.setOnMapClickListener {
                mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }

            googleMap?.setOnMarkerClickListener(this)
            // For showing a move to my location button
            isShowLoadingDialog(true)
            presenter?.getCenterUrger {
                isShowLoadingDialog(false)

                    it.data!!.forEach {
                        if (it.services?.isEmpty()==true){
                            llServie.visibility = View.GONE
                            llServie1.visibility = View.GONE
                            val text = it.schedule + "#" + it.address
                            val data = Gson().toJson(it)
                            val location = LatLng(it.lat!!.toDouble(),it.lon!!.toDouble())
                            val markerOptions = MarkerOptions().title(data)
                                .snippet(text)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_map))
                            googleMap?.addMarker(markerOptions.position(location))
                        }else if (it.services?.size==1){
                            llServie1.visibility = View.GONE
                            val text = it.schedule + "#" + it.address + "#" + it.services[0].image + "#" + it.services[0].name
                            val data = Gson().toJson(it)
                            val location = LatLng(it.lat!!.toDouble(),it.lon!!.toDouble())
                            val markerOptions = MarkerOptions().title(data)
                                .snippet(text)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_map))
                            googleMap?.addMarker(markerOptions.position(location))
                        }else {
                            val text = it.schedule + "#" + it.address + "#" + it.services!![0].image + "#" + it.services[0].name + "#" + it.services[1].image + "#" + it.services[1].name
                            val data = Gson().toJson(it)
                            val location = LatLng(it.lat!!.toDouble(),it.lon!!.toDouble())
                            val markerOptions = MarkerOptions().title(data)
                                .snippet(text)
                                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_map))
                            googleMap?.addMarker(markerOptions.position(location))
                        }
                    }

            }

            pos = LatLng(52.209267, 20.938165)
            val cameraPosition = CameraPosition.Builder().target(pos!!).zoom(10f).build()
            googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }


    override fun onMarkerClick(marker: Marker): Boolean {
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        val strs = marker.snippet!!.split("#").toTypedArray()

        txt_shedule.text = strs[0]
        txt_street.text = strs[1]
        try {
            txt_name_service.text = strs[3].substring(0,strs[3].lastIndexOf("-"))
            Glide.with(baseActivity)
                .load("https://${strs[2]}")
                .into(image_service)
        }catch (e :StringIndexOutOfBoundsException){
            txt_name_service.text = strs[3]
            Glide.with(baseActivity)
                .load("https://${strs[2]}")
                .into(image_service)
        }catch (e :ArrayIndexOutOfBoundsException){ more_button.visibility = View.GONE}

        try {
            txt_name_service1.text = strs[5].substring(0,strs[5].lastIndexOf("-"))
            Glide.with(baseActivity)
                .load("https://${strs[4]}")
                .into(image_service1)

        }catch (e :StringIndexOutOfBoundsException){
            txt_name_service1.text = strs[5]
            Glide.with(baseActivity)
                .load("https://${strs[4]}")
                .into(image_service1)
        }
        catch (e :ArrayIndexOutOfBoundsException){ more_button.visibility = View.GONE}



        more_button.setOnClickListener {

            addFragment(ServiceUrgerFragment.newInstance(marker.title.toString()))
            //navigator().navigateToFragment(ServiceUrgerFragment.newInstance(), null, true)
            mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }

        return true
    }


    override fun onResume() {
        super.onResume()
        mMapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView?.onLowMemory()
    }
}