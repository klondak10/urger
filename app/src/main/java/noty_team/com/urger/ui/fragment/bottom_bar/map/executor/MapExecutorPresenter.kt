package noty_team.com.urger.ui.fragment.bottom_bar.map.executor

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class MapExecutorPresenter(context: Context): BasePresenter(context) {

    fun getVacancies(onSuccess:(response: VacanciesListResponse)->Unit)
    {
            apiEmployer.getVacanciesForExecutor(Constants.documentItemLimit, 0, {
                onSuccess(it)
            }, {errorMessage, errorCode ->
                showLongToast(errorMessage)
            }).callRequest()
    }
}