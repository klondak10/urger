package noty_team.com.urger.ui.fragment.tabs.documents.add_document

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.ParcelFileDescriptor
import android.util.Log
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import io.reactivex.Single
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.api.data.AddDocumentRequest
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.request.add_document.create_document.CreateDocumentRequest
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.bitmapToFile
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*
import java.util.*

class AddDocumentPresenter(context: Context) : BasePresenter(context), AddDocumentContract.Presenter {

	lateinit var bitmap: File


	lateinit var onSuccess: (document: DocumentItem) -> Unit
	lateinit var pdf: File

	fun getBitmapFromFileData(file: File, onSuccess: (Bitmap) -> Unit) {
		var filePath = file.path
		val b = BitmapFactory.decodeFile(filePath)
		bitmap = bitmapToFile(context, b)
		getPdfBitmap(file)
		onSuccess(b)
	}

	fun isBitmapInitialize(): Boolean {
		return ::bitmap.isInitialized
	}

	fun getPdfBitmap(file: File) {
		pdf = file
		Single.fromCallable { getPdf(file) }
				.addRuntimeEnvironment()
				.subscribe(
						{
							if (it != null) {
								bitmap = bitmapToFile(context, it)
							}
						},
						{ dick ->
							val dd = 90
						}
				).addToDisposables()

	}


	fun getPdf(file: File): Bitmap? {

		val fileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY)
		val renderer = PdfRenderer(fileDescriptor)
		val pageCount = renderer.pageCount
		if (pageCount >= 0) {
			val page = renderer.openPage(0)
			val bitmap = Bitmap.createBitmap(
					page.getWidth(), page.getHeight(),
					Bitmap.Config.ARGB_8888
			)
			bitmap.setHasAlpha(true)
			page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY)
			page.close()
			return bitmap
		}
		renderer.close()
		return null
	}

	override fun addDocumentRequest(
			documentName: String, isDocumentLoad: (isLoad: Boolean) -> Unit,
			onSuccess: (document: DocumentItem) -> Unit
	) {
		this.onSuccess = onSuccess

		val fileBitmap = MultipartBody.Part.createFormData(
				"preview", bitmap.name,
				RequestBody.create("image/png".toMediaTypeOrNull(), bitmap))

		val filePDF = MultipartBody.Part.createFormData(
				"file", pdf.name,
				RequestBody.create("image/png".toMediaTypeOrNull(), pdf)
		)
		if(PaperIO.userRole == UserRole.COMPANY) {
			apiEmployer.loadDocument(PaperIO.companyData?.companyId
				?: -1, "${documentName}contract", fileBitmap, filePDF, {

				if (it.statusCode == 200 || it.statusCode == 201) {

					val t = it.map()

					onSuccess(t)
				} else {
					showShortToast(it.error)
					isDocumentLoad(false)
				}
			}, { errorMessage, errorCode ->
				showShortToast(context.getString(R.string.error_create_document))
				isDocumentLoad(false)
			}).callRequest()
		} else {
			apiEmployer.loadDocumentExecutor(RequestBody.create("multipart/form-data".toMediaTypeOrNull(), "${documentName}contract"), fileBitmap, filePDF, {
				if (it.statusCode == 200 || it.statusCode == 201) {
					val doc = it.map()
					onSuccess(doc)
				}else {
					showShortToast(it.error)
					isDocumentLoad(false)
				}
			}, { errorMessage, errorCode ->
				showShortToast(context.getString(R.string.error_create_document))
				isDocumentLoad(false)
			}).callRequest()
		}
	}


}