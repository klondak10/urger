package noty_team.com.urger.ui.fragment.drawer.feedback

import android.text.SpannableStringBuilder
import android.view.View
import kotlinx.android.synthetic.main.fragment_drawer_feedback.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class DrawerFeedbackFragment: BaseFragment<DrawerFeedbackPresenter>() {

    companion object {
        fun newInstance() = DrawerFeedbackFragment()
    }

    override fun layout() = R.layout.fragment_drawer_feedback

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = DrawerFeedbackPresenter(baseContext)
        bottomBarIsVisible(false)
        nb_back_press.setOnClickListener {
           bottomBarIsVisible(true)
           onBackPressed()
        }
        if(PaperIO.userRole == UserRole.EXECUTOR) {
            master_reg_name.text = SpannableStringBuilder("${PaperIO.executorData!!.firstName} ${PaperIO.executorData!!.lastName}")
            email_et.text = SpannableStringBuilder("${PaperIO.executorData!!.email}")
        } else {
            master_reg_name.text = SpannableStringBuilder("${PaperIO.employerData!!.firstName} ${PaperIO.employerData!!.lastName}")
            email_et.text = SpannableStringBuilder("${PaperIO.employerData!!.email}")
        }

        drawer_feedback_done.getClick {
            if (validator()) {
                presenter?.createFeedback(email_et.text.toString(), text_et.text.toString()) {
                    if (it) {
                        showShortToast(getString(R.string.success_create_callback))
                    } else {
                        showShortToast(getString(R.string.error_create_callback))
                    }
                    bottomBarIsVisible(true)
                    if (PaperIO.userRole == UserRole.COMPANY) {
                        CompanyActivity.start(baseActivity)
                        baseActivity.finish()
                    } else {
                        ExecutorActivity.start(baseActivity)
                        baseActivity.finish()
                    }
                }
            }
        }

    }

    private fun validator(): Boolean {
        val minLength = 2
        if(email_et.text.toString().length < minLength) {
            showShortToast(getString(R.string.error_empty_field))
            return false
        }
        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email_et.text.toString()).matches()) {
            showShortToast(getString(R.string.incorrect_email))
            return false
        }
        if(text_et.text.toString().length < minLength) {
            showShortToast(getString(R.string.error_empty_field))
            return false
        }
        return true
    }
}