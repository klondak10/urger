package noty_team.com.urger.ui.fragment.tabs.company.edit_profile.edit_personal_data

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_edit_personal_data.*
import kotlinx.android.synthetic.main.fragment_edit_personal_data.slave_pos_city_name
import kotlinx.android.synthetic.main.fragment_slave_position.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.ui.fragment.dialog.ChoosedSubCategory
import noty_team.com.urger.ui.fragment.dialog.DialogFragmentCategoryPicker
import noty_team.com.urger.utils.adapters.category.SingleCategory
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.util.*
import kotlin.collections.ArrayList

class EditPersonalDataFragment : BaseFragment<EditPersonalDataPresenter>(), ChoosedSubCategory {

	private var parentId:String = "0"
	private var parentIds: ArrayList<Int> = PaperIO.positionIdsExecutor?.toCollection(ArrayList()) ?: arrayListOf()

	private var countPosition = 0

	override fun pickSubcategory(id: String, parentCategory: String, name: String) {
		parentId = id

	    if(countPosition == 0){
			parentIds.add(0, id.toInt())
			slave_pos_name_first.text = "$name"
		} else if(countPosition == 1) {
			parentIds.add(1, id.toInt())
			slave_pos_name_second.text = "$name"
		}else if(countPosition == 2) {
			parentIds.add(2, id.toInt())
			slave_pos_name_third.text = "$name"

		}

	}

	companion object {
		fun newInstance() = EditPersonalDataFragment()
	}


	private val maxLength = 2

	var callback: () -> Unit = {}

	override fun layout() = R.layout.fragment_edit_personal_data

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = EditPersonalDataPresenter(baseContext)

		bottomBarIsVisible(false)

		initData()
		initAction()
	}

	private fun initData() {
		if(PaperIO.userRole == UserRole.EXECUTOR) {
			et_first_name.setText(PaperIO.executorData?.firstName)
			et_surname.setText(PaperIO.executorData?.lastName)
			et_email.setText(PaperIO.executorData?.email)
			if(PaperIO.positionIdsExecutor != null) {
				val positions = PaperIO.positionIdsExecutor
				val pos = (PaperIO.positionExecutor as String).split(",")
				positions?.forEachIndexed { index, i ->
					if (index == 0) {
						slave_pos_name_first.text = pos[index]
					}
					if (index == 1) {
						slave_pos_name_second.text = pos[index]
					}

					if (index == 2) {
						slave_pos_name_third.text = pos[index]
					}
				}
			}

		} else {
			ll_city.visibility = View.GONE
			layout_email.visibility = View.GONE
			layout_position.visibility = View.GONE
			et_first_name.setText(PaperIO.employerData?.firstName)
			et_surname.setText(PaperIO.employerData?.lastName)
			et_email.setText(PaperIO.employerData?.email)
		}
	}

	private fun initAction() {
		nb_profile_info.getClick {
			onBackPressed()
		}

		confirm_profile_info.getClick {

			if (validateNameSurname()) {
				isShowLoadingDialog(true)
				if(PaperIO.userRole == UserRole.EXECUTOR) {

					if (parentId.equals("0")){
						presenter?.editUserInfoNoId(
							et_first_name.text.trim().toString(),
							et_surname.text.trim().toString(),
							et_email.text.trim().toString()
						) {
							if (it) {
//							callback()
//							onBackPressed()
								Paper.book().write("city_place_id", slave_pos_city_name.text.trim().toString())
								isShowLoadingDialog(false)
								ExecutorActivity.start(baseActivity)
								baseActivity.finish()

							}
						}
					}else{

						PaperIO.positionIdsExecutor = parentIds.toTypedArray()
						PaperIO.positionExecutor = "${slave_pos_name_first.text}, ${slave_pos_name_second.text}, ${slave_pos_name_third.text}"
						presenter!!.editUserInfo(
							et_first_name.text.trim().toString(),
							et_surname.text.trim().toString(),
							et_email.text.trim().toString(),
							parentIds.toTypedArray(),
                            "${slave_pos_name_first.text}, ${slave_pos_name_second.text}, ${slave_pos_name_third.text}"

						) {
							if (it) {

//							callback()
//							onBackPressed()
								isShowLoadingDialog(false)
								ExecutorActivity.start(baseActivity)
								baseActivity.finish()

							}
						}
					}
				} else {
					presenter?.editUserInfoEmployer(
						et_first_name.text.trim().toString(),
						et_surname.text.trim().toString()
					) {
						if (it) {
//							callback()
//							onBackPressed()
							isShowLoadingDialog(false)
							CompanyActivity.start(baseActivity)
							baseActivity.finish()

						}
					}
				}
				}
		}

		slave_pos_name_first.getClick {
			countPosition = 0
			presenter?.getCategory {
				var liss: ArrayList<SingleCategory> = createListCategory(it)
				val fragmentCategory = DialogFragmentCategoryPicker.newInstance(liss)
				fragmentCategory.setChoosedSubCategory(this)
				fragmentCategory.show(fragmentManager!!, "dialog_category")
			}
		}

		slave_pos_name_second.getClick {
			countPosition = 1
			presenter?.getCategory {
				var liss: ArrayList<SingleCategory> = createListCategory(it)
				val fragmentCategory = DialogFragmentCategoryPicker.newInstance(liss)
				fragmentCategory.setChoosedSubCategory(this)
				fragmentCategory.show(fragmentManager!!, "dialog_category")
			}
		}
		slave_pos_name_third.getClick {
			countPosition = 2
			presenter?.getCategory {
				var liss: ArrayList<SingleCategory> = createListCategory(it)
				val fragmentCategory = DialogFragmentCategoryPicker.newInstance(liss)
				fragmentCategory.setChoosedSubCategory(this)
				fragmentCategory.show(fragmentManager!!, "dialog_category")
			}
		}


		slave_pos_city_name.getClick {
			callPlaceAutocompleteActivityIntent()
		}

		if (Paper.book().exist("city_place_id")){
			slave_pos_city_name.text = Paper.book().read("city_place_id")
		}

	}

	private fun callPlaceAutocompleteActivityIntent() {
		try {
			val intent = Autocomplete.IntentBuilder(
				AutocompleteActivityMode.OVERLAY,
				Arrays.asList(
					Place.Field.ADDRESS,
					Place.Field.ID,
					Place.Field.LAT_LNG,
					Place.Field.OPENING_HOURS
				)
			).setTypeFilter(TypeFilter.CITIES).build(baseActivity)

			startActivityForResult(intent, 8)

		} catch (e: GooglePlayServicesRepairableException) {
			showShortToast("error")
		} catch (e: GooglePlayServicesNotAvailableException) {
			showShortToast("error")

		} catch (e: Exception) {
			showShortToast("error")
		}

	}


	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == 8) {
			if (resultCode == AppCompatActivity.RESULT_OK) {
				if (data != null) {
					val place = Autocomplete.getPlaceFromIntent(data)
					slave_pos_city_name.setText(place.address)

				}
			}
		}
	}

	private fun createListCategory(it: CityResponse): ArrayList<SingleCategory> {
		var list: MutableList<SingleCategory> = arrayListOf()
		for (obj in it.data!!) {
			list.add(SingleCategory(obj?.name, obj?.subcategories))
		}
		return list as ArrayList<SingleCategory>
	}


	private fun validateNameSurname(): Boolean {
		try {
			val firstName = et_first_name.text.trim()
			 if (firstName.length < maxLength) {
				showShortToast(getString(R.string.name_short))
				return false
			}

			val surname = et_surname.text.trim()
			 if (surname.length < maxLength) {
				showShortToast(getString(R.string.lastname_short))
				return false
			}
			if(PaperIO.userRole == UserRole.EXECUTOR) {
				val email = et_email.text.trim()
				if (email.length < maxLength) {
					showShortToast(getString(R.string.incorrect_email))
					return false
				}
			}

		} catch (e: Exception) {

			showShortToast(resources.getString(R.string.exeption_validation))
			return false
		}
		return true
	}
}