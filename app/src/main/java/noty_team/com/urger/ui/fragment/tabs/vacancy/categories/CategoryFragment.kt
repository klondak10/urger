package noty_team.com.urger.ui.fragment.tabs.vacancy.categories

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_categories.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.CategoriesListAdapter
import noty_team.com.urger.ui.adapters.recycler.SubcategoriesListAdapter
import noty_team.com.urger.ui.fragment.tabs.vacancy.subcategory.SubcategoryFragment
import noty_team.com.urger.utils.api.response.categories_response.DataItem

class CategoryFragment : BaseFragment<CategoryPresenter>() {

    companion object {
        fun newInstance(data: ArrayList<DataItem>,
                        callback:(category: DataItem) -> Unit
        ) = CategoryFragment().apply {
            categories = data
            onCategorySelected = callback
        }
    }

    var onCategorySelected:(category: DataItem) -> Unit = {}

    var categories = arrayListOf<DataItem>()

    override fun layout() = R.layout.fragment_categories

    override fun initialization(view: View, isFirstInit: Boolean) {
        confirm_add_vacancy.visibility = View.INVISIBLE
        bottomBarIsVisible(false)
        nb_add_category.setOnClickListener {
            onBackPressed()
            //backNavigator().navigateBack()
        }

        presenter = CategoryPresenter(baseContext)

        initAdapter()
    }

    private fun initAdapter(){
        categories_list.layoutManager =
            LinearLayoutManager(baseActivity)
        categories_list.isNestedScrollingEnabled = false
        categories_list.adapter = CategoriesListAdapter(categories){
            if (it.subcategories.isNotEmpty()){

                addFragment(
                        SubcategoryFragment.newInstance(it.subcategories, onCategorySelected))

                /*navigator().navigateToFragment(
                    SubcategoryFragment.newInstance(it.subcategories, onCategorySelected),
                    null, true)*/
            }
        }
    }

}