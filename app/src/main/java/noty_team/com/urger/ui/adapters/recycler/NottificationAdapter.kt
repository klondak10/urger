package noty_team.com.urger.ui.adapters.recycler

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_notification.*
import noty_team.com.urger.R
import noty_team.com.urger.ui.adapters.items_adapter.PushItem
import noty_team.com.urger.utils.adapters.BaseAdapterPagination

class NottificationAdapter(list: ArrayList<PushItem>,
                           recyclerView: RecyclerView,
                           var onSwipeToDelete: (id: PushItem) -> Unit = {},
                           var onLoadData: (offset: Int) -> Unit = {}) :
	BaseAdapterPagination<PushItem>(list, recyclerView) {
	override fun onDataLoaded(listSize: Int) {
		onLoadData(listSize)
	}

	override fun onInit(pos: Int, viewHolder: MainViewHolder) {
		viewHolder.apply {
			try {

				itemView.setOnLongClickListener {
					onSwipeToDelete(list[adapterPosition])

					return@setOnLongClickListener true
				}

			} catch (e: Exception) {}
		}
	}

	override fun onBind(pos: Int, viewHolder: MainViewHolder) {
		val item = list[pos]
		viewHolder.apply {
           if(item.profilePhoto==true){
			   ic_avatar.visibility = View.VISIBLE
		   }else{
			   ic_avatar.visibility = View.GONE
		   }
			txt_title.text = item.title
			txt_body.text = item.body
			txt_time.text = item.time
		}

	}

	override val itemLayoutResourceId: Int = R.layout.item_notification

}