package noty_team.com.urger.ui.fragment.tabs.company.edit_profile.company_profile_info

import android.app.Activity
import android.app.Dialog
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.github.drjacky.imagepicker.ImagePicker
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import kotlinx.android.synthetic.main.fragment_company_personal_info.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.company.CompanyFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.camera.FilesPicker
import java.io.File

class CompanyProfileInfoFragment : BaseFragment<CompanyProfileInfoPresenter>() {

	companion object {
		fun newInstance() = CompanyProfileInfoFragment()
	}

	override fun layout() = R.layout.fragment_company_personal_info

	override fun initialization(view: View, isFirstInit: Boolean) {
		bottomBarIsVisible(false)

		presenter = CompanyProfileInfoPresenter(baseContext)
		initCompanyData()
		initAction()
	}

	private fun initCompanyData() {
		company_name_personal_info.text = PaperIO.companyData?.name
		company_nip_personal_info.text = PaperIO.companyData?.nip
		company_address_personal_info.text = PaperIO.companyData?.city
		company_position_personal_info.setText(PaperIO.companyData?.position)
		company_description_personal_info.setText(PaperIO.companyData?.description)
	}


	private fun initAction() {
		nb_company_info.setOnClickListener {
			onBackPressed()
		}

		confirm_company_info.setOnClickListener {

			if (validationField()) {
				presenter?.serverRequest(
						company_description_personal_info.text.toString(),
						company_position_personal_info.text.toString()

				) {
					if (it) {
						showShortToast(getString(R.string.success_info_company_update))
						NestedProfileCompanyFragment.isNeedUpdate = true
						onBackPressed()
					} else showShortToast(Constants.DISPOSABLE_ERROR)
				}
			}
		}

		company_add_logo_personal_info.setOnClickListener {
			showDialogChoose().show()
		}
	}

	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				presenter?.uploadCompanyLogo(File(uri.path.toString())) {
					NestedProfileCompanyFragment.isNeedUpdate = true
					CompanyFragment.isNeedUpdateCompanyLogo = true
				}

			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				presenter?.uploadCompanyLogo(File(uri.path.toString())) {
					NestedProfileCompanyFragment.isNeedUpdate = true
					CompanyFragment.isNeedUpdateCompanyLogo = true
				}
			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}

	private fun validationField(): Boolean {
		try {
			val position = company_position_personal_info.text.length
			if (position < 2) {
				showShortToast(resources.getString(R.string.enter_you_position))
				return false
			}

			if(company_description_personal_info.text.length < 2) {
				showShortToast(resources.getString(R.string.empty_description_company_info))
				return false
			}

			if(company_name_personal_info.text.length < 2) {
				showShortToast(resources.getString(R.string.empty_name_company_info))
				return false
			}

		} catch (e: java.lang.Exception) {
			showLongToast(resources.getString(R.string.fill_in_required_fields))
			return false
		}

		return true
	}
}