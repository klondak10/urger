package noty_team.com.urger.ui.fragment.tabs.vehicle.add_vehicle

import android.app.Activity
import android.app.Dialog
import android.graphics.Bitmap
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.github.drjacky.imagepicker.ImagePicker
import kotlinx.android.synthetic.main.fragment_add_vehicle.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.api.response.car.AddCarExecutorResponse
import noty_team.com.urger.utils.camera.FilesPicker
import java.io.File


class AddVehicleFragment : BaseFragment<AddVehiclePresenter>() {

    companion object {
        fun newInstance() = AddVehicleFragment()
    }
     var isAdd = false
    var callbackAddVehicle: (item: AddCarExecutorResponse) -> Unit = {}
    override fun layout() = R.layout.fragment_add_vehicle

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = AddVehiclePresenter(baseContext)
        isShowLoadingDialog(true)
        getVehicle()
        nb_add_vehicle.setOnClickListener {
            onBackPressed()
            //backNavigator().navigateBack()
        }
//        car_model_et.text.toString(),
        confirm_add_vehicle.setOnClickListener {
            if(validator() && !isAdd) {
                isAdd = true
                presenter?.addExecutorCar(spinner_car.selectedItem.toString(), year_of_issue_et.text.toString(),
                    color_et.text.toString(), spinner_fuel.selectedItem.toString(), {

                            showShortToast(getString(R.string.add_car_success))
                                callbackAddVehicle(it)
                        onBackPressed()
//                            if (PaperIO.userRole == UserRole.COMPANY) {
//                                CompanyActivity.start(baseActivity)
//                                baseActivity.finish()
//                            } else {
//                                ExecutorActivity.start(baseActivity)
//                                baseActivity.finish()
//                            }

                    }, {
                        showShortToast(it)
                    })
            }
            //backNavigator().navigateBack()
        }

//        car_model_et.addTextChangedListener(object: TextWatcher {
//            override fun afterTextChanged(s: Editable?) {}
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
//
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                vehicle_description.text = s.toString()
//            }
//        })


        year_of_issue_et.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                vehicle_description_year.text = " (${s.toString()})"
            }

        })
        color_et.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                vehicle_color.text = "${resources.getString(R.string.color_vehicle, s.toString())}"
            }

        })

        val dataAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, resources.getStringArray(
                R.array.filter_fuel))
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_fuel.adapter = dataAdapter
        spinner_fuel.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                vehicle_gas.text = selectedItem

            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        click_image_pick.getClick {
            showDialogChoose().show()
        }
    }

    private fun showDialogChoose(): Dialog {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_choose)

        val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
        val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
        val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

        ivCross.setOnClickListener {
            dialog.dismiss()
        }
        btnGallery.setOnClickListener {
            dialog.dismiss()
            pickGalleryImage()
        }
        btnCamera.setOnClickListener {
            dialog.dismiss()
            pickCameraImage()
        }
        return dialog
    }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!
                presenter?.getBitmapFromFileData(File(uri.path.toString())) {
                    setAvatar(it)
                }

            } else parseError(it)
        }

    private fun pickCameraImage() {
        cameraLauncher.launch(
            ImagePicker.with(requireActivity())
                .crop()
                .cameraOnly()
                .maxResultSize(720, 1280, true)
                .createIntent()
        )
    }

    private fun pickGalleryImage() {
        galleryLauncher.launch(
            ImagePicker.with(requireActivity())
                .crop()
                .galleryOnly()
                .cropFreeStyle()
                .maxResultSize(720, 1280, true)
                .galleryMimeTypes( // no gif images at all
                    mimeTypes = arrayOf(
                        "image/png",
                        "image/jpg",
                        "image/jpeg"
                    )
                )
                .createIntent()
        )
    }

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!

                presenter?.getBitmapFromFileData(File(uri.path.toString())) {
                    setAvatar(it)
                }
            } else parseError(it)
        }


    private fun parseError(activityResult: ActivityResult) {
        if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(
                requireContext(),
                ImagePicker.getError(activityResult.data),
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun getVehicle() {
        presenter?.getVehicleList {
            if (!it.isNullOrEmpty()) {
                isShowLoadingDialog(false)
                val adapter = ArrayAdapter(baseContext, R.layout.layout_spinner, it)
                spinner_car.adapter = adapter
                spinner_car.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        showShortToast(getString(R.string.need_choose_model_car))
                    }

                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        vehicle_description.text = spinner_car.selectedItem.toString()
                        spinner_car.setSelection(position)
                    }
                }
            }else {
                spinner_car.visibility = View.GONE
                et_car.visibility = View.VISIBLE
            }
        }
    }

    private fun validator(): Boolean {
        val length = 1
        if(spinner_car.selectedItem.toString().length < length) {
            showShortToast("${getString(R.string.error_input_et)}: ${car_model.text}")
            return false
        }
        if(year_of_issue_et.text.toString().length < length) {
            showShortToast("${getString(noty_team.com.urger.R.string.error_input_et)}: ${year_of_issue.text}")
            return false
        }
        if(color_et.text.toString().length < length) {
            showShortToast("${getString(noty_team.com.urger.R.string.error_input_et)}: ${color.text}")
            return false
        }
        if(!presenter!!.isBitmapInitialize()) {
            showShortToast(getString(R.string.error_add_car_photo))
            return false
        }
        return true
    }


    private fun setAvatar(image: Bitmap) {
            vehicle_image.visibility = View.VISIBLE
            vehicle_image.setImageBitmap(image)

    }
}