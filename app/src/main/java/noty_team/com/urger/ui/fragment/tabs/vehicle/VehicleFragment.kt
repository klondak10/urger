package noty_team.com.urger.ui.fragment.tabs.vehicle

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_vehicle.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.VehicleAdapter
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.housing.HousingFragment.Companion.isNeedUpdateHouse
import noty_team.com.urger.ui.fragment.tabs.vehicle.add_vehicle.AddVehicleFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.response.car.AddCarExecutorResponse
import noty_team.com.urger.utils.api.response.car.DataItemGetCars

class VehicleFragment : BaseFragment<VehiclePresenter>() {

	lateinit var adapterVehicle: VehicleAdapter
	var isCallback = false
	var onAddHouseAdded: Observable<Unit>? = null
	companion object {
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.vehicle"

		fun newInstance(isShowToolbar: Boolean): VehicleFragment {
			return VehicleFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
				}
			}
		}

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}

	override fun onResume() {
		super.onResume()
		if (isCallback) {
			isCallback = false
			onAddHouseAdded?.subscribe()
			isNeedUpdateHouse = true
		}
	}

	override fun layout() = R.layout.fragment_vehicle

	override fun initialization(view: View, isFirstInit: Boolean) {

		presenter = VehiclePresenter(baseContext)
		recycler_vehicle.layoutManager =
            LinearLayoutManager(baseActivity)
		adapterVehicle = VehicleAdapter(
			arrayListOf(), baseActivity, recycler_vehicle,
			onLongClick = { _it ->
				run {
					deleteDialog(getString(R.string.delete_car)) {
						adapterVehicle.remove(_it)
						presenter?.deleteCar(_it.id!!) {
							if(it) {
								showShortToast(getString(R.string.success_delete))
							}
							if(adapterVehicle.itemCount == 0) {
								empty_list.visibility = View.VISIBLE
							}
						}
					}
				}
			}
		)
		recycler_vehicle.adapter = adapterVehicle


		if (isShowToolbar(arguments)) {
			vehicle_fragment_toolbar_container.visibility = View.VISIBLE
			vehicle_fragment_fab.show()
		} else {
			vehicle_fragment_toolbar_container.visibility = View.GONE
			vehicle_fragment_fab.hide()
		}

		nb_fragment_vehicle.setOnClickListener {
			onBackPressed()
	//		backNavigator().navigateBack()
		}

		vehicle_fragment_fab.setOnClickListener {
			addFragment(AddVehicleFragment.newInstance())
			//navigator().navigateToFragment(AddVehicleFragment.newInstance(), null, true)
		}

		getVehicle(0)
	}

	private fun getVehicle(size: Int) {
		presenter?.getCarsPagination(size) { response ->

			if(response.isEmpty()) {
				empty_list.visibility = View.VISIBLE
			} else {
				empty_list.visibility = View.GONE
			}
			adapterVehicle.onNewDataReceived(ArrayList(response), Constants.documentItemLimit)
		}
	}

	fun openAddVehicleFragment() {
		addFragment(AddVehicleFragment.newInstance().apply {
			this@VehicleFragment.callbackAddVehicle = this@VehicleFragment.callbackAddVehicle
		})
	}

	var callbackAddVehicle: (item: AddCarExecutorResponse) -> Unit = {
		isCallback = true
		onAddHouseAdded = Observable.fromCallable { addNewCar(it) }

	}

	private fun addNewCar(housingItem: AddCarExecutorResponse) {
		var item = DataItemGetCars(housingItem.data!!.file, housingItem.data!!.color, housingItem.data!!.userId, housingItem.data!!.fuel, housingItem.data!!.model, housingItem.data!!.id, housingItem.data!!.issued)
		adapterVehicle.addNewItemInStart(
			item
		)
		empty_list?.visibility = View.GONE
		NestedProfileCompanyFragment.isNeedUpdate = true
	}
}