package noty_team.com.urger.ui.fragment.webview


import android.annotation.TargetApi
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import kotlinx.android.synthetic.main.fragment_webview.*

import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment

/**
 * A simple [Fragment] subclass.
 */
class WebviewFragment : BaseFragment<WebviewPresenter>() {

    override fun layout()= R.layout.fragment_webview

    override fun initialization(view: View, isFirstInit: Boolean) {
        val url = arguments!!["url"] as String
        initWebview(url)
        back_to_registration.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initWebview(url: String) {
        val webSettings = webView.getSettings()
        webSettings.setJavaScriptEnabled(true)
        webSettings.setSupportZoom(true)
        webSettings.setBuiltInZoomControls(true)
        webView.getSettings().setDomStorageEnabled(true)
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK)
        webSettings.setDisplayZoomControls(false)
        webSettings.setAllowFileAccess(true)
        webSettings.setLoadWithOverviewMode(true)
        webSettings.setUseWideViewPort(true)

        webView.setWebViewClient(object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                val uri = Uri.parse(url)
                return handleUri(uri)
            }

            @TargetApi(Build.VERSION_CODES.N)
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                val uri = request.url
                return handleUri(uri)
            }

            private fun handleUri(uri: Uri): Boolean {

                webView.loadUrl(uri.toString())
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {}

            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
                view.loadUrl(url)
            }
        })
        webView.loadUrl(url)
    }


    companion object {
        fun newInstance(url: String): WebviewFragment {
            return WebviewFragment().apply {
                arguments = Bundle().apply {
                    putString("url", url)
                }
            }
        }
    }
}
