package noty_team.com.urger.ui.fragment.login.choose_role.company

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import androidx.core.os.ConfigurationCompat
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_company_info.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.BuildConfig
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.nip.NIP24Client
import noty_team.com.urger.nip.Number
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.fragment.privacy.PrivacyFragment
import noty_team.com.urger.utils.api.request.create_company.CreateCompanyRequest
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import noty_team.com.urger.utils.api.response.sms_checked.UserRole


class CompanyInfoFragment : BaseFragment<CompanyInfoPresenter>() {

	private lateinit var nipClient: NIP24Client

	companion object {
		fun newInstance() = CompanyInfoFragment()
	}

	val minLength = 2

	override fun layout() = noty_team.com.urger.R.layout.fragment_company_info

	override fun initialization(view: View, isFirstInit: Boolean) {

		nipClient = NIP24Client(BuildConfig.IDENTIFICATOR_NIP, BuildConfig.KEY_NIP)

		presenter = CompanyInfoPresenter(baseContext)

		button_registration_company.setOnClickListener {
			createNewUserCompany()
		}


		open_policy_webview.setOnClickListener {
			val currentLocale = ConfigurationCompat.getLocales(resources.configuration)[0]
			val countryName = currentLocale.country


			if(countryName.equals("ru", false) ||
				countryName.equals("ua", false) ||
				countryName.equals("by", false)) {
				addFragment(PrivacyFragment.newInstance("https://drive.google.com/file/d/1KHr8k4wquc5SsqApG3-NbRUuFBKM2h2_/view"))
			} else {
				addFragment(PrivacyFragment.newInstance("https://drive.google.com/file/d/1DvT6KHrGSxYJrGewTXZPa3TfGpgBMqzK/view"))
			}
		}

		initTextWatcherForCompanyName()

	}

	private fun createNewUserCompany() {

		val firstName = company_info_first_name.text
		val secondName = company_info_second_name.text
		val companyNip = company_info_nip.text
		val email = et_email_company.text
		val nameCompany = company_info_name_company.text.toString()
		val addressCompany = company_info_address_company.text.toString()

		if (validator()) {
			presenter?.createCompanyRequest(
					CreateCompanyRequest(
							PaperIO.userPhoneNumber, firstName.toString(),
							secondName.toString(), companyNip.toString(),
							email.toString(),nameCompany, addressCompany
					)
			) { isOpen ->
				if (isOpen) {
					PaperIO.userRole = UserRole.COMPANY
					CompanyActivity.start(baseActivity)
				} else {
					PaperIO.signUp = Authorization.NEED_SING_UP
				}
			}
		}
	}

	private fun initTextWatcherForCompanyName() {
		company_info_nip.addTextChangedListener(object : TextWatcher {
			@SuppressLint("SetTextI18n")
			override fun onTextChanged(str: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
				when (str.length) {
					10 -> {
						Handler(Looper.getMainLooper()).post(Runnable {
							//do stuff like remove view etc
							try {
								val all = nipClient.getAllData(Number.NIP,str.toString())

								if (all != null) {

									company_info_address_company.text = all.postCode +" " + all.city +", " + all.street +", "+ all.streetNumber
									company_info_name_company.text = all.name

									System.err.println("myLogs: name " + all.name)
								}
								else {
									Toast.makeText(requireContext(), nipClient.lastError,Toast.LENGTH_SHORT).show()
									System.err.println("myLogs: error " + nipClient.lastError)
								}
							} catch (e: Exception) {
								e.printStackTrace()
							}
						})

//						company_info_name_company.text = all.registryName
//						presenter?.getCompanyNameByNip(str.toString()) {
//							company_info_name_company.text = it
//						}
					}
					else -> {
						company_info_name_company.text = " "

					}
				}
			}

			override fun beforeTextChanged(s: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}
			override fun afterTextChanged(arg0: Editable) {}
		})
	}

	private fun validator(): Boolean {
		try {

			val firstName = company_info_first_name.text.trim().length
			if (firstName < minLength) {
				showShortToast(getString(noty_team.com.urger.R.string.name_short))
				return false
			}

			val secondName = company_info_second_name.text.trim().length
			if (secondName < minLength) {
				showShortToast(getString(noty_team.com.urger.R.string.lastname_short))
				return false
			}

			val email = et_email_company.text.trim()
			if (email.isEmpty()) {
				showShortToast(getString(noty_team.com.urger.R.string.input_email))
				return false
			}

			if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
				showShortToast(getString(noty_team.com.urger.R.string.incorrect_email))
				return false
			}
			val companyNip = company_info_nip.text.trim()
			if (companyNip.isEmpty()) {
				showShortToast(getString(noty_team.com.urger.R.string.input_nip))
				return false
			}

			if (companyNip.length <= 9) {
				showShortToast(getString(noty_team.com.urger.R.string.short_nip))
				return false
			}
			val companyName = company_info_name_company.text.trim()
			if (companyName.isEmpty()) {
				showShortToast(getString(noty_team.com.urger.R.string.nip_not_find))
				return false
			}

			if (!agree_private_policy_checkbox.isChecked) {
				showShortToast(getString(noty_team.com.urger.R.string.confirm_privacy_policy))
				return false
			}

			if (!cb_confirm_terms_of_use.isChecked) {
				showShortToast(getString(noty_team.com.urger.R.string.confirm_terms_of_use))
				return false
			}
		} catch (e: java.lang.Exception) {
			showLongToast(getString(noty_team.com.urger.R.string.error_convert_data))
			return false
		}
		return true
	}

	override fun onDestroy() {
		super.onDestroy()
		subscriptions.clear()
	}
}