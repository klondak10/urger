package noty_team.com.urger.ui.fragment.login.phone.validation_phone

import noty_team.com.urger.utils.api.request.sms_phone_validation.UserPhoneValidationRequest

interface ValidatePhoneContract {
	interface View
	interface  Presenter{
		fun serverRequest(data: UserPhoneValidationRequest, openNextScreen: (isOpen: Boolean) -> Unit, showToast: (massage: String) -> Unit)
	}
}