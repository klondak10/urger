package noty_team.com.urger.ui.fragment.bottom_bar.chat

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Message
import java.util.*


class ConcreteChatPresenter(context: Context) : BasePresenter(context) {


    fun translateMessage(messages: ArrayList<Message>, pair: String, id: Int, onLoadData:(obj: Message, index: Int) -> Unit) {

        messages.forEachIndexed { index, message ->
            if(message.userId != id) {
                if (message.photo == null && message.text!!.isNotEmpty()) {
                    apiEmployer.translateMessage(message.text!!, pair, {
                        if(!it.matches.isNullOrEmpty()) {
                            var message: Message = messages[index]
                            message = Message(
                                message.message_id,
                                it.matches[0]!!.translation.toString(),
                                message.nickname,
                                message.avatar,
                                message.photo,
                                message.statusMessage,
                                message.userId,
                                message.date
                            )
                            onLoadData(message, index)
                        }
                    }, {errorMessage, errorCode ->
                        showShortToast(errorMessage)
                    }).callRequest()
                } else {
                    onLoadData(message, index)
                }
            } else {
                onLoadData(message, index)
            }
        }
    }

    fun translateAllMessage(messages: ArrayList<Message>, pair: String, id: Int, onLoadData:(ArrayList<Message>) -> Unit) {
        var list:ArrayList<Message> = arrayListOf()

        messages.forEachIndexed { index, message ->
            if(message.userId != id) {
                if (message.photo == null && message.text!!.isNotEmpty()) {
                    apiEmployer.translateMessage(message.text!!, pair, {
                        if(!it.matches.isNullOrEmpty()) {
                            var message: Message = messages[index]
                            message = Message(
                                message.message_id,
                                it.matches[0]!!.translation.toString(),
                                message.nickname,
                                message.avatar,
                                message.photo,
                                message.statusMessage,
                                message.userId,
                                message.date
                            )
                            list.add(message)
                            if(list.size == messages.size) {
                                onLoadData(list)
                            }

                        }
                    }, {errorMessage, errorCode ->
                        showShortToast(errorMessage)
                    }).callRequest()
                } else {
                    list.add(message)
                    if(list.size == messages.size) {
                        onLoadData(list)
                    }
                }
            } else {
                list.add(message)
                if(list.size == messages.size) {
                    onLoadData(list)
                }
            }
        }
    }



    fun translateLastMessage(messages: Message, pair: String, id: Int, onLoadData:(obj: Message) -> Unit) {
                if(messages.userId != id) {
                    if (messages.photo == null && messages.text!!.isNotEmpty()) {
                        apiEmployer.translateMessage(messages.text!!, pair, {
                            if (!it.matches.isNullOrEmpty()) {
                                var message: Message = messages
                                message = Message(
                                    message.message_id,
                                    it.matches[0]!!.translation.toString(),
                                    message.nickname,
                                    message.avatar,
                                    message.photo,
                                    message.statusMessage,
                                    message.userId,
                                    message.date
                                )
                                onLoadData(message)
                            }
                        }, { errorMessage, errorCode ->
                            showShortToast(errorMessage)
                        }).callRequest()
                    }
                } else {
                    onLoadData(messages)
                }
            }

}