package noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy.filter

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy.BottomVacancyNewsFragment
import noty_team.com.urger.utils.ModelFilter
import java.util.*
import android.widget.RadioButton
import kotlinx.android.synthetic.main.fragment_filter.*
import noty_team.com.urger.R


class FilterFragment : BaseFragment<FilterPresenter>() {

    private var isCleared = false
    companion object {
        fun newInstance(modelFilter: ModelFilter? = null): FilterFragment {
            if (modelFilter != null) {
                return FilterFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable("dataFilter", modelFilter)
                    }
                }
            } else {
                return FilterFragment()
            }
        }
    }
    var model: ModelFilter? = ModelFilter()
    override fun layout() = R.layout.fragment_filter

    override fun initialization(view: View, isFirstInit: Boolean) {

        expected_wage.hint = "от"

        if(arguments != null) {
            initFilterValue(arguments!!["dataFilter"] as ModelFilter)
        }
        nb_close_filter.setOnClickListener {
            //onBackPressed()
            replaceFragment(BottomVacancyNewsFragment.newInstance())
        }
        confirm_filter.setOnClickListener {
                validate()
                replaceFragment(BottomVacancyNewsFragment.newInstance(model))
        }

        input_city.getClick {
            changeLocale()
            callPlaceAutocompleteActivityIntent()
        }

        clear_filter.getClick {
            group_time.clearCheck()
            group_type.clearCheck()
            expected_wage.text.clear()
            input_city.text.clear()
            switch_house.isChecked = false
            sort_spinner.setSelection(0)
            model = null
            isCleared = true
        }

        switch_layout.getClick {
            switch_house.isChecked = !switch_house.isChecked
        }
    }

    private fun validate(){
        if(isCleared) {
            if(sort_spinner.selectedItemPosition == 1 || sort_spinner.selectedItemPosition == 0 || !expected_wage.text.toString().equals("") || group_time.checkedRadioButtonId == R.id.time_day ||
            group_time.checkedRadioButtonId == R.id.time_3days || group_time.checkedRadioButtonId == R.id.time_week || group_time.checkedRadioButtonId == R.id.time_month ||
            group_type.checkedRadioButtonId == R.id.type_full || group_type.checkedRadioButtonId == R.id.type_contract || group_type.checkedRadioButtonId == R.id.type_seasonal|| group_type.checkedRadioButtonId == R.id.type_side ||
                    switch_house.isChecked || !input_city.text.toString().equals("")) {
                model = ModelFilter()
            }
        }
        model?.type_sort = when(sort_spinner.selectedItemPosition) {
            0 -> null
            1 -> "asc"
            else ->{
                "desc" //2
            }
        }

        if (!expected_wage.text.toString().equals("")){
            model?.expectedWage =  expected_wage.text.toString().toInt()

        }else{
            model?.expectedWage = null
        }

        model?.timePublish = when(group_time.checkedRadioButtonId) {
            R.id.time_day -> 1
            R.id.time_3days -> 3
            R.id.time_week -> 7
            R.id.time_month -> 31
            else ->{
                null
            }
        }
        model?.type_employment = when(group_type.checkedRadioButtonId) {
            R.id.type_full -> "full_time"
            R.id.type_contract -> "contract"
            R.id.type_seasonal -> "part_time"
            R.id.type_side -> "temporary_job"
            else -> {
                null
            }
        }
        model?.hasHouse = if(switch_house.isChecked) 1 else 0
        model?.city = if(!input_city.text.toString().equals("")) input_city.text.toString().trim() else ""

    }

    private fun initFilterValue(model: ModelFilter) {
        if(model.type_sort.equals("asc")) {
            sort_spinner.setSelection(1)
        } else if(model.type_sort.equals("desc")) {
            sort_spinner.setSelection(2)
        } else {
            sort_spinner.setSelection(0)
        }

        expected_wage.text = SpannableStringBuilder(model.expectedWage?.toString() ?: "")

        when(model.timePublish) {
            1 -> (group_time.getChildAt(0) as RadioButton).isChecked = true
            3 -> (group_time.getChildAt(1) as RadioButton).isChecked = true
            7 -> (group_time.getChildAt(2) as RadioButton).isChecked = true
            31 -> (group_time.getChildAt(3) as RadioButton).isChecked = true
        }

        when(model.type_employment) {
            "full_time" -> (group_type.getChildAt(0) as RadioButton).isChecked = true
            "contract" -> (group_type.getChildAt(2) as RadioButton).isChecked = true
            "part_time" -> (group_type.getChildAt(1) as RadioButton).isChecked = true
            "temporary_job" ->(group_type.getChildAt(3) as RadioButton).isChecked = true
        }

        switch_house.isChecked = model.hasHouse == 1

        input_city.text  = SpannableStringBuilder(model.city)

    }

    private fun changeLocale() {
        val config = baseContext.resources.configuration
        val locale = Locale("en") // <---- your target language
        Locale.setDefault(locale)
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )
    }

    private fun callPlaceAutocompleteActivityIntent() {
        try {
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                Arrays.asList(
                    Place.Field.ADDRESS,
                    Place.Field.ID,
                    Place.Field.LAT_LNG,
                    Place.Field.OPENING_HOURS
                )
            ).setTypeFilter(TypeFilter.ADDRESS).build(baseActivity)

            startActivityForResult(intent, 8)

        } catch (e: GooglePlayServicesRepairableException) {
            showShortToast("error")
        } catch (e: GooglePlayServicesNotAvailableException) {
            showShortToast("error")

        } catch (e: Exception) {
            showShortToast("error")
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 8) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                if (data != null) {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    input_city.setText(place.address)
                }
            }
        }
    }
}