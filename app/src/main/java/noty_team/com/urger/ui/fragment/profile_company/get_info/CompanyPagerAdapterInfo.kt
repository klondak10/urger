package noty_team.com.urger.ui.fragment.profile_company.get_info

import androidx.fragment.app.FragmentManager
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.view_pager.BasePagerAdapter
import noty_team.com.urger.utils.adapters.CustomViewPager
import noty_team.com.urger.utils.getStringArray
import java.lang.Exception

class CompanyPagerAdapterInfo(fm: FragmentManager,
                              pager: CustomViewPager,
                              fragments: ArrayList<BaseFragment<*>> = arrayListOf()
) : BasePagerAdapter(fm, pager, fragments) {

    override fun getTitle(pageIndex: Int): String {
        var title = ""
        try {
            title = getStringArray(R.array.company_tab_info, context)[pageIndex]
        } catch (e: Exception) {
        }
        return title
    }
}