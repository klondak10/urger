package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_feedback.view.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.utils.api.response.feedback.get.executor.ListItem
import noty_team.com.urger.utils.setPhoto
import java.text.DateFormatSymbols

class FeedbackAdapter(var items: ArrayList<ListItem>, val context: Context) :
		RecyclerView.Adapter<FeedbackAdapter.ViewHolder>(), View.OnClickListener {

	var baseActivity = context as BaseActivity


	override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
		return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_feedback, parent, false))
	}

	override fun getItemCount() = items.size

	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val item = items.get(position)
		holder.userName.text = "${item!!.firstName} ${item!!.lastName}"
		holder.description.text = item.employerReview
		holder.rating.rating = (item.employerRate)!!.toFloat()
		if(item.avatar != null) {
			setPhoto(holder.userLogo, item.avatar)
		}
		val date = convertDate(items.get(position).executor_date)
		holder.date.text = "${date.substring(8)} ${getMonth(date.substring(5, 7).toInt())} ${date.substring(0, 4)}"

		if (position == itemCount - 1) {
			holder.divideLine.visibility = View.GONE
		}

		holder.seeMore.setOnClickListener {
			holder.description.toggle()
		}
	}

	fun setData(data: ArrayList<ListItem>) {
		items.clear()
		items.addAll(data)
		notifyDataSetChanged()
	}

	override fun onClick(v: View?) {
		when (v?.id) {

		}
	}

	class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
		var userLogo = view.user_logo_item_feedback
		var userName = view.user_name_item_feedback
		var rating = view.rating_item_feedback
		var description = view.description_item_feedback
		var date = view.date_item_feedback
		var divideLine = view.item_feedback_divide_line
		var seeMore = view.seeMore
	}

	private fun convertDate(date: String) =  date.substring(0, date.indexOf(' '))

	fun getMonth(month: Int): String {
		return DateFormatSymbols().getMonths()[month - 1]
	}
}