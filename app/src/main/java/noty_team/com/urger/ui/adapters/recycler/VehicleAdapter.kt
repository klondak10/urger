package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_vehicle.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.car.DataItemGetCars
import noty_team.com.urger.utils.setPhoto

class VehicleAdapter(list: ArrayList<DataItemGetCars>, val context: Context,
                     recyclerView: RecyclerView,
                     var onLoadData: (offset: Int) -> Unit = {},
                     var onClick: (data: DataItemGetCars) -> Unit = {},
                     var onLongClick: (data: DataItemGetCars) -> Unit = {}) :
    BaseAdapterPagination<DataItemGetCars>(list, recyclerView) {

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            try {
                itemView.setOnClickListener {
                    onClick(list[position])
                }

                itemView.setOnLongClickListener {

                    onLongClick(list[position])

                    return@setOnLongClickListener true
                }

            } catch (e: Exception) {
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            if(item.file != null) {
                setPhoto(vehicle_image, "http://${item.file.toString()}")
            } else {
                vehicle_image.setImageResource(R.drawable.image_default)
            }
//            val spannable = SpannableString(item.model + " (" + item.issued + ")")
//            spannable.setSpan(
//                StyleSpan(Typeface.BOLD),
//                0, item.model!!.length,
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//            )
            vehicle_description.text = "${item.model} (${item.issued})"
            vehicle_color.text = itemView.context.resources.getString(R.string.color_vehicle, item.color!!.replace("\"", ""))
            vehicle_gas.text = item.fuel
        }
    }

    override val itemLayoutResourceId = R.layout.item_vehicle
}