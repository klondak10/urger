package noty_team.com.urger.ui.fragment.tabs.documents

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.util.Log
import android.view.View
import io.paperdb.Paper
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_document.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.documents.add_document.AddDocumentFragment
import noty_team.com.urger.ui.fragment.tabs.documents.document_detail.FragmentDocumentDetail
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.document.DocumentsAdapterPagination


class DocumentFragment : BaseFragment<DocumentPresenter>() {

	enum class ParentScreen {
		KOLBASA,
		PAGER
	}

	companion object {
		private const val PARENT_SCREEN_KEY = "PARENT_SCREEN_KEY"
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.document"
		var isNeedUpdateDocument = false

		fun newInstance(isShowToolbar: Boolean, parentType: ParentScreen = ParentScreen.PAGER): DocumentFragment {

			return DocumentFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
					putSerializable(PARENT_SCREEN_KEY, parentType)
				}
			}
		}

		fun getParentScreenType(args: Bundle?) = args?.getSerializable(PARENT_SCREEN_KEY)
				?: ParentScreen.PAGER

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}

	var isCallback = false

	var onAddDocumentAdded: Observable<Unit>? = null

	private lateinit var documentsAdapterPagination: DocumentsAdapterPagination

	private val listDocuments = ArrayList<DocumentItem>()

	override fun layout() = R.layout.fragment_document

	override fun initialization(view: View, isFirstInit: Boolean) {

		presenter = DocumentPresenter(baseContext)

		if (isFirstInit) {
			isNeedUpdateDocument = false

			initRecycler()
			initAction()
			initToolbar()
		}

		getDocuments(0)
	}

	override fun onResume() {
		super.onResume()
		if (isNeedUpdateDocument && getParentScreenType(arguments) == ParentScreen.PAGER) {
			isNeedUpdateDocument = false
			documentsAdapterPagination?.clear()

			initRecycler()
			initAction()
			initToolbar()

		}

		if (isCallback) {
			isCallback = false
			onAddDocumentAdded?.subscribe()
			isNeedUpdateDocument = true
		}
	}

	private fun getDocuments(size: Int) {
		presenter?.getDocumentsPagination(size) { response ->
			if(response.isEmpty()) {
				if (Paper.book().exist("document")){
					empty_list.visibility = View.GONE
					txt_empty.visibility = View.VISIBLE
				}else{
					empty_list.visibility = View.VISIBLE
				}
			} else {
				empty_list.visibility = View.GONE
			}

			documentsAdapterPagination.onNewDataReceived(response)

		}

	}

	private fun initToolbar() {
		if (isShowToolbar(arguments)) {
			document_fragment_toolbar_container.visibility = View.VISIBLE
			document_fragment_fab.show()
		} else {
			document_fragment_toolbar_container.visibility = View.GONE
			document_fragment_fab.hide()
		}
	}

	private fun initRecycler() {

		recycler_document?.layoutManager =
            GridLayoutManager(baseActivity, 2)

		documentsAdapterPagination = DocumentsAdapterPagination(listDocuments, recycler_document,

				onClickPosition = { it_, position ->
					run {
						addFragment(FragmentDocumentDetail.newInstance().apply {
							fileName = it_.documentName
							addressLink = it_.file
							//listDocument = listDocuments
							//pos = position
						})
					}
				},
				onLongClick = { _it ->
					run {
						deleteDialog(getString(R.string.delete_document_text)) {
							presenter?.deleteDocument(_it.id) {
								if (it) {
									documentsAdapterPagination?.remove(_it)
									isNeedUpdateDocument = true
									if(documentsAdapterPagination.getSizeList() == 0) {
										empty_list.visibility = View.VISIBLE
									} else {
										empty_list.visibility = View.GONE
									}
									showShortToast(resources.getString(R.string.document_success_delete))
									NestedProfileCompanyFragment.isNeedUpdate = true
								} else {
									baseActivity.showShortToast("delete fold")
								}
							}
						}
					}
				}
		)

		recycler_document.adapter = documentsAdapterPagination
		documentsAdapterPagination.onListRefresh()
	}


	private fun initAction() {
		nb_fragment_document.setOnClickListener {
			onBackPressed()
		}

		document_fragment_fab.setOnClickListener {
			openAddDocumentsFragment()
		}
	}

//	var callbackAddDocument: (item: DocumentItem) -> Unit = {
//		isCallback = true
//		onAddDocumentAdded = Observable.fromCallable { addNewDocument(it) }
//
//	}
//
//	private fun addNewDocument(item: DocumentItem) {
//		Log.i("TEST", item.toString())
//		documentsAdapterPagination.onNewDataReceived()
//		empty_list.visibility = View.GONE
//		NestedProfileCompanyFragment.isNeedUpdate = true
//	}

	fun openAddDocumentsFragment() {
		addFragment(AddDocumentFragment.newInstance())
//		addFragment(AddDocumentFragment.newInstance().apply {
//			callbackAddDocument = this@DocumentFragment.callbackAddDocument
//		})
	}
}