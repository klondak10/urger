package noty_team.com.urger.ui.fragment.bottom_bar.notification

import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_preliminary.*
import kotlinx.android.synthetic.main.fragment_preliminary.recycler_preliminary
import kotlinx.android.synthetic.main.fragment_vehicle.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.push.FBService
import noty_team.com.urger.ui.adapters.items_adapter.PushItem
import noty_team.com.urger.ui.adapters.recycler.NottificationAdapter
import noty_team.com.urger.utils.api.response.edit_executor.User
import java.util.*
import kotlin.collections.ArrayList

class NotificationFragment : BaseFragment<NotificationPresenter>() {

	companion object {
		fun newInstance() = NotificationFragment()
	}
	lateinit var adapterPreliminary: NottificationAdapter
	override fun layout() = R.layout.fragment_notification
	var listPush: MutableList<PushItem> = ArrayList()
	override fun initialization(view: View, isFirstInit: Boolean) {

		presenter = NotificationPresenter(baseContext)
		initAdapter()

		getPreliminaryVacancy()

		drawer_notification.getClick {
			baseActivity.openDrawerLayout()
		}

		delete_not_list.getClick {
			adapterPreliminary.clear()
			txt_empty.visibility = View.VISIBLE
			Paper.book().delete("listPush"+PaperIO.userPhoneNumber)
		}


	}

	private fun getPreliminaryVacancy() =

		if (Paper.book().exist("listPush"+PaperIO.userPhoneNumber)) {
			if (Paper.book().exist("number"+PaperIO.userPhoneNumber)){
				if (Paper.book().read<String>("number"+PaperIO.userPhoneNumber).contains(PaperIO.userPhoneNumber))
				{
					listPush = Paper.book().read("listPush"+PaperIO.userPhoneNumber)
					listPush.reverse()

					adapterPreliminary.onNewDataReceivedWithReverse(ArrayList(listPush), 100)

					if (adapterPreliminary.itemCount>0) {
						txt_empty.visibility = View.GONE
					}else{
						txt_empty.visibility = View.VISIBLE
					}
				}
				else{
					txt_empty.visibility = View.VISIBLE
				}
			}else{
				txt_empty.visibility = View.VISIBLE
			}


		}else{
			txt_empty.visibility = View.VISIBLE
		}

	private fun initAdapter() {

		val manager =
            LinearLayoutManager(baseActivity)
		manager.setReverseLayout(true)
		manager.setStackFromEnd(true)
		recycler_preliminary.layoutManager = manager
		adapterPreliminary = NottificationAdapter(arrayListOf(), recycler_preliminary,
			onLoadData = {
				getPreliminaryVacancy()
			},
			onSwipeToDelete = {item ->
				run {
					deleteDialog(getString(R.string.delete_notification)) {
						adapterPreliminary.remove(item)
						Paper.book().write("listPush"+PaperIO.userPhoneNumber, adapterPreliminary.list)
					}
				}

			})
		recycler_preliminary.adapter = adapterPreliminary
	}
}