package noty_team.com.urger.ui.fragment.tabs.vacancy.favorite_vacancy

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_favorite_vacancy.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.FavoriteVacancyAdapter
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.TypeVacancy
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class FavoriteVacancyFragment : BaseFragment<FavoriteVacancyPresenter>() {
	private lateinit var vacancyAdapter: FavoriteVacancyAdapter
			companion object {
		private const val IS_SHOW_TOOLBAR = "urger.com.toolbar.favorite.vacancy"

		fun newInstance(isShowToolbar: Boolean): FavoriteVacancyFragment {
			return FavoriteVacancyFragment().apply {
				arguments = Bundle().apply {
					putBoolean(IS_SHOW_TOOLBAR, isShowToolbar)
				}
			}
		}

		fun isShowToolbar(args: Bundle?) = args?.getBoolean(IS_SHOW_TOOLBAR) ?: false
	}


	override fun layout() = R.layout.fragment_favorite_vacancy

	override fun initialization(view: View, isFirstInit: Boolean) {
		bottomBarIsVisible(false)
		presenter = FavoriteVacancyPresenter(baseContext)
		initVacanciesAdapter()
		if (isShowToolbar(arguments)) {
			favorite_vacancy_fragment_toolbar_container.visibility = View.VISIBLE
		} else {
			favorite_vacancy_fragment_toolbar_container.visibility = View.GONE
		}


		back_press.setOnClickListener {
			bottomBarIsVisible(true)
			onBackPressed()
		}

		recycler_favorite_vacancy.layoutManager =
            LinearLayoutManager(baseActivity)

		if(PaperIO.userRole == UserRole.EXECUTOR) {
			getVacancies()
		}

	}

	private fun getVacancies() {
		presenter?.getFavoriteVacancies({
			if(it.data.isEmpty()) {
				empty_lists.visibility = View.VISIBLE
			} else {
				empty_lists.visibility = View.GONE
			}
			vacancyAdapter.onNewDataReceived(it.map())
		}, {

		})
	}

	private fun initVacanciesAdapter() {
		recycler_favorite_vacancy.layoutManager =
            LinearLayoutManager(baseActivity)
		vacancyAdapter = FavoriteVacancyAdapter(arrayListOf(), recycler_favorite_vacancy,
			onLoadData = {
				//getVacancies()
			},
			onClick = {
				addFragment(VacancyDetailFragmnet.newInstance(it, TypeVacancy.NORMAL))
			},
			onSwipeToDelete = {
				deleteVacancy(it)

			}
		)
		recycler_favorite_vacancy.adapter = vacancyAdapter
	}

	private fun deleteVacancy(id: Int) {
		isShowLoadingDialog(true)
		presenter?.deleteFavoriteVacancy(id, {
			isShowLoadingDialog(false)
				showShortToast(getString(R.string.success_delete))
			}, {
			showShortToast(getString(R.string.error_delete))
		})
	}
}