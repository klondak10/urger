package noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data

data class DataMapResponse(

		var costOfRent: Int = 0,

		var houseId: Int = 0,

		var rooms: Int = 0,

		var address: String = "",

		var lng: String = "",

		var available: Boolean = false,

		var description: String = "",

		var currencyOfRent: String = "",

		var photos: List<String> = ArrayList(),

		var lat: String = "",

		var cost_of_rent: Int = 1500,

		var currency_of_rent: String = "USD"
)
