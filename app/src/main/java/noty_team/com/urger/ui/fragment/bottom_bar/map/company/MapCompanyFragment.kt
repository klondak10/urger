package noty_team.com.urger.ui.fragment.bottom_bar.map.company

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Typeface
import com.google.android.material.bottomsheet.BottomSheetBehavior
import androidx.drawerlayout.widget.DrawerLayout
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.gson.Gson
import com.tbruyelle.rxpermissions2.RxPermissions
import io.paperdb.Paper
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_map_company.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.qr_code.qr_code.QRCodeActivity
import noty_team.com.urger.ui.fragment.bottom_bar.chat.DialogChatFragment
import noty_team.com.urger.utils.ChatSocket
import noty_team.com.urger.utils.Rooms
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItemLocation
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.setPhoto
import org.json.JSONObject

@SuppressLint("ClickableViewAccessibility")
class MapCompanyFragment : BaseFragment<MapCompanyPresenter>(), GoogleMap.OnMarkerClickListener {

    companion object {
        fun newInstance() = MapCompanyFragment()
    }

    private var avatar: String? = null
    private var firstLocation: LatLng? = null
    lateinit var data: ArrayList<VacancyItemLocation>
    lateinit var currentVacancy: VacancyItemLocation
    private var roomsSocket = ChatSocket.provideSocketChat()
    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null
    private lateinit var mBottomSheetBehavior: BottomSheetBehavior<*>



    override fun layout() = R.layout.fragment_map_company

    @SuppressLint("CheckResult")
    override fun initialization(view: View, isFirstInit: Boolean) {

        drawer_map_company.setOnClickListener {
            baseActivity.openDrawerLayout()
        }

        initMap()
        initSocket()

        mBottomSheetBehavior = BottomSheetBehavior.from(bottom_marker_info_company)

        bottom_marker_info_company.setOnTouchListener(View.OnTouchListener { view, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {

                }
            }
            return@OnTouchListener true
        })
        write_button.getClick {

            baseActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
              mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            addFragment(DialogChatFragment.newInstance(Paper.book().read("currentVacancyid"), name_requested.text.toString(), avatar))
        }

        confirm_button.getClick {
            val rxPermissions = RxPermissions(this)

            rxPermissions
                .request(Manifest.permission.CAMERA)
                .subscribe { granted ->
                    if (granted) {
                        QRCodeActivity.start(baseActivity)
                    }
                }
        }
    }
    private fun initSocket() {

        if(!roomsSocket.connected())
            roomsSocket.connect()
        val jsonObject = JSONObject()
        if(PaperIO.userRole == UserRole.COMPANY) {
            jsonObject.put("userId", PaperIO.employerData!!.userId)
        } else {
            jsonObject.put("userId", PaperIO.executorData!!.userId)
        }
        roomsSocket.emit("getRooms", jsonObject).on("getRooms", OnChatsListener())
    }
    inner class OnChatsListener : Emitter.Listener {
        override fun call(vararg args: Any?) {
            val rooms = Gson().fromJson(args[0].toString(), Rooms::class.java) as Rooms
            if(!rooms.rooms.isNullOrEmpty()) {
                baseActivity.runOnUiThread {

                }
            } else {
                baseActivity.runOnUiThread {

                }
            }
        }
    }

    private fun initMap() {
        mMapView = map_company
        mMapView?.onCreate(arguments)

        try {
            MapsInitializer.initialize(baseActivity)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        getRequestedVacancy()

        mMapView?.getMapAsync { mMap ->
            googleMap = mMap

            googleMap?.setOnMapClickListener {
                mBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            googleMap?.setOnMarkerClickListener(this)
        }

        // For zooming automatically to the location of the marker
        if(firstLocation == null) {
            val cameraPosition = CameraPosition.Builder().target(LatLng(52.757322, 19.259165)).zoom(7f).build()
            googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }


    }

    private fun getRequestedVacancy() {
        presenter = MapCompanyPresenter(baseContext)
        presenter?.getCompaniesRequests  { it ->
            if(it.isNotEmpty()) {
                data = it
                it.forEach {
                    // 52.2322 21.0083 центр Варшавы
                    val location = LatLng(it.lat?.toDouble() ?: 52.2322, it.lng?.toDouble() ?: 21.0083)
                    val markerOptions = MarkerOptions().title(it.categoryName)
                        .snippet("${it.categoryName}\n${it.parentCategory}")
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.red_map))

                    googleMap?.addMarker(markerOptions.position(location))!!.tag = it.idVacancy
                    if (firstLocation == null) {
                        firstLocation = location
                        val cameraPosition = CameraPosition.Builder().target(firstLocation!!).zoom(14f).build()
                        googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                    }
                }
            }
        }

    }


    @SuppressLint("SetTextI18n")
    override fun onMarkerClick(marker: Marker): Boolean {
        mBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        data.forEach {
            if(marker.tag.toString().toInt() == it.idVacancy) {
                currentVacancy = it
                Paper.book().write("currentVacancyid",currentVacancy.id)
            }
        }
        if(currentVacancy != null) {
            val spannable = SpannableString(currentVacancy.categoryName + ", " + currentVacancy.parentCategory)
            spannable.setSpan(
                StyleSpan(Typeface.BOLD),
                0, currentVacancy.categoryName!!.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            category_requested.text = spannable
            car_requested.visibility = if(currentVacancy.auto!!) View.VISIBLE else View.GONE
            name_requested.text = "${currentVacancy.firstName} ${currentVacancy.lastName}"
            rating_requested.rating = currentVacancy.rate!!.rate ?: 0.0F
            review_requested.text = "${currentVacancy.rate!!.count.toString()} review"
            avatar = currentVacancy.avatar
            setPhoto((avatar_requested as ImageView), currentVacancy.avatar)
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        mMapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView?.onLowMemory()
    }
}