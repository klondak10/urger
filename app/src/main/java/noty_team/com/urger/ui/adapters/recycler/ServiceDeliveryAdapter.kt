package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import kotlinx.android.synthetic.main.item_vacancy.view.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.Room
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem



class ServiceDeliveryAdapter(
    var list: ArrayList<VacancyPaginationItem>,
    val context: Context,
    var onLoadData: (offset: Int) -> Unit = {},
    var onClick: (data: VacancyPaginationItem) -> Unit = {},
    var onLongClick: (data: VacancyPaginationItem) -> Unit = {},
    var callbackChatEmpty:(Boolean) -> Unit = {}

) : RecyclerView.Adapter<ServiceDeliveryAdapter.ViewHolder>() {

    fun setNewList(lists: ArrayList<VacancyPaginationItem>) {
        list.clear()
        list.addAll(lists)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val item = list[p1]
        p0.bind(item, onClick, onLongClick)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_vacancy, p0, false))
    }

    fun clear() {
        list.clear()
        notifyDataSetChanged()
    }
    fun addNewVacancy(item: VacancyPaginationItem) {
        list.add(0,item)
        notifyDataSetChanged()
    }
    fun addNewVacancies(item: ArrayList<VacancyPaginationItem>) {
        list.addAll(item)
        notifyDataSetChanged()
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val vacancy_company_name_position = view.vacancy_company_name_position
        val vacancy_location = view.vacancy_location
        val vacancy_price = view.vacancy_price
        val vacancy_type_employment = view.vacancy_type_employment
        val existHouse = view.existHouse


        fun bind(item: VacancyPaginationItem, onClick:(VacancyPaginationItem) -> Unit, onLongClick:(VacancyPaginationItem) -> Unit) {

                val spannable = SpannableString(item.category + ", " + item.parent_category)
                spannable.setSpan(
                    StyleSpan(Typeface.BOLD),
                    0, item.category.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                vacancy_company_name_position.text = spannable
                vacancy_location.text = item.address
                vacancy_type_employment.text = item.workSchedule
                vacancy_price.text = "${item.price} ${item.currencySymbol}"
                vacancy_type_employment.text = item.workSchedule

                existHouse.visibility = if(item.houseId == null) View.INVISIBLE else View.VISIBLE

            itemView.setOnClickListener {
                onClick(item)
            }
            itemView.setOnLongClickListener {
                onLongClick(item)
                return@setOnLongClickListener true
            }
        }
    }



    override fun getItemCount()= list.size

    fun deleteVacancy(id: Int): Boolean {
        var indexToDelete = -1
        try {
            list.forEachIndexed { index, item ->
                if (item.vacancyId == id){
                    indexToDelete = index
                    return@forEachIndexed
                }
            }
            return if (indexToDelete >= 0) {
                list.removeAt(indexToDelete)
                notifyItemRemoved(indexToDelete)
                true
            } else false
        }catch (e: java.lang.Exception){ return false}
    }

    fun editItem(data: VacancyPaginationItem){
        list.forEachIndexed { index, item ->
            if (item.vacancyId == data.vacancyId){
                list[index] = data
                notifyItemChanged(index)
                return@forEachIndexed
            }
        }
    }
}
