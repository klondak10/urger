package noty_team.com.urger.ui.fragment.tabs.vacancy.subcategory

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.categories_response.CategoryResponse
import noty_team.com.urger.utils.api.response.categories_response.DataItem

class SubcategoryPresenter(context: Context): BasePresenter(context) {

    fun getCategories(onSuccess:(response: CategoryResponse)->Unit){
        apiEmployer.getCategoriesList({
                    onSuccess(it)
                }, {errorMessage, errorCode ->
                    showLongToast(errorMessage)
                }
        ).callRequest()
    }
}