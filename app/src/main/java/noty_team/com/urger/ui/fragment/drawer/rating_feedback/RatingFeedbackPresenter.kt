package noty_team.com.urger.ui.fragment.drawer.rating_feedback

import android.content.Context
import android.util.Log
import noty_team.com.urger.base.mvp.fragment.BasePresenter

class RatingFeedbackPresenter(context: Context): BasePresenter(context) {

    fun addFeedbackFromExecutor(id: Int, rating:Int, text: String, isSuccessAddFeedback:(Boolean) -> Unit) {
        apiEmployer.addFeedbackFromExecutor(id, rating, text,  {
            if(it.statusCode == 200 || it.statusCode == 201) {
                isSuccessAddFeedback(true)
            } else if(it.statusCode == 500) {
                isSuccessAddFeedback(true)
            } else {
                isSuccessAddFeedback(false)
            }

        }, {errorMessage, errorCode ->
            isSuccessAddFeedback(false)
        }).callRequest()
    }

    fun addFeedbackFromCompany(id: Int, rating:Int, text: String, isSuccessAddFeedback:(Boolean) -> Unit) {
        apiEmployer.addFeedbackFromCompany(id, rating, text,  {
            if(it.statusCode == 200 || it.statusCode == 201) {
                isSuccessAddFeedback(true)
            } else if(it.statusCode == 500) {
                isSuccessAddFeedback(true)
            } else {
                isSuccessAddFeedback(false)
            }

        }, {errorMessage, errorCode ->

            isSuccessAddFeedback(false)
        }).callRequest()
    }
}