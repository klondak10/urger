package noty_team.com.urger.ui.fragment.profile_company.pager_company

import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.fragment_profile_company_pager.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.view_pager.MainPageAdapter
import noty_team.com.urger.ui.fragment.tabs.company.CompanyFragment
import noty_team.com.urger.ui.fragment.tabs.contract.ContractsFragment
import noty_team.com.urger.ui.fragment.tabs.documents.DocumentFragment
import noty_team.com.urger.ui.fragment.tabs.feedback.FeedbackFragment
import noty_team.com.urger.ui.fragment.tabs.housing.HousingFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.VacancyFragment

class PagerProfileCompanyFragment : BaseFragment<PagerProfileCompanyPresenter>() {

    companion object {
        fun newInstance(callback: (rootView: View) -> Unit = {}) =
            PagerProfileCompanyFragment().apply {
                onFragmentCreated = callback
            }
    }

    override fun layout() = R.layout.fragment_profile_company_pager

    var onFragmentCreated: (rootView: View) -> Unit = {}

    override fun initialization(view: View, isFirstInit: Boolean) {
            onFragmentCreated(view)
            initViewPager()
    }

    private fun initViewPager() {

        company_profile_view_pager.adapter = MainPageAdapter(
            childFragmentManager, company_profile_view_pager, arrayListOf(
                ContractsFragment.newInstance(false, ContractsFragment.ParentScreen.PAGER),
                CompanyFragment(),
                DocumentFragment.newInstance(false, DocumentFragment.ParentScreen.PAGER),
                VacancyFragment.newInstance(false, VacancyFragment.ParentScreen.PAGER),
                FeedbackFragment(),
                HousingFragment.newInstance(false, parentType = HousingFragment.ParentScreen.PAGER)
            )
        )

    }
}