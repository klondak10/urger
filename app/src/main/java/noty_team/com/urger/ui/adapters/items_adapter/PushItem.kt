package noty_team.com.urger.ui.adapters.items_adapter

import noty_team.com.urger.utils.adapters.BaseUiPaginationItem

data class PushItem(
    var profilePhoto: Boolean,
    var title: String,
    var body: String,
    var time: String
):BaseUiPaginationItem()