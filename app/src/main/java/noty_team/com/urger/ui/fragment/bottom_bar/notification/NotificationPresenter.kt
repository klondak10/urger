package noty_team.com.urger.ui.fragment.bottom_bar.notification

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.PreliminaryVacancyResponse

class NotificationPresenter(context: Context): BasePresenter(context) {


    fun getCompaniesRequests(onLoadData:(data: PreliminaryVacancyResponse) -> Unit) {
        apiEmployer.getPreliminaryVacancy({
            if(it.statusCode == 200|| it.statusCode == 201) {
                onLoadData(it)
            } else {
                showShortToast(it.error!!)
            }
        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }

    fun rejectVacancy(id: Int, isSuccessReject:(b: Boolean) -> Unit) {
        apiEmployer.rejectVacancy(id, {
            if(it.statusCode == 200|| it.statusCode == 201) {
                isSuccessReject(true)
            } else {
                showShortToast(it.error!!)
            }

        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }
}