package noty_team.com.urger.ui.fragment.tabs.vacancy.add_vacancy

import android.content.Context
import io.reactivex.Single
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.request.create_vacancy.CreateVacancyRequest
import noty_team.com.urger.utils.api.response.categories_response.CategoryResponse
import noty_team.com.urger.utils.api.response.create_vacancy.CreateVacancyResponse
import noty_team.com.urger.utils.api.response.currency.CurrencyResponse
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.api.response.get_house.GetHouseResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyResponse
import noty_team.com.urger.utils.api.response.wage_type.WageTypeResponse
import noty_team.com.urger.utils.api.response.work_schedule.WorkScheduleResponse

class AddVacancyPresenter(context: Context): BasePresenter(context) {

    fun getCategories(onSuccess:(response: CategoryResponse)->Unit, onError:()->Unit){
        apiEmployer.getCategoriesList({
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getCategory(onDataLoad: (cities: CityResponse) -> Unit) {
        val lang = when(context.resources.configuration.locale.toString()) {
            "pl" -> "pl" //польша
            "de" -> "ge" //германия
            "en" -> "en" // англ
            "ru" -> "ru"
            "uk" -> "ru"
            "be" -> "ru" // беларусь
            else -> "pl"
        }
        apiAuthorization.getCategory(lang, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onDataLoad(it)
            }
        }, {errorMessage, errorCode ->
        }).callRequest()
    }

    fun getBaseData(onAllRequestsLoaded:()->Unit,
                    onWageTypesSuccess:(response: WageTypeResponse)->Unit,
                    onScheduleTypesSuccess:(response: WorkScheduleResponse)->Unit,
                    onCurrencySuccess:(response: CurrencyResponse)->Unit,
                    onError:()->Unit){
        apiEmployer.zip(arrayListOf(
                getWageType(onWageTypesSuccess, onError),
                getWorkSchedule(onScheduleTypesSuccess, onError),
                getCurrency(onCurrencySuccess, onError)),
                onAllRequestsLoaded
        )
    }

    private fun getWageType(onSuccess:(response: WageTypeResponse)->Unit, onError:()->Unit): Single<*> {
        return apiEmployer.getWageType({
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        })
    }

    private fun getWorkSchedule(onSuccess:(response: WorkScheduleResponse)->Unit, onError:()->Unit): Single<*> {
        return apiEmployer.getWorkSchedule({
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        })
    }

    private fun getCurrency(onSuccess:(response: CurrencyResponse)->Unit, onError:()->Unit): Single<*> {
        return apiEmployer.getCurrency({
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        })
    }

    fun createVacancy(
            body: CreateVacancyRequest,
            onSuccess:(response: CreateVacancyResponse)->Unit,
            onError:()->Unit) {

        apiEmployer.createVacancy(body, {
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getVacancy(vacancyId: Int,
                   onSuccess: (vacancyItem: VacancyResponse) -> Unit,
                   onError:()->Unit) {
        apiEmployer.getVacancy(vacancyId, {
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getHouseItem(houseId: Int,
                     onSuccess: (house: GetHouseResponse) -> Unit,
                     onError:() -> Unit) {
        val companyId = PaperIO.companyData!!.companyId
        apiEmployer.getHouse(houseId, companyId,{
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun editVacancy(vacancyId: Int, body: CreateVacancyRequest,
                     onSuccess: (response: CreateVacancyResponse) -> Unit,
                     onError:() -> Unit) {
        apiEmployer.editVacancy(vacancyId, body, {
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }
}