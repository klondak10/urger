package noty_team.com.urger.ui.fragment.tabs.company.company_photo

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import noty_team.com.urger.ui.activity.company.CompanyActivity
import kotlin.math.abs


class StandardGestures(val HideBottomBar: CompanyActivity.HideBottomBar) :
		GestureDetector.SimpleOnGestureListener(), View.OnTouchListener {
	private val SWIPE_MIN_DISTANCE = 50
	private val SWIPE_THRESHOLD_VELOCITY = 100


	override fun onTouch(v: View?, event: MotionEvent?): Boolean {
		return true
	}

	override fun onFling(event1: MotionEvent, event2: MotionEvent, x: Float, y: Float): Boolean {
		val deltaX = event1.x - event2.x


		return if (abs(deltaX) >= SWIPE_MIN_DISTANCE) {
			HideBottomBar.hideBottomBar()
			true
		} else {
			HideBottomBar.showBottomBar()
			true
		}
	}
}