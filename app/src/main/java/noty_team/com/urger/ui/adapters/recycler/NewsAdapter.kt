package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.item_news.view.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.api.response.news.DataItem
import noty_team.com.urger.utils.setPhoto

class NewsAdapter(
    var items: ArrayList<DataItem>,
    val context: Context,
    val onClickItem:(DataItem) -> Unit

) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_news, parent, false)

        return ViewHolder(view).listen { position, type ->
            onClickItem(items[position])
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, index: Int) {
        val item = items[index]
        holder.bind(item)
    }


    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        var logo = view.logo_news as ImageView
        var title = view.title_news_tv as TextView
        var shortDescription = view.description_news_tv_texts as TextView

        fun bind(item: DataItem) {
            title.text = item.title
            shortDescription.text = item.description

            if(!item.image.equals("")) {
                if(item.image?.contains(",") == true) {
                    setPhoto(logo, "http://" + item.image.substring(0, item.image.indexOf(",")))
                }else {
                    setPhoto(logo, "http://" + item.image)
                }
            } else {
                logo.setImageResource(R.drawable.no_exist_photo)
            }
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }
        return this
    }
}