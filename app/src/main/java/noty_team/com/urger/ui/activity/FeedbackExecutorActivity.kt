package noty_team.com.urger.ui.activity

import android.content.Intent
import io.reactivex.Single
import kotlinx.android.synthetic.main.fragment_rating_feedback.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiEmployer
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import javax.inject.Inject

class FeedbackExecutorActivity : BaseActivity() {



    @Inject
    lateinit var apiEmployer: UrgerRetrofitApiEmployer
    init {
        val app = AppUrger
        app.instance.appComponent.inject(this)
    }

    companion object {

        val ID_INTENT = "idCompany"

        fun start(baseActivity: BaseActivity) {
            baseActivity.startActivity(
                Intent(
                    baseActivity,
                    FeedbackExecutorActivity::class.java
                )
            )
        }
    }

    override fun layout() = R.layout.fragment_rating_feedback

    override fun initialization() {
        val id = PaperIO.idCompany


        close_button.setOnClickListener {
            PaperIO.idCompany = -1
            ExecutorActivity.start(this)
            finish()
        }

        drawer_feedback_done.setOnClickListener {
            addFeedbackFromExecutor(id!!, simpleRatingBar.rating.toInt(), text_tv.text.toString()) {
                if (it) {
                    PaperIO.idCompany = -1
                    ExecutorActivity.start(this)
                    finish()
                    showShortToast(baseContext.resources.getString(R.string.success_feedback))
                } else {
                    showShortToast(baseContext.resources.getString(R.string.failure_feedback))
                }
            }
        }
    }

    override fun getDrawerLayout(): Nothing? = null

    override fun getNavigationView(): Nothing? = null

    override fun getBottomNavigationView(): Nothing? = null

    override fun openDrawerLayout() {}


    private fun addFeedbackFromExecutor(id: Int, rating:Int, text: String, isSuccessAddFeedback:(Boolean) -> Unit) {
        if(validator()) {
            apiEmployer.addFeedbackFromExecutor(id, rating, text, {
                if (it.statusCode == 200 || it.statusCode == 201) {
                    isSuccessAddFeedback(true)
                } else if (it.statusCode == 500) {
                    isSuccessAddFeedback(true)
                } else {
                    isSuccessAddFeedback(false)
                }

            }, { errorMessage, errorCode ->
                isSuccessAddFeedback(false)
            }).callRequest()
        }
    }

    fun <T> Single<T>.callRequest() {
        this.subscribe({}, {

        }).also { subscriptions.add(it) }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        PaperIO.idCompany = -1
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        PaperIO.idCompany = -1
    }

    private fun validator(): Boolean {
        if(simpleRatingBar.rating.toInt() == 0) {
            showShortToast(resources.getString(R.string.error_add_rate))
            return false
        } else if(text_tv.text.toString().length < 3) {
            showShortToast(resources.getString(R.string.error_add_review))
            return false
        }
        return true
    }

}