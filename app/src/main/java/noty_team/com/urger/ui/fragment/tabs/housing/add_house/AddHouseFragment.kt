package noty_team.com.urger.ui.fragment.tabs.housing.add_house

import android.app.Activity
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity.RESULT_OK
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_add_house.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.company.company_photo.CompanyPhotoFragment
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.adapters.company_photo.PhotoAdapter
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.request.create_house.CreateHouseRequest
import noty_team.com.urger.utils.camera.FilesPicker
import noty_team.com.urger.utils.setPhoto
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AddHouseFragment : BaseFragment<AddHousePresenter>() {


	companion object {
		fun newInstance() = AddHouseFragment()
	}

	var INDEX: String = ""

	var callbackAddHouse: (item: HousingItem) -> Unit = {}

	private var fileData = ArrayList<File>()
	private var itemHouse = CreateHouseRequest()
	private lateinit var mapCurrency: HashMap<Int, String>


	private lateinit var geoCoder: Geocoder

	override fun layout() = R.layout.fragment_add_house

	override fun initialization(view: View, isFirstInit: Boolean) {
		bottomBarIsVisible(false)
		presenter = AddHousePresenter(baseContext)

		if (isFirstInit) {
			geoCoder = Geocoder(baseContext)
			isShowLoadingDialog(true)
			initAction()
			initHousePhotoRecycler()
			getCurrency()
		}

		housing_address_all.addTextChangedListener(object : TextWatcher {
			override fun afterTextChanged(s: Editable?) {}

			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

			override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
				housing_location_preview.text = s.toString()
			}

		})

	}

	private fun getCurrency() {
		presenter?.getCurrency({
			showShortToast(it)
		}, { isSuccess, currencyList, currencyMap ->

			if (isSuccess) {
				this.mapCurrency = currencyMap
				val spinnerAdapter =
						ArrayAdapter(baseContext, R.layout.spinner_layout_item, R.id.spinner_text, currencyList)
				housing_currency_spinner.adapter = spinnerAdapter
			} else {
				showShortToast(getString(R.string.error_load_currency))
			}
			isShowLoadingDialog(false)
		})

	}

	private fun initHousePhotoRecycler() {
		recycler_photo_add_company.layoutManager =
            LinearLayoutManager(
                baseActivity,
                RecyclerView.HORIZONTAL,
                false
            )

		recycler_photo_add_company.adapter = PhotoAdapter(arrayListOf(), onClick = {

			val list = (recycler_photo_add_company.adapter as PhotoAdapter).getItemsList()

			addFragment(CompanyPhotoFragment.newInstance().apply {
				this.index = it
				listImages = list
				isLoadFromGallery = true
			})
		}, onLongClick = { _it, _index ->
			run {
				deleteDialog(getString(R.string.delete_company_image_text)) {
					(recycler_photo_add_company.adapter as PhotoAdapter).remove(_it)
					fileData.removeAt(_index)
					showShortToast(resources.getString(R.string.photo_success_delete))
					if (_index == 0)
						validateAfterDeletePhoto()
				}
			}
		})
	}

	private fun validateAfterDeletePhoto() {
		if (fileData.isEmpty()) {
			Picasso.get()
					.load(R.mipmap.test_image1)
					.into(image_preview_house)
		} else {
			fileData[0].let {
				setPhoto(image_preview_house, it.path)
			}
		}

	}

	private fun initAction() {

		nb_add_house.getClick {
			onBackPressed()
		}
		confirm_add_house.getClick {

			if (validator()) {
				itemHouse.available = false

				for ((i, v) in mapCurrency) {
					if (v == housing_currency_spinner.selectedItem.toString()) {
						itemHouse.currency_of_rent = i
					}
				}


				if (itemHouse.address != "" && itemHouse.rooms != 0 && itemHouse.cost_of_rent != 0 && itemHouse.currency_of_rent != -1) {
					presenter?.createHouse({
						isShowLoadingDialog(true)
					}, itemHouse, housing_description.text.toString().trim(), fileData, onDataLoad = {

						isShowLoadingDialog(false)
						callbackAddHouse(it)
						onBackPressed()
					})
				} else {
					showShortToast(resources.getString(R.string.fill_in_required_fields))
				}
			}


		}

		add_housing_photo.getClick {
			showDialogChoose().show()
		}

		housing_address_all.getClick {
			if (housing_address_all.text.isEmpty()) {
				callPlaceAutocompleteActivityIntent()
			} else {
				housing_address_all.isFocusable = true
				housing_address_all.isFocusableInTouchMode = true
				housing_address_all.isClickable = true
			}
		}

		descriptionTextWatcher()
	}

	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				if (fileData.size == 0) {

					uri.path!![0].let {
						setPhoto(image_preview_house, uri.path.toString())
					}
				}
				showShortToast(resources.getString(R.string.add_photo_house_success))
				fileData.addAll(mutableListOf(File(uri.path.toString())))

				val listPhoto = ArrayList<CompanyPhotoItem>()
    			listPhoto.add(CompanyPhotoItem(uri.path.toString()))

				NestedProfileCompanyFragment.isNeedUpdate = true

				//updated
				(recycler_photo_add_company.adapter as PhotoAdapter).addAll(listPhoto)
				recycler_photo_add_company.smoothScrollToPosition(0)


			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				if (fileData.size == 0) {

					uri.path!![0].let {
						setPhoto(image_preview_house, uri.path.toString())
					}
				}
				showShortToast(resources.getString(R.string.add_photo_house_success))
				fileData.addAll(mutableListOf(File(uri.path.toString())))

				val listPhoto = ArrayList<CompanyPhotoItem>()
				listPhoto.add(CompanyPhotoItem(uri.path.toString()))

				NestedProfileCompanyFragment.isNeedUpdate = true

				//updated
				(recycler_photo_add_company.adapter as PhotoAdapter).addAll(listPhoto)
				recycler_photo_add_company.smoothScrollToPosition(0)
			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}


	private fun descriptionTextWatcher() {
		housing_description.addTextChangedListener(object : TextWatcher {

			override fun afterTextChanged(s: Editable?) {
				housing_description_preview.text = s.toString()

			}

			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

			override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
				if (count < 9) {
					housing_description_preview.text = "Private studio with full bath \n and kitchen. Appartament..."
				}
			}
		})
	}

	private fun callPlaceAutocompleteActivityIntent() {
		try {
			val intent = Autocomplete.IntentBuilder(
					AutocompleteActivityMode.OVERLAY,
					Arrays.asList(
							Place.Field.ADDRESS,
							Place.Field.ID,
							Place.Field.LAT_LNG,
							Place.Field.OPENING_HOURS
					)
			).setTypeFilter(TypeFilter.ADDRESS).build(baseActivity)

			startActivityForResult(intent, 8)

		} catch (e: GooglePlayServicesRepairableException) {
			showShortToast("error")
		} catch (e: GooglePlayServicesNotAvailableException) {
			showShortToast("error")

		} catch (e: Exception) {
			showShortToast("error")
		}

	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == 8) {
			if (resultCode == RESULT_OK) {
				if (data != null) {

					housing_address_all.isFocusable = true
					housing_address_all.isFocusableInTouchMode = true
					housing_address_all.isClickable = true

					val place = Autocomplete.getPlaceFromIntent(data)

					if (place.getLatLng() != null)
						getPlaceInfo(
								place.getLatLng()?.latitude
										?: 0.0, place.getLatLng()?.longitude ?: 0.0
						)

					val name = place.getAddress()?.replace(INDEX, "") ?: ""

					housing_address_all.setText(name)
					housing_location_preview.text = name
					image_location.visibility = View.VISIBLE
					if (place.getLatLng() != null) {
							itemHouse.lat = place.getLatLng()?.latitude.toString()
							itemHouse.lng = place.getLatLng()?.longitude.toString()
						}

					itemHouse.address_place_id = place.id.toString()
					Log.d("myLogs","address_place_id "+place.id.toString())
				}
			}

		}
	}

	private fun getPlaceInfo(lat: Double, lon: Double) {
		var addresses: List<Address>? = null
		try {
			addresses = geoCoder.getFromLocation(lat, lon, 1)
		} catch (e: IOException) {
			e.printStackTrace()
		}

		if (addresses != null)
			if (addresses[0].getPostalCode() != null) {
				INDEX = addresses[0].getPostalCode()

			}

	}

	private fun validator(): Boolean {
		try {
			val address = housing_address_all.text.toString().trim()
			if (address.isEmpty()) {
				showShortToast(getString(R.string.input_correct_adress))
				return false
			} else itemHouse.address = housing_address_all.text.toString()


			val rooms = count_of_rooms.text.toString().trim()
			if (rooms.isEmpty()) {
				showShortToast("Введите количество комнат")
				return false
			} else itemHouse.rooms = count_of_rooms.text.toString().toInt()

			val costRentHouse = cost_of_rent_house.text.toString().trim()
			if (costRentHouse.isEmpty()) {
				showShortToast(getString(R.string.input_cost))
				return false
			} else itemHouse.cost_of_rent = cost_of_rent_house.text.toString().toInt()

			val description = housing_description.text.toString().trim()
			when {
				description.isEmpty() -> {
					showShortToast(getString(R.string.input_description))
					return false
				}
				description.length < 10 -> {
					showShortToast(getString(R.string.input_more_10_char))
					return false
				}

			}

			if (fileData.isEmpty()) {
				showShortToast(getString(R.string.pick_photo))
				return false
			}

		} catch (e: java.lang.Exception) {
			showLongToast(resources.getString(R.string.exeption_validation))
			return false
		}

		return true
	}
}