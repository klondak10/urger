package noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import android.view.View
import kotlinx.android.synthetic.main.fragment_executor_bottom_vacancy_news.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.view_pager.ExecutorBottomPagerAdapter
import noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy.filter.FilterFragment
import noty_team.com.urger.utils.ModelFilter

class BottomVacancyNewsFragment : BaseFragment<BottomExecutorPresenter>() {

	var modelFilter: ModelFilter? = null
	companion object {
		fun newInstance(modelFilter: ModelFilter? = null): BottomVacancyNewsFragment {
			if (modelFilter != null) {
				return BottomVacancyNewsFragment().apply {
					arguments = Bundle().apply {
						putParcelable("dataFilter", modelFilter)
					}
				}
			} else {
				return BottomVacancyNewsFragment()
			}
		}
	}

	override fun layout() = R.layout.fragment_executor_bottom_vacancy_news


	override fun initialization(view: View, isFirstInit: Boolean) {
		modelFilter = if(arguments != null) {
			arguments!!["dataFilter"] as ModelFilter
		} else {
			null
		}
		filter_bottom_vacancy.setOnClickListener {
            presenter?.pageFilter = 1
            replaceFragment(FilterFragment.newInstance(modelFilter))
        }

		drawer_bottom_vacancy.setOnClickListener {
			baseActivity.openDrawerLayout()
		}

		presenter = BottomExecutorPresenter(baseContext)

		initTabs()

		executor_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
			override fun onPageScrollStateChanged(p0: Int) {}

			override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}

			override fun onPageSelected(position: Int) {
				if(position == 0) {
					filter_bottom_vacancy.visibility = View.VISIBLE
				} else {
					filter_bottom_vacancy.visibility = View.GONE
				}
			}
		})
	}

	private fun initTabs() {
		if(modelFilter != null) {
			executor_view_pager.adapter = ExecutorBottomPagerAdapter(
				childFragmentManager, executor_view_pager, arrayListOf(
					BottomVacancyFragment.newInstance(modelFilter),
					NewsFragment.newInstance()
				)
			)} else {
			executor_view_pager.adapter = ExecutorBottomPagerAdapter(
				childFragmentManager, executor_view_pager, arrayListOf(
					BottomVacancyFragment.newInstance(),
					NewsFragment.newInstance()
				)
			)
		}
		executor_main_tabLayout.tabMode = TabLayout.MODE_FIXED
		executor_main_tabLayout.setupWithViewPager(executor_view_pager)
	}
}