package noty_team.com.urger.ui.fragment.bottom_bar.container.nested

import android.os.Bundle
import android.view.View
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.bottom_bar.ContainerFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.profile_company.pager_company.PagerProfileCompanyFragment
import noty_team.com.urger.utils.cicerone.Screens

class PagerProfileFragmentContainer : ContainerFragment() {

    companion object {
        fun getNewInstance(name: String, callback:(rootView: View)->Unit = {}): PagerProfileFragmentContainer {
            val fragment = PagerProfileFragmentContainer()

            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.arguments = arguments
            fragment.onFragmentCreated = callback

            return fragment
        }
    }
    var onFragmentCreated:(rootView: View)->Unit = {}

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(
                    Screens.FragmentScreen(
                            PagerProfileCompanyFragment.newInstance(onFragmentCreated)
                    )
            )
        }
    }
}