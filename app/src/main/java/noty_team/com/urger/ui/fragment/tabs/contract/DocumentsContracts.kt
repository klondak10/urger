package noty_team.com.urger.ui.fragment.tabs.contract

import noty_team.com.urger.utils.adapters.document.DocumentItem

interface DocumentsContracts {
	interface View
	interface Presenter {
		fun getContractPagination(listSize: Int, onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit)
		fun getDocumentsPagination(listSize: Int, onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit)
		fun deleteDocument(documentId: Int, isDeleteDocument: (isSuccess: Boolean) -> Unit)

	}
}