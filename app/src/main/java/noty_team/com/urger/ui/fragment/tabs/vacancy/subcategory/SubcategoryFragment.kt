package noty_team.com.urger.ui.fragment.tabs.vacancy.subcategory

import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_categories.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.SubcategoriesListAdapter
import noty_team.com.urger.ui.fragment.tabs.vacancy.add_vacancy.AddVacancyFragment
import noty_team.com.urger.utils.api.response.categories_response.DataItem

class SubcategoryFragment : BaseFragment<SubcategoryPresenter>() {

    companion object {
        fun newInstance(data: ArrayList<DataItem>,
                        callback:(category: DataItem) -> Unit
        ) = SubcategoryFragment().apply {
            categories = data
            onCategorySelected = callback
        }
    }

    var onCategorySelected:(category: DataItem) -> Unit = {}

    var categories = arrayListOf<DataItem>()

    override fun layout() = R.layout.fragment_categories

    override fun initialization(view: View, isFirstInit: Boolean) {

        screen_title_text_view.text = getString(R.string.tab_subcategories)

        bottomBarIsVisible(false)

        nb_add_category.getClick {
            onBackPressed()
        }

        confirm_add_vacancy.getClick{
            val selectedItem = (categories_list.adapter as SubcategoriesListAdapter).getCheckedItem()
            selectedItem?.let {
                onCategorySelected(it)

                onBackPressed()
                onBackPressed()

            }?:showShortToast(getString(R.string.pick_subcategory))
        }

        presenter = SubcategoryPresenter(baseContext)

        initAdapter()
    }

    private fun initAdapter(){
        categories_list.layoutManager =
            LinearLayoutManager(baseActivity)
        categories_list.isNestedScrollingEnabled = false
        categories_list.adapter = SubcategoriesListAdapter(categories){

        }
    }

}