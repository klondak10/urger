package noty_team.com.urger.ui.fragment.drawer.my_employee.preliminary

import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_preliminary.*
import kotlinx.android.synthetic.main.fragment_preliminary.empty_list_tv
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.PreliminaryAdapter
import noty_team.com.urger.ui.fragment.profile_executor.get_info.DetailProfileExecutor
import noty_team.com.urger.utils.Constants

class PreliminaryFragment : BaseFragment<PreliminaryPresenter>() {

    lateinit var adapterPreliminary: PreliminaryAdapter
    override fun layout() = R.layout.fragment_preliminary

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = PreliminaryPresenter(baseContext)
        initAdapter()

        getPreliminaryVacancy()
    }

    private fun getPreliminaryVacancy() {
        presenter?.getCompaniesRequests {
            if(it.data != null) {
                val filterList = it.data.filter { i -> i.status == 0 }
                if(!filterList.isNullOrEmpty()) {
                    empty_list_tv.visibility = View.GONE
                } else {
                    empty_list_tv.visibility = View.VISIBLE
                }
                adapterPreliminary.onNewDataReceived(ArrayList(filterList), Constants.documentItemLimit)
            }
        }
    }

    private fun initAdapter() {
        recycler_preliminary.layoutManager =
            LinearLayoutManager(baseActivity)

        adapterPreliminary = PreliminaryAdapter(arrayListOf(), recycler_preliminary, true,
            onLoadData = {
                getPreliminaryVacancy()
            },
            onClick = { it_ ->
                addFragment(DetailProfileExecutor.newInstance(it_.user!!, it_.auto!!))
            },
            onCancel = {item ->
                presenter?.rejectVacancy(item.id!!) {
                    showShortToast(resources.getString(R.string.success_delete))
                    adapterPreliminary.remove(item)
                }
            })
        recycler_preliminary.adapter = adapterPreliminary
    }
}