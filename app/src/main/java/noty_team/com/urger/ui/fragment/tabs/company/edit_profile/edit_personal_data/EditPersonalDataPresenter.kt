package noty_team.com.urger.ui.fragment.tabs.company.edit_profile.edit_personal_data

import android.content.Context
import io.paperdb.Paper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.request.edit_user_personal_info.EditEmployerInfoRequest
import noty_team.com.urger.utils.api.request.edit_user_personal_info.EditUserPersonalInfoRequest
import noty_team.com.urger.utils.api.response.get_city.CityResponse

class EditPersonalDataPresenter(context: Context) : EditPersonalDataContract.Presenter, BasePresenter(context) {

    override fun editUserInfo(
        firstName: String,
        lastName: String,
        email: String,
        idCategory: Array<Int>,
        position: String,
        isSuccess: (isUserEdit: Boolean) -> Unit
    ) {

        subscriptions.add(
            UrgerApi.USER_PROFILE_API.editUserPersonalInfo(

                EditUserPersonalInfoRequest(firstName, lastName,email, idCategory, position)
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        PaperIO.positionExecutor = it.data!!.executor!!.position ?: ""
                        it.parseUserInfo()
                        isSuccess(true)
                    } else {
                        isSuccess(false)
                        showShortToast(it.error)
                    }

                }, {
                    showShortToast(it.localizedMessage.toString())
                    isSuccess(false)
                })
        )
    }

    fun editUserInfoNoId(
        firstName: String,
        lastName: String,
        email: String,
        isSuccess: (isUserEdit: Boolean) -> Unit
    ) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.editUserPersonalInfo(

                EditUserPersonalInfoRequest(firstName, lastName,email)
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        it.parseUserInfo()
                        isSuccess(true)
                    } else {
                        isSuccess(false)
                        showShortToast(it.error)
                    }



                }, {
                    showShortToast(it.localizedMessage.toString())
                    isSuccess(false)
                })
        )
    }

    fun editUserInfoEmployer(
        firstName: String,
        lastName: String,
        isSuccess: (isUserEdit: Boolean) -> Unit
    ) {

        subscriptions.add(
            UrgerApi.USER_PROFILE_API.editInfoEmployer(PaperIO.employerData!!.userId!!,

                EditEmployerInfoRequest(firstName, lastName)
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        it.parseUser()
                        isSuccess(true)
                    } else {
                        isSuccess(false)
                        showShortToast(it.error)
                    }
                }, {
                    showShortToast(it.localizedMessage.toString())
                })
        )
    }

    fun getCategory(onDataLoad: (cities: CityResponse) -> Unit) {

        var lang = ""
        if(Paper.book().exist("language_app" + PaperIO.userPhoneNumber)) {
            lang = when (Paper.book().read("language_app" + PaperIO.userPhoneNumber) as Int) {
                0 -> "pl"
                1 -> "ge"
                2 -> "ru"
                else -> "pl"
            }
        }

        apiAuthorization.getCategory(lang, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onDataLoad(it)
            }
        }, {errorMessage, errorCode ->
        }).callRequest()
    }
}