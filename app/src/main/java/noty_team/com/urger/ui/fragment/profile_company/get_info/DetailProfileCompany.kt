package noty_team.com.urger.ui.fragment.profile_company.get_info

import android.os.Bundle
import android.util.Log
import com.google.android.material.tabs.TabLayout
import android.view.View
import io.paperdb.Paper
import kotlinx.android.synthetic.main.fragment_company.*
import kotlinx.android.synthetic.main.fragment_detail_company.*
import kotlinx.android.synthetic.main.fragment_detail_company.company_name
import kotlinx.android.synthetic.main.fragment_detail_company.gray_space
import kotlinx.android.synthetic.main.fragment_detail_company.nested_replace
import kotlinx.android.synthetic.main.fragment_detail_company.user_name_company_profile
import kotlinx.android.synthetic.main.fragment_profile_company.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.get_info.tabs.InfoHouseFragment
import noty_team.com.urger.ui.fragment.profile_company.pager_company.NestedProfileCompanyDetail
import noty_team.com.urger.ui.fragment.tabs.company.CompanyFragment
import noty_team.com.urger.ui.fragment.tabs.contract.ContractsFragment
import noty_team.com.urger.ui.fragment.tabs.documents.DocumentFragment
import noty_team.com.urger.ui.fragment.tabs.feedback.FeedbackFragment
import noty_team.com.urger.utils.api.model.CompanyData
import noty_team.com.urger.utils.setPhoto

class DetailProfileCompany : BaseFragment<DetailProfileCompanyPresenter>() {
    private var companyId = 0
    private var about = ""
    companion object {
        const val ID_COMPANY = "companyId"
        const val ABOUT_COMPANY = "companyAbout"
        var IsNested = false
        fun newInstance(companyId: Int): DetailProfileCompany {

            return DetailProfileCompany().apply {
                arguments = Bundle().apply {
                    putInt(ID_COMPANY, companyId)
                }
            }
        }
    }

    override fun layout() = R.layout.fragment_detail_company

    override fun initialization(view: View, isFirstInit: Boolean) {


        companyId = arguments?.getInt(ID_COMPANY)!!
        presenter = DetailProfileCompanyPresenter(baseContext)


        getInfo()

        bp_fragment.getClick {
            onBackPressed()
        }

        nested_replace.setOnClickListener {
            IsNested = !IsNested
            replaceNested(IsNested)
        }

    }

    private fun replaceNested(isNested: Boolean) {
        if (isNested) {
            company_view_pager_layout.visibility = View.GONE
            company_main_tabLayout.visibility = View.GONE
            gray_space.visibility = View.GONE
            nested_scroll_view_company_detail.visibility = View.VISIBLE
            nested_replace.setImageResource(R.mipmap.red_nested_icon)
            changeNestedFragment()
        } else {
            company_view_pager_layout.visibility = View.VISIBLE
            company_main_tabLayout.visibility = View.VISIBLE
            gray_space.visibility = View.VISIBLE
            nested_scroll_view_company_detail.visibility = View.GONE
            nested_replace.setImageResource(R.mipmap.list)

        }
    }

    private fun changeNestedFragment() {

        val fm = childFragmentManager

        val newFragment = NestedProfileCompanyDetail.newInstance(companyId)

        val transaction = fm.beginTransaction()

        transaction.add(R.id.nested_main_container_detail, newFragment)

        transaction.attach(newFragment)

        transaction.commitNow()
    }

    private fun getInfo() {
        initTab()
        initCountDataCompany()

        presenter?.getGetInfoCompany(companyId) {
            Paper.book().write("about",it.data?.about ?: "")
            company_name.text = it.data?.name ?: ""
        }

    }

    private fun initTab()  {

        Log.d("myLogs","initTab" )
           Paper.book().write("feedback","true")
           Paper.book().write("document","true")

           company_view_pager.adapter = CompanyPagerAdapterInfo(
                childFragmentManager, company_view_pager, arrayListOf(
                    ContractsFragment.newInstance(false, ContractsFragment.ParentScreen.PAGER),
                    DocumentFragment.newInstance(false, DocumentFragment.ParentScreen.PAGER),
                    FeedbackFragment(),
                    InfoHouseFragment.newInstance(companyId),
                    CompanyFragment.newInstance(false,companyId)
                )
            )
            company_main_tabLayout.setupWithViewPager(company_view_pager)


    }

    override fun onDestroy() {
        Paper.book().delete("document")
        super.onDestroy()
    }

    private fun initCountDataCompany() {


        presenter?.getEmployerInfo(Paper.book().read("user_id_employer")){


            PaperIO.companyData = CompanyData(it.data?.validatedCompany?.id ?: -1,
                it.data?.validatedCompany?.name ?: "",
                it.data?.validatedCompany?.address ?: "",
                it.data?.employer?.position ?: "",
                it.data?.validatedCompany?.about ?: " ",
                it.data?.validatedCompany?.nip ?: "",
                it.data?.validatedCompany?.logo ?: ""
            )


            company_name_center.text = it.data?.employer?.firstName + " " + it.data?.employer?.lastName
            user_name_company_profile.text = it.data?.employer?.firstName + " " + it.data?.employer?.lastName
        }
    }
}
