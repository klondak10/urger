package noty_team.com.urger.ui.fragment.login.choose_role

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_login_choose_role.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.login.choose_role.company.CompanyInfoFragment
import noty_team.com.urger.ui.fragment.login.choose_role.executor.ExecutorInfoFragment
import noty_team.com.urger.utils.ViewWeightAnimationWrapper
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.util.concurrent.TimeUnit

class ChooseRoleFragment : BaseFragment<ChooseRolePresenter>() {

	companion object {
		fun newInstance() = ChooseRoleFragment()
	}

	override fun layout() = R.layout.fragment_login_choose_role

	override fun initialization(view: View, isFirstInit: Boolean) {

		init()

	}

	private fun init() {

		/*PaperIO.signUp = Authorization.CHOOSE_ROLE*/

		//containers layout
		company_layout.setOnClickListener {
			isContainerClick(false, null)
			setNewSize(company_layout, executor_layout, UserRole.COMPANY)
		}
		executor_layout.setOnClickListener {
			isContainerClick(false, null)
			setNewSize(executor_layout, company_layout, UserRole.EXECUTOR)
		}
		//register btns
		choose_role_registration_company_btn.setOnClickListener {
			PaperIO.userRole = UserRole.COMPANY
			addFragment(CompanyInfoFragment.newInstance())
		}

		choose_role_executor_continue_btn.setOnClickListener {
			PaperIO.userRole = UserRole.EXECUTOR
			addFragment(ExecutorInfoFragment.newInstance())
		}
	}


	private fun setNewSize(bigLayout: FrameLayout?, smallLayout: FrameLayout?, role: UserRole) {

		hideUI()

		val animationWrapperEmployer = ViewWeightAnimationWrapper(bigLayout)
		val animEmployer = ObjectAnimator.ofFloat(
				animationWrapperEmployer,
				"weight",
				animationWrapperEmployer.weight,
				0.5f
		)

		val animationWrapperSeeker = ViewWeightAnimationWrapper(smallLayout)
		val animSeeker = ObjectAnimator.ofFloat(
				animationWrapperSeeker,
				"weight",
				animationWrapperSeeker.weight,
				1f
		)

		animEmployer.duration = 300
		animSeeker.duration = 300

		animSeeker.start()
		animEmployer.start()

		Observable
				.interval(500, TimeUnit.MILLISECONDS)
				.take(1)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe {
					makeChangeUI(role)

				}
	}

	@SuppressLint("ResourceAsColor")
	private fun makeChangeUI(role: UserRole) {

		val slide_up = AnimationUtils.loadAnimation(requireActivity(), R.anim.slide_up)

		when (role) {
			UserRole.COMPANY -> {
				choose_role_description_company.startAnimation(slide_up)
				choose_role_registration_company_btn.startAnimation(slide_up)
				text1.startAnimation(slide_up)

				choose_role_description_company.visibility = View.VISIBLE
				choose_role_registration_company_btn.visibility = View.VISIBLE

				choose_role_description_executor.visibility = View.GONE
				choose_role_executor_continue_btn.visibility = View.GONE

			}
			UserRole.EXECUTOR -> {
				choose_role_description_executor.startAnimation(slide_up)
				choose_role_executor_continue_btn.startAnimation(slide_up)
				text2.startAnimation(slide_up)


				choose_role_description_executor.visibility = View.VISIBLE
				choose_role_executor_continue_btn.visibility = View.VISIBLE

				choose_role_description_company.visibility = View.GONE
				choose_role_registration_company_btn.visibility = View.GONE

			}
		}

		isContainerClick(true, role)
	}

	private fun hideUI() {
		choose_role_description_company.visibility = View.GONE
		choose_role_executor_continue_btn.visibility = View.GONE

		choose_role_description_executor.visibility = View.GONE
		choose_role_registration_company_btn.visibility = View.GONE

	}

	private fun isContainerClick(isClick: Boolean, role: UserRole?) {
		if (isClick) {
			choose_role_registration_company_btn.isClickable = true
			choose_role_executor_continue_btn.isClickable = true



			when (role) {
				UserRole.COMPANY -> {
					company_layout.isClickable = false
					executor_layout.isClickable = true

				}
				UserRole.EXECUTOR -> {
					company_layout.isClickable = true
					executor_layout.isClickable = false

				}
			}

		} else {
			choose_role_registration_company_btn.isClickable = false
			choose_role_executor_continue_btn.isClickable = false
			company_layout.isClickable = false
			executor_layout.isClickable = false
		}

	}
}