package noty_team.com.urger.ui.adapters.recycler

import android.Manifest
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.item_responded_to_vacancy.*
import noty_team.com.urger.R
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.qr_code.qr_code.QRCodeActivity
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.vacancy.requests.DataItem
import noty_team.com.urger.utils.setPhoto

class VacancyDetailAdapter(items: ArrayList<DataItem>, recyclerView: RecyclerView,
                           var onLoadData: (offset: Int) -> Unit = {},
                           var onOpenProfile: (data: DataItem) -> Unit = {},
                           var onSendMessage: (data: DataItem) -> Unit = {}):
    BaseAdapterPagination<DataItem>(items, recyclerView) {

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            send_message.setOnClickListener {
                onSendMessage(list[position])
            }
            viewHolder.responded_to_vacancy_user_name.setOnClickListener {
                onOpenProfile(list[position])
            }
            viewHolder.responded_to_vacancy_user_logo.setOnClickListener {
                onOpenProfile(list[position])
            }
            viewHolder.open_qr.setOnClickListener {

                val rxPermissions = RxPermissions(open_qr.context as CompanyActivity)
                rxPermissions
                    .request(Manifest.permission.CAMERA)
                    .subscribe { granted ->
                        if (granted) {
                            QRCodeActivity.start(open_qr.context as CompanyActivity)
                        }
                }
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            responded_to_vacancy_user_name.text = "${item.user?.firstName} ${item.user?.lastName}"
            if(item.user?.avatar != null) {
                setPhoto(responded_to_vacancy_user_logo, item.user?.avatar!!)
            }
            image_exist_car.visibility = if(item.auto) View.VISIBLE else View.GONE
        }
    }

    override val itemLayoutResourceId = R.layout.item_responded_to_vacancy

}