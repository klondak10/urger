package noty_team.com.urger.ui.adapters.recycler

import android.view.View
import android.widget.CompoundButton
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.subcategory_item.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.api.response.categories_response.DataItem

class SubcategoriesListAdapter(list: ArrayList<DataItem> = arrayListOf(),
                               var onClick:(data: DataItem) -> Unit = {}):
        BaseAdapter<DataItem, SubcategoriesListAdapter.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.subcategory_item
    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    var lastCheckedIdex = -1

    init {
        list.forEachIndexed { index, item ->
            if (item.isChecked) {
                lastCheckedIdex = index
                return@forEachIndexed
            }
        }
    }

    fun getCheckedItem(): DataItem? {
        return list.find { it.isChecked }
    }

    private fun getCheckedChangeListener(adapterPosition: Int): CompoundButton.OnCheckedChangeListener {
        return CompoundButton.OnCheckedChangeListener { _, _ ->
            list[adapterPosition].isChecked = true
            if (lastCheckedIdex >= 0) {
                list[lastCheckedIdex].isChecked = false
                try {
                    notifyItemChanged(lastCheckedIdex)
                } catch (e: IllegalStateException) {
                }
            }
            lastCheckedIdex = adapterPosition
        }
    }

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {

        override fun bind(pos: Int) {
            subcategory_radio_button.setOnCheckedChangeListener(null)
            subcategory_radio_button.isChecked = list[pos].isChecked
            subcategory_radio_button.setOnCheckedChangeListener(getCheckedChangeListener(pos))

            subcategory_radio_button.text = list[pos].name

        }
    }
}