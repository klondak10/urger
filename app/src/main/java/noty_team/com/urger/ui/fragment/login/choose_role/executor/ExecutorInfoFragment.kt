package noty_team.com.urger.ui.fragment.login.choose_role.executor

import android.os.Parcel
import android.os.Parcelable
import androidx.core.os.ConfigurationCompat
import android.view.View
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog
import kotlinx.android.synthetic.main.fragment_slave_info.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.login.choose_role.executor.executor_position.ExecutorPositionFragment
import noty_team.com.urger.ui.fragment.privacy.PrivacyFragment
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class ExecutorInfoFragment : BaseFragment<ExecutorInfoPresenter>() {

    var infoUser: InfoExecutor? = null
     var birthdayDate: Date? = null
     var birthdayMyDate: String? = null
    var differenceYear: Long? = null
    private var isAddBirthday = false
    companion object {

        fun newInstance() = ExecutorInfoFragment()
    }

    override fun layout() = noty_team.com.urger.R.layout.fragment_slave_info

    override fun initialization(view: View, isFirstInit: Boolean) {

        open_policy_webview.setOnClickListener {
            val currentLocale = ConfigurationCompat.getLocales(resources.configuration)[0]
            val countryName = currentLocale.country


            if(countryName.equals("ru", false) ||
                countryName.equals("ua", false) ||
                countryName.equals("by", false)) {
                addFragment(PrivacyFragment.newInstance("https://drive.google.com/file/d/1KHr8k4wquc5SsqApG3-NbRUuFBKM2h2_/view"))
            } else {
                addFragment(PrivacyFragment.newInstance("https://drive.google.com/file/d/1DvT6KHrGSxYJrGewTXZPa3TfGpgBMqzK/view"))
            }
        }

        ripple_slave_position.setOnClickListener {
            if (validator()) {
                infoUser = InfoExecutor(
                    PaperIO.userPhoneNumber,
                    master_reg_name.text.toString(),
                    master_reg_fname.text.toString(),
                    master_reg_email.text.toString(),
                    birthdayMyDate!!
                )
                if (infoUser != null) {
                    addFragment(ExecutorPositionFragment.newInstance(infoUser))
                }
            }
        }

        master_reg_date_birthday.setOnClickListener {
            val calendar = Calendar.getInstance()
            var year = calendar.get(Calendar.YEAR)
            var month = calendar.get(Calendar.MONTH)
            var day = calendar.get(Calendar.DAY_OF_MONTH)
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val currentDate = sdf.parse("$year-$month-$day")

            val dialog = DatePickerFragmentDialog.newInstance({ view, year1, month1, dayOfMonth1 ->
                master_reg_date_birthday.text = "${dayOfMonth1} ${getMonth(month1 + 1)} $year1"
                birthdayMyDate = "${year1}-${month1}-${dayOfMonth1}"
                birthdayDate = sdf.parse("$year1-$month1-$dayOfMonth1")
                differenceYear = currentDate.time - birthdayDate!!.time
                differenceYear = (TimeUnit.DAYS.convert(differenceYear!!, TimeUnit.MILLISECONDS)) / 365

                if(differenceYear!!.toInt() >= 18) {
                    isAddBirthday = true
                    ripple_slave_position.isEnabled = true
                    button_slave_position.isEnabled = true
                } else {
                    showLongToast(getString(noty_team.com.urger.R.string.error_age))
                }
            }, year, month, day)

            dialog.show(baseActivity.supportFragmentManager, "tag")
           /* val dateDialog = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year1, month1, dayOfMonth1 ->

                master_reg_date_birthday.text = "${dayOfMonth1} - ${month1+1} - $year1"
                birthdayMyDate = "${year1}-${month1}-${dayOfMonth1}"
                birthdayDate = sdf.parse("$year1-$month1-$dayOfMonth1")
                differenceYear = currentDate.time - birthdayDate!!.time
                differenceYear = (TimeUnit.DAYS.convert(differenceYear!!, TimeUnit.MILLISECONDS)) / 365

                if(differenceYear!!.toInt() >= 18) {
                    isAddBirthday = true
                    ripple_slave_position.isEnabled = true
                    button_slave_position.isEnabled = true
                } else {
                    showLongToast(getString(R.string.error_age))
                }

            }, year, month, day)
            dateDialog.show()*/

        }
    }
    fun getMonth(month: Int): String {
        return DateFormatSymbols().getMonths()[month - 1]
    }

    private fun validator(): Boolean {
        try {
            val firstName = master_reg_name.text.trim().length
            val minLength = 2
            if (firstName < minLength) {
                showShortToast(getString(noty_team.com.urger.R.string.name_short))
                return false
            }

            val secondName = master_reg_fname.text.trim().length
            if (secondName < minLength) {
                showShortToast(getString(noty_team.com.urger.R.string.lastname_short))
                return false
            }

            val email = master_reg_email.text.toString().trim()
            if (email.isEmpty()) {
                showShortToast(getString(noty_team.com.urger.R.string.input_email))
                return false
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                showShortToast(getString(noty_team.com.urger.R.string.incorrect_email))
                return false
            }
            if(!isAddBirthday) {
                showShortToast(getString(noty_team.com.urger.R.string.incorrect_birthday))
                return false
            }

            if(!agree_private_policy_checkbox.isChecked) {

                showShortToast(getString(noty_team.com.urger.R.string.confirm_privacy_policy))
                return false
            }

            if(!agree_terms_of_use_checkbox.isChecked) {
                showShortToast(getString(noty_team.com.urger.R.string.confirm_terms_of_use))
                return false
            }

        } catch (e: java.lang.Exception) {
            showLongToast(getString(noty_team.com.urger.R.string.error_convert_data))
            return false
        }
        return true
    }
}

data class InfoExecutor(var phone:String = "", var first_name: String = "", var last_name: String = "",
                        var email:String = "", var birthday:String = "", var category_id: String = "", var city_id:String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(phone)
        parcel.writeString(first_name)
        parcel.writeString(last_name)
        parcel.writeString(birthday)
        parcel.writeString(category_id)
        parcel.writeString(city_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<InfoExecutor> {
        override fun createFromParcel(parcel: Parcel): InfoExecutor {
            return InfoExecutor(parcel)
        }

        override fun newArray(size: Int): Array<InfoExecutor?> {
            return arrayOfNulls(size)
        }
    }
}