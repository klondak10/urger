package noty_team.com.urger.ui.fragment.tabs.vacancy.add_vacancy

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.dialog_bottom_add_to_top.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.api.response.categories_response.DataItem
import android.widget.ArrayAdapter
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.fragment_add_vacancy.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.ui.fragment.dialog.ChoosedSubCategory
import noty_team.com.urger.ui.fragment.dialog.DialogFragmentCategoryPicker
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.housing.HousingFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.VacancyFragment
import noty_team.com.urger.utils.adapters.category.SingleCategory
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.api.request.create_vacancy.CreateVacancyRequest
import noty_team.com.urger.utils.api.response.currency.CurrencyItem
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItem
import noty_team.com.urger.utils.api.response.wage_type.WageItem
import noty_team.com.urger.utils.api.response.work_schedule.ScheduleItem
import noty_team.com.urger.utils.getStringArray
import noty_team.com.urger.utils.setPhoto
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class AddVacancyFragment : BaseFragment<AddVacancyPresenter>(), ChoosedSubCategory {
	override fun pickSubcategory(id: String, categoryParent: String,  name: String) {

		categoryItem.categoryId = id
		category_edit_text.setText(categoryParent)
		vacancy_edit_text.text = name
	}

	companion object {
		private const val PLACES_RESULT_CODE = 6 * 10 + 5 + 4
		private const val SCREEN_TYPE_KEY = "SCREEN_TYPE_KEY"
		private const val VACANCY_ID_KEY = "VACANCY_ID_KEY"
		fun newInstance(
				screenType: ScreenType,
				id: Int = -1,
				callback: (data: VacancyPaginationItem, isEdit: Boolean) -> Unit = { _, _ -> }
		) =
				AddVacancyFragment().apply {
					onReturn = callback
					arguments = Bundle().apply {
						putSerializable(SCREEN_TYPE_KEY, screenType)
						putInt(VACANCY_ID_KEY, id)
					}
				}

		fun getScreenType(arguments: Bundle?) = arguments?.getSerializable(SCREEN_TYPE_KEY)
				?: ScreenType.CREATING

		fun getVacancyId(arguments: Bundle?) = arguments?.getInt(VACANCY_ID_KEY, -1) ?: -1
	}



	override fun layout() = R.layout.fragment_add_vacancy

	enum class ReturnType {
		FROM_HOUSING,
		FROM_CATEGORIES,
		NONE
	}

	enum class ScreenType {
		CREATING,
		EDITING
	}

	var wageTypes = arrayListOf<WageItem>()
	var scheduleTypes = arrayListOf<ScheduleItem>()
	var currencyTypes = arrayListOf<CurrencyItem>()

	var isResult = false
	var returnType = ReturnType.NONE
	var categoryItem = DataItem()
	var housingItem = HousingItem()

	var addressId = ""
	var address = ""
	var addressCoordinates = LatLng(0.0, 0.0)

	var onReturn: (data: VacancyPaginationItem, isEdit: Boolean) -> Unit = { _, _ -> }

	override fun initialization(view: View, isFirstInit: Boolean) {

		bottomBarIsVisible(false)

		if (isFirstInit) {

			nb_add_vacancy.getClick {
				onBackPressed()
			}

			confirm_add_vacancy.getClick {
				showWarningDialog()
			}

			category_edit_text.getClick {
				getCategories()
			}

			address_edit_text.getClick {
				changeLocale()
				callPlaceAutocompleteActivityIntent()
			}

			use_housing_check_box.setOnCheckedChangeListener { buttonView, isChecked ->
				if (isChecked) {
					add_housing_layout.visibility = View.VISIBLE
				} else {
					add_housing_layout.visibility = View.GONE
				}
			}

			confirm_add_vacancy.setOnClickListener {
				if (validate()) {
					when (getScreenType(arguments)) {
						ScreenType.CREATING -> {
							createVacancy()
						}
						ScreenType.EDITING -> {
							editVacancy()

						}
					}
				}
			}

			company_name_text_view.text = PaperIO.companyData?.name ?: ""

			presenter = AddVacancyPresenter(baseContext)

			add_housing.getClick {
				addFragment(HousingFragment.newInstance(true, HousingFragment.ScreenType.RETURN) {
					isResult = true
					housingItem = it
					returnType = ReturnType.FROM_HOUSING
				})
			}

			getBaseData()
		}
	}

	private fun initDataFromVacancy() {
		val id = getVacancyId(arguments)
		if (id >= 0) {
			isShowLoadingDialog(true)
			presenter?.getVacancy(id, {
				isShowLoadingDialog(false)
				setVacancyData(it.data)
			}, {
				isShowLoadingDialog(false)
			})
		}
	}

	private fun setVacancyData(data: VacancyItem) {
		screen_title_text_view.text = when (getScreenType(arguments)) {
			ScreenType.CREATING -> getString(R.string.tab_vacancy)
			ScreenType.EDITING -> data.position
			else -> "TEST"
		}

		category_edit_text.setText(data.category_name)
		categoryItem.categoryId = data.categoryId?.toString() ?: ""
		categoryItem.name = data.category_name ?: ""

		vacancy_edit_text.setText(data.position)

		salary_edit_text.setText((data.wage ?: 0).toString())

		currencyTypes.forEachIndexed { index, item ->
			if (item.currencyCode == data.wageCurrency) {
				currency_spinner.setSelection(index)
				return@forEachIndexed
			}
		}

		wageTypes.forEachIndexed { index, item ->
			if (item.key == data.wageType) {
				wage_type_spinner.setSelection(index)
				return@forEachIndexed
			}
		}

		addressId = data.addressPlaceId ?: ""
		address = data.address ?: ""
		if (data.lat != null && data.lng != null)
			addressCoordinates = LatLng(data.lat.toDouble(), data.lng.toDouble())
		address_edit_text.setText(data.address)


		men_number_edit_text.setText((data.numberOfEmployees ?: 0).toString())

		scheduleTypes.forEachIndexed { index, item ->
			if (item.key == data.workSchedule) {
				schedule_spinner.setSelection(index)
				return@forEachIndexed
			}
		}

		short_description_edit_text.setText(data.shortDescription)

		full_description_edit_text.setText(data.fullDescription)

		tasks_edit_text.setText(data.responsibilities)

		requirements_edit_text.setText(data.requirements)

		we_offer_edit_text.setText(data.weOffer)

		additional_edit_text.setText(data.additionally)

		if (data.houseId != null) {
			use_housing_check_box.isChecked = true
			add_housing_layout.visibility = View.VISIBLE
			getHousing(data.houseId)
		} else {
			use_housing_check_box.isChecked = false
			add_housing_layout.visibility = View.GONE
		}

	}

	private fun getHousing(id: Int) {
		isShowLoadingDialog(true)
		presenter?.getHouseItem(id, {
			isShowLoadingDialog(false)
			housingItem = it.mapToHousingItem()
			setHousing()
		}, {
			isShowLoadingDialog(false)
		})
	}

	private fun createVacancy() {
		isShowLoadingDialog(true)
		val request = getRequest()
		request?.let {
			presenter?.createVacancy(it, {

				isShowLoadingDialog(false)
				val mapped = it.data?.map()
//				currencyTypes.forEach { item ->
//					if (item.currencyCode == it.data?.wageCurrency) {
//						mapped?.currencySymbol = item.currencySymbol ?: ""
//					}
//				}
				onReturn(mapped!!, false)
				NestedProfileCompanyFragment.isNeedUpdate = true
				VacancyFragment.isNeedUpdateVacancy = true
				showShortToast(resources.getString(R.string.vacancy_success_create))
				onBackPressed()
			}, {
				isShowLoadingDialog(false)
			})
		}
	}

	private fun editVacancy() {
		isShowLoadingDialog(true)
		val request = getRequest()
		val vacancyId = getVacancyId(arguments)
		request?.let {
			presenter?.editVacancy(vacancyId, it, {
				isShowLoadingDialog(false)
				val mapped = it.data?.map()
				currencyTypes.forEach { item ->
					if (item.currencyCode == it.data?.wageCurrency) {
						mapped?.currencySymbol = item.currencySymbol ?: ""
					}
				}
				onReturn(mapped!!, true)
				NestedProfileCompanyFragment.isNeedUpdate = true
				VacancyFragment.isNeedUpdateVacancy = true
				onBackPressed()
			}, {
				isShowLoadingDialog(false)
			})
		}
	}

	private fun changeLocale() {
		val config = baseContext.resources.configuration
		val locale = Locale("en") // <---- your target language
		Locale.setDefault(locale)
		config.locale = locale
		baseContext.resources.updateConfiguration(
			config,
			baseContext.resources.displayMetrics
		)
	}

	private fun callPlaceAutocompleteActivityIntent() {
		try {
			val intent = Autocomplete.IntentBuilder(
					AutocompleteActivityMode.OVERLAY,
					Arrays.asList(
							Place.Field.ADDRESS,
							Place.Field.ID,
							Place.Field.LAT_LNG,
							Place.Field.OPENING_HOURS
					)
			).setTypeFilter(TypeFilter.ADDRESS).build(baseActivity)

			startActivityForResult(intent, PLACES_RESULT_CODE)

		} catch (e: GooglePlayServicesRepairableException) {
			showShortToast("error")
		} catch (e: GooglePlayServicesNotAvailableException) {
			showShortToast("error")
		} catch (e: Exception) {
			showShortToast("error")
		}
	}


	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		super.onActivityResult(requestCode, resultCode, data)
		if (requestCode == PLACES_RESULT_CODE && resultCode == AppCompatActivity.RESULT_OK) {
			if (data != null) {
				val place = Autocomplete.getPlaceFromIntent(data)
				place.latLng?.let {
					val name = place.address ?: ""

					val postalCode = getAddressPostalCode(it.latitude, it.longitude)
					val addressWithoutPostalCode = name.replace(postalCode, "")
						//if (addressWithoutPostalCode.matches(".*\\d.*".toRegex())) {
						address = name
						address_edit_text.setText(name)
						addressId = place.id.toString()
					//} else {
					//	showLongToast("Your address incorrect")
					//}

					addressCoordinates = it

				} ?: showLongToast(getString(R.string.adress_incorrect))
			}
		}
	}

	private fun getAddressPostalCode(lat: Double, lon: Double): String {
		var addresses: List<Address>? = null
		try {
			addresses = Geocoder(baseContext).getFromLocation(lat, lon, 1)
		} catch (e: IOException) {
			e.printStackTrace()
		}
		if (addresses != null)
			if (addresses[0].postalCode != null) {
				return addresses[0].postalCode
			}
		return ""
	}

	private fun getBaseData() {
		isShowLoadingDialog(true)

		presenter?.getBaseData(
				onWageTypesSuccess = {
					wageTypes = it.data
					initWageTypeSpinner(it.data.flatMap { arrayListOf(it.value) } as ArrayList<String>)
				},
				onScheduleTypesSuccess = {
					scheduleTypes = it.data
					initScheduleSpinner(it.data.flatMap { arrayListOf(it.value) } as ArrayList<String>)
				},
				onCurrencySuccess = {
					currencyTypes = it.data
					initCurrencySpinner(it.data.flatMap {
						arrayListOf(it.currencyCode ?: "")
					} as ArrayList<String>)
				},
				onAllRequestsLoaded = {
					isShowLoadingDialog(false)


					when (getScreenType(arguments)) {
						ScreenType.CREATING -> {
						}
						ScreenType.EDITING -> {
							initDataFromVacancy()
						}
					}
				},
				onError = {
					isShowLoadingDialog(false)
				}
		)
	}

	private fun initCurrencySpinner(currencyTypes: ArrayList<String>) {
		val arrayAdapter =
				ArrayAdapter<String>(baseContext, R.layout.spinner_layout_item, R.id.spinner_text, currencyTypes)
		arrayAdapter.setDropDownViewResource(R.layout.spinner_layout_item)
		currency_spinner.adapter = arrayAdapter
	}

	private fun initWageTypeSpinner(wageTypes: ArrayList<String>) {
		val arrayAdapter = ArrayAdapter<String>(baseContext, R.layout.spinner_layout_item, R.id.spinner_text,getStringArray(R.array.wage_type,baseContext))
		arrayAdapter.setDropDownViewResource(R.layout.spinner_layout_item)
		wage_type_spinner.adapter = arrayAdapter
	}

	private fun initScheduleSpinner(scheduleItems: ArrayList<String>) {
		val arrayAdapter =
				ArrayAdapter<String>(baseContext, R.layout.spinner_layout_item, R.id.spinner_text,
					getStringArray(R.array.work_schedule,baseContext)
				)
		arrayAdapter.setDropDownViewResource(R.layout.spinner_layout_item)
		schedule_spinner.adapter = arrayAdapter
	}

	override fun onResume() {
		super.onResume()
		if (isResult) {
			isResult = false
			when (returnType) {
				ReturnType.FROM_CATEGORIES -> {
					//setCategory()
				}
				ReturnType.FROM_HOUSING -> {
					setHousing()
				}
				ReturnType.NONE -> {
				}
			}
			returnType = ReturnType.NONE
		}
	}


	private fun setHousing() {
		add_housing_preview_layout.visibility = View.VISIBLE
		setPhoto(housing_image_view, housingItem.preview)
		housing_address_text_view.text = housingItem.address
		housing_description_tv.text = housingItem.description
	}

	private fun getCategories() {

		presenter?.getCategory {

			val fragmentManager = activity?.supportFragmentManager

			var liss: ArrayList<SingleCategory> = createListCategory(it)
			val fragmentCategory = DialogFragmentCategoryPicker.newInstance(liss)
			fragmentCategory.setChoosedSubCategory(this)
			fragmentCategory.show(fragmentManager!!, "dialog_category")
		}
	}

	private fun createListCategory(it: CityResponse): ArrayList<SingleCategory> {
		var list: MutableList<SingleCategory> = arrayListOf()
		for (obj in it.data!!) {
			list.add(SingleCategory(obj?.name, obj?.subcategories))
		}
		return list as ArrayList<SingleCategory>
	}

	private fun showWarningDialog() {
		val cautionDialog = baseActivity.getBottomSheetDialog()
		cautionDialog.setContentView(R.layout.dialog_bottom_add_to_top)
		cautionDialog.cancel_button.setOnClickListener {
			cautionDialog.dismiss()

		}

		cautionDialog.ok_button.getClick {
			cautionDialog.dismiss()
			onBackPressed()
		}

		cautionDialog.show()
	}

	private fun validate(): Boolean {
		try {
			val category = category_edit_text.text.toString().trim()
			if (category.isEmpty()) {
				showLongToast(getString(R.string.choose_category))
				return false
			}

			val vacancy = vacancy_edit_text.text.toString().trim()
			if (vacancy.isEmpty()) {
				showLongToast(getString(R.string.choose_vacancy))
				return false
			}

			val salary = salary_edit_text.text.toString().trim()
			if (salary.isEmpty()) {
				showLongToast(getString(R.string.choose_wage))
				return false
			}

			val currency = currencyTypes[currency_spinner.selectedItemPosition]
			if (currency.currencyCode?.isEmpty() == true) {
				showLongToast("Укажите валюту")
				return false
			}

			val wage_type = wageTypes[wage_type_spinner.selectedItemPosition]
			if (wage_type.key.isEmpty()) {
				showLongToast(getString(R.string.choose_descr_wage))
				return false
			}

			val address = address_edit_text.text.toString().trim()
			if (address.isEmpty()) {
				showLongToast(getString(R.string.choose_adress))
				return false
			}

			val men_number = men_number_edit_text.text.toString().trim()
			if (men_number.isEmpty() && men_number != "0") {
				showLongToast(getString(R.string.choose_count_people))
				return false
			}

			val schedule = scheduleTypes[schedule_spinner.selectedItemPosition]
			if (schedule.key.isEmpty()) {
				showLongToast(getString(R.string.choose_time_work))
				return false
			}

			val short_description = short_description_edit_text.text.toString().trim()
			if (short_description.isEmpty()) {
				showLongToast(getString(R.string.choose_short_descr))
				return false
			}

			val full_description = full_description_edit_text.text.toString().trim()
			if (full_description.isEmpty()) {
				showLongToast(getString(R.string.choose_full_descr))
				return false
			}

			val tasks = tasks_edit_text.text.toString().trim()
			if (tasks.isEmpty()) {
				showLongToast(getString(R.string.choose_tasks))
				return false
			}

			val requirements = requirements_edit_text.text.toString().trim()
			if (requirements.isEmpty()) {
				showLongToast(getString(R.string.choose_requirements))
				return false
			}

			val we_offer = we_offer_edit_text.text.toString().trim()
			if (we_offer.isEmpty()) {
				showLongToast(getString(R.string.choose_your_condition))
				return false
			}

			if (use_housing_check_box.isChecked)
				if (housingItem.houseId < 0) {
					showLongToast(getString(R.string.choose_house))
					return false
				}
		} catch (e: java.lang.Exception) {
			showLongToast(getString(R.string.error_load_data))
			return false
		}

		return true
	}

	private fun getRequest(): CreateVacancyRequest? {
		try {
			val category = category_edit_text.text.toString().trim()

			val vacancy = vacancy_edit_text.text.toString().trim()

			val salary = salary_edit_text.text.toString().trim()

			val currency = currencyTypes[currency_spinner.selectedItemPosition]

			val wage_type = "netto"

			val company_name = company_name_text_view.text.toString().trim()

			val address = address_edit_text.text.toString().trim()

			val men_number = men_number_edit_text.text.toString().trim()

			val schedule = scheduleTypes[schedule_spinner.selectedItemPosition]

			val short_description = short_description_edit_text.text.toString().trim()

			val full_description = full_description_edit_text.text.toString().trim()

			val tasks = tasks_edit_text.text.toString().trim()

			val requirements = requirements_edit_text.text.toString().trim()

			val we_offer = we_offer_edit_text.text.toString().trim()

			val additional = additional_edit_text.text.toString().trim()

			return CreateVacancyRequest(
					workSchedule = schedule.key,
					shortDescription = short_description,
					requirements = requirements,
					weOffer = we_offer,
					houseId = if (use_housing_check_box.isChecked) housingItem.houseId else null,
					address = address,
					lat = addressCoordinates.latitude.toString(),
					lng = addressCoordinates.longitude.toString(),
					addressPlaceId = addressId,
					numberOfEmployees = men_number.toInt(),
					fullDescription = full_description,
					responsibilities = tasks,
					categoryId = categoryItem.categoryId.toInt(),
					additionally = if (additional.isNotEmpty()) additional else null,
					position = vacancy,
					wage = salary.toInt(),
					wageType = wage_type,
					wageCurrency = currency.currencyCode
			)
		} catch (e: java.lang.Exception) {
			Log.i("TEST", "AddVacancyFragment error ${e.message}")
			return null
		}
	}
}