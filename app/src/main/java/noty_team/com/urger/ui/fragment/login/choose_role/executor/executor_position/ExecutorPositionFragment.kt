package noty_team.com.urger.ui.fragment.login.choose_role.executor.executor_position

import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import androidx.core.os.ConfigurationCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import kotlinx.android.synthetic.main.fragment_slave_position.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.ui.fragment.dialog.ChoosedSubCategory
import noty_team.com.urger.ui.fragment.dialog.DialogFragmentCategoryPicker
import noty_team.com.urger.ui.fragment.login.choose_role.executor.InfoExecutor
import noty_team.com.urger.utils.adapters.category.SingleCategory
import noty_team.com.urger.utils.api.request.create_executor.CreateExecutorRequest
import noty_team.com.urger.utils.api.request.create_executor.Position
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.util.*
import android.util.DisplayMetrics
import com.esotericsoftware.kryo.util.IntArray
import io.paperdb.Paper


class ExecutorPositionFragment: BaseFragment<ExecutorPositionPresenter>(),
    ChoosedSubCategory {

    private var parentId: String = ""
    private var parentIds: ArrayList<Int> = arrayListOf()

    private var countPosition = 0

    override fun pickSubcategory(id: String, parentCategory: String, name: String) {
        parentId = id

        if(countPosition == 3) {
            showShortToast(getString(noty_team.com.urger.R.string.error_add_position))
        } else {
            parentIds.add(id.toInt())
            if (slave_pos_name.text.isEmpty()) {
                slave_pos_name.text = "${name}"
            } else {
                slave_pos_name.text = "${slave_pos_name.text}, ${name}"
            }
        }
        ++countPosition
        choose_positions_button.text = resources.getString(noty_team.com.urger.R.string.add_position)
    }

    companion object {
        fun newInstance(infoUser: InfoExecutor?): ExecutorPositionFragment {

            val bundle = Bundle()
            bundle.putParcelable("data", infoUser)
            val fragment = ExecutorPositionFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    private lateinit var geoCoder: Geocoder


    override fun layout() = noty_team.com.urger.R.layout.fragment_slave_position

    override fun initialization(view: View, isFirstInit: Boolean) {

        presenter = ExecutorPositionPresenter(baseContext)

        button_button_save_pos_slave.setOnClickListener{

            createNewUserExecutor()
        }

        choose_positions_button.setOnClickListener {

            val currentLocale = ConfigurationCompat.getLocales(resources.configuration)[0]
            val countryName = currentLocale.country

            val lang = when(countryName.toLowerCase()) {
                "pl" -> "pl" //польша
                "de" -> "ge" //германия
                "en" -> "en" // англ
                "ru" -> "ru"
                "uk" -> "ru"
                "be" -> "ru" // ,tkfhecm
                else -> "pl"
            }
            presenter?.getCategory(lang) {

                val fragmentManager = activity?.supportFragmentManager

                var liss: ArrayList<SingleCategory> = createListCategory(it)
                val fragmentCategory = DialogFragmentCategoryPicker.newInstance(liss)
                fragmentCategory.setChoosedSubCategory(this)
                fragmentCategory.show(fragmentManager!!, "dialog_category")
            }
        }

        geoCoder = Geocoder(baseContext)

        slave_pos_city_name.getClick {
            callPlaceAutocompleteActivityIntent()
        }

    }

    private fun createListCategory(it: CityResponse): ArrayList<SingleCategory> {
        var list: MutableList<SingleCategory> = arrayListOf()
        for (obj in it.data!!) {
            list.add(SingleCategory(obj?.name, obj?.subcategories))
        }
        return list as ArrayList<SingleCategory>
    }

    private fun createNewUserExecutor() {
        if (validator()) {
            val info = arguments?.get("data") as InfoExecutor
            PaperIO.positionExecutor = slave_pos_name.text.toString()
            PaperIO.positionIdsExecutor = parentIds.toTypedArray()
            presenter?.createExecutorRequest(
                CreateExecutorRequest(
                    PaperIO.userPhoneNumber,
                    info.first_name,
                    info.last_name,
                    info.email,
                    info.birthday,
                    Position(
                        parentIds.toTypedArray(),
                        slave_pos_city_name.text.trim().toString()
                    )
                )
            ) { isOpen ->
                if (isOpen) {

                    PaperIO.userRole = UserRole.EXECUTOR
                    ExecutorActivity.start(baseActivity)

                    Paper.book().write("city_place_id", slave_pos_city_name.text.trim().toString())
                } else {

                    PaperIO.signUp = Authorization.NEED_SING_UP
                }
            }
        }
    }

    private fun validator(): Boolean {
        try {

            val minLength = 2

            val secondName = slave_pos_city_name.text.trim().length
            if (secondName < minLength) {
                showShortToast(getString(noty_team.com.urger.R.string.name_city_short))
                return false
            }

        } catch (e: java.lang.Exception) {
            showLongToast(getString(noty_team.com.urger.R.string.error_convert_data))
            return false
        }
        return true
    }

    private fun callPlaceAutocompleteActivityIntent() {
        try {
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                Arrays.asList(
                    Place.Field.ADDRESS,
                    Place.Field.ID,
                    Place.Field.LAT_LNG,
                    Place.Field.OPENING_HOURS
                )
            ).setTypeFilter(TypeFilter.CITIES).build(baseActivity)

            startActivityForResult(intent, 8)

        } catch (e: GooglePlayServicesRepairableException) {
            showShortToast("error")
        } catch (e: GooglePlayServicesNotAvailableException) {
            showShortToast("error")

        } catch (e: Exception) {
            showShortToast("error")
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 8) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                if (data != null) {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    slave_pos_city_name.setText(place.address)
                }
            }
        }
    }

}