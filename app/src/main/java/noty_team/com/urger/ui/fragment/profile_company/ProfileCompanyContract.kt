package noty_team.com.urger.ui.fragment.profile_company

import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.urger.utils.api.response.upload_company_logo.UploadCompanyLogoResponse
import java.io.File

interface ProfileCompanyContract {
	interface View

	interface Presenter {
		fun updateAvatar(masterPhoto: File, isSetPhotoCallback: (isSuccess: Boolean) -> Unit, showToast: (toastMassage: String) -> Unit)
		fun isCanEditProfile(isCanEdit: (isCanEdit: Boolean) -> Unit)
		fun updateBackground(masterPhoto: File, isSetPhotoCallback: (isSuccess: UploadCompanyLogoResponse) -> Unit, showToast: (toastMassage: String) -> Unit)
	}
}