package noty_team.com.urger.ui.activity.qr_code.qr_code

import android.content.Intent
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import com.google.android.gms.tasks.OnSuccessListener
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.Result
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_qr_code_scaner.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.ui.fragment.tabs.vacancy.RequestedFragment
import noty_team.com.urger.utils.api.response.sms_checked.UserRole


class QRCodeActivity : BaseActivity(), ZXingScannerView.ResultHandler {


    lateinit var mScannerView: ZXingScannerView

    companion object {
        fun newInstance() = QRCodeActivity()

        fun start(baseActivity: BaseActivity) {
            baseActivity.startActivity(
                Intent(
                    baseActivity,
                    QRCodeActivity::class.java
                )
            )
        }
    }

    override fun layout() = R.layout.activity_qr_code_scaner

    override fun initialization() {


        nb_qr_scanner.setOnClickListener {
            finish()
        }

        mScannerView = findViewById(R.id.zxing_scanner)
    }

    override fun getDrawerLayout() = null
    override fun getNavigationView() = null
    override fun getBottomNavigationView() = null
    override fun openDrawerLayout() {}


    override fun onResume() {
        super.onResume()
        if(PaperIO.userRole == UserRole.COMPANY) {
            mScannerView?.setResultHandler(this) // Register ourselves as a handler for scan results.
            mScannerView?.startCamera()
        } else {
            val text = PaperIO.executorData?.userId.toString() // Whatever you need to encode in the QR code
            var multiFormatWriter = MultiFormatWriter()
            try {
                val bitMatrix = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE,300,300)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                image_qr_code.setImageBitmap(bitmap)
                layout_qr.setBackgroundColor(resources.getColor(R.color.colorGray))
                title_toolbar.text = getString(R.string.your_qr_code)
            } catch (e: WriterException) {
                e.printStackTrace()
            }
        }
    }

    override fun handleResult(rawResult: Result?) {
        mScannerView.visibility = View.GONE
        image_qr_code.visibility = View.GONE
        title_toolbar.text = getString(R.string.vacancies)
        addFragment(RequestedFragment.newInstance(rawResult.toString().toInt()))
    }

    override fun onBackPressed() {
        super.onBackPressed()

        finish()

    }

    public override fun onPause() {
        super.onPause()
        mScannerView?.stopCamera()           // Stop camera on pause
    }

}