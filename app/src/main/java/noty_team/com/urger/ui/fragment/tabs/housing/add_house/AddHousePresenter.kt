package noty_team.com.urger.ui.fragment.tabs.housing.add_house

import android.content.Context
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.fragment.tabs.housing.HousingContract
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.request.create_house.CreateHouseRequest
import java.io.File

class AddHousePresenter(context: Context) : BasePresenter(context), AddHouseContract.Presenter {
	override fun getCurrency(showToast: (toastMassage: String) -> Unit, onSuccess: (isSuccess: Boolean,
																					currencyList: List<String>,
																					currencyMap: HashMap<Int, String>) -> Unit) {
		apiEmployer.getCurrency({
			if (it.statusCode == 200 || it.statusCode == 201) {
				onSuccess(true, it.mapToList(), it.mapToHashMap())
			}
		}, { errorMessage, errorCode ->
			showToast(errorCode.toString())
		}).callRequest()
	}


	override fun createHouse(showToast: (toastMassage: String) -> Unit, createHouseRequest: CreateHouseRequest, description: String,
							 fileData: ArrayList<File>, onDataLoad: (housingItem: HousingItem) -> Unit) {
		apiEmployer.createHousing(PaperIO.companyData?.companyId
				?: -1,  createHouseRequest, description, fileData, {

			if (it.statusCode == 200 || it.statusCode == 201) {

				val responseHouse = HousingItem()

				responseHouse.address = it.data?.address ?: ""
				responseHouse.description = it.data?.description ?: ""
				responseHouse.houseId = it.data?.houseId ?: 0
				if (it.data?.photos.isNullOrEmpty()){
					responseHouse.preview = ""
				}else {
					responseHouse.preview = it.data?.photos?.get(0).toString()
				}

				onDataLoad(responseHouse)

			} else {
				showToast(it.error)
			}

		}, { errorMessage, errorCode ->

			showToast(errorMessage)
		}).callRequest()
	}
}