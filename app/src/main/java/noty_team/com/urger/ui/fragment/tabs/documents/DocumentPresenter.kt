package noty_team.com.urger.ui.fragment.tabs.documents

import android.content.Context
import io.paperdb.Paper
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.api.response.wage_executor.GetWageResponse

class DocumentPresenter(context: Context) : BasePresenter(context), DocumentsContract.Presenter {

	fun getWageExecutor(onGetWageSuccess: (wage: GetWageResponse) -> Unit) {

		apiEmployer.getWageExecutor({

			onGetWageSuccess(it)
			PaperIO.executorData = UserUrgerData(
				PaperIO.executorData?.userId,
				PaperIO.executorData?.phone,
				PaperIO.executorData?.firstName,
				PaperIO.executorData?.lastName,
				PaperIO.executorData?.email,
				PaperIO.executorData?.type,
				PaperIO.executorData?.avatar,
				PaperIO.executorData?.background,
				PaperIO.executorData?.birthday,
				it.data?.sallary)


		}, {errorMessage, errorCode ->

			showShortToast("getWageExecutor " + errorMessage)
		}).callRequest()
	}



	override fun getDocumentsPagination(listSize: Int,
							   onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit) {
		val page = listSize / Constants.documentItemLimit
		if(PaperIO.userRole == UserRole.COMPANY) {
			apiEmployer.getDocumentPagination(PaperIO.companyData?.companyId
				?: -1, Constants.documentItemLimit, page, {

				onDocumentsSuccess(it.map())

			}, { errorMessage, errorCode ->

			}).callRequest()
		} else {
			if (Paper.book().exist("document")){
				apiEmployer.getDocumentPagination(PaperIO.companyData?.companyId
					?: -1, Constants.documentItemLimit, page, {

					onDocumentsSuccess(it.map())

				}, { errorMessage, errorCode ->

				}).callRequest()

			}else {
				apiEmployer.getDocumentExecutorPagination(
					{
						onDocumentsSuccess(it.map())
					}, { errorMessage, errorCode ->
						//showShortToast(errorMessage)
					}).callRequest()
			}
		}
	}


	override fun deleteDocument(documentId: Int, isDeleteDocument: (isSuccess: Boolean) -> Unit) {
		if(PaperIO.userRole == UserRole.COMPANY) {
			apiEmployer.deleteDocument(PaperIO.companyData?.companyId ?: -1, documentId, {
				if (it.statusCode == 200 || it.statusCode == 201) {
					isDeleteDocument(true)
				} else {
					isDeleteDocument(false)
				}
		}, { errorMessage, errorCode ->
			showShortToast(errorMessage)
		}).callRequest()
		} else {
			apiEmployer.deleteDocumentExecutor(documentId, {
				if (it.statusCode == 200 || it.statusCode == 201) {
					isDeleteDocument(true)
				} else {
					isDeleteDocument(false)
				}
			}, { errorMessage, errorCode ->
				showShortToast(errorMessage)
			}).callRequest()
		}
	}
}