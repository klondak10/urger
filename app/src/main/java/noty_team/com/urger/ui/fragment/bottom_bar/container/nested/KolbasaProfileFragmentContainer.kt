package noty_team.com.urger.ui.fragment.bottom_bar.container.nested

import android.os.Bundle
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.bottom_bar.ContainerFragment
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.utils.cicerone.Screens

class KolbasaProfileFragmentContainer : ContainerFragment() {

    companion object {
        fun getNewInstance(name: String): KolbasaProfileFragmentContainer {
            val fragment = KolbasaProfileFragmentContainer()

            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.arguments = arguments

            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(Screens.FragmentScreen(NestedProfileCompanyFragment.newInstance()))
        }
    }
}