package noty_team.com.urger.ui.fragment.profile_executor

import android.content.Context
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.UrgerApi
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfileExecutorPresenter(context: Context) : BasePresenter(context),
    ProfileExecutorContract.Presenter {

    override fun updateAvatar(
        file: File,
        isSetPhotoCallback: (isSuccess: Boolean) -> Unit,
        showToast: (toastMassage: String) -> Unit
    ) {
        val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)

        subscriptions.add(
            UrgerApi.USER_PROFILE_API.updateUserPersonalAvatar(
                PaperIO.executorData?.userId
                    ?: -1, body
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        if (it.data?.image != null) {
                            it.parseImageUrl()
                            isSetPhotoCallback(true)
                        }
                    } else {
                        showToast(context.getString(R.string.error_load_image_toast))
                    }

                }, {
                    showToast(Constants.DISPOSABLE_ERROR)
                })
        )
    }

    fun getAvatar(isSetPhotoCallback: (isSuccess: Boolean) -> Unit,
                  showToast: (toastMassage: String) -> Unit) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.getUserPersonalAvatar(PaperIO.executorData?.userId ?: -1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        if (it.data?.image != null) {
                            it.parseImageUrl()
                            isSetPhotoCallback(true)
                        }
                    } else {
                        showToast(context.getString(R.string.error_load_image_toast))
                    }
                }, {
                    showToast(Constants.DISPOSABLE_ERROR)
                })
        )
    }

    fun updateBackground(
        file: File,
        isSetPhotoCallback: (isSuccess: Boolean) -> Unit,
        showToast: (toastMassage: String) -> Unit
    ) {
        val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.updateUserPersonalBackground(
                PaperIO.executorData?.userId
                    ?: -1, body
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        if (it.data?.image != null) {
                            it.parseImageUrl()
                            isSetPhotoCallback(true)
                        }
                    } else {
                        showToast(context.getString(R.string.error_load_image_toast))
                    }
                }, {
                    showToast(Constants.DISPOSABLE_ERROR)
                })
        )
    }
}