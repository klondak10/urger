package noty_team.com.urger.ui.fragment.profile_company.nested_company

import android.content.Context
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import io.reactivex.Single
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.currency.CurrencyResponse
import noty_team.com.urger.utils.api.response.delete_vacancy.DeleteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.AddFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.api.response.wage_type.WageTypeResponse
import noty_team.com.urger.utils.api.response.work_schedule.WorkScheduleResponse
import java.io.File

class NestedProfileCompanyPresenter(context: Context) : BasePresenter(context) {


    fun getBaseData(
        onAllRequestsLoaded: () -> Unit = {},
        onDocumentSuccess: (response: ArrayList<DocumentItem>) -> Unit = {},
        onHousingSuccess: (response: ArrayList<HousingItem>) -> Unit = {},
        onVacancySuccess: (response: VacanciesListResponse) -> Unit = {},
        onWageTypesSuccess: (response: WageTypeResponse) -> Unit = {},
        onScheduleTypesSuccess: (response: WorkScheduleResponse) -> Unit = {},
        onCurrencySuccess: (response: CurrencyResponse) -> Unit = {},
        onCompanyPhotosSuccess: (response: ArrayList<CompanyPhotoItem>) -> Unit = {},
        onStatisticSuccess: (countdocument: String, countHousing: String, countVacancies: String) -> Unit = { _, _, _ -> },
        onError: () -> Unit
    ) {
        apiEmployer.zip(
            arrayListOf(
                getDocuments(onDocumentSuccess, onError),
                getHousing(onHousingSuccess, onError),
                getVacancyes(onVacancySuccess, onError),
                getWageType(onWageTypesSuccess, onError),
                getWorkSchedule(onScheduleTypesSuccess, onError),
                getCurrency(onCurrencySuccess, onError),
                getCompanyPhotos(onCompanyPhotosSuccess, onError),
                getStatistic(onStatisticSuccess, onError)
            ),
            onAllRequestsLoaded
        )
    }

    fun addFavoriteVacancy(request: AddFavoriteVacancyExecutorRequest, onSuccess:(response: AddFavoriteVacancyResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
        } else {
            apiEmployer.addVacancyFavorite(request, {
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun getDocuments(onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit, onError: () -> Unit): Single<*> {
        return apiEmployer.getDocumentPagination(
            PaperIO.companyData?.companyId
                ?: -1, 2, 0, {
                onDocumentsSuccess(it.map())
            }, { errorMessage, errorCode ->
                onError()
            })
    }

    fun getHousing(
        onHousingSuccess: (hosing: ArrayList<HousingItem>) -> Unit, onError: () -> Unit
    ): Single<*> {
        return apiEmployer.getHousingPagination(
            PaperIO.companyData?.companyId
                ?: -1, 2, 0, {

                if (it.statusCode == 200 || it.statusCode == 201) {

                    val housinglist = ArrayList<HousingItem>()

                    it.data?.forEach {
                        housinglist.add(
                            HousingItem(
                                houseId = it?.houseId ?: -1,
                                address = it?.address ?: "",
                                description = it?.description ?: "",
                                preview = if(it!!.photos.isNullOrEmpty()) {
                                    ""
                                }else{
                                    it.photos!![0]!!
                                }
                            )
                        )
                    }

                    onHousingSuccess(housinglist)

                } else {
                    showLongToast(it.error!!)
                }
            }, { errorMessage, errorCode ->

                onError()
                showLongToast(errorCode.toString())
            })

    }

    fun getVacancyes(onSuccess: (response: VacanciesListResponse) -> Unit, onError: () -> Unit): Single<*> {
        return apiEmployer.getVacancies(2, 0, {

            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        })
    }

    private fun getWageType(onSuccess: (response: WageTypeResponse) -> Unit, onError: () -> Unit): Single<*> {
        return apiEmployer.getWageType({
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        })
    }

    private fun getWorkSchedule(onSuccess: (response: WorkScheduleResponse) -> Unit, onError: () -> Unit): Single<*> {
        return apiEmployer.getWorkSchedule({
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        })
    }


    private fun getCurrency(onSuccess: (response: CurrencyResponse) -> Unit, onError: () -> Unit): Single<*> {
        return apiEmployer.getCurrency({
            onSuccess(it)
        }, { errorMessage, errorCode ->
            onError()
            //showLongToast(errorMessage)
        })
    }


    fun getCompanyPhotos(onSuccess: (ArrayList<CompanyPhotoItem>) -> Unit, onError: () -> Unit): Single<*> {
        return apiEmployer.getPhotosCompany(PaperIO.companyData?.companyId ?: -1, {

            if (it.statusCode == 200 || it.statusCode == 201) {
                var listCompanyPhoto = it.map()

                listCompanyPhoto.reverse()
                onSuccess(listCompanyPhoto)
            } else {

            }
        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
            onError()
        })
    }


    fun getStatistic(
        onSuccess: (countDocument: String, countHousing: String, countVacancies: String) -> Unit,
        onError: () -> Unit
    ): Single<*> {
        return apiEmployer.getStatistic(PaperIO.companyData!!.companyId, {


            val countDocument = it.data?.documentsCount ?: 0
            val countHousing = it.data?.housesCount ?: 0
            val countVacancies = it.data?.vacanciesCount ?: 0

            onSuccess(countDocument.toString(), countHousing.toString(), countVacancies.toString())

        }, { errorMessage, errorCode ->
            showShortToast("NestedProfileCompanyPresenter getStatistic" + errorMessage)
            onError()
        })

    }

    fun deleteDocument(documentId: Int, isDeleteDocument: (isSuccess: Boolean) -> Unit) {
        apiEmployer.deleteDocument(PaperIO.companyData?.companyId ?: -1, documentId, {

            if (it.statusCode == 200 || it.statusCode == 201)
                isDeleteDocument(true)
            else
                isDeleteDocument(false)
        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
        }
        ).callRequest()
    }


    fun deleteHouse(
        houseId: Int,
        onSuccess: (isDeleted: Boolean) -> Unit
    ) {
        apiEmployer.deleteHouse(houseId, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                onSuccess(true)
            } else {
                showShortToast(it.error)
            }
        }, { errorMessage, errorCode ->
            showShortToast(errorCode.toString())
        }).callRequest()


    }


    fun deleteCompanyPhoto(photoName: String, onDeleteSuccess: (isSuccess: Boolean) -> Unit): Single<*> {
       return apiEmployer.deletePhotoCompany(PaperIO.companyData?.companyId ?: -1, photoName, {

            if (it.statusCode == 200 || it.statusCode == 201) {
                onDeleteSuccess(true)
            } else {
                onDeleteSuccess(false)
            }

        }, { errorMessage, errorCode ->

            showShortToast(Constants.DISPOSABLE_ERROR)
        })
    }

    fun addCompanyPhotos(list: ArrayList<File>, dataList: (ArrayList<CompanyPhotoItem>) -> Unit) {
        apiEmployer.addCompanyPhoto(PaperIO.companyData?.companyId ?: -1, list, {

            if (it.statusCode == 200 || it.statusCode == 201) {
                val listPhotoItem = it.map()

                list.reverse()
                dataList(listPhotoItem)
            } else {
               showShortToast(it.error)
            }

        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }

    fun deleteVacancy(vacancyId: Int, onSuccess:(response: DeleteVacancyResponse)->Unit, onError:()->Unit){
        apiEmployer.deleteVacancy(vacancyId,{
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getFeedbackCompany(dataLoad:(GetFeedbackExecutorResponse) -> Unit) {
        apiEmployer.getFeedbackCompany({

            if(it.statusCode == 200 || it.statusCode == 201) {
                dataLoad(it.map())
            } else {
                //showShortToast(context.getString(R.string.get_feedback_error))
            }

        }, {errorMessage, errorCode ->
            //showShortToast(errorMessage)
        }).callRequest()
    }
}