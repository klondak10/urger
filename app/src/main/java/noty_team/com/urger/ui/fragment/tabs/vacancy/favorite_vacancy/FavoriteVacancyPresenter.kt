package noty_team.com.urger.ui.fragment.tabs.vacancy.favorite_vacancy

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.favorite_vacancy.DeleteFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.favorite_vacancy.favorite.GetFavoriteVacanciesResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class FavoriteVacancyPresenter(context: Context): BasePresenter(context) {

    fun getFavoriteVacancies(onSuccess:(response: GetFavoriteVacanciesResponse)->Unit, onError:()->Unit){
        if(PaperIO.userRole == UserRole.COMPANY) {

        } else {
            apiEmployer.getFavoriteVacanciesForExecutor({
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun deleteFavoriteVacancy(vacancyId: Int, onSuccess:(response: DeleteFavoriteVacancyResponse)->Unit, onError:()->Unit){
        apiEmployer.deleteFavoriteVacancy(vacancyId,{
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }
}