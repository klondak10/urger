package noty_team.com.urger.ui.fragment.profile_executor.nested_executor

import android.app.Dialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_profile_nested_executor_section.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.ExperienceAdapter
import noty_team.com.urger.ui.adapters.recycler.FeedbackAdapter
import noty_team.com.urger.ui.adapters.recycler.PortfolioAdapter
import noty_team.com.urger.ui.adapters.recycler.VehicleAdapter
import noty_team.com.urger.ui.fragment.profile_company.nested_company.NestedProfileCompanyFragment
import noty_team.com.urger.ui.fragment.tabs.documents.DocumentFragment
import noty_team.com.urger.ui.fragment.tabs.documents.add_document.AddDocumentFragment
import noty_team.com.urger.ui.fragment.tabs.documents.document_detail.FragmentDocumentDetail
import noty_team.com.urger.ui.fragment.tabs.experience.ExperienceFragment
import noty_team.com.urger.ui.fragment.tabs.experience.add_experience.AddExperienceFragment
import noty_team.com.urger.ui.fragment.tabs.feedback.FeedbackFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.PortfolioFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.add_portfolio.AddPortfolioFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.partfolio_detail.PortfolioDetailFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.VacancyFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.ui.fragment.tabs.vehicle.VehicleFragment
import noty_team.com.urger.ui.fragment.tabs.vehicle.add_vehicle.AddVehicleFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.document_nested.DocumentAdapter
import noty_team.com.urger.utils.adapters.vacancy.VacancyAdapterPagination
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.car.DataItemGetCars
import noty_team.com.urger.utils.api.response.currency.CurrencyItem
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem
import noty_team.com.urger.utils.api.response.work_schedule.ScheduleItem


class NestedProfileExecutorFragment : BaseFragment<NestedProfileExecutorPresenter>() {
	private val listDocuments = ArrayList<DocumentItem>()
	private lateinit var documentsAdapterPagination: DocumentAdapter
	lateinit var portfolioNestedAdapter: PortfolioAdapter
	lateinit var vehicleNestedAdapter: VehicleAdapter
	lateinit var experienceNestedAdapter: ExperienceAdapter
    lateinit var vacancyNestedAdapter: VacancyAdapterPagination
    lateinit var feedbackNestedAdapter: FeedbackAdapter


    private var scheduleTypes = arrayListOf<ScheduleItem>()
    private var currencyTypes = arrayListOf<CurrencyItem>()

	companion object {
		fun newInstance() = NestedProfileExecutorFragment()
	}

	override fun layout() = R.layout.fragment_profile_nested_executor_section
	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = NestedProfileExecutorPresenter(baseContext)
		initDocuments()
		initExperience()
		initFeedback()
		initPortfolio()
		initVacancy()
		initVehicle()

		initClick()

	}

	override fun onResume() {
		super.onResume()
			documentsAdapterPagination?.clear()
			getDocuments(0)

	}

	private fun initClick() {
		nested_see_more_documents.setOnClickListener {

			addFragment(DocumentFragment.newInstance(true))
			//navigator().navigateToFragment(DocumentFragment.newInstance(true), null, true)
		}

		nested_see_more_experience.setOnClickListener {
			addFragment(ExperienceFragment.newInstance(true))

			//navigator().navigateToFragment(ExperienceFragment.newInstance(true), null, true)
		}

		nested_see_more_feedback.setOnClickListener {

			addFragment(FeedbackFragment.newInstance(true))
			//navigator().navigateToFragment(FeedbackFragment.newInstance(true), null, true)
		}

		nested_see_more_portfolio.setOnClickListener {
			addFragment(PortfolioFragment.newInstance(true))
			//navigator().navigateToFragment(PortfolioFragment.newInstance(true), null, true)
		}

		nested_see_more_vacancy.getClick {
			addFragment(VacancyFragment.newInstance(true))
		//	navigator().navigateToFragment(VacancyFragment.newInstance(true), null, true)
		}

		nested_see_more_vehicle.setOnClickListener {

			addFragment(VehicleFragment.newInstance(true))
		//	navigator().navigateToFragment(VehicleFragment.newInstance(true), null, true)
		}

		nested_fab_document.getClick {
			addFragment(AddDocumentFragment.newInstance())
		}

		nested_fab_experience.getClick {
			addFragment(AddExperienceFragment.newInstance())
		}

		nested_fab_portfolio.getClick {
			addFragment(AddPortfolioFragment.newInstance())
		}

		nested_fab_vehicle.getClick {
			addFragment(AddVehicleFragment.newInstance())
		}
	}


	private fun initDocuments() {
		recycler_nested_executor_document.layoutManager =
            GridLayoutManager(baseActivity, 2)
		documentsAdapterPagination = DocumentAdapter(listDocuments, recycler_nested_executor_document,
			onLoadData = {
				getDocuments(it)
			},
			onClickItem = { it_ ->
				run {
					addFragment(FragmentDocumentDetail.newInstance().apply {
						fileName = it_.documentName
						addressLink = it_.file
					})
				}
			},
			onLongClick = { _it ->
				run {
					deleteDialog(getString(R.string.delete_document_text)) {
						presenter?.deleteDocument(_it.id) {
							if (it) {
								getDocuments(0)
								DocumentFragment.isNeedUpdateDocument = true
								showShortToast(resources.getString(R.string.document_success_delete))
								NestedProfileCompanyFragment.isNeedUpdate = true
							} else {
								baseActivity.showShortToast(getString(R.string.error_delete))
							}
						}
					}
				}
			})
		recycler_nested_executor_document.adapter = documentsAdapterPagination
	}

	private fun initExperience() {
		recycler_nested_executor_experience.layoutManager =
            LinearLayoutManager(baseActivity)
		experienceNestedAdapter = ExperienceAdapter(
			arrayListOf(), baseActivity, recycler_nested_executor_experience,
			onLongClick = { _it ->
				run {
					deleteDialog(getString(R.string.delete_experience)) {
						//experienceNestedAdapter.remove(_it)
						deleteExperience(_it)
					}
				}
			})
		recycler_nested_executor_experience.adapter = experienceNestedAdapter
		getExperiencesList(0)
	}

	private fun getExperiencesList(size: Int) {

		presenter?.getExperincePagination(size) { response ->
			var list: ArrayList<DataItem>
			list = try {
				arrayListOf(response[0], response[1], response[2])
			} catch (e: IndexOutOfBoundsException) {
				response
			}
			experienceNestedAdapter?.onNewDataReceived(list, Constants.documentItemLimit)
		}
	}

	private fun deleteExperience(_it: DataItem) {
		presenter?.deleteExperienceExecutor(_it.id!!) {
			if(it) {
				showShortToast(getString(R.string.success_delete))
				getExperiencesList(0)
			}
		}
	}

	private fun initFeedback() {

		recycler_nested_executor_feedback.layoutManager =
            LinearLayoutManager(baseActivity)
		feedbackNestedAdapter = FeedbackAdapter(arrayListOf(), baseActivity)

		recycler_nested_executor_feedback.adapter = feedbackNestedAdapter
		getFeedback()
	}

	private fun getFeedback() {

		presenter?.getFeedbacksExecutor {
			if (!it.data!!.list.isNullOrEmpty()) {
				last_feedback_tv.visibility = View.VISIBLE
				feedbackNestedAdapter.setData(arrayListOf(it.data.list[it.data.list.size - 1]))
			}

			ratingBarNested.rating = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toFloat()
			count_ratings_nested.text = "${it.data.total.count} review"
			rating_tv_nested.text = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toString()
		}
	}

	private fun initPortfolio() {
		recycler_nested_executor_portfolio.layoutManager =
            GridLayoutManager(baseActivity, 2)
		portfolioNestedAdapter = PortfolioAdapter(
			arrayListOf(), baseActivity, recycler_nested_executor_portfolio,
			onLongClick = { _it ->
				run {
					deleteDialog(getString(R.string.delete_portfolio)) {
						portfolioNestedAdapter.remove(_it)
						presenter?.deletePortfolioExecutor(_it.id!!) {
							if(it) {
								showShortToast(getString(R.string.success_delete))
								getPortfolioList()
							}

						}
					}
				}
			}, onClick = {
                addFragment(PortfolioDetailFragment.newInstance(it))
            })
		recycler_nested_executor_portfolio.adapter = portfolioNestedAdapter
        getPortfolioList()
	}

	private fun getPortfolioList() {
		presenter?.getPortfolioPagination(0) {
			var list: ArrayList<noty_team.com.urger.utils.api.response.portfolio.DataItem>
			try {
				list = arrayListOf(it[0], it[1], it[2])
			} catch (e: java.lang.IndexOutOfBoundsException) {
				list = it
			}
			portfolioNestedAdapter.onNewDataReceived(list)
		}
	}


	private fun initVacancy() {
        recycler_nested_executor_vacancy.layoutManager =
            LinearLayoutManager(baseActivity)
        recycler_nested_executor_vacancy.isNestedScrollingEnabled = false
        vacancyNestedAdapter = VacancyAdapterPagination(arrayListOf(), recycler_nested_executor_vacancy,
            onClick = {
                addFragment(VacancyDetailFragmnet.newInstance(it.vacancyId))
            },
            onLongClick = { _it ->
                showDialog(_it.vacancyId)
            }
        )

		recycler_nested_executor_vacancy.adapter = vacancyNestedAdapter
        getVacancies(0)
	}

	private fun showDialog(vacancyId: Int) {
		val vacancyDialog = Dialog(baseActivity)

		vacancyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		vacancyDialog.setContentView(R.layout.dialog_executor_vacancy)

		val addToFavButton:Button  = vacancyDialog.findViewById(R.id.add_to_favorite_button)
		val cancelButton = vacancyDialog.findViewById<Button>(R.id.cancel_vacancy_button)

		cancelButton.setOnClickListener {
			vacancyDialog.dismiss()
		}

		addToFavButton.setOnClickListener {
			val request = AddFavoriteVacancyExecutorRequest(vacancyId)
			presenter?.addFavoriteVacancy(request, {response ->
				if(response.statusCode == 200) {
					showShortToast(getString(R.string.vacancy_add_to_favorite))
				}
			}, {
				showShortToast(getString(R.string.error_add))
			})
			vacancyDialog.dismiss()
		}

		vacancyDialog.show()
	}

    private fun getVacancies(size: Int) {
        presenter?.getVacancies(size) {
			val list = it.map(scheduleTypes, currencyTypes)
			var smallList: ArrayList<VacancyPaginationItem> = arrayListOf()
			try {
				smallList.add(list[1])
				smallList.add(list[0])

			} catch (e: IndexOutOfBoundsException) {
				smallList = list
			}

            vacancyNestedAdapter.onNewDataReceived(smallList, Constants.documentItemLimit)
        }
    }

    private fun deleteVacancy(id: Int) {
        isShowLoadingDialog(true)
        presenter?.deleteVacancy(id, {
            isShowLoadingDialog(false)
			getVacancies(0)
            if (vacancyNestedAdapter.deleteVacancy(id)) {
                VacancyFragment.isNeedUpdateVacancy = true
                NestedProfileCompanyFragment.isNeedUpdate = true
                showShortToast(getString(R.string.success_delete_toast))
            }
        }, {
            isShowLoadingDialog(false)
        })
    }

	private fun initVehicle() {
		recycler_nested_executor_vehicle.layoutManager =
            LinearLayoutManager(baseActivity)

		vehicleNestedAdapter = VehicleAdapter(
			arrayListOf(), baseActivity, recycler_nested_executor_vehicle,
			onLongClick = { _it ->
				run {
					deleteDialog(getString(R.string.delete_car)) {
						//vehicleNestedAdapter.remove(_it)
						deleteVehicle(_it)
					}
				}
			})
		recycler_nested_executor_vehicle.adapter = vehicleNestedAdapter
		getVehicle(0)
	}

	private fun deleteVehicle(car: DataItemGetCars) {
		presenter?.deleteCar(car.id!!) {
			if(it) {
				showShortToast(getString(R.string.success_delete))
				getVehicle(0)
			}
		}
	}


	private fun getVehicle(size: Int) {
		presenter?.getCarsPagination(size) { response ->
			var list: List<DataItemGetCars>
			try {
				list = listOf(response[0], response[1], response[2])
			} catch (e: Exception) {
				list = response
			}
			vehicleNestedAdapter.onNewDataReceived(ArrayList(list), Constants.documentItemLimit)
		}
	}

	private fun getDocuments(size: Int) {
		var list: ArrayList<DocumentItem> = arrayListOf()
		presenter?.getDocumentsPagination(size) { response ->
			try {
				list.add(response[0])
				list.add(response[1])
				list.add(response[2])
			} catch (e: IndexOutOfBoundsException) {
				list = response
			}
			documentsAdapterPagination.onNewDataReceived(list, Constants.documentItemLimit)
		}
	}
}