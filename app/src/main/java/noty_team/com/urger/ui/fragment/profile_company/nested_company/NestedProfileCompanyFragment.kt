package noty_team.com.urger.ui.fragment.profile_company.nested_company

import android.app.Activity
import android.app.Dialog
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.github.drjacky.imagepicker.ImagePicker
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_add_house.*
import kotlinx.android.synthetic.main.fragment_profile_nested_company_section.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.FeedbackAdapter
import noty_team.com.urger.ui.fragment.tabs.company.CompanyFragment
import noty_team.com.urger.ui.fragment.tabs.company.company_photo.CompanyPhotoFragment
import noty_team.com.urger.ui.fragment.tabs.company.edit_profile.company_profile_info.CompanyProfileInfoFragment
import noty_team.com.urger.ui.fragment.tabs.documents.DocumentFragment
import noty_team.com.urger.ui.fragment.tabs.documents.add_document.AddDocumentFragment
import noty_team.com.urger.ui.fragment.tabs.documents.document_detail.FragmentDocumentDetail
import noty_team.com.urger.ui.fragment.tabs.feedback.FeedbackFragment
import noty_team.com.urger.ui.fragment.tabs.housing.HousingFragment
import noty_team.com.urger.ui.fragment.tabs.housing.add_house.AddHouseFragment
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.HouseDetailFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.VacancyFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.add_vacancy.AddVacancyFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.adapters.company_photo.PhotoAdapter
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.document_nested.DocumentAdapter
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.adapters.housing_nested.HousingNestedAdapter
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.adapters.vacancy_nested.VacancyNestedAdapter
import noty_team.com.urger.utils.api.response.currency.CurrencyItem
import noty_team.com.urger.utils.api.response.feedback.get.executor.ListItem
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.work_schedule.ScheduleItem
import noty_team.com.urger.utils.camera.FilesPicker
import noty_team.com.urger.utils.setPhoto
import noty_team.com.urger.utils.waitUntilViewDrawn
import java.io.File


class NestedProfileCompanyFragment : BaseFragment<NestedProfileCompanyPresenter>() {

	companion object {
		fun newInstance() = NestedProfileCompanyFragment()
		var isNeedUpdate = false
	}

	var countDocument = 0
	var countVacancy = 0
	var countHousing = 0

	var documentAdapter: DocumentAdapter? = null
	lateinit var feedbackNestedAdapter: FeedbackAdapter
	var housingAdapter: HousingNestedAdapter? = null

	var photoAdapter: PhotoAdapter? = null

	private lateinit var vacancyAdapter: VacancyNestedAdapter

	private var scheduleTypes = arrayListOf<ScheduleItem>()

	private var currencyTypes = arrayListOf<CurrencyItem>()

	var listVacancyItems = VacanciesListResponse()

	var isDocumentCallback = false

	var isHouseCallback = false

	private var fileData = ArrayList<File>()

	var onAddDocumentAdded: Observable<Unit>? = null

	var onAddHouseAdded: Observable<Unit>? = null


	override fun layout() = noty_team.com.urger.R.layout.fragment_profile_nested_company_section

	override fun initialization(view: View, isFirstInit: Boolean) {

		isShowLoadingDialog(true)
		presenter = NestedProfileCompanyPresenter(baseContext)

		if (isFirstInit) {
			waitUntilViewDrawn(see_more_housing) {
				zipRequest()
			}
		} else {
			if (isNeedUpdate) {
				isNeedUpdate = false
				documentAdapter?.clear()
				housingAdapter?.clear()
				vacancyAdapter?.clear()

				waitUntilViewDrawn(see_more_housing) {
					zipRequest()
				}
			}
			callbackDocument()
			callbackHousing()
		}

		initFeedback()
		initAction()
	}


	private fun callbackDocument() {
		if (isDocumentCallback) {
			isDocumentCallback = false
			onAddDocumentAdded?.subscribe()

		} else {
			isShowLoadingDialog(false)
		}

	}

	private fun callbackHousing() {
		if (isHouseCallback) {
			isHouseCallback = false
			onAddHouseAdded?.subscribe()
		} else {
			isShowLoadingDialog(false)
		}
	}

	private fun zipRequest() {

		presenter?.getBaseData(onAllRequestsLoaded = {

			initCompanyLogo()
			initCompanyInfo()

			updateVacancy()

			isShowLoadingDialog(false)

		}, onDocumentSuccess = {

			initDocuments(it)
		}, onHousingSuccess = {
			initHousing(it)

		}, onVacancySuccess = {

			listVacancyItems = it

			initVacanciesAdapter()
		}, onWageTypesSuccess = {

		}, onScheduleTypesSuccess = {
			scheduleTypes = it.data

		}, onCurrencySuccess = {

			currencyTypes = it.data

		}, onCompanyPhotosSuccess = {

			initCompanyPhoto(it)
			company_photo_nested


		}, onStatisticSuccess = { countDocument, countHousing, countVacancies ->

			this.countDocument = countDocument.toIntOrNull() ?: 0
			this.countHousing = countHousing.toIntOrNull() ?: 0
			this.countVacancy = countVacancies.toIntOrNull() ?: 0

			initCompanyStatistic(countDocument, countHousing, countVacancies)

		}, onError = {
			showShortToast(getString(noty_team.com.urger.R.string.error_load_data))
		})

	}

	private fun updateVacancy() {
		val list = listVacancyItems.map(scheduleTypes, currencyTypes)

		if (list.isNotEmpty()) {

			try {
				addNewVacancy(list[2])
				addNewVacancy(list[1])
				addNewVacancy(list[0])
			} catch (e: IndexOutOfBoundsException) {
				addNewVacancy(list[0])
			}



			nested_see_more_vacancy.visibility = View.VISIBLE
			//nested_fab_vacancy.hide()
		} else {
			nested_see_more_vacancy.visibility = View.GONE
			nested_fab_vacancy.show()
		}
	}

	private fun initCompanyInfo() {
		//init company name
		nested_global_company_name.text = "in " + PaperIO.companyData?.name
		//initPosition
		if (PaperIO.companyData?.position != null && PaperIO.companyData?.position != "")
			nested_global_company_position.text = " ${PaperIO.companyData?.position}"
		else nested_global_company_position.text = getString(noty_team.com.urger.R.string.some_position)

		//description

		val descriptionData = PaperIO.companyData?.description?.trim() ?: ""
		if (descriptionData != "") {

			nested_company_description.text = PaperIO.companyData?.description
//            nested_company_see_more_container.visibility = View.VISIBLE

		} else {
//            nested_company_see_more_container.visibility = View.GONE
			nested_company_description.text = " "
		}

		//location
		nested_global_company_location.text = PaperIO.companyData?.city


	}

	private fun initCompanyPhoto(response: ArrayList<CompanyPhotoItem>) {

		company_photo_nested.layoutManager =
            LinearLayoutManager(
                baseActivity,
                RecyclerView.HORIZONTAL,
                false
            )

		this.photoAdapter = PhotoAdapter(response,
				onClick = {

					addFragment(CompanyPhotoFragment.newInstance().apply {
						this.index = it
						listImages = photoAdapter?.getItemsList() ?: arrayListOf()
					})

				}, onLongClick = { _it, _index ->
			run {
				deleteDialog(getString(noty_team.com.urger.R.string.delete_company_image_text)) {
					deletePhotoCompany(_it)
				}
			}
		})

		company_photo_nested.adapter = this.photoAdapter

	}

	private fun deletePhotoCompany(companyItem: CompanyPhotoItem) {
		presenter?.apply {
			val photoName = companyItem.image.substring(companyItem.image.indexOfLast { it == '/' } + 1)
			deleteCompanyPhoto(photoName) {
				if (it) {
					showShortToast(resources.getString(noty_team.com.urger.R.string.photo_success_delete))
					CompanyFragment.isNeedUpdateProfile = true
					photoAdapter?.remove(companyItem)
				} else {
					showShortToast(getString(noty_team.com.urger.R.string.error_delete))
				}
			}.callRequest()
		}
	}

	private fun initCompanyLogo() {
		if (PaperIO.companyData?.logo != null && PaperIO.companyData?.logo != "") {
			nested_company_logo_img.visibility = View.VISIBLE
			nested_company_logo_tv.visibility = View.GONE

			setPhoto(nested_company_logo_img, PaperIO.companyData?.logo ?: "")

		} else {
			nested_company_logo_img.visibility = View.GONE
			nested_company_logo_tv.visibility = View.VISIBLE

			nested_company_logo_tv.text = PaperIO.companyData?.name?.substring(0, 1)
		}
	}

	private fun initCompanyStatistic(countDocument: String, countHousing: String, countVacancies: String) {

		company_document_statistic_nested.text = countDocument
		company_vacancy_statistic_nested.text = countVacancies
		company_housing_statistic_nested.text = countHousing
	}

	private fun initVacanciesAdapter() {
		recycler_nested_vacancy.layoutManager =
            LinearLayoutManager(baseActivity)
		vacancyAdapter = VacancyNestedAdapter(arrayListOf(),
				onClick = {
					addFragment(VacancyDetailFragmnet.newInstance(it.vacancyId))
				},
				onLongClick = { _it ->
					showDialog(_it.vacancyId)
				}
		)

		recycler_nested_vacancy.adapter = vacancyAdapter

	}

	private fun showDialog(vacancyId: Int) {
		val vacancyDialog = Dialog(baseActivity)

		vacancyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

		vacancyDialog.setContentView(noty_team.com.urger.R.layout.dialog_employer_vacancy)

		val editButton: Button = vacancyDialog.findViewById(noty_team.com.urger.R.id.edit_vacancy_button)
		val deleteButton: Button = vacancyDialog.findViewById(noty_team.com.urger.R.id.delete_vacancy_button)
		val cancelButton = vacancyDialog.findViewById<Button>(noty_team.com.urger.R.id.cancel_vacancy_button)

		cancelButton.setOnClickListener {
			vacancyDialog.dismiss()
		}

		editButton.setOnClickListener {
			addFragment(AddVacancyFragment.newInstance(
				AddVacancyFragment.ScreenType.EDITING,
				vacancyId
			) { data, isEdit ->
				if (!isEdit) addNewVacancy(data)
				else editVacancy(data)
			})
			vacancyDialog.dismiss()
		}
		deleteButton.setOnClickListener {
			deleteVacancy(vacancyId)
			vacancyDialog.dismiss()
		}

		vacancyDialog.show()
	}

	private fun initHousing(listHousing: ArrayList<HousingItem>) {
		if (listHousing.size == 0) {
			nested_fab_housing.show()
			see_more_housing.visibility = View.GONE
			isShowLoadingDialog(false)
		} else {

			recycler_nested_housing.layoutManager =
                LinearLayoutManager(baseActivity)

			housingAdapter = HousingNestedAdapter(listHousing,
					onClickItem = {
						addFragment(HouseDetailFragment.newInstance().apply {
							houseId = it.houseId
							companyId = PaperIO.companyData!!.companyId
						})

					}, onLongClick = { housing ->
				deleteDialog(getString(noty_team.com.urger.R.string.delete_hosing_item_text)) {
					presenter?.deleteHouse(housing.houseId) {
						if (it) {

							deleteHouse(housing)


						} else {
							showShortToast(getString(noty_team.com.urger.R.string.error_delete))
						}
					}
				}
			})
			recycler_nested_housing.adapter = housingAdapter
			//nested_fab_housing.hide()
			see_more_housing.visibility = View.VISIBLE
			isShowLoadingDialog(false)
		}

	}

	private fun deleteHouse(housing: HousingItem) {
		housingAdapter?.remove(housing)
		HousingFragment.isNeedUpdateHouse = true
		showShortToast(resources.getString(noty_team.com.urger.R.string.success_delete))
		if (this.countHousing > 0) {
			company_housing_statistic_nested.text = (--countHousing).toString()
		}

		requestStatistic()

		if (housingAdapter?.itemCount == 0) {
			nested_fab_housing.show()
			see_more_housing.visibility = View.GONE
		} else {
			requestHousing()
		}
	}

	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				fileData.addAll(mutableListOf(File(uri.path.toString())))

				presenter?.addCompanyPhotos(fileData,
					dataList = {
						CompanyFragment.isNeedUpdateProfile = true
						showShortToast(resources.getString(noty_team.com.urger.R.string.add_photo_company_success))
						photoAdapter?.addAllInStart(it)
						company_photo_nested.smoothScrollToPosition(0)
					})


			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!

				fileData.addAll(mutableListOf(File(uri.path.toString())))

				presenter?.addCompanyPhotos(fileData,
					dataList = {
						CompanyFragment.isNeedUpdateProfile = true
						showShortToast(resources.getString(noty_team.com.urger.R.string.add_photo_company_success))
						photoAdapter?.addAllInStart(it)
						company_photo_nested.smoothScrollToPosition(0)
					})

			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}

	private fun initAction() {

		add_company_photos_nested.getClick {
			showDialogChoose().show()
		}

		nested_fab_vacancy.getClick {
			openAddVacancyFragment()
		}

		nested_fab_housing.getClick {
			openAddHouseFragment()
		}

		nested_fab_document.getClick {
			openAddDocumentsFragment()
		}

		company_detail_btn.getClick {
			addFragment(CompanyProfileInfoFragment.newInstance())
		}

		nested_company_see_more_company.getClick {
			addFragment(CompanyFragment.newInstance(true))
		}

		nested_company_see_more_document.getClick {
			addFragment(DocumentFragment.newInstance(true, DocumentFragment.ParentScreen.KOLBASA))
		}

		nested_company_see_more_vacancy.getClick {
			addFragment(VacancyFragment.newInstance(true, VacancyFragment.ParentScreen.KOLBASA))
		}

		nested_company_see_more_feedback.getClick {
			addFragment(FeedbackFragment.newInstance(true))
		}

		nested_company_see_more_housing.getClick {
			addFragment(
					HousingFragment.newInstance(
							isShowToolbar = true,
							parentType = HousingFragment.ParentScreen.KOLBASA
					)

			)
		}

	}

	var callbackAddDocument: (item: DocumentItem) -> Unit = {
		isDocumentCallback = true
		onAddDocumentAdded = Observable.fromCallable {
			val listDocument = arrayListOf(it)
			initDocuments(listDocument)
		}
		DocumentFragment.isNeedUpdateDocument = true
	}

	var callbackAddHouse: (item: HousingItem) -> Unit = {
		isHouseCallback = true
		onAddHouseAdded = Observable.fromCallable {
			initHousing(arrayListOf(it))
		}

		HousingFragment.isNeedUpdateHouse = true

	}

	private fun initDocuments(list: ArrayList<DocumentItem>) {
		rootView?.let {
			waitUntilViewDrawn(it) {
				if (list.size == 0) {
					nested_fab_document.show()
					document_see_more.visibility = View.GONE
					isShowLoadingDialog(false)
				} else {
					recycler_nested_document.layoutManager =
                        GridLayoutManager(
                            baseActivity,
                            2
                        )
					documentAdapter = DocumentAdapter(list, recycler_nested_document,
							onClickItem = {
								addFragment(FragmentDocumentDetail.newInstance().apply {
									fileName = it.documentName
									addressLink = it.file
								})
							}, onLongClick = { document ->
						deleteDialog(getString(noty_team.com.urger.R.string.delete_document_text)) {
							presenter?.deleteDocument(document.id) {
								if (it) {
									showShortToast(resources.getString(noty_team.com.urger.R.string.document_success_delete))
									deleteDocument(document)
								} else {
									showShortToast(getString(noty_team.com.urger.R.string.error_delete))
								}
							}
						}
					})

					recycler_nested_document.adapter = documentAdapter
					//nested_fab_document.hide()
					document_see_more.visibility = View.VISIBLE
					isShowLoadingDialog(false)
				}
			}
		}

	}

	private fun deleteDocument(document: DocumentItem) {
		documentAdapter?.remove(document)
		if (countDocument > 0) {
			company_document_statistic_nested.text = (--countDocument).toString()
		} else {
			company_document_statistic_nested.text = "0"
		}

		DocumentFragment.isNeedUpdateDocument = true

		if (documentAdapter?.itemCount == 0) {
			nested_fab_document.show()
			document_see_more.visibility = View.GONE
		} else {
			requestDocuments()
		}
	}

	private fun requestDocuments() {
		presenter?.apply {
			getDocuments({
				documentAdapter?.clear()
				documentAdapter?.addAllInStart(it)
				isShowLoadingDialog(false)
			}, {
				showShortToast("requestDocuments " + getString(noty_team.com.urger.R.string.error_load_data))
			}).callRequest()
		}
	}

	private fun requestStatistic() {
		presenter?.apply {
			getStatistic({ countDocument, countHousing, countVacancies ->
				initCompanyStatistic(countDocument, countHousing, countVacancies)
			}, {

			}).callRequest()
		}
	}

	private fun requestHousing() {
		presenter?.apply {
			getHousing({
				housingAdapter?.clear()
				housingAdapter?.addAllInStart(it)
				isShowLoadingDialog(false)
			}, {
				showShortToast(getString(noty_team.com.urger.R.string.error_load_data))
			}).callRequest()
		}
	}

	private fun openAddDocumentsFragment() {
		addFragment(AddDocumentFragment.newInstance().apply {
			callbackAddDocument = this@NestedProfileCompanyFragment.callbackAddDocument
		})
	}

	private fun openAddHouseFragment() {
		addFragment(AddHouseFragment.newInstance().apply {
			callbackAddHouse = this@NestedProfileCompanyFragment.callbackAddHouse
		})
	}

	private fun openAddVacancyFragment() {
		addFragment(AddVacancyFragment.newInstance(AddVacancyFragment.ScreenType.CREATING) { data, isEdit ->
			if (!isEdit) addNewVacancy(data)
			else editVacancy(data)
		})
	}


	private fun addNewVacancy(vacancyItem: VacancyPaginationItem) {
		rootView?.let {
			waitUntilViewDrawn(it) {
//				val scheduleItems = scheduleTypes.filter { it.key == vacancyItem.workSchedule }
//				val schedule: String = if (scheduleItems.isNotEmpty()) scheduleItems[0].value else ""
//				vacancyItem.workSchedule = schedule
				val listVacancy = arrayListOf<VacancyPaginationItem>()
				listVacancy.add(vacancyItem)

				vacancyAdapter?.addAllInStart(listVacancy)
				vacancyAdapter.notifyDataSetChanged()

				nested_see_more_vacancy.visibility = View.VISIBLE
				//nested_fab_vacancy.hide()
			}
		}
	}

	private fun editVacancy(vacancyItem: VacancyPaginationItem) {
		val scheduleItems = scheduleTypes.filter { it.key == vacancyItem.workSchedule }
		val schedule: String = if (scheduleItems.isNotEmpty()) scheduleItems[0].value else ""
		vacancyItem.workSchedule = schedule
		vacancyAdapter?.editItem(vacancyItem)
	}

	private fun deleteVacancy(id: Int) {
		isShowLoadingDialog(true)

		presenter?.deleteVacancy(id, {
			if (vacancyAdapter.deleteVacancy(id)) {
				VacancyFragment.isNeedUpdateVacancy = true
				if (vacancyAdapter?.itemCount == 0) {
					nested_fab_vacancy.show()
					nested_see_more_vacancy.visibility = View.GONE
					isShowLoadingDialog(false)
				} else {
					requestVacancy()
				}

				if (countVacancy > 0) {
					company_vacancy_statistic_nested.text = (--countVacancy).toString()
				} else {
					company_vacancy_statistic_nested.text = "0"
				}
				showShortToast(getString(noty_team.com.urger.R.string.success_delete))
			}
		}, {
			isShowLoadingDialog(false)
		})
	}

	private fun initFeedback() {
		recycler_nested_company_feedback.layoutManager =
            LinearLayoutManager(baseActivity)
		feedbackNestedAdapter = FeedbackAdapter(arrayListOf(), baseActivity)

		recycler_nested_company_feedback.adapter = feedbackNestedAdapter

		getFeedback()
	}

	private fun getFeedback() {

		presenter?.getFeedbackCompany {

			if (!it.data!!.list.isNullOrEmpty()) {
				last_feedback_tv.visibility = View.VISIBLE
				feedbackNestedAdapter.setData(ArrayList(it.data.list.sortedWith(compareByDescending(
					ListItem::employerRate))))
			}

			ratingBar_nested_company.rating = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toFloat()
			count_ratings_nested_company.text = "${it.data.total.count.toString()} review"

			rating_tv_nested_company.text = (Math.round(it.data.total!!.rate!! * 10) / 10.0).toString()
		}
	}

	private fun requestVacancy() {
		presenter?.apply {
			getVacancyes({
				vacancyAdapter?.clear()

				listVacancyItems = it
				updateVacancy()
				isShowLoadingDialog(false)
			}, {
				showShortToast(getString(noty_team.com.urger.R.string.error_load_data))
			}).callRequest()
		}
	}
}