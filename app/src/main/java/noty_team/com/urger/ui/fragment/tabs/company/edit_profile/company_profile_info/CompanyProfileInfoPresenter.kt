package noty_team.com.urger.ui.fragment.tabs.company.edit_profile.company_profile_info

import android.content.Context
import android.util.Log
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.request.edit_company_position.EditCompanyPositionRequest
import noty_team.com.urger.utils.api.request.update_validate_company.UpdateValidateCompanyRequest
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class CompanyProfileInfoPresenter(context: Context) : BasePresenter(context) {

	lateinit var isSuccess: (isSuccess: Boolean) -> Unit

	fun serverRequest(description: String, userPosition: String, isSuccess: (isSuccess: Boolean) -> Unit) {
		this.isSuccess = isSuccess

		apiEmployer.updateValidateCompany(PaperIO.companyData?.companyId ?: -1,
				UpdateValidateCompanyRequest(description), {

			it.updateDescription(userPosition)


			editUserPosition(userPosition)

		}, { errorMessage, errorCode ->
			showShortToast(errorMessage)
		}).callRequest()
	}

	private fun editUserPosition(userPosition: String) {
		apiEmployer.editCompanyPosition(PaperIO.employerData?.userId
				?: -1, EditCompanyPositionRequest(userPosition), {
			it.parsePosition()
			isSuccess(true)

		}, { errorMessage, errorCode ->
			showShortToast(errorMessage)
		}).callRequest()
	}

	fun uploadCompanyLogo(file: File, isSuccess: (isSuccess: Boolean) -> Unit) {
		val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
		val body = MultipartBody.Part.createFormData("image", file.name, requestFile)

		apiEmployer.uploadCompanyLogo(PaperIO.companyData?.companyId ?: -1, body, {

			it.parseCompanyLogo()
			isSuccess(true)
			showShortToast(context.getString(R.string.company_logo_update))
		}, { errorMessage, errorCode ->
			showShortToast(errorMessage)
		}).callRequest()
	}


}