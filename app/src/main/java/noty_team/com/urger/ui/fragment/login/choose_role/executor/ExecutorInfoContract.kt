package noty_team.com.urger.ui.fragment.login.choose_role.executor

import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.api.request.create_executor.CreateExecutorRequest
import noty_team.com.urger.utils.api.response.get_city.CityResponse

interface ExecutorInfoContract {

    interface Presenter {
        fun createExecutorRequest(
            createExecutorRequest: CreateExecutorRequest,
            openAppCallback: (isOpen: Boolean) -> Unit
        )

        fun getCategory(lang: String, onDataLoad: (cities: CityResponse) -> Unit)
    }
}