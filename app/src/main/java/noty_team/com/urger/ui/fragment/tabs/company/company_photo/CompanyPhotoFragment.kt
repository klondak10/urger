package noty_team.com.urger.ui.fragment.tabs.company.company_photo

import android.view.View
import kotlinx.android.synthetic.main.fragment_company_photos.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.adapters.ImageViewPager
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.waitUntilViewDrawn


class CompanyPhotoFragment : BaseFragment<CompanyPhotoPresenter>() {
	companion object {
		fun newInstance() = CompanyPhotoFragment()
	}

	var index: Int = 0
	var isLoadFromGallery = false
	var listImages = arrayListOf<CompanyPhotoItem>()

	override fun layout() = R.layout.fragment_company_photos

	override fun initialization(view: View, isFirstInit: Boolean) {
		bottomBarIsVisible(false)

		presenter = CompanyPhotoPresenter(baseContext)

		waitUntilViewDrawn(salon_image_view_pager) {
			salon_image_view_pager.adapter = ImageViewPager(baseContext, listImages, isLoadFromGallery)
			salon_image_view_pager.currentItem = index
		}

		initAction()
	}

	private fun initAction() {
		navigate_back_photo.getClick {
			onBackPressed()
		}
	}

	override fun onResume() {
		super.onResume()
		initAction()
	}
}