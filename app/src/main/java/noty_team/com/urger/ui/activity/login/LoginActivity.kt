package noty_team.com.urger.ui.activity.login

import android.content.Intent
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.ui.fragment.login.choose_role.ChooseRoleFragment
import noty_team.com.urger.ui.fragment.login.choose_role.company.CompanyInfoFragment
import noty_team.com.urger.ui.fragment.login.phone.validation_phone.ValidatePhoneFragment
import noty_team.com.urger.utils.api.response.sms_checked.Authorization

class LoginActivity : BaseActivity() {
	companion object {
		fun start(baseActivity: BaseActivity) {
			baseActivity.startActivity(
					Intent(
							baseActivity,
							LoginActivity::class.java
					).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
			)
		}
	}

	override fun layout() = R.layout.activity_login

	override fun initialization() {
		//replaceFragment(CompanyInfoFragment.newInstance())
		when (PaperIO.signUp) {
			Authorization.CHOOSE_ROLE -> {
				replaceFragment(ChooseRoleFragment.newInstance())
			}
			else -> {

				replaceFragment(ValidatePhoneFragment.newInstance())
			}
		}

	}

	override fun getDrawerLayout() = null
	override fun getNavigationView() = null
	override fun getBottomNavigationView() = null
	override fun openDrawerLayout() {}

}