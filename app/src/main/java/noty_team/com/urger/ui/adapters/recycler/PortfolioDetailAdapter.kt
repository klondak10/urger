package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView


import kotlinx.android.synthetic.main.item_portfolio_image.view.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.setPhoto

class PortfolioDetailAdapter(val listImage: Array<String>, val context: Context,
                             val onChangePhoto: (id: Int) -> Unit = {}):
        RecyclerView.Adapter<PortfolioDetailAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(
            R.layout.item_portfolio_image, parent, false))
    }

    override fun getItemCount(): Int {
        return listImage.size
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {

        val item = listImage[p1]
        if(item.length > 1) {
            setPhoto(holder.portfolio_image, "http://${item}")
        } else {
            holder.portfolio_image.setImageResource(R.drawable.no_exist_photo)
        }
        holder.portfolio_image.setOnClickListener {
                onChangePhoto(p1)

        }
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var portfolio_image = view.portfolio_image as ImageView
    }
}