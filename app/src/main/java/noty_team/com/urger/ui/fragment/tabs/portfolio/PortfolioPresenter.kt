package noty_team.com.urger.ui.fragment.tabs.portfolio

import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.response.portfolio.DataItem

class PortfolioPresenter(context: Context): BasePresenter(context) {

    fun getPortfolioPagination(listSize: Int,
                               onPortfolioSuccess: (portfolio: ArrayList<DataItem>) -> Unit) {

        val page = listSize / Constants.documentItemLimit

        apiEmployer.getPortfolioPagination(
            Constants.documentItemLimit, page, {
                onPortfolioSuccess(it.data!!)

            }, { errorMessage, errorCode ->
                showShortToast("$errorCode $errorMessage")
            }).callRequest()

    }

    fun deletePortfolioExecutor(id: Int, isSuccessDelete: (b: Boolean) -> Unit) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.deletePortfolioExecutor(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        isSuccessDelete(true)
                    } else {
                        isSuccessDelete(false)
                        showShortToast(it.error!!)
                    }
                }, {

                })
        )
    }
}