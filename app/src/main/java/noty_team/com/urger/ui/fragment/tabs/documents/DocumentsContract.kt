package noty_team.com.urger.ui.fragment.tabs.documents

import noty_team.com.urger.utils.adapters.document.DocumentItem

interface DocumentsContract {
	interface View
	interface Presenter {
		fun getDocumentsPagination(listSize: Int, onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit)
		fun deleteDocument(documentId: Int, isDeleteDocument: (isSuccess: Boolean) -> Unit)

	}
}