package noty_team.com.urger.ui.fragment.tabs.experience.add_experience

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.car.AddCarExecutorResponse
import noty_team.com.urger.utils.api.response.experience_executor.AddExperienceExecutorResponse
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.bitmapToFile
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddExperiencePresenter(context: Context) : BasePresenter(context){

    lateinit var bitmap: File

    fun isBitmapInitialize(): Boolean {
        return ::bitmap.isInitialized
    }

    fun getBitmapFromFile(file: FileData, onSuccess: (Bitmap) -> Unit) {
        var filePath = file.file.path
        val b = BitmapFactory.decodeFile(filePath)
        bitmap = bitmapToFile(context, b)
        onSuccess(b)
    }

    fun getBitmapFromFileData(file: File, onSuccess: (Bitmap) -> Unit) {
        var filePath = file.path
        val b = BitmapFactory.decodeFile(filePath)
        bitmap = bitmapToFile(context, b)
        onSuccess(b)
    }

    fun addExperienceExecutor(name: String,
                              city: String,
                              position: String,
                              period: String,
                              isAddExperienceCallback: (isSuccess: AddExperienceExecutorResponse) -> Unit) {
        val filePart = MultipartBody.Part.createFormData(
                "company_logo", bitmap.name,
                RequestBody.create("image/png".toMediaTypeOrNull(), bitmap))

        apiEmployer.addExperienceExecutor(RequestBody.create("multipart/form-data".toMediaTypeOrNull(), name), city, RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), position), period, filePart, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                isAddExperienceCallback(it)
            }else {
                showShortToast(it.error!!)
                AddExperienceFragment().isAdd = false
            }
        }, { errorMessage, errorCode ->
            showShortToast(context.getString(R.string.error_add_experience))
            AddExperienceFragment().isAdd = false
        }).callRequest()
    }

    fun getCategory(onDataLoad: (cities: CityResponse) -> Unit) {
        val lang = when(context.resources.configuration.locale.toString()) {
            "pl" -> "pl" //польша
            "de" -> "ge" //германия
            "en" -> "en" // англ
            "ru" -> "ru"
            "uk" -> "ru"
            "be" -> "ru" // ,tkfhecm
            else -> "pl"
        }
        apiAuthorization.getCategory(lang, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onDataLoad(it)
            }
        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }
}