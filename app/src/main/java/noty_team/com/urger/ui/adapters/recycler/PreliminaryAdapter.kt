package noty_team.com.urger.ui.adapters.recycler

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import kotlinx.android.synthetic.main.item_preliminary.preliminary_price
import kotlinx.android.synthetic.main.item_preliminary.preliminary_rating
import kotlinx.android.synthetic.main.item_preliminary.preliminary_review
import kotlinx.android.synthetic.main.item_preliminary.preliminary_user_logo
import kotlinx.android.synthetic.main.item_preliminary.preliminary_user_name
import kotlinx.android.synthetic.main.item_preliminary_2.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.DataItem
import noty_team.com.urger.utils.setPhoto

class PreliminaryAdapter(items: ArrayList<DataItem>,
                         recyclerView: RecyclerView,
                         isPreliminary: Boolean,
                         var onLoadData: (offset: Int) -> Unit = {},
                         var onCancel:(item: DataItem) -> Unit = {},
                         var onClick: (data: DataItem) -> Unit = {}) :
BaseAdapterPagination<DataItem>(items, recyclerView)  {

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            itemView.setOnClickListener {
                onClick(list[position])
            }
            cancel_button?.setOnClickListener{
                onCancel(list[position])
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            if(item.user?.avatar != null) {
                setPhoto((preliminary_user_logo as ImageView), item.user?.avatar!!)
            }
            image_exist_car?.visibility = if(item.auto!!) View.VISIBLE else View.GONE
            preliminary_user_name.text = "${item.user!!.firstName} ${item.user!!.lastName}"
            preliminary_review.text = "${item.rate!!.count.toString()} ${itemView.context.getString(R.string.reviewText)}"
            preliminary_rating.rating = item.rate!!.rate ?: 0.0F
            preliminary_price.text = "${item.vacancy!!.wage} ${item.vacancy!!.wageCurrency}"
        }
    }

    override val itemLayoutResourceId =  if(isPreliminary)R.layout.item_preliminary_2 else R.layout.item_preliminary

}