package noty_team.com.urger.ui.fragment.tabs.vehicle.add_vehicle

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.car.AddCarExecutorResponse
import noty_team.com.urger.utils.bitmapToFile
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AddVehiclePresenter(context: Context): BasePresenter(context) {

    lateinit var bitmap: File


    fun isBitmapInitialize(): Boolean {
        return ::bitmap.isInitialized
    }

    fun getBitmapFromFile(file: FileData, onSuccess: (Bitmap) -> Unit) {
        var filePath = file.file.path
        val b = BitmapFactory.decodeFile(filePath)
        bitmap = bitmapToFile(context, b)
        onSuccess(b)
    }

    fun getBitmapFromFileData(file: File, onSuccess: (Bitmap) -> Unit) {
        var filePath = file.path
        val b = BitmapFactory.decodeFile(filePath)
        bitmap = bitmapToFile(context, b)
        onSuccess(b)
    }


    fun getVehicleList(onDataLoad:(list: ArrayList<String>) -> Unit) {
        apiEmployer.getModelCar({
            if(!it.results.isNullOrEmpty()) {
                onDataLoad(it.map())
            } else {
                showShortToast(it.message.toString())
                onDataLoad(arrayListOf())
            }

        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
            onDataLoad(arrayListOf())
        }).callRequest()
    }


    fun addExecutorCar(
        model: String,
        year: String,
        color: String,
        fuel: String,
        isSetCarCallback: (isSuccess: AddCarExecutorResponse) -> Unit,
        showToast: (toastMassage: String) -> Unit
    ) {

        val filePart = MultipartBody.Part.createFormData(
            "file", bitmap.name,
            RequestBody.create("image".toMediaTypeOrNull(), bitmap)
        )

        apiEmployer.addCarExecutor(RequestBody.create("multipart/form-data".toMediaTypeOrNull(), model), RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), year), filePart, color, RequestBody.create("multipart/form-data".toMediaTypeOrNull(), fuel), {
            if (it.statusCode == 200 || it.statusCode == 201) {
                isSetCarCallback(it)

            } else {
                isSetCarCallback(AddCarExecutorResponse())
                showToast(context.getString(R.string.error_load_car))
                AddVehicleFragment().isAdd = false
            }
        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
            AddVehicleFragment().isAdd = false
            isSetCarCallback(AddCarExecutorResponse())
        }).callRequest()
    }
}