package noty_team.com.urger.ui.fragment.tabs.portfolio.partfolio_detail

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_portfolio_detail.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.PortfolioDetailAdapter
import noty_team.com.urger.ui.fragment.tabs.portfolio.add_portfolio.AddPortfolioFragment
import noty_team.com.urger.utils.api.response.portfolio.DataItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.setPhoto

class PortfolioDetailFragment : BaseFragment<PortfolioDetailPresenter>() {

    lateinit var adapterDetailPortfolio: PortfolioDetailAdapter

    companion object {
        fun newInstance(dataItem: DataItem): PortfolioDetailFragment {
            return PortfolioDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("data", dataItem)
                }
            }
        }
    }

    override fun layout() = R.layout.fragment_portfolio_detail

    override fun initialization(view: View, isFirstInit: Boolean) {
        isShowLoadingDialog(true)
        val obj = requireArguments()["data"] as DataItem
        description_portfolio.text = obj.description!!.replace("\\n", " ")
        name_portfolio.text = obj.name
        val listImage = obj.file!!.split(",").toTypedArray()
        setPhoto(main_image_portfolio, "http://${listImage[0]}")

        adapterDetailPortfolio = PortfolioDetailAdapter(listImage , baseContext,
            onChangePhoto = {

                if(adapterDetailPortfolio.itemCount > 1) {
                    setPhoto(main_image_portfolio, "http://${listImage[it]}")
                }
            }
        )
        val layoutManager = LinearLayoutManager(
            baseContext,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recycler_view_portfolio_image.layoutManager = layoutManager
        recycler_view_portfolio_image.adapter = adapterDetailPortfolio

//        if (PaperIO.userRole == UserRole.COMPANY) {
//            edit_portfolio_detail.visibility = View.GONE
//        } else {
//            edit_portfolio_detail.visibility = View.VISIBLE
//        }


//        edit_portfolio_detail.setOnClickListener {
//            //addFragment(AddPortfolioFragment.newInstance())
//            //navigator().navigateToFragment(AddPortfolioFragment.newInstance(), null, true)
//        }

        nb_portfolio_detail.setOnClickListener {
            onBackPressed()
            //backNavigator().navigateBack()
        }
        isShowLoadingDialog(false)
    }
}