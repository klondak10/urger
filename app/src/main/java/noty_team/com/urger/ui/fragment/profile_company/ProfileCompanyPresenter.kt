package noty_team.com.urger.ui.fragment.profile_company

import android.content.Context
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.response.upload_company_logo.UploadCompanyLogoResponse
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ProfileCompanyPresenter(context: Context) : BasePresenter(context), ProfileCompanyContract.Presenter {

    override fun updateAvatar(
        file: File,
        isSetPhotoCallback: (isSuccess: Boolean) -> Unit,
        showToast: (toastMassage: String) -> Unit
    ) {
        val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)

        subscriptions.add(
            UrgerApi.USER_PROFILE_API.updateUserPersonalAvatar(
                PaperIO.employerData?.userId
                    ?: -1, body
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        if (it.data?.image != null) {
                            it.parseImageUrl()
                            isSetPhotoCallback(true)
                        }
                    } else {
                        showToast(context.getString(R.string.error_load_image))
                    }
                }, {
                    showToast(Constants.DISPOSABLE_ERROR)
                })
        )
    }

    override fun isCanEditProfile(isCanEdit: (isCanEdit: Boolean) -> Unit) {
        subscriptions.add(UrgerApi.USER_PROFILE_API.isCanEditPersonalInfo().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                when {
                    it.data?.canUpdate == true -> isCanEdit(true)
                    it.data?.canUpdate == false -> isCanEdit(false)
                    else -> showShortToast(it.error)
                }
            }, {
                showShortToast(Constants.DISPOSABLE_ERROR)
            }
            )
        )
    }

    fun getAvatar(isSetPhotoCallback: (isSuccess: Boolean) -> Unit,
                  showToast: (toastMassage: String) -> Unit) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.getUserPersonalAvatar(PaperIO.executorData?.userId ?: -1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        if (it.data?.image != null) {
                            it.parseImageUrl()
                            isSetPhotoCallback(true)
                        }
                    } else {
                        showToast(context.getString(R.string.error_load_image))
                    }
                }, {
                    showToast(Constants.DISPOSABLE_ERROR)
                })
        )
    }

    override fun updateBackground(
        file: File,
        isSetPhotoCallback: (isSuccess: UploadCompanyLogoResponse) -> Unit,
        showToast: (toastMassage: String) -> Unit
    ) {
        val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.uploadCompanyLogo(PaperIO.companyData?.companyId ?: -1, body).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        it.parseCompanyLogo()
                    } else {
                        showToast(context.getString(R.string.error_load_image))
                    }
                }, {
                    showToast(Constants.DISPOSABLE_ERROR)
                })
        )
    }
}