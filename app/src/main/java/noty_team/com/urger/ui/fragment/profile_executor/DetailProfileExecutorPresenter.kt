package noty_team.com.urger.ui.fragment.profile_executor

import android.content.Context
import android.provider.SyncStateContract
import android.util.Log
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.all_info.AllInfoExecutorResponse
import noty_team.com.urger.utils.api.response.all_info.DocumentsItem
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.get_document.GetDocumentsPaginationResponse

class DetailProfileExecutorPresenter(context: Context): BasePresenter(context){

    fun getInfo(id: Int, isLoadData:(AllInfoExecutorResponse) -> Unit) {
        apiEmployer.getAllInfoExecutor(id, {
            if(it.statusCode == 200) {
                isLoadData(it)
            }
        }, {errorMessage, errorCode ->
            //showShortToast(errorMessage)
        }).callRequest()
    }

    fun getDocumentsCompanys(companyId: Int, isLoadData: (ArrayList<DocumentItem>) -> Unit) {
        apiEmployer.getDocumentPagination(companyId, 20, 0, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                isLoadData(it.maps())
            }else {
                showShortToast("getDocumentsCompany ${it.error}")
            }

        }, {errorMessage, errorCode ->
            showShortToast("getDocumentsCompany" + errorMessage)
        }).callRequest()
    }


}