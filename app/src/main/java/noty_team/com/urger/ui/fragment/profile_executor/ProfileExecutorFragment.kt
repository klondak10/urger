package noty_team.com.urger.ui.fragment.profile_executor

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import com.google.android.material.tabs.TabLayout
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.github.drjacky.imagepicker.ImagePicker
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import kotlinx.android.synthetic.main.fragment_profile_executor.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.view_pager.ExecutorPageAdapter
import noty_team.com.urger.ui.fragment.profile_executor.nested_executor.NestedProfileExecutorFragment
import noty_team.com.urger.ui.fragment.tabs.company.edit_profile.edit_personal_data.EditPersonalDataFragment
import noty_team.com.urger.ui.fragment.tabs.documents.DocumentFragment
import noty_team.com.urger.ui.fragment.tabs.documents.add_document.AddDocumentFragment
import noty_team.com.urger.ui.fragment.tabs.experience.ExperienceFragment
import noty_team.com.urger.ui.fragment.tabs.experience.add_experience.AddExperienceFragment
import noty_team.com.urger.ui.fragment.tabs.feedback.FeedbackFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.PortfolioFragment
import noty_team.com.urger.ui.fragment.tabs.portfolio.add_portfolio.AddPortfolioFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.VacancyFragment
import noty_team.com.urger.ui.fragment.tabs.vehicle.VehicleFragment
import noty_team.com.urger.ui.fragment.tabs.vehicle.add_vehicle.AddVehicleFragment
import noty_team.com.urger.utils.ControllableAppBarLayout
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.camera.FilesPicker
import noty_team.com.urger.utils.setPhoto
import java.io.File
import java.text.DateFormatSymbols


class ProfileExecutorFragment : BaseFragment<ProfileExecutorPresenter>() {

	lateinit var toolbar: Toolbar
	lateinit var drawerButton: View
    var image = ""

	companion object {
		fun newInstance() = ProfileExecutorFragment()
		var isNestedExecutor = false
	}

	override fun layout() = R.layout.fragment_profile_executor

	override fun initialization(view: View, isFirstInit: Boolean) {

		presenter = ProfileExecutorPresenter(baseContext)

		toolbar = executor_toolbar_main
		drawerButton = executor_drawer_button

		if (isFirstInit) {
			replaceNested(isNestedExecutor)

		}

		appBarOffset()
		bottomBarIsVisible(true)

		nested_replace_executor.setOnClickListener {
			isNestedExecutor = !isNestedExecutor
			replaceNested(isNestedExecutor)
		}


		drawerButton.setOnClickListener {
			baseActivity.openDrawerLayout()
		}

		avatar.setOnClickListener {
			image = "avatar"
			showDialogChoose().show()
		}

		fragment_profile_executor_background.setOnClickListener {
			image = "background"
			showDialogChoose().show()
		}
		initUserInfo()

	}

	private fun showDialogChoose(): Dialog {
		val dialog = Dialog(requireActivity())
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
		dialog.setCancelable(true)
		dialog.setContentView(R.layout.dialog_choose)

		val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
		val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
		val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

		ivCross.setOnClickListener {
			dialog.dismiss()
		}
		btnGallery.setOnClickListener {
			dialog.dismiss()
			pickGalleryImage()
		}
		btnCamera.setOnClickListener {
			dialog.dismiss()
			pickCameraImage()
		}
		return dialog
	}

	private val cameraLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				if (image == "avatar"){
					presenter?.updateAvatar(File(uri.path.toString()), {
						if (it) setAvatar()
					}, {
						showLongToast(it)
					})
				}else{
					presenter?.updateBackground(File(uri.path.toString()), {
						if (it) {
							setUserBackground()
						}
					}, {
						showLongToast(it)
					})
				}

			} else parseError(it)
		}

	private fun pickCameraImage() {
		cameraLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.cameraOnly()
				.maxResultSize(720, 1280, true)
				.createIntent()
		)
	}

	private fun pickGalleryImage() {
		galleryLauncher.launch(
			ImagePicker.with(requireActivity())
				.crop()
				.galleryOnly()
				.cropFreeStyle()
				.maxResultSize(720, 1280, true)
				.galleryMimeTypes( // no gif images at all
					mimeTypes = arrayOf(
						"image/png",
						"image/jpg",
						"image/jpeg"
					)
				)
				.createIntent()
		)
	}

	private val galleryLauncher =
		registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
			if (it.resultCode == Activity.RESULT_OK) {
				val uri = it.data?.data!!
				if (image == "avatar"){
					presenter?.updateAvatar(File(uri.path.toString()), {
						if (it) setAvatar()
					}, {
						showLongToast(it)
					})
				}else{
					presenter?.updateBackground(File(uri.path.toString()), {
						if (it) {
							setUserBackground()
						}
					}, {
						showLongToast(it)
					})
				}

			} else parseError(it)
		}


	private fun parseError(activityResult: ActivityResult) {
		if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
			Toast.makeText(
				requireContext(),
				ImagePicker.getError(activityResult.data),
				Toast.LENGTH_SHORT
			)
				.show()
		}
	}


	private fun initUserInfo() {
		setAvatar()
		setUserBackground()
		setUserPersonalInfo()

	}

	private fun setUserPersonalInfo() {

		user_name_executor.text = "${PaperIO.executorData?.firstName}  ${PaperIO.executorData?.lastName}"
		val b = PaperIO.executorData?.birthday
		executor_birthday.text =
			"${b?.substringAfterLast("-")} ${getMonthForInt(b!!.substring(b.indexOf("-") + 1, b.lastIndexOf("-")).toInt())} ${b.substringBefore("-")}"
		profile_executor_user_rating.rating = PaperIO.executorData!!.rate ?: 0.0F
		drawerListener.onDrawerUserInfoChanged()

	}

	private fun getMonthForInt(num: Int): String {
		var month = "wrong"
		val dfs = DateFormatSymbols()
		val months = dfs.months
		if (num in 0..11) {
			month = months[num]
		}
		return month
	}

	private fun initToolbar() {

	}


	@SuppressLint("RestrictedApi")
	private fun initTab() {

		if (executor_view_pager.adapter != null) {
			(executor_view_pager.adapter as ExecutorPageAdapter).destroyAllFragment()
		}

		executor_view_pager.adapter = ExecutorPageAdapter(
				childFragmentManager, executor_view_pager, arrayListOf(
				DocumentFragment(),
				ExperienceFragment(),
				FeedbackFragment(),
				PortfolioFragment(),
				VacancyFragment(),
				VehicleFragment()

		)
		)
		executor_main_tabLayout.setupWithViewPager(executor_view_pager)


		if (PaperIO.userRole == UserRole.COMPANY) {
			executor_fab_main.visibility = View.GONE
			edit_personal_data_executor.visibility = View.GONE

//			profile_executor_add_friend_btn.visibility = View.VISIBLE
//			profile_executor_send_massage_btn.visibility = View.VISIBLE


		} else {
//			profile_executor_add_friend_btn.visibility = View.GONE
//			profile_executor_send_massage_btn.visibility = View.GONE

			edit_personal_data_executor.setOnClickListener {

				addFragment(EditPersonalDataFragment.newInstance())
				// navigator().navigateToFragment(EditPersonalDataFragment.newInstance(), null, true)
			}

			edit_personal_data_executor.visibility = View.VISIBLE

			executor_fab_main.setOnClickListener {
				when (executor_main_tabLayout.selectedTabPosition) {
					0 -> {
						addFragment(AddDocumentFragment.newInstance())
						//navigator().navigateToFragment(AddDocumentFragment.newInstance(), null, true)
					}
					1 -> {
						addFragment(AddExperienceFragment.newInstance())
						//navigator().navigateToFragment(AddExperienceFragment.newInstance(), null, true)
					}
					3 -> {
						addFragment(AddPortfolioFragment.newInstance())
						//navigator().navigateToFragment(AddPortfolioFragment.newInstance(), null, true)
					}
					5 -> {
						addFragment(AddVehicleFragment.newInstance())
						//navigator().navigateToFragment(AddVehicleFragment.newInstance(), null, true)
					}
				}
			}
		}

		executor_main_tabLayout.addOnTabSelectedListener(object :
				TabLayout.OnTabSelectedListener {
			override fun onTabSelected(tab: TabLayout.Tab) {

				changeToolbarText(tab.position, executor_app_bar_layout.state)
				if (PaperIO.userRole != UserRole.COMPANY)
					when (tab.position) {

						0 -> {
							executor_fab_main.show()
						}
						1 -> {
							executor_fab_main.show()
						}
						3 -> {
							executor_fab_main.show()
						}
						5 -> {
							executor_fab_main.show()
						}
						else -> {
							executor_fab_main.hide()
						}
					}
				else executor_fab_main.hide()
			}

			override fun onTabUnselected(tab: TabLayout.Tab) {}
			override fun onTabReselected(tab: TabLayout.Tab) {}
		})
	}

	private fun appBarOffset() {
		executor_app_bar_layout.setOnStateChangeListener(object : ControllableAppBarLayout.OnStateChangeListener {
			override fun onStateChange(toolbarChange: ControllableAppBarLayout.State) {
				when (toolbarChange) {
					ControllableAppBarLayout.State.COLLAPSED -> {
						if (isNestedExecutor) {

							executor_toolbar_text.text = user_name_executor.text

						} else changeToolbarText(executor_main_tabLayout.selectedTabPosition, toolbarChange)
					}
					ControllableAppBarLayout.State.EXPANDED -> {
						executor_toolbar_text.text = " "
					}
					ControllableAppBarLayout.State.IDLE -> {
					}
				}
			}
		})
	}

	private fun changeToolbarText(position: Int, state: ControllableAppBarLayout.State?) {
		if (state != ControllableAppBarLayout.State.EXPANDED)
			executor_toolbar_text.text = "${PaperIO.executorData!!.firstName} ${PaperIO.executorData!!.lastName}"
	}

	private fun replaceNested(isNested: Boolean) {
		if (isNested) {
			initNestedTab()
			executor_main_tab_container.visibility = View.GONE
			nested_executor_container.visibility = View.VISIBLE
			executor_view_pager.adapter = null
			executor_fab_main.hide()
			nested_replace_executor.setImageResource(R.mipmap.red_nested_icon)
			initNestedTab()

		} else {
			initTab()
			nested_executor_container.visibility = View.GONE
			executor_main_tab_container.visibility = View.VISIBLE
			nested_replace_executor.setImageResource(R.mipmap.list)
			initToolbar()
			initTab()
		}
	}

	private fun initNestedTab() {
		if (executor_view_pager.adapter != null) {
			(executor_view_pager.adapter as ExecutorPageAdapter).destroyAllFragment()
		}

		executor_view_pager.adapter = ExecutorPageAdapter(
				childFragmentManager, executor_view_pager, arrayListOf(
				NestedProfileExecutorFragment.newInstance()
		)
		)

		executor_view_pager.adapter?.notifyDataSetChanged()

	}

	private fun setAvatar() {

		if (PaperIO.executorData?.avatar != "" && PaperIO.executorData?.avatar != null) {
			executor_avatar.visibility = View.VISIBLE
			executor_avatar_tv.visibility = View.GONE
			setPhoto(executor_avatar, PaperIO.executorData?.avatar ?: "")

		}else{
			executor_avatar.visibility = View.GONE
			executor_avatar_tv.visibility = View.VISIBLE

			val avatarLogoText =
				PaperIO.executorData?.firstName?.substring(0, 1) + PaperIO.executorData?.lastName?.substring(0, 1)
			executor_avatar_tv.text = avatarLogoText
		}
		drawerListener.onDrawerAvatarChanged()
	}

	private fun setUserBackground() {
		if (PaperIO.executorData?.background != "" && PaperIO.executorData?.background != null) {
			profile_executor_background_img.visibility = View.VISIBLE

			setPhoto(profile_executor_background_img, PaperIO.executorData?.background ?: "")

		} else {
			profile_executor_background_img.visibility = View.INVISIBLE

		}
		drawerListener.onDrawerBackgroundChanged()
	}
}