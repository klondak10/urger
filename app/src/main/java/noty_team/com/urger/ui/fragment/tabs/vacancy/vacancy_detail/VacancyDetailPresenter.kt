package noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail

import android.content.Context
import android.util.Log
import io.paperdb.Paper
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.get_house.GetHouseResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItem
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.api.response.vacancy.requests.GetRequestsVacancyResponse

class VacancyDetailPresenter(context: Context) : BasePresenter(context), VacancyDetailContract.Presenter {

    override fun getVacancy(vacancyId: Int,
                            typeVacancy: TypeVacancy,
                            onDataLoad: (vacancyItem: VacancyResponse) -> Unit,
                            onError: () -> Unit) {
        if(typeVacancy == TypeVacancy.FAVORITE) {
            apiEmployer.getFavoriteVacancy(vacancyId, {
                if (it.statusCode == 200 || it.statusCode == 201) {

                    onDataLoad(it)
                } else {
                    showLongToast(it.error)
                }
            }, {errorMessage, errorCode ->
                showLongToast(Constants.DISPOSABLE_ERROR)
            }).callRequest()
        } else {
            if (PaperIO.userRole == UserRole.COMPANY) {
                apiEmployer.getVacancy(vacancyId, {

                    if (it.statusCode == 200 || it.statusCode == 201) {
                        onDataLoad(it)
                    } else {
                        onError()
                        showLongToast(it.error)
                    }

                }, { errorMessage, errorCode ->
                    onError()
                    showLongToast(errorMessage)

                }).callRequest()
            } else {

                var item = VacancyResponse()
                apiEmployer.getVacancyForExecutor(vacancyId, {
                    val obj = it.data
                    val response = VacancyItem(obj.id!!.toInt(), obj.workSchedule, obj.shortDescription, obj.requirements,
                        obj.weOffer,
                        obj.houseId?.toInt(),
                        obj.address,
                        obj.validatedCompanyId?.toInt(),
                        obj.lng,
                        obj.addressPlaceId,
                        obj.wageCurrency,
                        obj.numberOfEmployees!!.toInt(),
                        obj.fullDescription,
                        obj.responsibilities,
                        obj.categoryId!!.toInt(),
                        obj.additionally,
                        obj.position,
                        obj.lat,
                        obj.wage!!.toInt(),
                        obj.wageType,
                        obj.category_name,
                        obj.parent_category,
                        obj.created_at,
                        obj.city_id,
                        obj.company_logo,
                        obj.company_name,
                        obj.company_about)
                    item.data = response

                    if (it.statusCode == 200 || it.statusCode == 201) {
                        onDataLoad(item)
                        Paper.book().write("user_id_employer",it.data.user_id)
                    } else{
                        onError()
                        showLongToast(it.error+it.statusMassage)
                    }
                }, { errorMessage, errorCode ->
                    onError()
                    showLongToast(Constants.DISPOSABLE_ERROR)
                }).callRequest()
            }
        }
    }

    fun participateVacancy(vacancyId: Int, onSuccessParticipate:(isSuccess: Boolean) ->Unit) {
        apiEmployer.participateVacancy(vacancyId, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onSuccessParticipate(true)
            }else if(it.statusCode == 500) {
                onSuccessParticipate(true)
            } else if(it.statusCode == 400) {
                showShortToast(context.getString(R.string.participate_error))
                onSuccessParticipate(false)
            }
        }, {errorMessage, errorCode ->
            //showLongToast(errorMessage)
            onSuccessParticipate(true)
            //onSuccessParticipate(false)
        }).callRequest()

    }

    fun getContractPagination(validated_company_id:Int,listSize: Int,
                              onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit) {
        val page = listSize / Constants.documentItemLimit

        apiEmployer.getDocumentPagination(validated_company_id, Constants.documentItemLimit, page, {

            onDocumentsSuccess(it.mapPre())

        }, { errorMessage, errorCode ->

        }).callRequest()

    }

    fun getRequestVacancy(vacancyId: Int, onDataLoad: (vacancyItem: GetRequestsVacancyResponse) -> Unit) {
        apiEmployer.getRequestsVacancy(vacancyId, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                onDataLoad(it)
            } else{
                showLongToast(it.error!!)
            }
        }, {errorMessage, errorCode ->
            showLongToast("getRequestVacancy" + errorMessage)
        }).callRequest()
    }

    fun getFavoriteVacancy(id: Int, onDataLoad: (vacancyItem: VacancyResponse) -> Unit) {
        apiEmployer.getFavoriteVacancy(id, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                onDataLoad(it)
            } else {
                showLongToast(it.error)
            }
        }, {errorMessage, errorCode ->

            showLongToast(Constants.DISPOSABLE_ERROR)
        }).callRequest()
    }


    override fun getHouseItem(houseId: Int, companyId: Int, onDataLoad: (house: DataMapResponse) -> Unit) {

        apiEmployer.getHouse(houseId, companyId, {

            if (it.statusCode == 200 || it.statusCode == 201) {

                onDataLoad(mapResponse(it))

            } else {
            }

        }, { errorMessage, errorCode ->

        }).callRequest()
    }

    private fun mapResponse(it: GetHouseResponse): DataMapResponse {
        val data = DataMapResponse()
        data.houseId = it.data?.houseId ?: 0
        data.address = it.data?.address ?: ""
        data.available = it.data?.available ?: false
        data.lat = it.data?.lat ?: ""
        data.lng = it.data?.lng ?: ""
        data.rooms = it.data?.rooms ?: 0
        data.photos = it.data?.photos ?: arrayListOf()
        data.description = it.data?.description ?: ""
        data.costOfRent = it.data?.costOfRent ?: 0
        data.currencyOfRent = it.data?.currencyOfRent ?: ""
        return data
    }
}