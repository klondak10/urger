package noty_team.com.urger.ui.fragment.bottom_bar.container

import android.os.Bundle
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.bottom_bar.ContainerFragment
import noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy.BottomVacancyNewsFragment
import noty_team.com.urger.utils.cicerone.Screens

class BottomVacancyFragmentContainer : ContainerFragment() {

	companion object {
		fun getNewInstance(name: String): BottomVacancyFragmentContainer {
			val fragment = BottomVacancyFragmentContainer()

			val arguments = Bundle()
			arguments.putString(EXTRA_NAME, name)
			fragment.setArguments(arguments)

			return fragment
		}
	}

	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)

		if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
			getCicerone().router.replaceScreen(Screens.FragmentScreen(BottomVacancyNewsFragment.newInstance()))
		}
	}
}