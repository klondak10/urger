package noty_team.com.urger.ui.fragment.drawer.my_employee.hired

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.PreliminaryVacancyResponse

class HiredPresenter(context: Context): BasePresenter(context) {
    fun getCompaniesRequests(onLoadData:(data: PreliminaryVacancyResponse) -> Unit) {
        apiEmployer.getPreliminaryVacancy({
            if(it.statusCode == 200|| it.statusCode == 201) {
                onLoadData(it)
            } else {
                //showShortToast(it.error!!)
            }
        }, {errorMessage, errorCode ->
            //showShortToast(errorMessage)
        }).callRequest()
    }
}