package noty_team.com.urger.ui.fragment.tabs.documents.document_detail

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.view.View
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.fragment_document_detail.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import java.io.File


class FragmentDocumentDetail : BaseFragment<DocumentDetailPresenter>() {

	companion object {
		fun newInstance() = FragmentDocumentDetail()
	}

	lateinit var addressLink: String
	lateinit var fileName: String

	override fun layout() = R.layout.fragment_document_detail

	override fun initialization(view: View, isFirstInit: Boolean) {
		baseActivity.registerReceiver(onDownloadComplete, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))

		if (!addressLink.isNullOrEmpty()) {
			isShowLoadingDialog(true)
			beginDownload()
		} else {
			showShortToast(getString(R.string.error_load_data))
		}

		initAction()
	}

	private fun initAction() {
		back_privacy_policy.getClick {
			file?.delete()
			onBackPressed()
		}
	}

	private var file: File? = null
	private var downloadID: Long = 0

	private val onDownloadComplete = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			//Fetching the download id received with the broadcast
			val id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
			//Checking if the received broadcast is for our enqueued download by matching download id
			if (downloadID == id) {
				pdf_view.fromFile(file?.absoluteFile)
						.enableSwipe(true)
						.swipeHorizontal(false)
						.enableDoubletap(true)
						.enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
						.password(null)
						.scrollHandle(null)
						.onLoad {

							file?.delete()
							isShowLoadingDialog(false)
						}
						.enableAntialiasing(true) // improve rendering a little bit on low-res screens
						.spacing(0)
						.pageFitPolicy(FitPolicy.WIDTH)
						.load()

			}
		}
	}

	private fun beginDownload() {

		val nameDir = if (!fileName.isNullOrEmpty()) {
			fileName
		} else {
			"PDF View"
		}

		document_detail_text.text = if (!fileName.isNullOrEmpty()) {
			fileName
		} else {
			"PDF View"
		}

		file = File(baseActivity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), nameDir)

		val request = DownloadManager.Request(Uri.parse(addressLink))
				.setTitle(nameDir)
				.setDescription("Downloading")
				.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
				.setDestinationUri(Uri.fromFile(file))// Uri of the destination file
				.setAllowedOverMetered(true)
				.setAllowedOverRoaming(true)
		val downloadManager = baseActivity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
		downloadID = downloadManager.enqueue(request)
	}

	override fun onDestroy() {
		super.onDestroy()
		baseActivity.unregisterReceiver(onDownloadComplete)
	}
}