package noty_team.com.urger.ui.activity.splash

import android.app.Application
import android.content.Context
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.utils.api.request.refresh_token_request.RefreshTokenRequest
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.util.concurrent.TimeUnit

class SplashPresenter(context: Context) : BasePresenter(context), SplashContract.Presenter {

    private lateinit var chooseActivityCallback: (isUserExist: Boolean) -> Unit

    override fun dispose() {
        subscriptions.clear()
    }

    override fun serverRequest(
        chooseActivityCallback: (isUserExist: Boolean) -> Unit) {

        this.chooseActivityCallback = chooseActivityCallback

        if (!PaperIO.refreshToken.equals("")) {
            refreshTokenRequest()
        } else {
            emulateLoading(false)
        }

    }

    private fun emulateLoading(isUserExist: Boolean) {
        subscriptions.add(
            Observable
                .timer(2000, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({}, {}, {
                    this.chooseActivityCallback(isUserExist)
                })
        )
    }

    private fun refreshTokenRequest() {

        apiAuthorization.refreshToken(
            RefreshTokenRequest(PaperIO.refreshToken), {

                if (it.statusCode == 200 || it.statusCode == 201) {
                    if (it.data != null) {
                        it.parseToken()
                    }
                    if (PaperIO.userRole == UserRole.COMPANY) {
                        getEmployerInfo()
                    } else {
                        getExecutorInfo()
                    }

                } else {
                    showShortToast(it.error ?: "some error")
                }

            }, { errorMessage, errorCode ->

                showShortToast(errorMessage)
            }
        ).callRequest()
    }

    private fun getEmployerInfo() {
        apiEmployer.getEmployerInfo(PaperIO.employerData?.userId ?: -1, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                it.parseCompany()
                it.parseUser()
                it.isCanUpdate()
                emulateLoading(true)
            } else {
                emulateLoading(false)
                showShortToast(it.error)
            }
        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }

    private fun getExecutorInfo() {
        apiEmployer.getExecutorInfo(PaperIO.employerData?.userId ?: -1, {
            if (it.statusCode == 200 || it.statusCode == 201) {

                 it.parseUser()
                 emulateLoading(true)
            } else {
                emulateLoading(false)
                showShortToast(it.error)
            }
        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }
}