package noty_team.com.urger.ui.fragment.tabs.vacancy.categories

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.categories_response.CategoryResponse
import noty_team.com.urger.utils.api.response.categories_response.DataItem

class CategoryPresenter(context: Context): BasePresenter(context) {

    fun getCategories(onSuccess:(response: CategoryResponse)->Unit){
        apiEmployer.getCategoriesList({
                    onSuccess(it)
                }, {errorMessage, errorCode ->
                    showLongToast(errorMessage)
                }
        ).callRequest()
    }
}