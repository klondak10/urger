package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_portfolio.*
import kotlinx.android.synthetic.main.item_portfolio.portfolio_logo
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.portfolio.DataItem
import noty_team.com.urger.utils.setPhoto

class PortfolioAdapter(list: ArrayList<DataItem>, val context: Context,
                       recyclerView: RecyclerView,
                       var onLoadData: (offset: Int) -> Unit = {},
                       var onClick: (data: DataItem) -> Unit = {},
                       var onLongClick: (data: DataItem) -> Unit = {}) :
    BaseAdapterPagination<DataItem>(list, recyclerView) {

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            try {
                itemView.setOnClickListener {
                    onClick(list[position])
                }

                itemView.setOnLongClickListener {

                    onLongClick(list[position])

                    return@setOnLongClickListener true
                }

            } catch (e: Exception) {
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            if(item.file != null) {
                setPhoto(portfolio_logo, "https://${item.file.toString().substringBefore(',')}")
            } else {
                portfolio_logo.setImageResource(R.drawable.image_default)
            }
            description_item_portfolio.text = "${item.name}"
        }
    }

    override val itemLayoutResourceId = R.layout.item_portfolio

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }
}