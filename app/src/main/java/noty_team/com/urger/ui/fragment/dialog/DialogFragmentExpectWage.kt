package noty_team.com.urger.ui.fragment.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_expected_wage.*
import kotlinx.android.synthetic.main.dialog_expected_wage.view.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R

class DialogFragmentExpectWage : DialogFragment(){

    lateinit var callback: OnClickWageCallback

    companion object {
        fun newInstance(wage: Int): DialogFragmentExpectWage{
            return DialogFragmentExpectWage().apply {
                arguments = Bundle().apply {
                    putInt("wage", wage)
                }
            }
        }
        fun newInstance(): DialogFragmentExpectWage{
            return DialogFragmentExpectWage()
                    }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.dialog_expected_wage, container, false)
        if (dialog != null && dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }

        return view
    }

    fun setOnClickCallback(callbacks: OnClickWageCallback) {
        callback = callbacks
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        wage_expected_et.setText(PaperIO.executorData?.expectedWage!!.toString())
        save_button.setOnClickListener {
            callback.saveWage(view.wage_expected_et.text.toString().trim().toInt())
            dismiss()
        }
        cancel_button.setOnClickListener {
            dismiss()
        }
    }
}

interface OnClickWageCallback {
    fun saveWage(wage: Int)
}