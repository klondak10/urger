package noty_team.com.urger.ui.fragment.profile_executor.get_info.experience

import androidx.recyclerview.widget.LinearLayoutManager

import android.view.View
import kotlinx.android.synthetic.main.fragment_experience.*
import kotlinx.android.synthetic.main.fragment_info_executor.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.ExperienceAdapter
import noty_team.com.urger.ui.fragment.profile_executor.DetailProfileExecutorPresenter
import noty_team.com.urger.utils.api.response.all_info.DocumentsItem
import noty_team.com.urger.utils.api.response.all_info.ExpiriencesItem
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem
import kotlinx.android.synthetic.main.fragment_experience.empty_list as empty_list1

class ExperienceFragment : BaseFragment<DetailProfileExecutorPresenter>() {
	lateinit var adapterExperienceAdapter: ExperienceAdapter

	var data: List<ExpiriencesItem?> = listOf()

	companion object {

		fun newInstance(): ExperienceFragment {
			return ExperienceFragment()
		}

	}

	override fun layout() = R.layout.fragment_info_executor

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = DetailProfileExecutorPresenter(baseContext)
        if(data.isNullOrEmpty()) {
            empty_list.visibility = View.VISIBLE
        } else {
			initAdapter(convertData())
		}
	}

	private fun initAdapter(list: ArrayList<DataItem>) {
		recycler_list.layoutManager =
            LinearLayoutManager(baseActivity)
		adapterExperienceAdapter = ExperienceAdapter(
			list, baseActivity, recycler_list)
		recycler_list.adapter = adapterExperienceAdapter
	}

	private fun convertData():ArrayList<DataItem> {
		val list: ArrayList<DataItem> = arrayListOf()
		data.forEach {
			val item = DataItem(it!!.period, it.companyLogo, it.city, it.userId, it.companyName, it.id, it.position)
			list.add(item)
		}
		return list
	}
}