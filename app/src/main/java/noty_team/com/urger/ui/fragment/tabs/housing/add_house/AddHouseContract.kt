package noty_team.com.urger.ui.fragment.tabs.housing.add_house

import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.request.create_house.CreateHouseRequest
import java.io.File

interface AddHouseContract {
	interface Presenter {
		fun getCurrency(showToast: (toastMassage: String) -> Unit, onSuccess: (isSuccess: Boolean, currencyList: List<String>,currencyMap:HashMap<Int,String>) -> Unit)
		fun createHouse(showToast: (toastMassage: String) -> Unit, createHouseRequest: CreateHouseRequest, description: String, fileData: ArrayList<File>, onDataLoad: (housingItem: HousingItem) -> Unit)
	}
}