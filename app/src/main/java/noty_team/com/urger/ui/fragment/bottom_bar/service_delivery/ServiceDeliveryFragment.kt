package noty_team.com.urger.ui.fragment.bottom_bar.service_delivery

import android.app.Dialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_service_delivery.*
import kotlinx.android.synthetic.main.fragment_service_delivery.empty_list
import kotlinx.android.synthetic.main.fragment_service_delivery.search_toolbar_container
import kotlinx.android.synthetic.main.fragment_service_delivery.toolbar_vacancy_et
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.ServiceDeliveryAdapter
import noty_team.com.urger.ui.fragment.tabs.vacancy.add_vacancy.AddVacancyFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.api.response.currency.CurrencyItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.api.response.work_schedule.ScheduleItem

class ServiceDeliveryFragment : BaseFragment<ServiceDeliveryPresenter>() {

    private var currentVacancies: ArrayList<VacancyPaginationItem> = arrayListOf()
    lateinit var vacancyAdapter: ServiceDeliveryAdapter
    private var scheduleTypes = arrayListOf<ScheduleItem>()
    private var currencyTypes = arrayListOf<CurrencyItem>()
    var isLoading = false
    companion object {
        fun newInstance() = ServiceDeliveryFragment()
    }

    override fun layout() = R.layout.fragment_service_delivery

    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = ServiceDeliveryPresenter(baseContext)
        drawer_service_delivery.setOnClickListener {
            baseActivity.openDrawerLayout()

        }
//        filter_service_delivery.setOnClickListener {
//
//            addFragment(FilterFragment.newInstance())
//            //navigator().navigateToFragment(FilterFragment.newInstance(), null, true)пеш
//        }

        search_service_delivery.getClick {
            toolbar_vacancy_et?.setText("")
            toolbar_service.visibility = View.GONE
            search_toolbar_container.visibility = View.VISIBLE

            toolbar_vacancy_et.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    //vacancyAdapter.filter.filter(p0)
                    if(p0!!.isNotEmpty()) {
                        val query = p0.toString().toLowerCase()
                        val list = currentVacancies.filter {
                            it.category.toLowerCase().contains(query) || it.parent_category.toLowerCase().contains(
                                query
                            )
                        }
                        if (list.isEmpty()) {
                            empty_list?.visibility = View.VISIBLE
                        } else {
                            empty_list?.visibility = View.GONE
                        }
                        vacancyAdapter.setNewList(ArrayList(list))
                        vacancyAdapter.notifyDataSetChanged()
                    }else {
                        vacancyAdapter.setNewList(ArrayList(currentVacancies))
                        vacancyAdapter.notifyDataSetChanged()
                    }
                }
            })
        }
        nb_bottom_chat_search_back.setOnClickListener {

            /// invisible keyboard
            baseActivity.toggleKeyboard(false)
            empty_list?.visibility = View.GONE
            if(!currentVacancies.isEmpty()) {
                vacancyAdapter.list = currentVacancies
                vacancyAdapter.notifyDataSetChanged()
            }
            toolbar_service.visibility = View.VISIBLE
            search_toolbar_container.visibility = View.GONE
        }

        getVacancies(0)

        recycler_service_delivery.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recycler_service_delivery.getLayoutManager() as LinearLayoutManager
                if (!isLoading) {

                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == vacancyAdapter.itemCount - 1) {

                        isLoading = true
                        presenter?.loadMoreVacancy({
                            currentVacancies.addAll(it.map(scheduleTypes, currencyTypes))
                            vacancyAdapter.addNewVacancies(it.map(scheduleTypes, currencyTypes))
                            isLoading = false
                        }, {
                            isLoading = false
                        })


                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }})
    }

    private fun initAdapter(list: ArrayList<VacancyPaginationItem>) {
        recycler_service_delivery.layoutManager =
            LinearLayoutManager(baseActivity)
        vacancyAdapter = ServiceDeliveryAdapter(list, baseContext,
            onLoadData = {
                getVacancies(it)
            },
            onClick = {
                addFragment(VacancyDetailFragmnet.newInstance(it.vacancyId))
            },
            onLongClick = {
                showDialog(it.vacancyId)
            },
            callbackChatEmpty = {
                if(it) {
                    empty_list.visibility = View.VISIBLE
                } else {
                    search_service_delivery.visibility = View.VISIBLE
                }
            })
        recycler_service_delivery.adapter = vacancyAdapter
    }

    private fun showDialog(vacancyId: Int) {
        val vacancyDialog = Dialog(baseActivity)

        vacancyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        if(PaperIO.userRole == UserRole.COMPANY) {
            vacancyDialog.setContentView(R.layout.dialog_employer_vacancy)
        }

        val editButton: Button = vacancyDialog.findViewById(R.id.edit_vacancy_button)
        val deleteButton: Button = vacancyDialog.findViewById(R.id.delete_vacancy_button)
        val cancelButton = vacancyDialog.findViewById<Button>(R.id.cancel_vacancy_button)

        cancelButton.setOnClickListener {
            vacancyDialog.dismiss()
        }
        editButton.setOnClickListener {
            addFragment(AddVacancyFragment.newInstance(
                AddVacancyFragment.ScreenType.EDITING,
                vacancyId
            ) { data, isEdit ->
                if (!isEdit) addNewVacancy(data)
                else editVacancy(data)
            })
            vacancyDialog.dismiss()
        }
        deleteButton.setOnClickListener {
            deleteVacancy(vacancyId)
            vacancyDialog.dismiss()
        }
        vacancyDialog.show()
    }

    private fun deleteVacancy(id: Int) {
        isShowLoadingDialog(true)
        presenter?.deleteVacancy(id, {
            isShowLoadingDialog(false)
            if (vacancyAdapter.deleteVacancy(id)) {
                if(vacancyAdapter.itemCount == 0) {
                    empty_list?.visibility = View.VISIBLE
                    search_service_delivery?.visibility = View.GONE
                } else {
                    empty_list?.visibility = View.GONE
                }
                showShortToast(getString(R.string.success_delete))
            }
        }, {
            isShowLoadingDialog(false)
        })
    }

    private fun addNewVacancy(vacancyItem: VacancyPaginationItem) {
        val scheduleItems = scheduleTypes.filter { it.key == vacancyItem.workSchedule }
        val schedule: String = if (scheduleItems.isNotEmpty()) scheduleItems[0].value else ""
        vacancyItem.workSchedule = schedule
        vacancyAdapter.addNewVacancy(vacancyItem)
        vacancyAdapter.notifyDataSetChanged()
    }

    private fun editVacancy(vacancyItem: VacancyPaginationItem) {
        val scheduleItems = scheduleTypes.filter { it.key == vacancyItem.workSchedule }
        val schedule: String = if (scheduleItems.isNotEmpty()) scheduleItems[0].value else ""
        vacancyItem.workSchedule = schedule
        vacancyAdapter.editItem(vacancyItem)
    }

    private fun getVacancies(size: Int) {
        presenter?.getVacancies(size, {
            if(it.data.isEmpty()) {
                empty_list.visibility = View.VISIBLE
            } else {
                initAdapter(it.map(scheduleTypes, currencyTypes))
                currentVacancies.addAll(it.map(scheduleTypes, currencyTypes))
                search_service_delivery.visibility = View.VISIBLE
            }

        }, {

        })
    }
}