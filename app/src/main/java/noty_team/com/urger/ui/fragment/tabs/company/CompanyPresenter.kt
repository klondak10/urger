package noty_team.com.urger.ui.fragment.tabs.company

import android.content.Context
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.io.File

class CompanyPresenter(context: Context) : BasePresenter(context), CompanyContract.Presenter {
	//lateinit var showToast: (massage: String) -> Unit

	override fun addCompanyPhotos(list: ArrayList<File>, dataList: (ArrayList<CompanyPhotoItem>) -> Unit) {
		apiEmployer.addCompanyPhoto(PaperIO.companyData?.companyId ?: -1, list, {

			if (it.statusCode == 200 || it.statusCode == 201) {
				val listPhotoItem = it.map()

				//list.reverse()
				dataList(listPhotoItem)
			} else {
				showShortToast(it.error)
			}

		}, { errorMessage, errorCode ->
			showShortToast(context.getString(R.string.error_add_photo))
		}).callRequest()
	}

	override fun getCompanyPhotos(companyId: Int, dataList: (ArrayList<CompanyPhotoItem>) -> Unit, showToast: (massage: String) -> Unit) {
		if(PaperIO.userRole == UserRole.COMPANY) {
			apiEmployer.getPhotosCompany(PaperIO.companyData?.companyId!!, {

				if (it.statusCode == 200 || it.statusCode == 201) {
					var listCompanyPhoto = it.map()

					listCompanyPhoto.reverse()
					dataList(listCompanyPhoto)
				} else {
					showToast(context.getString(R.string.error_load_photos))
				}
			}, { errorMessage, errorCode ->
				showToast(Constants.DISPOSABLE_ERROR)
			}).callRequest()
		} else {
			apiEmployer.getPhotosCompany(companyId, {

				if (it.statusCode == 200 || it.statusCode == 201) {
					var listCompanyPhoto = it.map()

					listCompanyPhoto.reverse()
					dataList(listCompanyPhoto)
				} else {
					showToast(context.getString(R.string.error_load_photos))
				}
			}, { errorMessage, errorCode ->
				showToast(Constants.DISPOSABLE_ERROR)
			}).callRequest()
		}


	}

	override fun deleteCompanyPhoto(photoName: String, showToast: (massage: String) -> Unit, onDeleteSuccess: (isSuccess: Boolean) -> Unit) {

		apiEmployer.deletePhotoCompany(PaperIO.companyData?.companyId ?: -1, photoName, {

			if (it.statusCode == 200 || it.statusCode == 201) {
				onDeleteSuccess(true)
			} else {
				onDeleteSuccess(false)
			}

		}, { errorMessage, errorCode ->

			showToast(Constants.DISPOSABLE_ERROR)
		}).callRequest()

	}

	override fun getStatistic(companyId: Int, showToast: (massage: String) -> Unit, onDataLoad: (countDocument: String, countHousing: String, countVacancies: String) -> Unit) {
		apiEmployer.getStatistic(companyId, {


			val countDocument = it.data?.documentsCount ?: 0
			val countHousing = it.data?.housesCount ?: 0
			val countVacancies = it.data?.vacanciesCount ?: 0

			onDataLoad(countDocument.toString(), countHousing.toString(), countVacancies.toString())

		}, { errorMessage, errorCode ->
			//showShortToast("CompanyPresenter getStatistic" + errorMessage)
		}).callRequest()

	}
}