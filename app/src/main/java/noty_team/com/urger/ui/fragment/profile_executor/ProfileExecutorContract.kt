package noty_team.com.urger.ui.fragment.profile_executor

import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import java.io.File

interface ProfileExecutorContract {
    interface View

    interface Presenter {
        fun updateAvatar(masterPhoto: File, isSetPhotoCallback: (isSuccess: Boolean) -> Unit, showToast: (toastMassage: String) -> Unit)
       // fun updateBackground(masterPhoto: FileData, isSetPhotoCallback: (isSuccess: Boolean) -> Unit, showToast: (toastMassage: String) -> Unit)
    }
}