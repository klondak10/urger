package noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy

import android.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_bottom_vacancy.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.NewsAdapter
import noty_team.com.urger.ui.adapters.recycler.PortfolioDetailAdapter
import noty_team.com.urger.ui.fragment.FragmentImageViewer
import noty_team.com.urger.utils.api.response.news.DataItem

class NewsFragment : BaseFragment<BottomExecutorPresenter>() {

    private lateinit var newsAdapter: NewsAdapter

    companion object {
        fun newInstance() = NewsFragment()
    }


    override fun layout() = R.layout.fragment_bottom_vacancy

    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = BottomExecutorPresenter(baseContext)
        initAdapter()
        getNews()
    }

    private fun getNews() {
        presenter!!.getNews {
            if (!it.data.isNullOrEmpty()) {
                newsAdapter.items = ArrayList(it.data)
                newsAdapter.notifyDataSetChanged()
            } else {
                empty_list.visibility = View.VISIBLE
            }
        }
    }

    private fun initAdapter() {
        newsAdapter = NewsAdapter(arrayListOf(), baseContext) {
            showDialog(it)
        }
        recycler_bottom_list.layoutManager =
            LinearLayoutManager(baseActivity)
        recycler_bottom_list.adapter = newsAdapter
    }

    private fun showDialog(it: DataItem) {
        val listImage = it.image?.split(",")?.toTypedArray()
        val mView = LayoutInflater.from(baseContext).inflate(R.layout.dialog_detail_news, null)

        val title = mView.findViewById(R.id.title_detail_news) as TextView
        val description = mView.findViewById(R.id.description_detail_news) as TextView
        val recyclerView = mView.findViewById(R.id.recycler_view_image) as RecyclerView


        title.text = it.title
        description.text = it.description

        val adapterDetailPortfolio = listImage?.let { it1 ->
            PortfolioDetailAdapter(it1, baseContext) {
                val fragmentExpectedWage = FragmentImageViewer.newInstance("http://" + listImage[it])
                fragmentExpectedWage.show(baseActivity.supportFragmentManager, "dialog_expected_wage")
            }
        }
        val layoutManager = LinearLayoutManager(
            baseContext,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapterDetailPortfolio

        val alert = AlertDialog.Builder(baseActivity)
        alert.setView(mView)
        alert.setCancelable(true)

        alert.show()
    }
}
