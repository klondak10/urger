package noty_team.com.urger.ui.fragment.bottom_bar.map.company

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.get_vacancy.VacancyItemLocation
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.PreliminaryVacancyResponse

class MapCompanyPresenter(context: Context): BasePresenter(context) {

    fun getCompaniesRequests(onLoadData:(data: ArrayList<VacancyItemLocation>) -> Unit) {
        apiEmployer.getPreliminaryVacancy({
            if(it.statusCode == 200|| it.statusCode == 201) {
                onLoadData(it.mapLocation())
            } else {
                showShortToast(it.error!!)
            }
        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()
    }

}