package noty_team.com.urger.ui.activity.executor

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.ConfigurationCompat
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.firebase.messaging.FirebaseMessaging
import com.jakewharton.rxbinding2.view.RxView
import com.tbruyelle.rxpermissions2.RxPermissions
import com.zeugmasolutions.localehelper.LocaleHelper
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegate
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import io.paperdb.Paper
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_executor.*
import kotlinx.android.synthetic.main.main_content.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.ApplicationLanguageHelper
import noty_team.com.urger.R
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiEmployer
import noty_team.com.urger.base.AppUrger
import noty_team.com.urger.base.mvp.DrawerListener
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.ui.activity.qr_code.qr_code.QRCodeActivity
import noty_team.com.urger.ui.fragment.dialog.CloseAlertDialog
import noty_team.com.urger.ui.fragment.dialog.DialogFragmentExpectWage
import noty_team.com.urger.ui.fragment.dialog.OnClickWageCallback
import noty_team.com.urger.ui.fragment.drawer.feedback.DrawerFeedbackFragment
import noty_team.com.urger.ui.fragment.drawer.service_urger.urger_services_map.UrgerServicesMapFragment
import noty_team.com.urger.ui.fragment.drawer.settings.SettingFragment
import noty_team.com.urger.ui.fragment.profile_executor.ProfileExecutorFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.favorite_vacancy.FavoriteVacancyFragment
import noty_team.com.urger.utils.ActionDebounceFilter
import noty_team.com.urger.utils.ChatSocket
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.model.UserUrgerData
import noty_team.com.urger.utils.api.request.edit_user_personal_info.FcmRequest
import noty_team.com.urger.utils.api.response.sms_checked.Authorization
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.cicerone.Screens
import noty_team.com.urger.utils.setPhoto
import noty_team.com.urger.utils.waitUntilViewDrawn
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

class ExecutorActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
		BottomNavigationView.OnNavigationItemSelectedListener, DrawerListener, OnClickWageCallback {
	private var socket = ChatSocket.provideSocketChat()
	@Inject
	lateinit var apiEmployer: UrgerRetrofitApiEmployer
	init {
		val app = AppUrger
		app.instance.appComponent.inject(this)
	}

	companion object {
		fun start(baseActivity: Activity) {
			baseActivity.startActivity(
					Intent(
							baseActivity,
							ExecutorActivity::class.java
					).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
			)

		}

	}


	override fun layout() = R.layout.activity_executor



	@SuppressLint("CheckResult")
	override fun initialization() {

		initView()
		PaperIO.userRole = UserRole.EXECUTOR
		PaperIO.signUp = Authorization.ALREADY_LOGIN

		if(!socket.connected()) {
			socket.connect()
		}
		val jsonObject = JSONObject()
		jsonObject.put("userId", PaperIO.executorData!!.userId)
		socket.emit("newmess",jsonObject).on("newmess") {
			val messages = it[0].toString()
			runOnUiThread {
				setCounterNewMessage(messages, bottom_nav)
			}
		}

		FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
			if(result != null){
				UrgerApi.USER_PROFILE_API.updateFcmExecutor(FcmRequest(result)).subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe({}, {})
			}
		}


	}

	override fun onDestroy() {
		super.onDestroy()
		socket.off()
		socket.disconnect()
	}


	private fun initView() {

		replaceFragment(ProfileExecutorFragment.newInstance())

		bottom_nav.selectedItemId = R.id.bn_profile

		nav_view_executor.setNavigationItemSelectedListener(this)

		bottom_nav.setOnNavigationItemSelectedListener(this)

	}

	private fun setAvatarDrawer() {
		waitUntilViewDrawn(drawer_layout_executor) {
			val drawerImage = nav_view_executor.findViewById<ImageView>(R.id.drawer_avatar_img)
			val drawerWithoutImage = nav_view_executor.findViewById<TextView>(R.id.drawer_avatar_tv)

			if (drawerImage != null && drawerWithoutImage != null) {
				if (PaperIO.executorData?.avatar != "" && PaperIO.executorData?.avatar != null) {
					drawerImage.visibility = View.VISIBLE
					drawerWithoutImage.visibility = View.GONE

					setPhoto(drawerImage, PaperIO.executorData?.avatar ?: "")
				} else {
					drawerImage.visibility = View.GONE
					drawerWithoutImage.visibility = View.VISIBLE

					val avatarLogoText =
						PaperIO.executorData?.firstName?.substring(0, 1) + PaperIO.executorData?.lastName?.substring(0, 1)
					drawerWithoutImage.text = avatarLogoText
				}
			}
		}
	}

	@SuppressLint("SetTextI18n")
	private fun setDrawerUserInfo() {
		waitUntilViewDrawn(drawer_layout_executor) {
			val firstNameSecondName = nav_view_executor.findViewById<TextView>(R.id.drawer_first_second_name)
			if (firstNameSecondName != null) {
				firstNameSecondName.text = PaperIO.executorData?.firstName + " " + PaperIO.executorData?.lastName
			}
		}
	}

	private fun setDrawerBackground() {
		waitUntilViewDrawn(drawer_layout_executor) {
			val drawerImage = nav_view_executor.findViewById<ImageView>(R.id.image_drawer)
			if (drawerImage != null) {
				if (PaperIO.executorData?.background != "" && PaperIO.executorData?.background != null) {
					drawerImage.visibility = View.VISIBLE
					setPhoto(drawerImage, PaperIO.executorData?.background ?: "")
				} else {
					drawerImage.visibility = View.GONE
				}
			}
		}
	}

	override fun onDrawerAvatarChanged() {
		setAvatarDrawer()
	}

	override fun onDrawerBackgroundChanged() {
		setDrawerBackground()
	}

	override fun onDrawerUserInfoChanged() {
		setDrawerUserInfo()
	}


	@SuppressLint("CheckResult")
	override fun onNavigationItemSelected(item: MenuItem): Boolean {

		when (item.itemId) {

			//drawer_company
			R.id.nav_favorites -> {

				addFragment(FavoriteVacancyFragment.newInstance(true))
			}

			R.id.nav_service_urger -> {

				addFragment(UrgerServicesMapFragment.newInstance())
			}

			R.id.nav_feedback -> {

				addFragment(DrawerFeedbackFragment.newInstance())
			}
			R.id.nav_settings -> {
				addFragment(SettingFragment.newInstance())
			}
			R.id.nav_expected_wages -> {
				apiEmployer.getWageExecutor({

					PaperIO.executorData = UserUrgerData(
						PaperIO.executorData?.userId,
						PaperIO.executorData?.phone,
						PaperIO.executorData?.firstName,
						PaperIO.executorData?.lastName,
						PaperIO.executorData?.email,
						PaperIO.executorData?.type,
						PaperIO.executorData?.avatar,
						PaperIO.executorData?.background,
						PaperIO.executorData?.birthday,
						it.data?.sallary?:0)

					val fragmentExpectedWage = DialogFragmentExpectWage.newInstance(PaperIO.executorData?.expectedWage!!)
					fragmentExpectedWage.setOnClickCallback(this)
					fragmentExpectedWage.show(supportFragmentManager, "dialog_expected_wage")

				}, {errorMessage, errorCode ->

					val fragmentExpectedWage = DialogFragmentExpectWage.newInstance(PaperIO.executorData?.expectedWage!!)
					fragmentExpectedWage.setOnClickCallback(this)
					fragmentExpectedWage.show(supportFragmentManager, "dialog_expected_wage")
				}).callRequest()

			}
			R.id.nav_generate_qr_code -> {
				val rxPermissions = RxPermissions(this)
				rxPermissions
						.request(Manifest.permission.CAMERA)
						.subscribe { granted ->
							if (granted) {
								QRCodeActivity.start(this)
							} else {
							}
						}
			}

			R.id.nav_logout -> {
				CloseAlertDialog(
					closeApp = {
						PaperIO.refreshToken = ""
						PaperIO.accessToken = ""
						it.cancel()
						it.dismiss()
						this.finish()
					},
					stay = {
						it.cancel()
						it.dismiss()
				}).show(supportFragmentManager, "CloseAlertDialog")
			}

			//bottom bar
			R.id.bn_service_delivery -> {
				bottom_nav.changeTab(0, bottom_nav.selectedItemId == R.id.bn_service_delivery)

			}
			R.id.bn_notification -> {

				bottom_nav.changeTab(1, bottom_nav.selectedItemId == R.id.bn_notification)
			}
			R.id.bn_chats -> {
				bottom_nav.changeTab(2, bottom_nav.selectedItemId == R.id.bn_chats)
			}
			R.id.bn_map -> {

				bottom_nav.changeTab(3, bottom_nav.selectedItemId == R.id.bn_map)

			}
			R.id.bn_profile -> {
				bottom_nav.changeTab(4, bottom_nav.selectedItemId == R.id.bn_profile)
			}

		}

		drawer_layout_executor.closeDrawer(GravityCompat.START)
		uncheckAllItem()
		return true
	}
	private fun uncheckAllItem() {
		val size = nav_view_executor.menu.size()
		for (i in 0 until size) {
			nav_view_executor.menu.getItem(i).isCheckable = false
		}
	}

	override fun saveWage(wage: Int) {
		apiEmployer.updateWageExecutor(wage, {
			if(it.statusCode == 200 || it.statusCode == 201) {
				showShortToast(getString(R.string.success_update_wage))
			} else {
				showShortToast(getString(R.string.failure_update_wage))
			}
		}, {errorMessage, errorCode ->

			showShortToast(errorMessage)
		}).callRequest()
	}

	fun <T> Single<T>.callRequest() {
		this.subscribe({}, {

		}).also { subscriptions.add(it) }
	}

	override fun getDrawerLayout() = drawer_layout_executor

	override fun getNavigationView() = nav_view_executor

	override fun getBottomNavigationView() = bottom_nav

	override fun openDrawerLayout() {
		drawer_layout_executor.openDrawer(GravityCompat.START)
	}


	private var isFastClick: Boolean = false
	private val clicksFilter = ActionDebounceFilter()

	private fun BottomNavigationView.changeTab(pos: Int, wasSelected: Boolean): Boolean {
		isFastClick = false

		subscriptions.add(RxView.focusChanges(this)
				.filter {
					clicksFilter.filterAction()
				}
				.subscribe {
					if (!wasSelected) {
						when (pos) {
							0 -> selectTab(Screens.MainScreens.BOTTOM_VACANCY_FRAGMENT)
							1 -> selectTab(Screens.MainScreens.NOTIFICATION)
							2 -> selectTab(Screens.MainScreens.CHAT)
							3 -> selectTab(Screens.MainScreens.MAP)
							4 -> selectTab(Screens.MainScreens.PROFILE)
						}
						isFastClick = true
					}
				}
		)

		return isFastClick
	}

	private fun selectTab(screen: Screens.MainScreens) {
		val tab: String = screen.name
		val fm = supportFragmentManager
		var currentFragment: Fragment? = null
		val fragments = fm.fragments
		if (fragments != null) {
			for (f in fragments) {
				if (f.isVisible) {
					currentFragment = f
					break
				}
			}
		}
		val newFragment = fm.findFragmentByTag(tab)

		if (currentFragment != null && newFragment != null && currentFragment === newFragment) return

		val transaction = fm.beginTransaction()
		if (newFragment == null) {
			transaction.add(R.id.main_fragment_container, Screens.TabScreen(screen).fragment, tab)
		}

		if (currentFragment != null) {
			transaction.detach(currentFragment)
		}

		if (newFragment != null) {
			transaction.attach(newFragment)
		}
		transaction.commitNow()
	}
}