package noty_team.com.urger.ui.fragment.login.choose_role.company

import android.content.Context
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.request.create_company.CreateCompanyRequest

class CompanyInfoPresenter(context: Context) : BasePresenter(context), CompanyInfoContract.Presenter {


	lateinit var openAppCallback: (isOpen: Boolean) -> Unit

	override fun createCompanyRequest(
			createCompanyRequest: CreateCompanyRequest,
			openAppCallback: (isOpen: Boolean) -> Unit

	) {
		this.openAppCallback = openAppCallback

		apiAuthorization.createCompany(createCompanyRequest, {

			if (it.statusCode == 200 || it.statusCode == 201) {
				it.parseToken()
				it.parseCompany()
				it.parseUser()
				it.isCanUpdate()

				getEmployerInfo()
			} else {
				openAppCallback(false)
				showShortToast(it.error)
			}
		}, { errorMessage, errorCode ->
			showShortToast(errorMessage)
		}).callRequest()
	}

	fun getCompanyNameByNip(nip: String, companyNameCallback: (companyName: String) -> Unit) {

		apiAuthorization.getCompanyNameByNip(nip, {
			if (it.statusCode == 200 || it.statusCode == 201)
				companyNameCallback(it.data?.companyName ?: context.resources.getString(R.string.nip_not_find))
			else showShortToast(it.error)
		}, { errorMessage, _ ->
			showShortToast(errorMessage)
		}).callRequest()

	}

	private fun getEmployerInfo() {
		apiEmployer.getEmployerInfo(PaperIO.employerData?.userId ?: -1, {
			if (it.statusCode == 200 || it.statusCode == 201) {
				it.parseCompany()
				it.parseUser()
				it.isCanUpdate()
				openAppCallback(true)
			} else {
				showShortToast(it.error)
			}
		}, { errorMessage, errorCode ->

			showShortToast(errorMessage)

		}).callRequest()
	}
}