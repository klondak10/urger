package noty_team.com.urger.ui.fragment.tabs.experience.add_experience

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.widget.AppCompatButton
import com.github.drjacky.imagepicker.ImagePicker
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.button.MaterialButton
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import kotlinx.android.synthetic.main.fragment_add_experience.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.dialog.ChoosedSubCategory
import noty_team.com.urger.ui.fragment.dialog.DialogFragmentCategoryPicker
import noty_team.com.urger.utils.adapters.category.SingleCategory
import noty_team.com.urger.utils.api.response.experience_executor.AddExperienceExecutorResponse
import noty_team.com.urger.utils.api.response.get_city.CityResponse
import noty_team.com.urger.utils.camera.FilesPicker
import java.io.File
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

class AddExperienceFragment : BaseFragment<AddExperiencePresenter>(), ChoosedSubCategory {
    override fun pickSubcategory(id: String,parentCategory: String, name: String) {
        position_et.text = "$name"
    }
    var isAdd: Boolean= false
    var callbackAddExperience: (item: AddExperienceExecutorResponse) -> Unit = {}
    companion object {
        fun newInstance() = AddExperienceFragment()
    }

    override fun layout() = R.layout.fragment_add_experience

    override fun initialization(view: View, isFirstInit: Boolean) {
        presenter = AddExperiencePresenter(baseContext)
        nb_add_experience.setOnClickListener {
            onBackPressed()
            //backNavigator().navigateBack()
        }
        confirm_add_experience.setOnClickListener {
            if(validator() && !isAdd) {
                isAdd = true
                presenter?.addExperienceExecutor(company_name_et.text.toString(),
                    city_name_et.text.toString(), position_et.text.toString(), "${period_work_with_tv.text} ${period_work_by_tv.text}") {

                            showShortToast(getString(R.string.add_experience_success))
                            callbackAddExperience(it)
                    onBackPressed()
//                            if (PaperIO.userRole == UserRole.COMPANY) {
//                                CompanyActivity.start(baseActivity)
//                                baseActivity.finish()
//                            } else {
//                                ExecutorActivity.start(baseActivity)
//                                baseActivity.finish()

                        }
                    }

           // backNavigator().navigateBack()
        }

        city_name_et.getClick {
            callPlaceAutocompleteActivityIntent()
        }

        position_et.getClick {
            presenter?.getCategory {

                val fragmentManager = activity?.supportFragmentManager

                var liss: ArrayList<SingleCategory> = createListCategory(it)
                val fragmentCategory = DialogFragmentCategoryPicker.newInstance(liss)
                fragmentCategory.setChoosedSubCategory(this)
                fragmentCategory.show(fragmentManager!!, "dialog_category")
            }
        }
        period_work_with_tv.setOnClickListener {
            showDatePickerDialog(it)
        }

        period_work_by_tv.setOnClickListener {
            showDatePickerDialog(it)
        }

        add_logo_company.setOnClickListener {
            showDialogChoose().show()
        }

    }

    private fun showDialogChoose(): Dialog {
        val dialog = Dialog(requireActivity())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_choose)

        val btnGallery = dialog.findViewById(R.id.btnGallery) as AppCompatButton
        val btnCamera = dialog.findViewById(R.id.btnCamera) as AppCompatButton
        val ivCross = dialog.findViewById(R.id.ivCross) as ImageView

        ivCross.setOnClickListener {
            dialog.dismiss()
        }
        btnGallery.setOnClickListener {
            dialog.dismiss()
            pickGalleryImage()
        }
        btnCamera.setOnClickListener {
            dialog.dismiss()
            pickCameraImage()
        }
        return dialog
    }

    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!
                presenter?.getBitmapFromFileData(File(uri.path.toString())) {
                    setLogo(it)
                }

            } else parseError(it)
        }

    private fun pickCameraImage() {
        cameraLauncher.launch(
            ImagePicker.with(requireActivity())
                .crop()
                .cameraOnly()
                .maxResultSize(720, 1280, true)
                .createIntent()
        )
    }

    private fun pickGalleryImage() {
        galleryLauncher.launch(
            ImagePicker.with(requireActivity())
                .crop()
                .galleryOnly()
                .cropFreeStyle()
                .maxResultSize(720, 1280, true)
                .galleryMimeTypes( // no gif images at all
                    mimeTypes = arrayOf(
                        "image/png",
                        "image/jpg",
                        "image/jpeg"
                    )
                )
                .createIntent()
        )
    }

    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!
                presenter?.getBitmapFromFileData(File(uri.path.toString())) {
                    setLogo(it)
                }
            } else parseError(it)
        }


    private fun parseError(activityResult: ActivityResult) {
        if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(
                requireContext(),
                ImagePicker.getError(activityResult.data),
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

    private fun createListCategory(it: CityResponse): ArrayList<SingleCategory> {
        var list: MutableList<SingleCategory> = arrayListOf()
        for (obj in it.data!!) {
            list.add(SingleCategory(obj?.name, obj?.subcategories))
        }
        return list as ArrayList<SingleCategory>
    }

    private fun setLogo(data: Bitmap) {
        company_logo_img.visibility = View.VISIBLE
        company_logo_img.setImageBitmap(data)
    }

    private fun showDatePickerDialog(it: View?) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)


        val dateDialog = DatePickerDialog(requireActivity(), { view, year1, month1, dayOfMonth1 ->

            (it as TextView).text = "$year1-${month1 + 1}-$dayOfMonth1"

        }, year, month, day)
        dateDialog.show()
    }

    private fun convertDateToTimestamp(dateStr: String):Timestamp {
        val date = SimpleDateFormat("yyyy-MM-dd").parse(dateStr)
        return Timestamp(date.time)
    }

    private fun equalsTimeStamp(timeStart: Timestamp, timeEnd: Timestamp): Boolean {
        val difference = (timeEnd.time - timeStart.time) / (1000 * 60 * 60 * 24)
        return difference >= 30
    }
    private fun currentTimestamp(): Timestamp {
        return Timestamp(Date().time)
    }

    private fun callPlaceAutocompleteActivityIntent() {
        try {
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,
                mutableListOf(
                    Place.Field.ADDRESS,
                    Place.Field.ID,
                    Place.Field.LAT_LNG,
                    Place.Field.OPENING_HOURS
                )
            ).setTypeFilter(TypeFilter.CITIES).build(baseActivity)

            startActivityForResult(intent, 8)

        } catch (e: GooglePlayServicesRepairableException) {
            showShortToast("error")
        } catch (e: GooglePlayServicesNotAvailableException) {
            showShortToast("error")

        } catch (e: Exception) {
            showShortToast("error")
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 8) {
            if (resultCode == AppCompatActivity.RESULT_OK) {
                if (data != null) {
                    val place = Autocomplete.getPlaceFromIntent(data)
                    city_name_et.text = place.address
                }
            }
        }
    }

    private fun validator(): Boolean {
        val minLength = 2
        try {
            if(company_name_et.text.trim().length < minLength) {
                showShortToast(getString(R.string.company_name_short))
                return false
            }

            if(city_name_et.text.trim().length < minLength) {
                showShortToast(getString(R.string.name_city_short))
                return false
            }

            if(position_et.text.trim().length < minLength) {
                showShortToast(getString(R.string.name_position_short))
                return false
            }

            if(period_work_with_tv.text.trim().length < minLength && period_work_by_tv.text.trim().length < minLength ) {
                showShortToast(getString(R.string.input_period_work))
                return false
            }
            if(!presenter!!.isBitmapInitialize()) {
                showShortToast(getString(R.string.empty_logo))
                return false
            }
            if(!equalsTimeStamp(convertDateToTimestamp(period_work_with_tv.text.toString()),convertDateToTimestamp(period_work_by_tv.text.toString()))) {
                showShortToast(getString(R.string.error_period))
                return false
            }
            if(convertDateToTimestamp(period_work_with_tv.text.toString()) > currentTimestamp()) {
                showShortToast(getString(R.string.error_period_2))
                return false
            }

            if(convertDateToTimestamp(period_work_by_tv.text.toString()) > currentTimestamp()) {
                showShortToast(getString(R.string.error_period_3))
                return false
            }
        } catch (e: java.lang.Exception) {
            showLongToast(getString(R.string.error_load_data))
            return false
        }
        return true
    }
}