package noty_team.com.urger.ui.fragment.drawer.feedback

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter

class DrawerFeedbackPresenter(context: Context): BasePresenter(context) {

    fun createFeedback(email: String, text: String, isSuccess: (successCreate: Boolean) -> Unit) {

        apiEmployer.addFeedback(email, text, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                isSuccess(true)
            } else {
                isSuccess(false)
            }
        }, {errorMessage, errorCode ->
            showShortToast(errorMessage)
            isSuccess(false)
        }).callRequest()
    }
}