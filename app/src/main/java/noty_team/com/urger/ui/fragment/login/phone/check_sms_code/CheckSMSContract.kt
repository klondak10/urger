package noty_team.com.urger.ui.fragment.login.phone.check_sms_code

interface CheckSMSContract {
	interface View
	interface Presenter {
		fun serverRequest(smsCode: Int, isNeedSingUpCallback: (isSignUP: Boolean) -> Unit, showToastCallback: (massage: String) -> Unit)
	}
}