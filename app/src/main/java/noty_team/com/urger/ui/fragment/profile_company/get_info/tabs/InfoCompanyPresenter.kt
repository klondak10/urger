package noty_team.com.urger.ui.fragment.profile_company.get_info.tabs

import android.content.Context
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem

class InfoCompanyPresenter(context: Context) : BasePresenter(context) {

    fun getCompanyPhotos(companyId: Int, dataList: (ArrayList<CompanyPhotoItem>) -> Unit, showToast: (massage: String) -> Unit) {

            apiEmployer.getPhotosCompany(companyId, {

                if (it.statusCode == 200 || it.statusCode == 201) {
                    var listCompanyPhoto = it.map()

                    listCompanyPhoto.reverse()
                    dataList(listCompanyPhoto)
                } else {
                    showToast(context.getString(R.string.error_load_photos))
                }
            }, { errorMessage, errorCode ->
                showToast(Constants.DISPOSABLE_ERROR)
            }).callRequest()
    }
}