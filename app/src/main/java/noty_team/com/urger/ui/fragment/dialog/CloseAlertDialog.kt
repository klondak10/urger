package noty_team.com.urger.ui.fragment.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import noty_team.com.urger.R


class CloseAlertDialog(val closeApp:(dialog:DialogInterface)->Unit, val stay:(dialog:DialogInterface)->Unit) : DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {


        return AlertDialog.Builder(requireActivity(), R.style.CustomAlertDialog)
            .setTitle(requireActivity().getString(R.string.title_dialog_close))
            .setMessage(requireActivity().getString(R.string.message_dialog_close))
            .setPositiveButton(requireActivity().getString(R.string.yes)) { dialog, which ->
                closeApp(dialog)
            }
            .setNegativeButton(requireActivity().getString(R.string.no)) { dialog, _ ->
                stay(dialog)
            }
            .create()
    }



}