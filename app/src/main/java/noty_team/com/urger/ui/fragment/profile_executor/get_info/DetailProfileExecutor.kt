package noty_team.com.urger.ui.fragment.profile_executor.get_info

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.drawerlayout.widget.DrawerLayout
import android.view.View
import com.google.gson.Gson
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_detail_executor.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.qr_code.qr_code.QRCodeActivity
import noty_team.com.urger.ui.adapters.view_pager.ExecutorPagerAdapterInfo
import noty_team.com.urger.ui.fragment.bottom_bar.chat.DialogChatFragment
import noty_team.com.urger.ui.fragment.profile_executor.DetailProfileExecutorPresenter
import noty_team.com.urger.ui.fragment.profile_executor.get_info.documents.DocumentsFragment
import noty_team.com.urger.ui.fragment.profile_executor.get_info.experience.ExperienceFragment
import noty_team.com.urger.ui.fragment.profile_executor.get_info.feedback.FeedbackFragment
import noty_team.com.urger.ui.fragment.profile_executor.get_info.portfolio.PortfolioFragment
import noty_team.com.urger.ui.fragment.profile_executor.get_info.vehicle.VehicleFragment
import noty_team.com.urger.utils.ChatSocket
import noty_team.com.urger.utils.Rooms
import noty_team.com.urger.utils.api.response.all_info.AllInfoExecutorResponse
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.User
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.setPhoto
import org.json.JSONObject
import java.text.DateFormatSymbols


class DetailProfileExecutor : BaseFragment<DetailProfileExecutorPresenter>() {


    private lateinit var infoAll: AllInfoExecutorResponse
    private var info: User? = null
    private var roomsSocket = ChatSocket.provideSocketChat()

    companion object {
        fun newInstance(userInfo: User, hasCar: Boolean): DetailProfileExecutor {
            return DetailProfileExecutor()
                .apply {
                arguments = Bundle().apply {
                    putParcelable("info", userInfo)
                    putBoolean("hasCar", hasCar)
                }
            }
        }

        fun newInstance(id: Int): DetailProfileExecutor {
            return DetailProfileExecutor()
                .apply {
                    arguments = Bundle().apply {
                        putInt("id", id)
                    }
                }
        }
    }

    private fun initSocket() {

        if(!roomsSocket.connected())
            roomsSocket.connect()
        val jsonObject = JSONObject()
        if(PaperIO.userRole == UserRole.COMPANY) {
            jsonObject.put("userId", PaperIO.employerData!!.userId)
        } else {
            jsonObject.put("userId", PaperIO.executorData!!.userId)
        }
        roomsSocket.emit("getRooms", jsonObject).on("getRooms", OnChatsListener())
    }

    inner class OnChatsListener : Emitter.Listener {
        override fun call(vararg args: Any?) {
            val rooms = Gson().fromJson(args[0].toString(), Rooms::class.java) as Rooms
            if(!rooms.rooms.isNullOrEmpty()) {
                baseActivity.runOnUiThread {

                }
            } else {
                baseActivity.runOnUiThread {

                }
            }
        }
    }

    override fun initialization(view: View, isFirstInit: Boolean) {
        try {
            info = arguments!!["info"] as User
        } catch (e: TypeCastException){}
        presenter =
            DetailProfileExecutorPresenter(
                baseContext
            )

        bp_fragment.getClick {

            onBackPressed()
        }

        getInfo()
        initSocket()

        profile_messenger_btn.getClick {
            baseActivity.drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                addFragment(DialogChatFragment.newInstance(infoAll.data!!.executor!!.id!!, "${infoAll.data!!.executor!!.firstName} ${infoAll.data!!.executor!!.lastName}", "${infoAll.data!!.executor!!.avatar}"))


        }

        profile_responded_to_vacancy_btn.getClick {
            QRCodeActivity.start(baseActivity as CompanyActivity)

        }
    }

    private fun getInfo() {

        presenter?.getInfo(info?.id ?: arguments!!["id"] as Int) {

            infoAll = it
            initTab(it)
            if(it.data!!.rate != null) {
                rating_item_feedback.rating = it.data.rate!!.toFloat()
            }
            name_executor_toolbar.text = "${it.data.executor!!.firstName} ${it.data.executor!!.lastName}"
            val b = it.data.executor.birthday
            executor_birthday_detail.text =
                "${b?.substringAfterLast("-")} ${getMonthForInt(b!!.substring(b.indexOf("-") + 1, b.lastIndexOf("-")).toInt())} ${b.substringBefore("-")}"
            if(it.data.executor.avatar != null) {
                setPhoto(executor_avatar_detail, it.data.executor.avatar)
                executor_avatar_tv.visibility = View.GONE
                executor_avatar_detail.visibility = View.VISIBLE
            }else{
                executor_avatar_tv.text = "${it.data.executor!!.firstName!!.substring(0,1)} ${it.data.executor!!.lastName!!.substring(0,1)}"
                executor_avatar_tv.visibility = View.VISIBLE
                executor_avatar_detail.visibility = View.GONE
            }
            if(it.data.executor.background != null) {
                setPhoto(profile_executor_background_img, it.data.executor.background)
            }
            image_car_exist.visibility = if(arguments!!["hasCar"] as Boolean) View.VISIBLE else View.GONE
        }
    }

    fun addFragment() {
       /* val ft = baseActivity.supportFragmentManager.beginTransaction()
        ft.replace(noty_team.com.urger.R.id.executor_container, FeedbackFragment.newInstance(false))
        ft.commit()*/
    }

    @SuppressLint("RestrictedApi")
    private fun initTab(it: AllInfoExecutorResponse) {

            executor_view_pager.adapter = ExecutorPagerAdapterInfo(
                childFragmentManager, executor_view_pager, arrayListOf(
                    DocumentsFragment().apply {
                        data = it.data!!.documents!!
                    },
                    ExperienceFragment().apply {
                        data = it.data!!.expiriences!!
                    },
                    FeedbackFragment().apply {
                        data = it.data!!.reviews!!
                    },
                    PortfolioFragment().apply {
                        data = it.data!!.portfolios!!
                    },
                    VehicleFragment().apply {
                        data = it.data!!.autos!!
                    }
                )
            )
            executor_main_tabLayout.setupWithViewPager(executor_view_pager)

    }

    fun getMonthForInt(num: Int): String {
        var month = "wrong"
        val dfs = DateFormatSymbols()
        val months = dfs.months
        if (num in 0..11) {
            month = months[num]
        }
        return month
    }

    override fun layout() = R.layout.fragment_detail_executor
}