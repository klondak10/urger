package noty_team.com.urger.ui.fragment.drawer.settings

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.core.os.ConfigurationCompat
import com.google.firebase.messaging.FirebaseMessaging
import io.paperdb.Paper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_settings.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.utils.ApplicationLanguageHelper
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.activity.company.CompanyActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import noty_team.com.urger.ui.fragment.bottom_bar.chat.DialogChooseLanguage
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.request.edit_user_personal_info.FcmRequest
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import java.util.*
import kotlin.collections.ArrayList


class SettingFragment : BaseFragment<SettingPresenter>() {

    companion object {
        fun newInstance() = SettingFragment()
    }

    private var currentPosChooseDialog = 0

    var callbackChooseLanguage: (pos: Int) -> Unit = {

        val currentLocale = ConfigurationCompat.getLocales(resources.configuration)[0]
        val countryName = currentLocale.country

        if(currentPosChooseDialog != it) {
            currentPosChooseDialog = it
            val pair = when (it) {
                0 -> "pl"
                1 -> "de"
                2 -> "ru"
                else -> countryName //по умолчанию
            }
            Paper.book().write("language_app" + PaperIO.userPhoneNumber, it)
            baseActivity.updateLocale(Locale(pair))
        }
    }

    override fun layout() = R.layout.fragment_settings



    @SuppressLint("CheckResult")
    override fun initialization(view: View, isFirstInit: Boolean) {
        bottomBarIsVisible(false)
        presenter = SettingPresenter(baseContext)




        currentPosChooseDialog = if (Paper.book().exist("language_app" + PaperIO.userPhoneNumber) ){
            Paper.book().read("language_app" + PaperIO.userPhoneNumber)
        }else{
            when (baseActivity.currentLanguage.country) {
                "PL" -> 0
                "DE" -> 1
                "RU" -> 2
                else -> 0
            }
        }

        if(Paper.book().contains("notification" + PaperIO.userPhoneNumber)) {
            switch_notification.isChecked = Paper.book().read("notification" + PaperIO.userPhoneNumber)
        }
        back_press.setOnClickListener {
            bottomBarIsVisible(true)
            onBackPressed()
        }

        layout_about_app.setOnClickListener {
            showDialog()
        }

        layout_language.setOnClickListener {
            val manager = fragmentManager
            val dialog = DialogChooseLanguage().apply {
                callbackChooseLanguage = this@SettingFragment.callbackChooseLanguage
                position = this@SettingFragment.currentPosChooseDialog
            }

            val bundle = Bundle().apply {
                putStringArrayList(dialog.DATA, getItems())     // Require ArrayList
                putInt(dialog.SELECTED, this@SettingFragment.currentPosChooseDialog)
            }

            dialog.arguments = bundle
            dialog.show(manager!!, "Dialog")
        }

        switch_notification.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked) {

                FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
                    if (result != null) {
                        if (PaperIO.userRole == UserRole.COMPANY) {

                            UrgerApi.USER_PROFILE_API.updateFcmEmployer(PaperIO.employerData!!.userId!!, FcmRequest(result)).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({}, {})

                        } else {
                            UrgerApi.USER_PROFILE_API.updateFcmExecutor(FcmRequest(result)).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({}, {})
                        }
                    }
                }

            } else {
                presenter!!.updateFcm("") {
                    if(it) {
                        showShortToast(resources.getString(noty_team.com.urger.R.string.success_changed))
                    } else {
                        showShortToast(resources.getString(noty_team.com.urger.R.string.failure_changed))
                    }
                }
            }
        }
    }

    private fun showDialog() {
        val mView = LayoutInflater.from(baseContext).inflate(R.layout.dialog_about_app, null)

        val title = mView.findViewById(R.id.title_about_app) as TextView
        val description = mView.findViewById(R.id.description_about_app) as TextView


        title.text = resources.getString(R.string.about_app)
        description.text = resources.getString(R.string.about_app_text)

        val alert = AlertDialog.Builder(baseActivity)
        alert.setView(mView)
        alert.setCancelable(true)

        alert.show()
    }

    private fun getItems(): ArrayList<String>? {
        return (resources.getStringArray(R.array.language_app)).toCollection(ArrayList())
    }
}