package noty_team.com.urger.ui.fragment.tabs.portfolio.add_portfolio

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.portfolio.AddPortfolioExecutorResponse
import noty_team.com.urger.utils.bitmapToFile
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.io.File

class AddPortfolioPresenter(context: Context): BasePresenter(context) {

    lateinit var bitmap: File
    var listBitmap: ArrayList<File> = arrayListOf()

    fun isBitmapInitialize(): Boolean {
        return ::bitmap.isInitialized
    }

    fun getBitmapFromFileData(file: File, onSuccess: (Bitmap) -> Unit) {
        var filePath = file.path
        val b = BitmapFactory.decodeFile(filePath)
        bitmap = bitmapToFile(context, b)
        listBitmap.add(bitmap)
        onSuccess(b)
    }

    fun getBitmapFromFile(file: FileData, onSuccess: (Bitmap) -> Unit) {
        var filePath = file.file.path
        val b = BitmapFactory.decodeFile(filePath)
        bitmap = bitmapToFile(context, b)
        listBitmap.add(bitmap)
        onSuccess(b)
    }

    fun addPortfolioExecutor(name: String, description: String,
                             isAddPortfolioCallback: (isSuccess: AddPortfolioExecutorResponse) -> Unit,
                             showToast: (toastMassage: String) -> Unit = {}) {

            apiEmployer.addPortfolioExecutor(RequestBody.create("multipart/form-data".toMediaTypeOrNull(), name), RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(), description), listBitmap, {
                if (it.statusCode == 200 || it.statusCode == 201) {
                    isAddPortfolioCallback(it)

                } else {
                    showToast(it.error!!)
            AddPortfolioFragment().isAdd = false
                }
            }, { errorMessage, errorCode ->
                showToast(errorMessage)
                AddPortfolioFragment().isAdd = false
            }).callRequest()
    }
}