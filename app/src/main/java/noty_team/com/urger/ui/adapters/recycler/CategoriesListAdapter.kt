package noty_team.com.urger.ui.adapters.recycler

import android.view.View
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.category_item.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.api.response.categories_response.DataItem
import java.lang.Exception

class CategoriesListAdapter(list: ArrayList<DataItem> = arrayListOf(),
                            var onClick:(data: DataItem) -> Unit = {}):
        BaseAdapter<DataItem, CategoriesListAdapter.ViewHolder>(list) {

    override fun getItemViewType(position: Int) = R.layout.category_item
    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    inner class ViewHolder(view: View) : BaseAdapter.BaseViewHolder(view), LayoutContainer {

        init {
            itemView.setOnClickListener {
                try {
                    onClick(list[adapterPosition])
                }catch (e: Exception){}
            }
        }

        override fun bind(pos: Int) {
            category_button.text = list[pos].name
        }
    }
}