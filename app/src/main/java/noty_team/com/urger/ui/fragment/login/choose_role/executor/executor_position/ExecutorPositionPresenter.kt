package noty_team.com.urger.ui.fragment.login.choose_role.executor.executor_position

import android.content.Context
import android.util.Log
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.fragment.login.choose_role.executor.ExecutorInfoContract
import noty_team.com.urger.utils.api.request.create_executor.CreateExecutorRequest
import noty_team.com.urger.utils.api.response.get_city.CityResponse

class ExecutorPositionPresenter(context: Context): BasePresenter(context), ExecutorInfoContract.Presenter {

    lateinit var openAppCallback: (isOpen: Boolean) -> Unit

    override fun createExecutorRequest(
        createExecutorRequest: CreateExecutorRequest,
        openAppCallback: (isOpen: Boolean) -> Unit
    ) {

        this.openAppCallback = openAppCallback
        apiAuthorization.createExecutor(createExecutorRequest, {

            if (it.statusCode == 200 || it.statusCode == 201) {
                it.parseToken()
                it.parseUser()
                it.isCanUpdate()
                openAppCallback(true)

            } else {
                openAppCallback(false)
                showShortToast(it.error)
            }
        }, { errorMessage, errorCode ->
            showShortToast(errorMessage)
        }).callRequest()

    }

    override fun getCategory(lang: String, onDataLoad: (cities: CityResponse) -> Unit) {
        apiAuthorization.getCategory(lang, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onDataLoad(it)
            }
        }, {errorMessage, errorCode ->
        }).callRequest()
    }
}