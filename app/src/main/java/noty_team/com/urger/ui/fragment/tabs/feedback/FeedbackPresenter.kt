package noty_team.com.urger.ui.fragment.tabs.feedback

import android.content.Context
import android.util.Log
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.api.response.feedback.get.executor.Data
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse

class FeedbackPresenter(context: Context): BasePresenter(context) {

    fun getFeedbacksExecutor(dataLoad:(GetFeedbackExecutorResponse) -> Unit) {
        apiEmployer.getFeedbackExecutor({

            if(it.statusCode == 200 || it.statusCode == 201) {
                dataLoad(it)
            } else {
                //showShortToast(context.getString(R.string.get_feedback_error))
            }

        }, {errorMessage, errorCode ->
            //Log.i("TEST", "error")
            //showShortToast(errorMessage)
        }).callRequest()
    }

    fun getFeedbackCompany(dataLoad:(GetFeedbackExecutorResponse) -> Unit) {
        apiEmployer.getFeedbackCompany({

            if(it.statusCode == 200 || it.statusCode == 201) {

                dataLoad(it.map())
            } else {
                //showShortToast(context.getString(R.string.get_feedback_error))
            }

        }, {errorMessage, errorCode ->
            //showShortToast(errorMessage)
        }).callRequest()
    }
}