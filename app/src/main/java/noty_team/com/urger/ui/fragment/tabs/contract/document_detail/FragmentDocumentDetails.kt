package noty_team.com.urger.ui.fragment.tabs.contract.document_detail

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_document_details.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.adapters.document.DocumentItem


class FragmentDocumentDetails : BaseFragment<DocumentDetailPresenters>() {

	companion object {
		fun newInstance() = FragmentDocumentDetails()
	}

	lateinit var addressLink: String
	lateinit var fileName: String
	var pos: Int = 0
	var listDocument = ArrayList<DocumentItem>()

	override fun layout() = R.layout.fragment_document_details

	override fun initialization(view: View, isFirstInit: Boolean) {

		beginDownload()

		initAction()
	}

	private fun initAction() {
		back_privacy_policy.getClick {
			onBackPressed()
		}
	}


	private fun beginDownload() {

		isShowLoadingDialog(true)
		document_detail_text.text = if (!fileName.isNullOrEmpty()) {
			fileName
		} else {
			"Pre View"
		}

		pager.setAdapter(
			MyPager(
				baseContext,
				listDocument
			)
		)
		pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener{
			override fun onPageScrollStateChanged(p0: Int) {}

			override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
				document_detail_text.text = listDocument.get(p0).documentName
			}

			override fun onPageSelected(p0: Int) {}
		})

		pager.currentItem = pos
	}
	inner class MyPager(
		private val context: Context,
		private val list: ArrayList<DocumentItem>
	) : PagerAdapter() {

		override fun instantiateItem(container: ViewGroup, position: Int): Any {
			val view =
				LayoutInflater.from(context).inflate(R.layout.item_photo_pager, null)
			val imageView =
				view.findViewById<ImageView>(R.id.Imageview)

			Glide.with(context)
				.load(list[position].image)
				.listener(object : RequestListener<Drawable> {
					override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
						isShowLoadingDialog(false)
						return  false
					}
					override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
						isShowLoadingDialog(false)
						//do something when picture already loaded
						return false
					}
				})
				.into(imageView)

			container.addView(view)

			return view
		}

		override fun destroyItem(
			container: ViewGroup,
			position: Int,
			view: Any
		) {
			container.removeView(view as View)
		}

		override fun getCount(): Int {
			return list.size
		}

		override fun isViewFromObject(
			view: View,
			`object`: Any
		): Boolean {
			return `object` === view
		}

	}


}