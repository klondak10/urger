package noty_team.com.urger.ui.adapters.view_pager


import androidx.fragment.app.FragmentManager
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.utils.adapters.CustomViewPager
import noty_team.com.urger.utils.getStringArray
import java.lang.Exception

class ExecutorPageAdapter (
    fm: FragmentManager,
    pager: CustomViewPager,
    fragments: ArrayList<BaseFragment<*>> = arrayListOf()
) : BasePagerAdapter(fm, pager, fragments) {

    override fun getTitle(pageIndex: Int): String {
        var title = ""
        try {
            title = getStringArray(R.array.executor_main_tab, context)[pageIndex]
        } catch (e: Exception) {
        }
        return title
    }
}