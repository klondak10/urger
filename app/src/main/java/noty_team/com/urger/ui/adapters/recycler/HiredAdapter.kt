package noty_team.com.urger.ui.adapters.recycler

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_hired.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.get_vacancy.preliminary.DataItem
import noty_team.com.urger.utils.setPhoto

class HiredAdapter(items: ArrayList<DataItem>,
                   recyclerView: RecyclerView,
                   var onLoadData: (offset: Int) -> Unit = {},
                   var onClick: (data: DataItem) -> Unit = {},
                   var onDeleteUser: (data: DataItem) -> Unit = {}) :
    BaseAdapterPagination<DataItem>(items, recyclerView)  {

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            itemView.setOnClickListener {
                onClick(list[position])
            }
            delete_user.setOnClickListener {
                onDeleteUser(list[position])
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            if(item.user?.avatar != null) {
                setPhoto(preliminary_user_logo, item.user?.avatar!!)
            }
            image_exist_car.visibility = if(item.auto!!) View.VISIBLE else View.GONE
            preliminary_user_name.text = "${item.user!!.firstName} ${item.user!!.lastName}"
            name_vacancy_tv.text = "${item.vacancy!!.parentCategory}, ${item.vacancy!!.categoryName}"
        }
    }

    override val itemLayoutResourceId =  R.layout.item_hired

}