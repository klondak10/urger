package noty_team.com.urger.ui.fragment.tabs.contract.add_document

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.pdf.PdfRenderer
import android.os.ParcelFileDescriptor
import android.util.Log
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData
import io.reactivex.Single
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.bitmapToFile
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.*

class AddDocumentPresenters(context: Context) : BasePresenter(context), AddDocumentContracts.Presenter {

	lateinit var onSuccess: (document: DocumentItem) -> Unit
	lateinit var bitmap: File
	lateinit var pdf: FileData

	fun isBitmapInitialize(): Boolean {
		return ::bitmap.isInitialized
	}

	fun getBitmapFromFileData(file: File, onSuccess: (Bitmap) -> Unit) {
		var filePath = file.path
		val b = BitmapFactory.decodeFile(filePath)
		bitmap = bitmapToFile(context, b)

		onSuccess(b)
	}


	override fun addDocumentRequest(
			documentName: String, isDocumentLoad: (isLoad: Boolean) -> Unit,
			onSuccess: (document: DocumentItem) -> Unit
	) {
		this.onSuccess = onSuccess

		val fileBitmap = MultipartBody.Part.createFormData(
				"preview", bitmap.name,
				RequestBody.create("image/png".toMediaTypeOrNull(), bitmap))


		val filePDF = MultipartBody.Part.createFormData(
				"file", bitmap.name,
				RequestBody.create("image/png".toMediaTypeOrNull(), bitmap)
		)


		apiEmployer.loadDocument(PaperIO.companyData?.companyId
				?: -1, "${documentName}document", fileBitmap, filePDF, {

				if (it.statusCode == 200 || it.statusCode == 201) {

					val t = it.map()

					onSuccess(t)
				} else {
					showShortToast(it.error)
					isDocumentLoad(false)
				}
			}, { errorMessage, errorCode ->
				showShortToast(context.getString(R.string.error_create_document))
				isDocumentLoad(false)
			}).callRequest()

	}


}