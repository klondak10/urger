package noty_team.com.urger.ui.fragment.bottom_bar.container

import android.os.Bundle
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.ui.fragment.bottom_bar.ContainerFragment
import noty_team.com.urger.ui.fragment.bottom_bar.map.company.MapCompanyFragment
import noty_team.com.urger.ui.fragment.bottom_bar.map.executor.MapExecutorFragment
import noty_team.com.urger.utils.api.response.sms_checked.UserRole
import noty_team.com.urger.utils.cicerone.Screens

class MapFragmentContainer : ContainerFragment() {

    companion object {
        fun getNewInstance(name: String): MapFragmentContainer {
            val fragment = MapFragmentContainer()

            val arguments = Bundle()
            arguments.putString(EXTRA_NAME, name)
            fragment.setArguments(arguments)

            return fragment
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (childFragmentManager.findFragmentById(R.id.ftc_container) == null) {
            getCicerone().router.replaceScreen(
                if (PaperIO.userRole == UserRole.COMPANY) Screens.FragmentScreen(MapCompanyFragment.newInstance())
                else Screens.FragmentScreen(MapExecutorFragment.newInstance())
            )
        }
    }
}