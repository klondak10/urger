package noty_team.com.urger.ui.fragment.profile_company.pager_company

import android.content.Context
import android.util.Log
import io.paperdb.Paper
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.CompanyPhotoItem
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.adapters.housing.HousingItem
import noty_team.com.urger.utils.api.response.company.GetCompanyInfoResponse
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.get_employer.GetEmployerInfoResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class NestedProfileCompanyDetailPresenter(context: Context) : BasePresenter(context) {

    fun getGetInfoCompany(companyId: Int, isLoadData:(GetCompanyInfoResponse) -> Unit) {
        apiEmployer.getInfoCompany(companyId, {

            if(it.statusCode == 200) {
                isLoadData(it)
            } else {
                showShortToast("getGetInfoCompany ${it.error}")
            }

        }, {errorMessage, errorCode ->
           // showShortToast(errorMessage)
        }).callRequest()
    }

    fun getEmployerInfo(id: Int, isLoadData:(GetEmployerInfoResponse) -> Unit) {
        apiEmployer.getEmployerInfo(id, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                isLoadData(it)
            } else {
                showShortToast(it.error)
            }
        }, { errorMessage, errorCode ->

            //showShortToast(errorMessage)

        }).callRequest()
    }

    fun getCompanyPhotos(companyId: Int, dataList: (ArrayList<CompanyPhotoItem>) -> Unit, showToast: (massage: String) -> Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getPhotosCompany(PaperIO.companyData?.companyId!!, {

                if (it.statusCode == 200 || it.statusCode == 201) {
                    var listCompanyPhoto = it.map()

                    listCompanyPhoto.reverse()
                    dataList(listCompanyPhoto)
                } else {
                    showToast(context.getString(R.string.error_load_photos))
                }
            }, { errorMessage, errorCode ->
               // showToast(Constants.DISPOSABLE_ERROR)
            }).callRequest()
        } else {
            apiEmployer.getPhotosCompany(companyId, {

                if (it.statusCode == 200 || it.statusCode == 201) {
                    var listCompanyPhoto = it.map()

                    listCompanyPhoto.reverse()
                    dataList(listCompanyPhoto)
                } else {
                    showToast(context.getString(R.string.error_load_photos))
                }
            }, { errorMessage, errorCode ->
                //showToast(Constants.DISPOSABLE_ERROR)
            }).callRequest()
        }
    }

    fun getStatistic(companyId: Int, showToast: (massage: String) -> Unit, onDataLoad: (countDocument: String, countHousing: String, countVacancies: String) -> Unit) {
        apiEmployer.getStatistic(companyId, {


            val countDocument = it.data?.documentsCount ?: 0
            val countHousing = it.data?.housesCount ?: 0
            val countVacancies = it.data?.vacanciesCount ?: 0

            onDataLoad(countDocument.toString(), countHousing.toString(), countVacancies.toString())

        }, { errorMessage, errorCode ->
            //showShortToast("CompanyPresenter getStatistic" + errorMessage)
        }).callRequest()

    }

    fun getContractPagination(companyId: Int, listSize: Int,
                              onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit) {
        val page = listSize / Constants.documentItemLimit

        apiEmployer.getDocumentPagination(companyId, Constants.documentItemLimit, page, {

            onDocumentsSuccess(it.mapPre())

        }, { errorMessage, errorCode ->

        }).callRequest()

    }

    fun getDocumentsPagination(id: Int, listSize: Int,
                               onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit) {
        val page = listSize / Constants.documentItemLimit

            apiEmployer.getDocumentPagination(id, Constants.documentItemLimit, page, {

                onDocumentsSuccess(it.map())

            }, { errorMessage, errorCode ->

            }).callRequest()

    }

    fun getFeedbackCompany(dataLoad:(GetFeedbackExecutorResponse) -> Unit) {
        apiEmployer.getFeedbackCompany({

            if(it.statusCode == 200 || it.statusCode == 201) {

                dataLoad(it.map())
            } else {
                //showShortToast(context.getString(R.string.get_feedback_error))
            }

        }, {errorMessage, errorCode ->
            //showShortToast(errorMessage)
        }).callRequest()
    }

    fun getHousing(
        id: Int = PaperIO.companyData?.companyId!!,
        listSize: Int,
        onHousingSuccess: (documents: ArrayList<HousingItem>) -> Unit,
        showToasMassage: (massage: String) -> Unit
    ) {
        val page = listSize / Constants.documentItemLimit
        apiEmployer.getHousingPagination(id, Constants.documentItemLimit, page, {

            if (it.statusCode == 200 || it.statusCode == 201) {

                var housinglist = ArrayList<HousingItem>()

                it.data?.forEach {
                    housinglist.add(
                        HousingItem(
                            houseId = it?.houseId ?: -1,
                            address = it?.address ?: "",
                            description = it?.description ?: "",
                            preview = if(it!!.photos.isNullOrEmpty()) {
                                ""
                            }else{
                                it.photos!![0]!!
                            }
                        )
                    )
                }
                onHousingSuccess(housinglist)

            } else {

                showToasMassage(it.error!!)
            }

        }, { errorMessage, errorCode ->
            showToasMassage(errorCode.toString())

        }).callRequest()

    }
}