package noty_team.com.urger.ui.adapters.recycler

import android.content.Context
import android.graphics.Typeface
import androidx.recyclerview.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import kotlinx.android.synthetic.main.item_experience.*
import noty_team.com.urger.R
import noty_team.com.urger.utils.adapters.BaseAdapterPagination
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem
import noty_team.com.urger.utils.setPhoto

class ExperienceAdapter(list: ArrayList<DataItem>, val context: Context,
                        recyclerView: RecyclerView,
                        var onLoadData: (offset: Int) -> Unit = {},
                        var onClick: (data: DataItem) -> Unit = {},
                        var onLongClick: (data: DataItem) -> Unit = {}) :
    BaseAdapterPagination<DataItem>(list, recyclerView) {

    override fun onDataLoaded(listSize: Int) {
        onLoadData(listSize)
    }

    override fun onInit(pos: Int, viewHolder: MainViewHolder) {
        viewHolder.apply {
            try {
                itemView.setOnClickListener {
                    onClick(list[position])
                }

                itemView.setOnLongClickListener {

                    onLongClick(list[position])

                    return@setOnLongClickListener true
                }

            } catch (e: Exception) {
            }
        }
    }

    override fun onBind(pos: Int, viewHolder: MainViewHolder) {
        var item = list[pos]
        viewHolder.apply {
            if(item.companyLogo != null) {
                setPhoto(company_logo_experience_item,"http://${item.companyLogo.toString()}" )
            } else {
                company_logo_experience_item.setImageResource(R.drawable.image_default)
            }
            val spannable = SpannableString(item.position  + " in ${item.companyName!!.replace("\\n", " ")}")
            spannable.setSpan(
                StyleSpan(Typeface.BOLD),
                0, item.position!!.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            //company_name_experience_item.text = context.resources.getString(R.string.portfolio_name, "${item.position}", "${item.companyName!!.replace("\\n", " ")}")
            company_name_experience_item.text = spannable
            val otPeriod = item.period!!.substring(1, item.period!!.indexOf(" "))
            val doPeriod = item.period!!.substring(item.period!!.indexOf(" ") + 1, item.period!!.length - 1)
            time_work_experience_item.text = "${otPeriod} - ${doPeriod}"
        }
    }

    override val itemLayoutResourceId = R.layout.item_experience
}