package noty_team.com.urger.ui.fragment.tabs.company.edit_profile.company_profile_info

interface CompanyProfileInfoContract {
	interface View

	interface Presenter {
		fun serverRequest(description: String,  userPosition: String,toast: (massage: String) -> Unit, isSuccess:(isSuccess: Boolean) -> Unit)
	}
}