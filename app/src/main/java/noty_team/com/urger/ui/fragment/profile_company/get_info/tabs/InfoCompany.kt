package noty_team.com.urger.ui.fragment.profile_company.get_info.tabs

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.fragment_company.*
import kotlinx.android.synthetic.main.fragment_company.recycler_company_photo
import kotlinx.android.synthetic.main.fragment_info_company_detail.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.profile_company.get_info.DetailProfileCompany
import noty_team.com.urger.ui.fragment.tabs.company.company_photo.CompanyPhotoFragment
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.company_photo.PhotoAdapter

class InfoCompany : BaseFragment<InfoCompanyPresenter>() {

    private lateinit var photoAdapter: PhotoAdapter

    companion object {

        fun newInstance(companyId: Int, about: String): InfoCompany {

            return InfoCompany().apply {
                arguments = Bundle().apply {
                    putInt(DetailProfileCompany.ID_COMPANY, companyId)
                    putString(DetailProfileCompany.ABOUT_COMPANY, about)
                }
            }
        }
    }

    override fun initialization(view: View, isFirstInit: Boolean) {

        initCompanyRecycler()
        getInfoCompany()
        company_description.text = requireArguments()!![DetailProfileCompany.ABOUT_COMPANY] as String
    }

    private fun getInfoCompany() {

    }

    override fun layout() = R.layout.fragment_info_company_detail

    private fun initCompanyRecycler() {
        presenter = InfoCompanyPresenter(baseContext)
        recycler_company_photo.layoutManager =
            LinearLayoutManager(
                baseActivity,
                RecyclerView.HORIZONTAL,
                false
            )
        recycler_company_photo.isNestedScrollingEnabled = false

        loadCompanyPhotoList()

        this.photoAdapter = PhotoAdapter(arrayListOf(), onClick = { index ->

            addFragment(CompanyPhotoFragment.newInstance().apply {
                this.index = index
                listImages = photoAdapter.getItemsList()
            })

        })


        recycler_company_photo.adapter = this.photoAdapter
    }

    private fun loadCompanyPhotoList() {
        val companyId = arguments?.getInt(DetailProfileCompany.ID_COMPANY)
        presenter?.getCompanyPhotos(companyId!!, {
            this.photoAdapter.addAll(it)
        }, {
            baseActivity.showShortToast(Constants.DISPOSABLE_ERROR)
        })
    }

}