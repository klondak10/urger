package noty_team.com.urger.ui.fragment.bottom_bar.bottom_vacancy

import android.app.Dialog
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.Window
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_bottom_vacancy.*
import kotlinx.android.synthetic.main.fragment_chat.*
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.fragment.tabs.vacancy.vacancy_detail.VacancyDetailFragmnet
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.ModelFilter
import noty_team.com.urger.utils.adapters.vacancy.VacancyBottomAdapter
import noty_team.com.urger.utils.adapters.vacancy.VacancyPaginationItem
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class BottomVacancyFragment : BaseFragment<BottomExecutorPresenter>() {

    var isLoading = false
    var modelFilter: ModelFilter? = null
    companion object {
        fun newInstance(modelFilter: ModelFilter? = null): BottomVacancyFragment {
            if (modelFilter != null) {
                return BottomVacancyFragment().apply {
                    arguments = Bundle().apply {
                        putParcelable("dataFilter", modelFilter)
                    }
                }
            } else {
                return BottomVacancyFragment()
            }
        }
    }

    override fun layout() = R.layout.fragment_bottom_vacancy

    private lateinit var vacancyAdapter: VacancyBottomAdapter

    override fun initialization(view: View, isFirstInit: Boolean) {


        initVacanciesAdapter()
        presenter = BottomExecutorPresenter(baseContext)

        if(arguments != null) {
            modelFilter = requireArguments()["dataFilter"] as ModelFilter
        } else {
            modelFilter = null
        }
        if(PaperIO.userRole == UserRole.EXECUTOR) {
            getVacancies()
            isShowLoadingDialog(true)
        }

        recycler_bottom_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                if (modelFilter == null) {
                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() == vacancyAdapter.itemCount - 1) {

                        presenter?.loadMoreVacancy({
                            vacancyAdapter.loadMoreVacancies(it.map())
                        },{})
                    }
                }
                super.onScrolled(recyclerView, dx, dy)
            }})
    }

    private fun localeFilter(list: ArrayList<VacancyPaginationItem>) {
        var data = ArrayList<VacancyPaginationItem>()
        data.addAll(list)
        if (!modelFilter!!.city.isNullOrEmpty()) {
            data.retainAll(list.filter {
                it.address == modelFilter!!.city
            }.toSet())
        }

        if(modelFilter!!.hasHouse == 1) {
            data.retainAll(list.filter { it.houseId != null }.toSet())
        } else {
            data.retainAll(list.filter { it.houseId == null }.toSet())
        }

        if(modelFilter!!.type_employment != null) {
            when(modelFilter!!.type_employment) {
                "full_time" -> data.retainAll(list.filter { it.workSchedule == "Полная занятость"}.toSet())
                "contract" -> data.retainAll(list.filter { it.workSchedule ==  "Контракт"}.toSet())
                "part_time" -> data.retainAll(list.filter { it.workSchedule ==  "Частичная занятость"}
                    .toSet())
                "temporary_job" -> data.retainAll(list.filter { it.workSchedule == "Временная работа" }
                    .toSet())
            }
        }
        if(modelFilter!!.expectedWage != null) {
            data.retainAll(list.filter { it.price >= modelFilter!!.expectedWage!! }.toSet())
        }

        if(modelFilter!!.type_sort != null) {
            if(modelFilter!!.type_sort == "asc") {
                data = ArrayList(data.sortedBy { it.price })
            } else {
                data = ArrayList(data.sortedByDescending { it.price })
            }
        }

        if (data.isEmpty()) {
            empty_list_search.visibility = View.VISIBLE
        }else {
            empty_list_search.visibility = View.GONE
        }
        vacancyAdapter.list.clear()
        vacancyAdapter.notifyDataSetChanged()
        vacancyAdapter.addNewVacancies(data)
    }


    private fun getVacancies() {
        if(modelFilter == null) {

            presenter?.getVacancies({
                isShowLoadingDialog(false)
                if (it.data.isEmpty()) {
                    empty_list.visibility = View.VISIBLE
                } else {
                    empty_list.visibility = View.GONE
                }
                vacancyAdapter.onNewDataReceived(
                    it.map(),
                    Constants.vacancyItemLimit
                )
            }, {})
        } else {
            if (modelFilter != null) {
                presenter?.getVacanciesWithFilter(modelFilter!!.timePublish ?: 100, { data ->
                    isShowLoadingDialog(false)
                    localeFilter(data.map())
                }, {})
            }
        }
    }

    private fun initVacanciesAdapter() {
        recycler_bottom_list.layoutManager =
            LinearLayoutManager(baseActivity)
        vacancyAdapter = VacancyBottomAdapter(arrayListOf(), recycler_bottom_list,
            onLoadData = {

            },
            onClick = {
                addFragment(VacancyDetailFragmnet.newInstance(it.vacancyId))
            }
            ,onLongClick = {
                showDialog(it.vacancyId)
            }
        )
        recycler_bottom_list.adapter = vacancyAdapter
    }

    private fun showDialog(vacancyId: Int) {
        val vacancyDialog = Dialog(baseActivity)

        vacancyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        vacancyDialog.setContentView(R.layout.dialog_executor_vacancy)

        val addToFavButton: Button = vacancyDialog.findViewById(R.id.add_to_favorite_button)
        val cancelButton = vacancyDialog.findViewById<Button>(R.id.cancel_vacancy_button)

        cancelButton.setOnClickListener {
            vacancyDialog.dismiss()
        }

        addToFavButton.setOnClickListener {
            val request = AddFavoriteVacancyExecutorRequest(vacancyId)
            presenter?.addFavoriteVacancy(request, {response ->
                if(response.statusCode == 200) {
                    showShortToast(getString(R.string.vacancy_add_to_favorite))
                }
            }, {
                showShortToast(getString(R.string.error_add_vacancy_to_favorite))
            })
            vacancyDialog.dismiss()
        }

        vacancyDialog.show()
    }
}