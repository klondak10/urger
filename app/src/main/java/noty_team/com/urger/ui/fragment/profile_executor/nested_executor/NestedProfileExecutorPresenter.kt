package noty_team.com.urger.ui.fragment.profile_executor.nested_executor

import android.content.Context
import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import noty_team.com.masterovik.utils.paper.PaperIO
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.utils.Constants
import noty_team.com.urger.utils.adapters.document.DocumentItem
import noty_team.com.urger.utils.api.UrgerApi
import noty_team.com.urger.utils.api.request.create_vacancy.AddFavoriteVacancyExecutorRequest
import noty_team.com.urger.utils.api.response.car.DataItemGetCars
import noty_team.com.urger.utils.api.response.delete_vacancy.DeleteVacancyResponse
import noty_team.com.urger.utils.api.response.experience_executor.get.DataItem
import noty_team.com.urger.utils.api.response.favorite_vacancy.AddFavoriteVacancyResponse
import noty_team.com.urger.utils.api.response.feedback.get.executor.GetFeedbackExecutorResponse
import noty_team.com.urger.utils.api.response.get_vacancy.VacanciesListResponse
import noty_team.com.urger.utils.api.response.sms_checked.UserRole

class NestedProfileExecutorPresenter(context: Context): BasePresenter(context) {

    fun getDocumentsPagination(listSize: Int,
                               onDocumentsSuccess: (documents: ArrayList<DocumentItem>) -> Unit) {
        val page = listSize / Constants.documentItemLimit
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getDocumentPagination(
                PaperIO.companyData?.companyId
                    ?: -1, Constants.documentItemLimit, page, {

                    onDocumentsSuccess(it.map())

                }, { errorMessage, errorCode ->

                }).callRequest()
        } else {
            apiEmployer.getDocumentExecutorPagination(
                {
                    onDocumentsSuccess(it.map())
                }, { errorMessage, errorCode ->
                    //showShortToast(errorMessage)
                }).callRequest()
        }
    }

    fun getVacancies(listSize: Int, onSuccess:(response: VacanciesListResponse)->Unit){
        val page = listSize / Constants.documentItemLimit
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.getVacancies(Constants.documentItemLimit, page, {
                onSuccess(it)
            }, { errorMessage, errorCode ->
                showLongToast(errorMessage)
            }).callRequest()
        } else {
            apiEmployer.getVacanciesForExecutor(Constants.documentItemLimit, 0,{
                onSuccess(it)
            }, {errorMessage, errorCode ->
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun addFavoriteVacancy(request: AddFavoriteVacancyExecutorRequest, onSuccess:(response: AddFavoriteVacancyResponse)->Unit, onError:()->Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
        } else {
            apiEmployer.addVacancyFavorite(request, {
                onSuccess(it)
            }, {errorMessage, errorCode ->
                onError()
                showLongToast(errorMessage)
            }).callRequest()
        }
    }

    fun deleteVacancy(vacancyId: Int, onSuccess:(response: DeleteVacancyResponse)->Unit, onError:()->Unit){
        apiEmployer.deleteVacancy(vacancyId,{
            onSuccess(it)
        }, {errorMessage, errorCode ->
            onError()
            showLongToast(errorMessage)
        }).callRequest()
    }

    fun getPortfolioPagination(listSize: Int,
                               onPortfolioSuccess: (portfolio: ArrayList<noty_team.com.urger.utils.api.response.portfolio.DataItem>) -> Unit)

    {
        val page = listSize / Constants.documentItemLimit

        apiEmployer.getPortfolioPagination(
            Constants.documentItemLimit, page, {

                onPortfolioSuccess(it.data!!)

            }, { errorMessage, errorCode ->
                //showShortToast(errorMessage)
            }).callRequest()

    }

    fun deletePortfolioExecutor(id: Int, isSuccessDelete: (b: Boolean) -> Unit) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.deletePortfolioExecutor(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        isSuccessDelete(true)
                    } else {
                        isSuccessDelete(false)
                        showShortToast(it.error!!)
                    }
                }, {

                })
        )
    }

    fun deleteDocument(documentId: Int, isDeleteDocument: (isSuccess: Boolean) -> Unit) {
        if(PaperIO.userRole == UserRole.COMPANY) {
            apiEmployer.deleteDocument(PaperIO.companyData?.companyId ?: -1, documentId, {
                if (it.statusCode == 200 || it.statusCode == 201) {
                    isDeleteDocument(true)
                } else {
                    isDeleteDocument(false)
                }
            }, { errorMessage, errorCode ->
                showShortToast(errorMessage)
            }).callRequest()
        } else {
            apiEmployer.deleteDocumentExecutor(documentId, {
                if (it.statusCode == 200 || it.statusCode == 201) {
                    isDeleteDocument(true)
                } else {
                    isDeleteDocument(false)
                }
            }, { errorMessage, errorCode ->
                showShortToast(errorMessage)
            }).callRequest()
        }
    }

    fun getCarsPagination(listSize: Int,
                          onDocumentsSuccess: (cars: List<DataItemGetCars>) -> Unit) {
        val page = listSize / Constants.documentItemLimit
        apiEmployer.getCarsListExecutor( Constants.documentItemLimit, page, {
            if(it.statusCode == 200 || it.statusCode == 201) {
                onDocumentsSuccess(it.data!!)
            }
        }, { errorMessage, errorCode ->
            showShortToast("$errorCode $errorMessage")
        }).callRequest()

    }


    fun deleteCar(carId: Int, isDeleteCar: (isSuccess: Boolean) -> Unit) {

        apiEmployer.deleteCarExecutor(carId, {
            if (it.statusCode == 200 || it.statusCode == 201) {
                isDeleteCar(true)
            } else {
                isDeleteCar(false)
            }
        }, { errorMessage, errorCode ->
            showShortToast("$errorCode $errorMessage")
        }).callRequest()

    }

    fun getExperincePagination(listSize: Int,
                                        onExperinceSuccess: (documents: ArrayList<DataItem>) -> Unit)

    {
        val page = listSize / Constants.documentItemLimit

        apiEmployer.getExperincePagination(
            Constants.documentItemLimit, page, {

                onExperinceSuccess(it.map())

            }, { errorMessage, errorCode ->
                //showShortToast(errorMessage)
            }).callRequest()

    }

    fun deleteExperienceExecutor(id :Int, isSuccessDelete: (b: Boolean) -> Unit) {
        subscriptions.add(
            UrgerApi.USER_PROFILE_API.deleteExperienceExecutor(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.statusCode == 200 || it.statusCode == 201) {
                        isSuccessDelete(true)
                    } else {
                        showShortToast(it.error!!)
                        isSuccessDelete(false)
                    }
                }, {
                })
        )
    }

    fun getFeedbacksExecutor(dataLoad:(GetFeedbackExecutorResponse) -> Unit) {
        apiEmployer.getFeedbackExecutor({

            if(it.statusCode == 200 || it.statusCode == 201) {
                dataLoad(it)
            } else {

               // showShortToast(context.getString(R.string.get_feedback_error))
            }

        }, {errorMessage, errorCode ->
            //Log.i("TEST", "error")
            //showShortToast(errorMessage)
        }).callRequest()
    }
}