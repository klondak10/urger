package noty_team.com.urger.ui.fragment.tabs.contract.add_document


import noty_team.com.urger.utils.adapters.document.DocumentItem

interface AddDocumentContracts {
    interface Presenter {
        fun addDocumentRequest(
            documentName: String,
            isDocumentLoad: (isLoad: Boolean) -> Unit,
            isSuccess: (document: DocumentItem) -> Unit
        )
    }
}