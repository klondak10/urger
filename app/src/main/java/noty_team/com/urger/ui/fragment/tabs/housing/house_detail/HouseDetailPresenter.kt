package noty_team.com.urger.ui.fragment.tabs.housing.house_detail

import android.content.Context
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.ui.fragment.tabs.housing.house_detail.data.DataMapResponse
import noty_team.com.urger.utils.api.response.get_house.GetHouseResponse

class HouseDetailPresenter(context: Context) : BasePresenter(context), HouseDetailContract.Presenter {
	override fun getHouseItem(houseId: Int, companyId: Int, showToast: (it: String) -> Unit, onDataLoad: (house: DataMapResponse) -> Unit) {

		apiEmployer.getHouse(houseId, companyId, {

			if (it.statusCode == 200 || it.statusCode == 201) {

				onDataLoad(mapResponse(it))

			} else {
				showToast(it.error)
			}

		}, { errorMessage, errorCode ->
			showToast(errorMessage)

		}).callRequest()

	}


	fun getCurrency(onSuccess: (isSuccess: Boolean,
								currencyMap: HashMap<Int, String>) -> Unit) {
		apiEmployer.getCurrency({
			if (it.statusCode == 200 || it.statusCode == 201) {
				onSuccess(true, it.mapToHashMap())
			}
		}, { errorMessage, errorCode ->
			showShortToast(errorMessage)
		}).callRequest()
	}

	private fun mapResponse(it: GetHouseResponse): DataMapResponse {
		val data = DataMapResponse()

		data.address = it.data?.address ?: ""
		data.available = it.data?.available ?: false
		data.lat = it.data?.lat ?: ""
		data.lng = it.data?.lng ?: ""
		data.rooms = it.data?.rooms ?: 0
		data.photos = it.data?.photos ?: arrayListOf()
		data.description = it.data?.description ?: ""
		data.costOfRent = it.data?.costOfRent ?: 0
		data.cost_of_rent = it.data?.costOfRent ?: 0
		data.currency_of_rent = it.data?.currencyOfRent ?: ""

		return data
	}

}