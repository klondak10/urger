package noty_team.com.urger.ui.fragment.profile_executor.get_info.portfolio

import androidx.recyclerview.widget.GridLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_info_executor.*
import noty_team.com.urger.R
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.ui.adapters.recycler.PortfolioAdapter
import noty_team.com.urger.ui.fragment.profile_executor.DetailProfileExecutorPresenter
import noty_team.com.urger.utils.api.response.all_info.PortfoliosItem
import noty_team.com.urger.utils.api.response.portfolio.DataItem

class PortfolioFragment : BaseFragment<DetailProfileExecutorPresenter>() {
	lateinit var adapterPortfolioFragment: PortfolioAdapter

	var data: List<PortfoliosItem?> = listOf()
	companion object {

		fun newInstance(): PortfolioFragment {
			return PortfolioFragment()
		}

	}


	override fun layout() = R.layout.fragment_info_executor

	override fun initialization(view: View, isFirstInit: Boolean) {
		presenter = DetailProfileExecutorPresenter(baseContext)
		if(data.isNullOrEmpty()) {
			empty_list.visibility = View.VISIBLE
		} else {
			initAdapter(convertData())
		}
	}

	private fun initAdapter(list: ArrayList<DataItem>) {
		recycler_list.layoutManager =
            GridLayoutManager(baseActivity, 2)
		adapterPortfolioFragment = PortfolioAdapter(
			list, baseActivity, recycler_list)

		recycler_list.adapter = adapterPortfolioFragment
	}

	private fun convertData(): ArrayList<DataItem> {
		val list: ArrayList<DataItem> = arrayListOf()
		data.forEach {
			val item = DataItem(it!!.file, it.userId, it.name, it.description, it.id)
			list.add(item)
		}
		return list
	}
}