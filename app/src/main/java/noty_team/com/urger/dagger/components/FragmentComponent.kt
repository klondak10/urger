package noty_team.com.urger.dagger.components

import dagger.Subcomponent
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.base.mvp.fragment.BaseFragment
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.dagger.modules.FragmentModule
import noty_team.com.urger.dagger.scopes.FragmentScope

@FragmentScope
@Subcomponent(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(target: BaseActivity)
    fun inject(target: BaseFragment<BasePresenter>)
}