package noty_team.com.urger.dagger.modules

import dagger.Module
import dagger.Provides
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.dagger.scopes.FragmentScope


@Module
class FragmentModule(private val baseActivity: BaseActivity) {

    @Provides
    @FragmentScope
    fun provideBaseActivity(): BaseActivity = baseActivity

}