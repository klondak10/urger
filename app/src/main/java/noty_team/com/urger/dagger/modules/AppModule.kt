package noty_team.com.urger.dagger.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiAuthorization
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiEmployer
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    fun provideRequestsDisposable() = CompositeDisposable()


    @Provides
    @Singleton
    fun provideUrgerApi(context: Context, retrofit: Retrofit):
            UrgerRetrofitApiAuthorization = UrgerRetrofitApiAuthorization(context, retrofit)

    @Provides
    @Singleton
    fun provideUrgerUserApi(context: Context, retrofit: Retrofit):UrgerRetrofitApiEmployer = UrgerRetrofitApiEmployer(context, retrofit)

}