package noty_team.com.urger.dagger.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RetrofitScope