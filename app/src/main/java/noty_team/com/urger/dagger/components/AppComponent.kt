package noty_team.com.urger.dagger.components

import android.app.Application
import dagger.Component
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiAuthorization
import noty_team.com.urger.api.retrofit.UrgerRetrofitApiEmployer
import noty_team.com.urger.base.mvp.activity.BaseActivity
import noty_team.com.urger.base.mvp.fragment.BasePresenter
import noty_team.com.urger.dagger.modules.AppModule
import noty_team.com.urger.dagger.modules.FragmentModule
import noty_team.com.urger.dagger.modules.RetrofitApiModule
import noty_team.com.urger.ui.activity.FeedbackExecutorActivity
import noty_team.com.urger.ui.activity.executor.ExecutorActivity
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        RetrofitApiModule::class
    ]
)
interface AppComponent {
    fun plus(fragmentModule: FragmentModule): FragmentComponent

    fun inject(app: Application)
    fun inject(target: UrgerRetrofitApiAuthorization)
    fun inject(target:UrgerRetrofitApiEmployer)
    fun inject(target: BasePresenter)

    fun inject(target: ExecutorActivity)

    fun inject(target: FeedbackExecutorActivity)

}