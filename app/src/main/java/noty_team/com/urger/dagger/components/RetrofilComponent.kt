package noty_team.com.urger.dagger.components

import dagger.Subcomponent
import noty_team.com.urger.dagger.modules.RetrofitApiModule
import noty_team.com.urger.dagger.scopes.RetrofitScope

@RetrofitScope
@Subcomponent(modules = [RetrofitApiModule::class])
interface RetrofilComponent{

}